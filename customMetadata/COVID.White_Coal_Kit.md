<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>White Coal Kit</label>
    <protected>false</protected>
    <values>
        <field>Content__c</field>
        <value xsi:type="xsd:string">White coal is a form of fuel produced by drying chopped wood over a fire. It differs from charcoal which is carbonised wood. White coal was used in England to melt lead ore from the mid-sixteenth to the late seventeenth centuries. It produces more heat than green wood but less than charcoal and thus prevents the lead evaporating.

White coal could be used mixed with charcoal for other industrial uses than lead smelting.
White coal was produced in distinctive circular pits with a channel, known as Q-pits.
They are frequently found in the woods of South Yorkshire.</value>
    </values>
    <values>
        <field>Header__c</field>
        <value xsi:type="xsd:string">White Coal</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Screen_Name__c</field>
        <value xsi:type="xsd:string">Homepage</value>
    </values>
</CustomMetadata>
