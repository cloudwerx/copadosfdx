<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Assign Lightning Record Pages</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
                &quot;ffrr__ActualVForecastLine__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__ActualvsForecastLineRecordPage__page&quot;
                    }
                },
                &quot;ffrr__BatchTrackingControl__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__BatchTrackingControlRecordPage__page&quot;
                    }
                },
                &quot;ffrr__BatchTrackingControlLine__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__BatchTrackingControlLineRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RevenueRecognitionErrorLogLineItem__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__ErrorLogLineItemRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RevenueRecognitionErrorLog__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__ErrorLogRecordPage__page&quot;
                    }
                },
                &quot;ffrr__ErrorsManager__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__ErrorsManagerRecordPage__page&quot;
                    }
                },
                &quot;ffrr__FieldMappingDefinition__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__FieldMappingDefinitionRecordPage__page&quot;
                    }
                },
                &quot;ffrr__FieldMapping__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__FieldMappingRecordPage__page&quot;
                    }
                },
                &quot;ffrr__ForecastScheduleDefinitionLine__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__ForecastScheduleDefinitionLineRecordPage__page&quot;
                    }
                },
                &quot;ffrr__ForecastScheduleDefinition__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__ForecastScheduleDefinitionRecordPage__page&quot;
                    }
                },
                &quot;ffrr__PerformanceObligationLineItem__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__PerformanceObligationLineItemRecordPage__page&quot;
                    }
                },
                &quot;ffrr__PerformanceObligation__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__PerformanceObligationRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RecognitionPeriod__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RecognitionPeriodRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RecognitionViewFilter__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RecognitionViewFilterRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RecognitionView__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RecognitionViewRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RecognitionViewTab__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RecognitionViewTabRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RecognitionViewValue__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RecognitionViewValueRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RecognitionYear__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RecognitionYearRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RevenueContract__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RevenueContractRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RevenueForecastTransactionLine__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RevenueForecastTransactionLineRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RevenueForecastTransaction__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RevenueForecastTransactionRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RevenueRecognitionTransactionLine__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RevenueRecognitionTransactionLineRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RevenueRecognitionTransaction__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RevenueRecognitionTransactionRecordPage__page&quot;
                    }
                },
                &quot;ffrr__RevenueRecognitionTransactionSummary__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__RevenueTransactionSummaryRecordPage__page&quot;
                    }
                },
                &quot;ffrr__Settings__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__SettingsRecordPage__page&quot;
                    }
                },
                &quot;ffrr__TemplateMapping__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__TemplateMappingRecordPage__page&quot;
                    }
                },
                &quot;ffrr__Template__c&quot;: {
                    &quot;View&quot;: {
                        &quot;lightning&quot;: &quot;ffrr__TemplateRecordPage__page&quot;
                    }
                }
            }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__LEXLayouts</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__LEXLayoutsFeatureStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Action Override</value>
    </values>
</CustomMetadata>
