<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Create Fields for RRT to JNL Integration</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
			&quot;ffrr__RevenueRecognitionTransaction__c.link_FFA_id__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot; : &quot;Lookup&quot;,
				&quot;Custom Field Label&quot; : &quot;Journal&quot;,
				&quot;Custom Field Reference To&quot; : &quot;c2g__CodaJournal__c&quot;,
				&quot;Custom Field Relationship Name&quot; : &quot;RevenueRecognitionTransactions&quot;
			},
			&quot;ffrr__RevenueRecognitionTransaction__c.link_FFA_state__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;Text&quot;,
				&quot;Custom Field Label&quot;: &quot;Journal Creation State&quot;,
				&quot;Custom Field Description&quot;: &quot;Indicates whether the journal was created successfully by the RRT to Journal Integration.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Indicates whether the journal was created successfully by the RRT to Journal Integration.&quot;,
				&quot;Custom Field Length&quot;: &quot;255&quot;
			},
			&quot;ffrr__RevenueRecognitionTransaction__c.link_FFA_errors__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;TextArea&quot;,
				&quot;Custom Field Label&quot;: &quot;Journal Creation Error&quot;,
				&quot;Custom Field Description&quot;: &quot;Errors that occurred when creating the journal by the RRT to journal Integration.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Errors that occurred when creating the journal by the RRT to journal Integration.&quot;
			},
			&quot;ffrr__RevenueRecognitionTransactionLine__c.link_FFA_id__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot; : &quot;Lookup&quot;,
				&quot;Custom Field Label&quot; : &quot;Journal Line&quot;,
				&quot;Custom Field Reference To&quot; : &quot;c2g__CodaJournalLineItem__c&quot;,
				&quot;Custom Field Relationship Name&quot; : &quot;RevenueRecognitionTransactionLines&quot;
			},
			&quot;ffrr__RevenueRecognitionTransactionLine__c.link_FFA_state__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;Text&quot;,
				&quot;Custom Field Label&quot;: &quot;Journal Line Creation State&quot;,
				&quot;Custom Field Description&quot;: &quot;Indicates whether the journal line was created successfully by the RRT to Journal Integration.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Indicates whether the journal line was created successfully by the RRT to Journal Integration.&quot;,
				&quot;Custom Field Length&quot;: &quot;255&quot;
			},
			&quot;ffrr__RevenueRecognitionTransactionLine__c.link_FFA_errors__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;TextArea&quot;,
				&quot;Custom Field Label&quot;: &quot;Journal Line Creation Error&quot;,
				&quot;Custom Field Description&quot;: &quot;Errors that occurred when creating the journal line by the RRT to journal Integration.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Errors that occurred when creating the journal line by the RRT to journal Integration.&quot;
			},	
			&quot;c2g__CodaJournal__c.link_FFRR_id__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot; : &quot;Lookup&quot;,
				&quot;Custom Field Label&quot; : &quot;Revenue Recognition Transaction&quot;,
				&quot;Custom Field Reference To&quot; : &quot;ffrr__RevenueRecognitionTransaction__c&quot;,
				&quot;Custom Field Relationship Name&quot; : &quot;Journals&quot;
			},
			&quot;c2g__CodaJournal__c.link_FFRR_state__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;Text&quot;,
				&quot;Custom Field Label&quot;: &quot;Revenue Recognition Update State&quot;,
				&quot;Custom Field Description&quot;: &quot;Indicates whether the RRT was updates successfully by the RRT to Journal Integration.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Indicates whether the RRT was updates successfully by the RRT to Journal Integration.&quot;,
				&quot;Custom Field Length&quot;: &quot;255&quot;
			},
			&quot;c2g__CodaJournal__c.link_FFRR_errors__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;TextArea&quot;,
				&quot;Custom Field Label&quot;: &quot;Revenue Recognition Update Error&quot;,
				&quot;Custom Field Description&quot;: &quot;Errors that occurred when updating the RRT by the RRT to journal Integration.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Errors that occurred when updating the RRT by the RRT to journal Integration.&quot;
			},
			&quot;c2g__CodaJournalLineItem__c.link_FFRR_id__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot; : &quot;Lookup&quot;,
				&quot;Custom Field Label&quot; : &quot;Revenue Recognition Transaction Line&quot;,
				&quot;Custom Field Reference To&quot; : &quot;ffrr__RevenueRecognitionTransactionLine__c&quot;,
				&quot;Custom Field Relationship Name&quot; : &quot;JournalLineItems&quot;
			},
			&quot;c2g__CodaJournalLineItem__c.link_FFRR_state__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;Text&quot;,
				&quot;Custom Field Label&quot;: &quot;Revenue Recognition Line Update State&quot;,
				&quot;Custom Field Description&quot;: &quot;Indicates whether the RRTL was updates successfully by the RRT to Journal Integration.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Indicates whether the RRTL was updates successfully by the RRT to Journal Integration.&quot;,
				&quot;Custom Field Length&quot;: &quot;255&quot;
			},
			&quot;c2g__CodaJournalLineItem__c.link_FFRR_errors__c&quot; : {
				&quot;Custom Field Operation&quot; : {
					&quot;enable&quot; : &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;TextArea&quot;,
				&quot;Custom Field Label&quot;: &quot;Revenue Recognition Line Update Error&quot;,
				&quot;Custom Field Description&quot;: &quot;Errors that occurred when updating the RRTL by the RRT to journal Integration.&quot;,
				&quot;Custom Field Inline Help Text&quot;: &quot;Errors that occurred when updating the RRTL by the RRT to journal Integration.&quot;
			}
		}</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RRTToJournalIntegration</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Required</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RRTToJournalIntegrationFieldFeatureStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Custom Field</value>
    </values>
</CustomMetadata>
