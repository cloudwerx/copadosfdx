<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Review Timesheet Slack Reminder Setting</label>
    <protected>false</protected>
    <values>
        <field>Batch_Run_Emails__c</field>
        <value xsi:type="xsd:string">gbaviskar@cloudwerx.co,rkandul@cloudwerx.co</value>
    </values>
    <values>
        <field>Good_Bye_Messages__c</field>
        <value xsi:type="xsd:string">Farewell, Friend;Later, gator.;Until next Time!;Take luck!;Hasta luego.;See ya later.;In a while, crocodile.;Have a good one!;Ciao;Hasta lasagna.;See you soon.;</value>
    </values>
    <values>
        <field>Mothly_Reminder_Message__c</field>
        <value xsi:type="xsd:string">Hi there firstName, :wave:
Please don’t forget to submit/review your timesheet for last `month` before the end of business hours today!

https://i.imgflip.com/4ftetn.jpg

Thanks
randomGoodByMessage

(This is an auto-generated message, please do not reply)</value>
    </values>
    <values>
        <field>Pune_User_Access_Token__c</field>
        <value xsi:type="xsd:string">xoxp-326514896850-398191693861-2516844927424-3afcdc71618a94a987bb2d154a37fbb7</value>
    </values>
    <values>
        <field>Send_Monthly_Reminders__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Send_Weekly_Reminders__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Sydney_User_Access_Token__c</field>
        <value xsi:type="xsd:string">xoxp-326514896850-2117399597495-2515104451425-7cdec03392f5bebc6541b1aed6453fad</value>
    </values>
    <values>
        <field>Users_to_Exclude_from_reminders__c</field>
        <value xsi:type="xsd:string">0036F00003mlkak,0036F00003lt0jb,0036F00003lt0dKQAQ</value>
    </values>
    <values>
        <field>Weekly_Reminder_Message__c</field>
        <value xsi:type="xsd:string">Hi there firstName, :wave:
Please don’t forget to submit/review your timesheet for this `week` before the end of business hours today!

https://blog-cdn.everhour.com/blog/wp-content/uploads/2019/09/images.jpeg

Thanks
randomGoodByMessage

(This is an auto-generated message, please do not reply)</value>
    </values>
</CustomMetadata>
