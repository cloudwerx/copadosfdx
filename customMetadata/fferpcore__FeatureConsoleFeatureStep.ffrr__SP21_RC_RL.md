<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Update Revenue Contract Related List</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
        &quot;name&quot;: &quot;ffrr__RevenueContract__c-ffrr__Revenue Contract Layout&quot;,
        &quot;relatedLists&quot;: {
          &quot;ffrr__RevenueScheduleLine__c.ffrr__RevenueContract__c&quot;: {
            &quot;operation&quot;: &quot;Add&quot;,
            &quot;anchor&quot;: &quot;ffrr__RevenueForecastTransactionLine__c.ffrr__RevenueContract__c&quot;,
            &quot;fields&quot;: [
              &quot;NAME&quot;,
              &quot;ffrr__PerformanceObligation__c&quot;,
              &quot;ffrr__PeriodStart__c&quot;,
              &quot;ffrr__PeriodEnd__c&quot;,
              &quot;ffrr__RevenueAsCurrency__c&quot;
            ],
            &quot;sortOrder&quot;: &quot;ASC&quot;
          }
        }
      }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__SP21</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RSSP21RevenueContractRelatedListUpdateStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Page Layout</value>
    </values>
</CustomMetadata>
