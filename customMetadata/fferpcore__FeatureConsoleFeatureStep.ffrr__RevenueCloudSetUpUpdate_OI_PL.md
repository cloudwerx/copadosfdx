<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Clear Unused Order Product Layout</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
        &quot;name&quot;: &quot;OrderItem-blng__Billing Order Product Layout&quot;,
        &quot;fields&quot;: {
          &quot;blng__AllocatedRevenueAmount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Remove&quot;,
              &quot;disable&quot;: &quot;Add&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Revenue Recognition Information__0&quot;,
              &quot;disable&quot;: &quot;Revenue Recognition Information__0&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;blng__RevenueLiabilityAmount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Remove&quot;,
              &quot;disable&quot;: &quot;Add&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;blng__AllocatedRevenueAmount__c&quot;,
              &quot;disable&quot;: &quot;blng__AllocatedRevenueAmount__c&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;blng__RevenueScheduleStatus__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Remove&quot;,
              &quot;disable&quot;: &quot;Add&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Revenue Recognition Information__1&quot;,
              &quot;disable&quot;: &quot;Revenue Recognition Information__1&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;blng__RevenueExpectedAmount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Remove&quot;,
              &quot;disable&quot;: &quot;Add&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;blng__RevenueScheduleStatus__c&quot;,
              &quot;disable&quot;: &quot;blng__RevenueScheduleStatus__c&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          }
        }
      }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RevenueCloudSetUp</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RevenueCloudUpdateLayoutOrderItemFeatureStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">17.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Page Layout</value>
    </values>
</CustomMetadata>
