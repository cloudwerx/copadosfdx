<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SP18 Updating RC Page Layout Step</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
        &quot;name&quot;: &quot;ffrr__RevenueContract__c-ffrr__Revenue Contract Layout&quot;,
        &quot;sections&quot;: {
          &quot;Cost Summary&quot;: {
            &quot;operation&quot;: &quot;Add&quot;,
            &quot;anchor&quot;: &quot;Revenue Summary&quot;,
            &quot;detail&quot;: &quot;true&quot;,
            &quot;edit&quot;: &quot;false&quot;,
            &quot;style&quot;: &quot;TwoColumnsLeftToRight&quot;
          },
          &quot;Revenue Allocation&quot;: {
            &quot;operation&quot;: &quot;Add&quot;,
            &quot;anchor&quot;: &quot;Information&quot;,
            &quot;detail&quot;: &quot;true&quot;,
            &quot;edit&quot;: &quot;true&quot;,
            &quot;style&quot;: &quot;TwoColumnsLeftToRight&quot;
          }
        },
        &quot;fields&quot;: {
          &quot;ffrr__RevenueAllocated__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Remove&quot;,
              &quot;disable&quot;: &quot;Add&quot;
            },
            &quot;behaviour&quot;: {
              &quot;enable&quot;: &quot;Readonly&quot;,
              &quot;disable&quot;: &quot;Readonly&quot;
            },
            &quot;anchor&quot;: {
              &quot;disable&quot;: &quot;ffrr__TotalSSP__c&quot;
            }
          },
          &quot;ffrr__NullSSPCount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Revenue Allocation__0&quot;,
              &quot;disable&quot;: &quot;Revenue Summary__0&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__ZeroSSPCount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__NullSSPCount__c&quot;,
              &quot;disable&quot;: &quot;ffrr__NullSSPCount__c&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__TotalSSP__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__ZeroSSPCount__c&quot;,
              &quot;disable&quot;: &quot;ffrr__ZeroSSPCount__c&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__AllocationStatus__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__TotalSSP__c&quot;,
              &quot;disable&quot;: &quot;ffrr__PerformanceObligationsCount__c&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;CreatedById&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__PerformanceObligationsCount__c&quot;,
              &quot;disable&quot;: &quot;ffrr__TotalAllocatedRevenue__c&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;LastModifiedById&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__Active__c&quot;,
              &quot;disable&quot;: &quot;ffrr__TotalRecognizedToDate__c&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__TotalAllocatedRevenue__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Revenue Allocation__1&quot;,
              &quot;disable&quot;: &quot;ffrr__RevenueAllocated__c&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__RevenueOverride__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__TotalAllocatedRevenue__c&quot;,
              &quot;disable&quot;: &quot;ffrr__TotalRevenue__c&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__Revenue__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__RevenueOverride__c&quot;,
              &quot;disable&quot;: &quot;ffrr__RevenueOverride__c&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__TotalAmortizedToDate__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Cost Summary__1&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__TotalCost__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Cost Summary__0&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__TotalRecognizedToDate__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Revenue Summary__1&quot;,
              &quot;disable&quot;: &quot;ffrr__Revenue__c&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__TotalRevenue__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Revenue Summary__0&quot;,
              &quot;disable&quot;: &quot;Revenue Summary__1&quot;
            },
            &quot;behaviour&quot;: &quot;Readonly&quot;
          }
        },
        &quot;relatedLists&quot;: {
          &quot;ffrr__PerformanceObligation__c.ffrr__RevenueContract__c&quot;: {
            &quot;fieldActions&quot;: {
              &quot;ffrr__Cost__c&quot;: {
                &quot;operation&quot;: {
                  &quot;enable&quot;: &quot;Add&quot;,
                  &quot;disable&quot;: &quot;Remove&quot;
                },
                &quot;anchor&quot;: {
                  &quot;enable&quot;: &quot;ffrr__AllocatedRevenue__c&quot;
                }
              }
            }
          }
        }
      }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__SP18</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__UpgradeToSpring18ContractPageLayoutDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Page Layout</value>
    </values>
</CustomMetadata>
