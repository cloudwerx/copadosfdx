<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Configure RM for use with PSA Timecards</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
                &quot;pse__Timecard__c.ffrrtemplate__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Lookup&quot;,
                    &quot;Custom Field Label&quot;: &quot;Actuals Template&quot;,
                    &quot;Custom Field Reference To&quot;: &quot;ffrr__Template__c&quot;,
                    &quot;Custom Field Relationship Name&quot;: &quot;TimecardSplit&quot;
                },
                &quot;ffrr__RevenueRecognitionTransactionLine__c.ffrrTimecard__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Lookup&quot;,
                    &quot;Custom Field Label&quot;: &quot;Timecard Split&quot;,
                    &quot;Custom Field Reference To&quot;: &quot;pse__Timecard__c&quot;,
                    &quot;Custom Field Relationship Name&quot;: &quot;RevenueRecognitionTransactionLines&quot;
                },
                &quot;ffrr__RevenueForecastTransactionLine__c.ffrrTimecard__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Lookup&quot;,
                    &quot;Custom Field Label&quot;: &quot;Timecard Split&quot;,
                    &quot;Custom Field Reference To&quot;: &quot;pse__Timecard__c&quot;,
                    &quot;Custom Field Relationship Name&quot;: &quot;RevenueForecastTransactionLines&quot;
                },
                &quot;pse__Timecard__c.ffrrResourceName__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Text&quot;,
                    &quot;Custom Field Length&quot;: &quot;255&quot;,
                    &quot;Custom Field Label&quot;: &quot;Resource Name&quot;
                },
                &quot;pse__Timecard__c.ffrrBillRate__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Currency&quot;,
                    &quot;Custom Field Precision&quot;: &quot;16&quot;,
                    &quot;Custom Field Scale&quot;: &quot;2&quot;,
                    &quot;Custom Field Label&quot;: &quot;Bill Rate&quot;,
                    &quot;Custom Field Formula&quot;: &quot;pse__Timecard_Header__r.pse__Bill_Rate__c&quot;
                },
                &quot;pse__Timecard__c.ffrrPercentComplete__c&quot;: {
                    &quot;Custom Field Operation&quot;: {
                        &quot;enable&quot;: &quot;Add&quot;,
                        &quot;disable&quot;: &quot;Remove&quot;
                    },
                    &quot;Custom Field Type&quot;: &quot;Percent&quot;,
                    &quot;Custom Field Precision&quot;: &quot;8&quot;,
                    &quot;Custom Field Scale&quot;: &quot;2&quot;,
                    &quot;Custom Field Label&quot;: &quot;% Complete&quot;
                }
            }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RMPSASmartStartStep</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Custom Field</value>
    </values>
</CustomMetadata>
