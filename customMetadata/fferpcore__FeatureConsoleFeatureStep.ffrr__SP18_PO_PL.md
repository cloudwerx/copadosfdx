<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SP18 Updating PO Page Layout Step</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
        &quot;name&quot;: &quot;ffrr__PerformanceObligation__c-ffrr__Performance Obligation Layout&quot;,
        &quot;sections&quot;: {
          &quot;Cost Amortization&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Revenue Recognition&quot;,
            &quot;detail&quot;: &quot;true&quot;,
            &quot;edit&quot;: &quot;true&quot;,
            &quot;style&quot;: &quot;TwoColumnsLeftToRight&quot;
          }
        },
        &quot;fields&quot;: {
          &quot;ffrr__Cost__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Cost Amortization__0&quot;,
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__AmortizedToDate__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrr__Cost__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__CostBalanceSheetAccount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Cost Amortization__1&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__CostIncomeStatementAccount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrr__CostBalanceSheetAccount__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__CostCostCenter__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrr__CostIncomeStatementAccount__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__ControllingCostPOLI__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrr__ControllingPOLI__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__ffrrTemplate__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__AccountName__c&quot;,
              &quot;disable&quot;: &quot;Revenue Recognition__0&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__RevenueRecognitionComplete__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__Active__c&quot;,
              &quot;disable&quot;: &quot;ffrr__RecognizedToDate__c&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__Revenue__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Revenue Recognition__0&quot;,
              &quot;disable&quot;: &quot;Revenue Allocation__1&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          }
        },
        &quot;relatedLists&quot;: {
          &quot;ffrr__PerformanceObligationLineItem__c.ffrr__PerformanceObligation__c&quot;: {
            &quot;fieldActions&quot;: {
              &quot;ffrr__Cost__c&quot;: {
                &quot;operation&quot;: {
                  &quot;enable&quot;: &quot;Add&quot;,
                  &quot;disable&quot;: &quot;Remove&quot;
                },
                &quot;anchor&quot;: &quot;__END__&quot;
              }
            }
          }
        }
      }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__SP18</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__UpgradeToSpring18PerformanceObligationPageLayoutDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Page Layout</value>
    </values>
</CustomMetadata>
