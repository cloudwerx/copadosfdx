<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Update Order Product Page Layout</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
        &quot;name&quot;: &quot;OrderItem-blng__Billing Order Product Layout&quot;,
        &quot;fields&quot;: {
          &quot;ffrrtemplate__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;blng__RevenueRecognitionRule__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrrPercentComplete__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrtemplate__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrrRecognizedToDate__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrPercentComplete__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrrIncomeStatementAccount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrRecognizedToDate__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrrPerUnitStandaloneSellingPrice__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Revenue Recognition Information__1&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
           &quot;ffrrStandaloneSellingPrice__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrPerUnitStandaloneSellingPrice__c&quot;,
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrrIncludeInRevenueRecognition__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrStandaloneSellingPrice__c&quot;,
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrrDelivered__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrIncludeInRevenueRecognition__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrrRevenueRecognitionComplete__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrDelivered__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrrBalanceSheetAccount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrRevenueRecognitionComplete__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          }
        }
      }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RevenueCloudSetUp</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RevenueCloudLayoutOrderItemFeatureStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Page Layout</value>
    </values>
</CustomMetadata>
