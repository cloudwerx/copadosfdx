<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Revenue Management Workspace</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>fferpcore__ItemKey__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>fferpcore__ItemType__c</field>
        <value xsi:type="xsd:string">fferpcore__exp_navigationPane</value>
    </values>
    <values>
        <field>fferpcore__LargeData__c</field>
        <value xsi:type="xsd:string">[{&quot;text&quot;:&quot;Frequent Tasks&quot;,&quot;items&quot;:[{&quot;icon&quot;:{&quot;namespace&quot;:&quot;ffrr&quot;,&quot;fileName&quot;:&quot;rm_actuals&quot;},&quot;text&quot;:&quot;Manage Revenue&quot;,&quot;items&quot;:[{&quot;text&quot;:&quot;Recognize Revenue&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__navItemPage&quot;,&quot;attributes&quot;:{&quot;apiName&quot;:&quot;ffrr__RecognizeRevenue&quot;}}},{&quot;text&quot;:&quot;Forecast Revenue&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__navItemPage&quot;,&quot;attributes&quot;:{&quot;apiName&quot;:&quot;ffrr__RevenueForecast&quot;}}},{&quot;text&quot;:&quot;Revenue Management Task Launcher&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__navItemPage&quot;,&quot;attributes&quot;:{&quot;apiName&quot;:&quot;ffrr__RevenueManagementTaskLauncher&quot;}}}]},{&quot;icon&quot;:{&quot;namespace&quot;:&quot;ffrr&quot;,&quot;fileName&quot;:&quot;rm_rr_transaction&quot;},&quot;text&quot;:&quot;Revenue Recognition Transactions&quot;,&quot;items&quot;:[{&quot;text&quot;:&quot;Revenue Recognized - In Progress&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__RevenueRecognitionTransaction__c&quot;,&quot;actionName&quot;:&quot;list&quot;},&quot;state&quot;:{&quot;filterName&quot;:&quot;ffrr__RevenueRecognizedInProgress&quot;}}},{&quot;text&quot;:&quot;Revenue Recognized - Committed&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__RevenueRecognitionTransaction__c&quot;,&quot;actionName&quot;:&quot;list&quot;},&quot;state&quot;:{&quot;filterName&quot;:&quot;ffrr__RevenueRecognizedCommitted&quot;}}},{&quot;text&quot;:&quot;Revenue Recognized - Discarded&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__RevenueRecognitionTransaction__c&quot;,&quot;actionName&quot;:&quot;list&quot;},&quot;state&quot;:{&quot;filterName&quot;:&quot;ffrr__RevenueRecognizedDiscarded&quot;}}},{&quot;text&quot;:&quot;Revenue Recognized - Errors&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__RevenueRecognitionTransaction__c&quot;,&quot;actionName&quot;:&quot;list&quot;},&quot;state&quot;:{&quot;filterName&quot;:&quot;ffrr__RevenueRecognizedError&quot;}}},{&quot;text&quot;:&quot;Actual Vs Forecast Lines&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__ActualVForecastLine__c&quot;,&quot;actionName&quot;:&quot;list&quot;},&quot;state&quot;:{&quot;filterName&quot;:&quot;ffrr__All&quot;}}}]},{&quot;icon&quot;:{&quot;namespace&quot;:&quot;ffrr&quot;,&quot;fileName&quot;:&quot;rm_rf_transactions&quot;},&quot;text&quot;:&quot;Revenue Forecast Transactions&quot;,&quot;items&quot;:[{&quot;text&quot;:&quot;Revenue Forecast - All&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__RevenueForecastTransaction__c&quot;,&quot;actionName&quot;:&quot;list&quot;},&quot;state&quot;:{&quot;filterName&quot;:&quot;ffrr__All&quot;}}},{&quot;text&quot;:&quot;Revenue Forecast - All Drafts&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__RevenueForecastTransaction__c&quot;,&quot;actionName&quot;:&quot;list&quot;},&quot;state&quot;:{&quot;filterName&quot;:&quot;ffrr__All_Drafts&quot;}}}]},{&quot;icon&quot;:{&quot;namespace&quot;:&quot;ffrr&quot;,&quot;fileName&quot;:&quot;rm_contracts&quot;},&quot;text&quot;:&quot;Revenue Contracts&quot;,&quot;items&quot;:[{&quot;text&quot;:&quot;Create New Revenue Contract&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__RevenueContract__c&quot;,&quot;actionName&quot;:&quot;new&quot;}}},{&quot;text&quot;:&quot;All - Revenue Allocation&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__RevenueContract__c&quot;,&quot;actionName&quot;:&quot;list&quot;},&quot;state&quot;:{&quot;filterName&quot;:&quot;ffrr__AllRevenueAllocation&quot;}}},{&quot;text&quot;:&quot;All - Revenue Recognition&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__RevenueContract__c&quot;,&quot;actionName&quot;:&quot;list&quot;},&quot;state&quot;:{&quot;filterName&quot;:&quot;ffrr__AllRevenueRecognition&quot;}}},{&quot;text&quot;:&quot;Revenue not allocated&quot;,&quot;pageRef&quot;:{&quot;type&quot;:&quot;standard__objectPage&quot;,&quot;attributes&quot;:{&quot;objectApiName&quot;:&quot;ffrr__RevenueContract__c&quot;,&quot;actionName&quot;:&quot;list&quot;},&quot;state&quot;:{&quot;filterName&quot;:&quot;ffrr__RevenueNotAllocated&quot;}}}]}]}]</value>
    </values>
    <values>
        <field>fferpcore__SObjectField__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>fferpcore__SObjectType__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>fferpcore__SmallData__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
