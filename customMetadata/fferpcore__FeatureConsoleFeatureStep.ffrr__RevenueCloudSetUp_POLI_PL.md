<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Update POLI Page Layout</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
        &quot;name&quot;: &quot;ffrr__PerformanceObligationLineItem__c-ffrr__Performance Obligation Line Item Layout&quot;,
        &quot;sections&quot;: {
          &quot;Source Record Relationships&quot;: {
            &quot;operation&quot;: &quot;Add&quot;,
            &quot;anchor&quot;: &quot;Cost&quot;,
            &quot;detail&quot;: &quot;true&quot;,
            &quot;edit&quot;: &quot;true&quot;,
            &quot;style&quot;: &quot;TwoColumnsTopToBottom&quot;
          }
        },
        &quot;fields&quot;: {
          &quot;ffrrOrderItem__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Source Record Relationships__0&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrrInvoiceLine__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrOrderItem__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrrCreditNoteLine__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrInvoiceLine__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          }
        }
      }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RevenueCloudSetUp</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RevenueCloudLayoutPOLIFeatureStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">9.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Page Layout</value>
    </values>
</CustomMetadata>
