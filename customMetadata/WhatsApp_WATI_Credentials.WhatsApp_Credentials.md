<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>WhatsApp Credentials</label>
    <protected>false</protected>
    <values>
        <field>Access_Token__c</field>
        <value xsi:type="xsd:string">Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzYTZmMjNiNS0xZGIwLTQyNjktYWE5OS0wZGE3NDhmOWU4MjkiLCJ1bmlxdWVfbmFtZSI6InJrYW5kdWxAY2xvdWR3ZXJ4LmNvIiwibmFtZWlkIjoicmthbmR1bEBjbG91ZHdlcnguY28iLCJlbWFpbCI6InJrYW5kdWxAY2xvdWR3ZXJ4LmNvIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQURNSU5JU1RSQVRPUiIsImV4cCI6MjUzNDAyMzAwODAwLCJpc3MiOiJDbGFyZV9BSSIsImF1ZCI6IkNsYXJlX0FJIn0.JXmzyR6aJpXnCITSKYcOm0M6Bk5x2eTVW5beOf6Z7ls</value>
    </values>
    <values>
        <field>End_Point_URL__c</field>
        <value xsi:type="xsd:string">https://live-server-1632.wati.io/api/v1/sendTemplateMessage/</value>
    </values>
</CustomMetadata>
