<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>4</label>
    <protected>false</protected>
    <values>
        <field>Payment_Method__c</field>
        <value xsi:type="xsd:string">netbanking</value>
    </values>
    <values>
        <field>Transaction_Fee__c</field>
        <value xsi:type="xsd:double">2.07</value>
    </values>
</CustomMetadata>
