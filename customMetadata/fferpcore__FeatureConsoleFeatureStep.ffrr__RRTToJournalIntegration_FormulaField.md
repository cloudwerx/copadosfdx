<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Create Formula Fields for RRT to JNL Int</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
			&quot;ffrr__RevenueRecognitionTransaction__c.JournalStatus__c&quot;: {
				&quot;Custom Field Operation&quot;: {
					&quot;enable&quot;: &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;Text&quot;,
				&quot;Custom Field Label&quot;: &quot;Journal Status&quot;,
				&quot;Custom Field Formula&quot;: &quot;TEXT(link_FFA_id__r.c2g__JournalStatus__c)&quot;
			},
			&quot;ffrr__RevenueRecognitionTransaction__c.JournalDate__c&quot;: {
				&quot;Custom Field Operation&quot;: {
					&quot;enable&quot;: &quot;Add&quot;
				},
				&quot;Custom Field Type&quot;: &quot;Date&quot;,
				&quot;Custom Field Label&quot;: &quot;Journal Date&quot;,
				&quot;Custom Field Formula&quot;: &quot;link_FFA_id__r.c2g__JournalDate__c&quot;
			}
		}</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RRTToJournalIntegration</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Required</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RRTToJournalIntegrationFieldFeatureStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Custom Field</value>
    </values>
</CustomMetadata>
