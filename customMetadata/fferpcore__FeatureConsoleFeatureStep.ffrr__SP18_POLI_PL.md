<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SP18 Updating Poli Page Layout Step</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
        &quot;name&quot;: &quot;ffrr__PerformanceObligationLineItem__c-ffrr__Performance Obligation Line Item Layout&quot;,
        &quot;sections&quot;: {
          &quot;Revenue&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Fields&quot;,
            &quot;detail&quot;: &quot;true&quot;,
            &quot;edit&quot;: &quot;true&quot;,
            &quot;style&quot;: &quot;TwoColumnsLeftToRight&quot;
          },
          &quot;Cost&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Revenue&quot;,
            &quot;detail&quot;: &quot;true&quot;,
            &quot;edit&quot;: &quot;true&quot;,
            &quot;style&quot;: &quot;TwoColumnsLeftToRight&quot;
          }
        },
        &quot;fields&quot;: {
          &quot;ffrr__BalanceSheetAccount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Revenue__0&quot;,
              &quot;disable&quot;: &quot;ffrr__CostCenterAccount__c&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__IncomeStatementAccount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__BalanceSheetAccount__c&quot;,
              &quot;disable&quot;: &quot;ffrr__BalanceSheetAccount__c&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__CostCenterAccount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__IncomeStatementAccount__c&quot;,
              &quot;disable&quot;: &quot;ffrr__Description__c&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__Revenue__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;Revenue__1&quot;,
              &quot;disable&quot;: &quot;ffrr__Active__c&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__CostBalanceSheetAccount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Cost__0&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__CostIncomeStatementAccount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrr__CostBalanceSheetAccount__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__CostCostCenterAccount__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrr__CostIncomeStatementAccount__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__Cost__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Cost__1&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__Completed__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__Description__c&quot;,
              &quot;disable&quot;: &quot;ffrr__PercentageComplete__c&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__Active__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Move&quot;,
              &quot;disable&quot;: &quot;Move&quot;
            },
            &quot;anchor&quot;: {
              &quot;enable&quot;: &quot;ffrr__Completed__c&quot;,
              &quot;disable&quot;: &quot;ffrr__Completed__c&quot;
            },
            &quot;behaviour&quot;: &quot;Edit&quot;
          },
          &quot;ffrr__IsControllingCostPOLI__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrr__IsControllingPOLI__c&quot;,
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrr__ValueType__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;Information__1&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          }
        }
      }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__SP18</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__UpgradeToSpring18PerformanceObligationLineItemPageLayoutDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Page Layout</value>
    </values>
</CustomMetadata>
