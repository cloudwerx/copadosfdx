<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Update Usage Summary Page Layout - Cost</label>
    <protected>false</protected>
    <values>
        <field>fferpcore__Configuration__c</field>
        <value xsi:type="xsd:string">{
        &quot;name&quot;: &quot;blng__UsageSummary__c-blng__Usage Summary Layout&quot;,
        &quot;fields&quot;: {
          &quot;ffrrUnitCost__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;blng__AppliedUnitPrice__c&quot;,
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
          &quot;ffrrTotalCost__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrUnitCost__c&quot;,
            &quot;behaviour&quot;: &quot;Readonly&quot;
          },
           &quot;ffrrAmortizedToDate__c&quot;: {
            &quot;operation&quot;: {
              &quot;enable&quot;: &quot;Add&quot;,
              &quot;disable&quot;: &quot;Remove&quot;
            },
            &quot;anchor&quot;: &quot;ffrrRecognizedToDate__c&quot;,
            &quot;behaviour&quot;: &quot;Edit&quot;
          }
        }
      }</value>
    </values>
    <values>
        <field>fferpcore__DisableRevertAction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>fferpcore__Feature__c</field>
        <value xsi:type="xsd:string">ffrr__RevenueCloudSetUp</value>
    </values>
    <values>
        <field>fferpcore__IsOptional__c</field>
        <value xsi:type="xsd:string">Optional</value>
    </values>
    <values>
        <field>fferpcore__StepDescription__c</field>
        <value xsi:type="xsd:string">ffrr__RevenueCloudCostAmortizationLayoutUsageFeatureStepDescription</value>
    </values>
    <values>
        <field>fferpcore__StepNumber__c</field>
        <value xsi:type="xsd:double">14.0</value>
    </values>
    <values>
        <field>fferpcore__StepType__c</field>
        <value xsi:type="xsd:string">Page Layout</value>
    </values>
</CustomMetadata>
