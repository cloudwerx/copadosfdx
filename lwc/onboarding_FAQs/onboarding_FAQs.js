/**
* @name         Onboarding_FAQs
* @author       Girish Baviskar
* @date         23-2-2021
* @description  Shows Faqs to new hires on onboarding community.
**/
import { LightningElement, track, api } from 'lwc';
import getListOfFaqs from '@salesforce/apex/Onboarding_FaqsController.getListOfFaqs';

export default class Onboarding_FAQs extends LightningElement {
    activeSections = ['A', 'C'];
    activeSectionsMessage = '';
    @track faqs;

    @api contactId; // contact id to show relevant faqs.
    connectedCallback() {
        this.getFaqs();
    }

    // gets list of faqs for given contact.
    getFaqs(){    
        getListOfFaqs({contactId : this.contactId}).then(res =>{
            this.faqs = res;
            //console.log('Result' + JSON.stringify(res) );
            //console.log('faqs list : ' + this.faqs);
        }).catch(err => {
            //console.log(err)
        });

    }
    handleSectionToggle(event) {
        const openSections = event.detail.openSections;

       /*  if (openSections.length === 0) {
            this.activeSectionsMessage = 'All sections are closed';
        } else {
            this.activeSectionsMessage =
                'Open sections: ' + openSections.join(', ');
        } */
    }


}