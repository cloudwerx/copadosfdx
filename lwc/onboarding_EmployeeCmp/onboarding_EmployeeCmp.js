/************************************************************************************************
* @Name         onboarding_EmployeeCmp
* @Author       Yash Bhalerao
* @Date         19-01-2021
* @Description  LWC for Onboarding Employee
* @TestClass    
*************************************************************************************************/

import { LightningElement, api, wire, track } from 'lwc';
import showAllTasks from '@salesforce/apex/OnboardingUIController.showAllTasks';
import updateTaskStatus from '@salesforce/apex/OnboardingUIController.updateTaskStatus';
//import showPicklistValues from '@salesforce/apex/OnboardingUIController.showPicklistValues';
import saveOnboardingFeedback from '@salesforce/apex/OnboardingUIController.saveOnboardingFeedback';
import OnboardingStatus from '@salesforce/apex/OnboardingUIController.OnboardingStatus';


import { NavigationMixin } from 'lightning/navigation';

import { refreshApex } from '@salesforce/apex';
import { updateRecord } from 'lightning/uiRecordApi';

import ID_FIELD from '@salesforce/schema/Task.Id';
import IS_COMPLETED_FIELD from '@salesforce/schema/Task.Is_Completed__c';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

//import { fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';

import { publish, MessageContext } from 'lightning/messageService';
import filterContactsMC from '@salesforce/messageChannel/FilterContactsMessageChannel__c';


/*{ label : 'Assigned To', fieldName : 'OwnerId', editable: true},
{ label : 'Status', fieldName : 'Status', editable: true},
    { label : 'Priority', fieldName : 'Priority', editable: true},
    { label : 'Status', fieldName : 'Is_Completed__c', editable: true, type : 'boolean', initialWidth: 60},*/

const columns = [
    { label : 'Subject', fieldName : 'Subject', editable: false, wrapText: true, hideDefaultActions: true, initialWidth: (screen.width*0.25)},
    { label : 'Description', fieldName : 'Description', editable: false, hideDefaultActions: true, wrapText: true, initialWidth: (screen.width*0.4)},
    /*{
        label: 'Download',
        fieldName: 'Help_Doc_URL__c',
        type: 'url',
        typeAttributes: {label: 'download', target: '_blank'},
        initialWidth: 80,
        wrapText: true,
        hideDefaultActions: true
    },*/
    {label: 'Details', type: "button", initialWidth: (screen.width * 0.075), typeAttributes: {  
        label: 'Details',  
        name: 'View',  
        title: 'View',  
        disabled: false,  
        value: 'view',
        class: {fieldName : 'Detail_Button__c'} 
    }},
    //{ label : 'Employer Comments', fieldName : 'Employer_Comments__c', editable: false, wrapText: true, hideDefaultActions: true},
    { label: 'Action', type: 'fileUpload', fieldName: 'Id', initialWidth: 120, hideDefaultActions: true, typeAttributes: { acceptedFormats: '.jpg,.jpeg,.pdf,.png', fileUploaded:{fieldName: 'Is_Upload_Required__c'}, slackMessage:{fieldName: 'Is_Slack_msg_Required__c'}, informationForm:{fieldName: 'Is_Information_Form_required__c'}, isComplete:{fieldName: 'Is_Completed__c'} }, wrapText: true},
    { label: 'Status', fieldName: 'Is_Completed__c', type: 'toggleButton', initialWidth: 80, hideDefaultActions: true,
            typeAttributes: { 
                buttonDisabled: { fieldName: 'isDisabled' }, 
                rowId: { fieldName: 'Id' }, 
            }
    },
    {
        label: 'View',
        fieldName: 'Uploaded_File_URL__c',
        type: 'url',
        typeAttributes: {label: 'view', target: '_blank'},
        initialWidth: 5,
        wrapText: true,
        hideDefaultActions: true
    }
];

export default class Onboarding_EmployeeCmp extends NavigationMixin(LightningElement) //extends LightningElement 
{
    columns = columns;

    @track data = [];

    @track showInformationForm = false;

    @api contact = {};

    @wire(CurrentPageReference) pageRef;

    @wire(MessageContext)
    messageContext;

    //@track dataList = [];
    draftValues = [];

    @track isFeedbackModalOpen = false;

    @track contactToSend = {'sObjectType': 'Contact'};

    @api isonboardingcomplete;
    @api usertype;

    @track disableButton;

    @track x;

    @track isSaveButtonDisabled = false;

    @track showLoadingSpinner = false;

    @track isOpen = false;
    @track recId;

    @track showDatatable;
    @track hideDatatable;

    connectedCallback() 
    {   
        this.showLoadingSpinner = true;
        showAllTasks({contactId : this.contact}).then(res => {
            if(res.length == 0)
            {
                this.showDatatable = false;
                this.hideDatatable = true;
            }
            else
            {
                this.showDatatable = true;
                this.hideDatatable = false;
            } 
            this.data = res;
            this.showLoadingSpinner = false;
            //this.data.forEach(this.trialFunction);
            refreshApex(this.data); 
        }).catch(err => console.error(err));

        //console.log('location.host : ' + location.host);
        //console.log('screen.height : ' + screen.height);
        //console.log('screen.width : ' + screen.width);
    }


    renderedCallback()
    {
        
        //this.isonboardingcomplete == 'true' || 
        if(this.usertype == 'Employer')
        {
            //console.log('this.usertype : ' + this.usertype);
            //console.log('inside if');
            this.disableCmp();
            this.isSaveButtonDisabled = true;
            //this.disableSaveButton();
            
        }

        //console.log('this.contact : ' + this.contact);
        var myObj = {};
        myObj["Id"] = this.contact;
        publish(this.messageContext, filterContactsMC, myObj);
        //console.log('Event fired');

        this.checkOnboardingStatus();

    }

    callRowAction( event ) 
    {  
        this.recId =  event.detail.row.Id;
        //this.taskidtosend = this.recId;
        //console.log('taskidtosend : ' + taskidtosend);  
        const actionName = event.detail.action.name;  
        if ( actionName === 'View') 
        {  
            //console.log('event.detail.row.Is_Downloadable__c : ' + event.detail.row.Is_Downloadable__c);
            //console.log('actionName : ' + actionName);
            //this.isOpen = true;

            if(event.detail.row.Is_Downloadable__c == true)
            {
                this[NavigationMixin.Navigate]({  
                    type: 'standard__webPage',
                    attributes: {
                        //url:'https://cloudwerx--clouddev.lightning.force.com/' + 'sfc/servlet.shepherd/document/download/0690T000000N4oVQAS'
                        url:event.detail.row.Help_Doc_URL__c
                    }
                }, false
               );
            }
            else
            {
                this.isOpen = true;
            }

  
            /*this[NavigationMixin.Navigate]({  
                 type: 'standard__webPage',
                 attributes: {
                     //url:'https://cloudwerx--clouddev.lightning.force.com/' + 'sfc/servlet.shepherd/document/download/0690T000000N4oVQAS'
                     url:'https://drive.google.com/uc?export=download&id=1mq5UetT3xyqYw16Qs4SlhtbPZ1R6P_X0'
                 }
             }, false
            );*/  
  
        }
    }

    closeModal()
    {
        this.isOpen = false;
    }

    checkOnboardingStatus()
    {
        var that = this;

        OnboardingStatus({contactId : this.contact}).then(response =>{
            //console.log('response : ' + response);
            if(response.Onboarding_Feedback_Text__c != null || response.Is_Employee_Onboarding_Complete__c == false) // response.Onboarding_Feedback_Text__c != null ||  || response.Is_Employee_Onboarding_Complete__c == false 
            {
                //that.disableSaveButton();
                that.isSaveButtonDisabled = true;
            }
            else if(response.Onboarding_Feedback_Text__c == null && response.Is_Employee_Onboarding_Complete__c == true) // response.Onboarding_Feedback_Rating__c == null && || response.Is_Employee_Onboarding_Complete__c == true
            {
                this.disableCmp();
                that.isSaveButtonDisabled = false;
            }
        });
    }

    disableCmp()
    {
        //console.log('inside disableCmp');
        var element = this.template.querySelectorAll(".employee").forEach(y=>{
                //console.log('y : ' + y);
                y.className="approved";
             });
    }

    enableSaveButton()
    {
        /*var element2 = this.template.querySelectorAll(".approvedButton").forEach(z=>{
            z.className="saveButton";
         });*/
         this.isSaveButtonDisabled = false;
    }

    disableSaveButton()
    {
        /*var element1 = this.template.querySelectorAll(".saveButton").forEach(y=>{
            y.className="approvedButton";
         });*/
         this.isSaveButtonDisabled = true;
    }

    disableSaveButtonAfterSubmit()
    {
        /*var element1 = this.template.querySelectorAll(".saveButton").forEach(y=>{
            y.className="approvedButton";
         });*/
         this.isSaveButtonDisabled = true;
    }

    handleUploadFinished(event) 
    {
        event.stopPropagation();
        //console.log('data => ', JSON.stringify(event.detail.data));
    }

    handleSelectedRec(event)
    {
        var that = this;
        //console.log('event.detail.value.rowId : ' + event.detail.value.rowId);
        //console.log('event.detail.value : ' + event.detail.value);

        updateTaskStatus({taskToUpdateId : event.detail.value.rowId}).then(response =>{
            that.x = response.Is_Employee_Onboarding_Complete__c;
            //console.log('response from task update Is_Employee_Onboarding_Complete__c : ' + response.Is_Employee_Onboarding_Complete__c);
            const toastEvent = new ShowToastEvent({
                title : 'Status of task changed successfully.',
                variant : 'success',
            });
            this.dispatchEvent(toastEvent);

            if(that.x == true)
            {
                that.enableSaveButton();
            }
            else
            {
                that.disableSaveButton();
            }

            //refreshApex(this.data);
            this.connectedCallback();

            }).catch(error => {
            //console.log('Error - Cannot change status ' + error);
            });
    }

    handleChange(event)
    {
        if(event.target.name == 'Onboarding Rating')
        {
            this.contactToSend.Onboarding_Feedback_Rating__c = event.target.value;
            //console.log('event.target.value : ' + event.target.value);
        }
        else
        {
            this.contactToSend.Onboarding_Feedback_Text__c = event.target.value;
        }
    }

    openFeedbackModal() {
        // to open modal set isModalOpen tarck value as true
        this.isFeedbackModalOpen = true;
    }
    closeFeedbackModal() {
        // to close modal set isModalOpen tarck value as false
        this.isFeedbackModalOpen = false;
    }
    submitFeedbackDetails() 
    {

        this.contactToSend.Id = this.contact;

        if(this.contactToSend.Onboarding_Feedback_Text__c == null)
        {
            const toast = new ShowToastEvent({
                title : 'Please enter feedback.',
                variant : 'error',
            });
            this.dispatchEvent(toast);
        }

        else
        {
            saveOnboardingFeedback({contactToUpdate : this.contactToSend}).then(res => {
                this.disableCmp();
                this.disableSaveButtonAfterSubmit(); 
                const toastEvent = new ShowToastEvent({
                    title : 'Feedback saved.',
                    variant : 'success',
                });
                this.dispatchEvent(toastEvent);
            }).catch(err => console.error(err));

            this.isFeedbackModalOpen = false;
        }
    }
}