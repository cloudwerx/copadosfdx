import { LightningElement, wire, track, api } from 'lwc';
import getContactList from '@salesforce/apex/MyFieldSetApexClass.getContactList';
import updateContact from '@salesforce/apex/MyFieldSetApexClass.updateContact';
//import displayContacts from '@salesforce/apex/MyFieldSetApexClass.displayContacts';
import {refreshApex} from '@salesforce/apex';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import FIRSTNAME_FIELD from '@salesforce/schema/Contact.FirstName';
import LASTNAME_FIELD from '@salesforce/schema/Contact.LastName';
import CONTACT_ID from '@salesforce/schema/Contact.Id'

import {subscribe,unsubscribe,MessageContext,publish} from 'lightning/messageService';
import filterContactsMC from '@salesforce/messageChannel/FilterContactsMessageChannel__c';
import newMC from '@salesforce/messageChannel/NewMessageChannel__c';

export default class Onboarding_App_Contact_Form extends LightningElement 
{

    subscription = null;

    @wire(MessageContext)
    messageContext;

    @api showform = false;
    @api showinformationform = false;

    @api isonboardingcomplete;
    @api usertype;

    @track cons = {};

    @track error;
    @api contacttoshow = {};

    @track contact = {'sObjectType': 'Contact'};

    @track name = 'Name';
    @track mobile = 'Mobile Number';
    @track email = 'Email';
    @track birthDate = 'Birth Date';

    @track uanNumber = 'UAN Number';
    @track salaryCtc = 'Salary CTC';

    /*@wire(conFieldSet, { conid: '0030T00000EPkjVQAT' })
    wiredContacts({ error, data }) {
        if (data) 
        {
            this.cons = data;
            this.error = undefined;
            console.log('this.cons : ' + this.cons.Name);
            console.log('data : ' + data);
        } 
        else if (error) 
        {
            this.error = error;
            this.cons = undefined;
        }
    }*/

    connectedCallback()
    {

        getContactList({conid : this.contacttoshow}).then(res => {
            //console.log('this.contacttoshow : ' + this.contacttoshow);
            //console.log('res : ' + res); 
            this.cons = res;
            //console.log('this.cons : ' + this.cons);
            //this.showForm = true;
            //this.showinformationform = false;
            refreshApex(this.cons); 
        }).catch(err => console.error(err));

        if (!this.subscription) {
            this.subscription = subscribe(
                this.messageContext,
                newMC,
                (message) => this.handleSave()
            );
        }
    }

    callSave()
    {
        
    }

    disconnectedCallback() {
        unsubscribe(this.subscription);
        this.subscription = null;
    }

    renderedCallback()
    {
        //console.log('renderedCallback this.isonboardingcomplete : ' + this.isonboardingcomplete);
        //console.log('this.usertype : ' + this.usertype);

        //this.isonboardingcomplete == 'true' ||

        if(this.usertype == 'Employer')
        {
            //console.log('inside if');
            this.disableCmp();
            
        }
    }

    disableCmp()
    {
        //console.log('this.isonboardingcomplete : ' + this.isonboardingcomplete);
        var element = this.template.querySelectorAll(".in").forEach(y=>{
                //console.log('y : ' + y);
                y.disabled=true;
             });
    }

    handleInputChange(event)
    {
        var date = new Date();
        var dateString = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 ))
                    .toISOString()
                    .split("T")[0];

        //console.log(dateString);

        //var d = new Date();
        //console.log(d);

        if(event.target.name == 'name')
        {
            this.contact.Name = event.target.value;
        }
        else if(event.target.name == 'mobile')
        {
            //console.log(event.target.value.length);
            this.contact.MobilePhone = event.target.value;
        }
        else if(event.target.name == 'email')
        {
            this.contact.Email = event.target.value;
        }
        else if(event.target.name == 'birthdate')
        {
            //console.log(event.target.value);
            if(event.target.value >= dateString)
            {
                alert('Invalid date');
                /*const toastEvent = new ShowToastEvent({
                    title : 'Please select valid date.',
                    variant : 'error',
                });
                this.dispatchEvent(toastEvent);*/    
            }
            else
            {
                this.contact.Birthdate = event.target.value;
            }    
        }
        else if(event.target.name == 'uanNumber')
        {
            this.contact.UAN_Number__c = event.target.value;
        }
        else
        {
            this.contact.Salary_CTC__c = event.target.value;
        }
    }

    handleSave()
    {
        this.contact.Id = this.cons.Id;
        updateContact({contactToUpdate : this.contact}).then(res => { 
            const toastEvent = new ShowToastEvent({
                title : 'Contact updated.',
                variant : 'success',
            });
            this.dispatchEvent(toastEvent);
        }).catch(err => console.error(err));
    }
}