/************************************************************************************************
* @Name         onboarding_FileUploadDataTable
* @Author       Yash Bhalerao
* @Date         19-01-2021
* @Description  Datatable with file upload component
* @TestClass    
*************************************************************************************************/

import { LightningElement , api} from 'lwc';
import documentUploadRender from './onboarding_DocumentUploadRender.html';
import toggleButtonColumnTemplate from './onboarding_ToggleButtonColumnTemplate.html';
import LightningDatatable from 'lightning/datatable';


export default class Onboarding_FileUploadDataTable extends LightningDatatable 
{

    static customTypes = {   //it show that we are creating custom type
        fileUpload: {  // type of custom element
            template: documentUploadRender, // Which html will render
            typeAttributes: ['acceptedFileFormats','fileUploaded','slackMessage','informationForm','isComplete']  // attribute of that type
        },
        toggleButton: {
            template: toggleButtonColumnTemplate,
            standardCellLayout: true,
            typeAttributes: ['buttonDisabled', 'rowId'],
        }
    };
}