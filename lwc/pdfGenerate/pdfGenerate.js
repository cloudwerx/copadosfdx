import { LightningElement, wire, api } from "lwc";
import fetchAttachment from "@salesforce/apex/FileUploader.fetchAttachment";

export default class PdfGenerate extends LightningElement {
  pdfData;
  @api taskid;

  /*@wire(fetchAttachment)
  wiredAttachment({ error, data }) {
    if (data) {
      //console.log('data : '+data);
      this.pdfData = data;
      this.onLoad();
    } else if (error) {
      console.log(error);
    }
  }*/

  connectedCallback()
  {
    //console.log('this.taskid : ' + this.taskid);
    fetchAttachment({taskId : this.taskid}).then(res => { 
      this.pdfData = res;    
      this.onLoad();
      }).catch(err => console.error(err));
  }
  

  onLoad() 
  {
    this.template
      .querySelector("iframe")
      .contentWindow.postMessage(this.pdfData, "*");
  }
}