/**
* @name         Onboarding_login
* @author       Girish Baviskar
* @date         15-1-2021
* @description  One component to hold them all.
**/
import { LightningElement, track, wire } from 'lwc';
//import generateAndEmailOtp from '@salesforce/apex/Onboarding_MasterComponentController.generateAndEmailOtp';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import CloudwerxLogoOnboarding from '@salesforce/resourceUrl/CloudwerxLogoOnboarding';
import { NavigationMixin } from 'lightning/navigation';
import { CurrentPageReference } from 'lightning/navigation';
export default class Onboarding_MasterComponent extends NavigationMixin(LightningElement)  {
    
    
    
    @track showMasterDiv = false;
    
    @track loginPageLink = new URL(window.location.href) + 'login-page/';   
    @track data = [];
    @track contact = {};
    @track contactId;
    @track isOnboardingComplete;
    @track userType;

    @track loginPageRef;
    @track user;
    @track defaultTab;
    @track showBackButton;

    showSpinner = true;
    @track cloudwerxLogoURL = CloudwerxLogoOnboarding;
    @track isUserAuthenticated = false;
    @wire(CurrentPageReference)
    pageRef;
    
   
    connectedCallback(){ 
        this.contactId = new URL(window.location.href).searchParams.get('recordId');
        this.isUserAuthenticated = new URL(window.location.href).searchParams.get('Authenticated');
        //this.isOnboardingComplete = new URL(window.location.href).searchParams.get('onboardingComplete');
        this.userType = new URL(window.location.href).searchParams.get('userType');
        //console.log('===========This is the param========'+this.pageRef);
        //console.log('===========This is the param========'+ JSON.stringify(this.pageRef));
        //console.log('===========This is the param========'+this.homePageUrl);
        //console.log('contact id'+this.contactId);
        //console.log('isUserAuthenticated'+this.isUserAuthenticated);
        //console.log('this.userType : ' + this.userType);

        if(this.isUserAuthenticated == 'true') {
            this.showMasterDiv = true;
        } else {
            this.showMasterDiv = false;
        }

        if(this.userType == 'Employer') {
            this.defaultTab = "three";
            this.showBackButton = true;
        } else {
            this.defaultTab = "one";
            this.showBackButton = false;
        }
        //var element = this.template.getElementById("demo");
        //element.classList.add("approved");

        /*var x = this.template.querySelectorAll(data-id=='disable');
        for(var y in x)
        {
            //console.log('y : ' + y);
            y.classList.add("approved");
        }*/
        this.showSpinner = false;
    }

    backClickHandler() {
        window.history.back();
    }

    renderedCallback()
    {
        
    }
    logoutClickHandler() {
        //console.log('inside logoutclickHandler');
        this.loginPageRef = {
            type: 'standard__namedPage',
            attributes: {
                pageName: 'home'
            },
            state : {
            }
        };
        //console.log('home page reference: ' + JSON.stringify(this.onbordingListPageRef));
        this[NavigationMixin.Navigate](this.loginPageRef);
    }

   
    getPageReference() {
        //console.log('===========This is the param========'+this.currentPageReference);
        //console.log('===========This is the param========'+ JSON.stringify(this.currentPageReference));
    }
  
    
}