<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Innstillinger</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Innstillinger</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Innstillinger</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Innstillinger</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Innstillinger</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Innstillinger</value>
    </caseValues>
    <fields>
        <help>API-navn på feltet på kildeobjektet som inneholder informasjon som skal vises om en post. Ofte vil dette være standard Salesforce-konto, men det kan være hvilken som helst tekst, plukliste, oppslag, nummer, autonummer eller formelfelt.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>API-navnet til feltet på kildeobjektet som angir om det er aktivt. For å inkludere poster i Inntektsadministrasjon når et felt har en bestemt verdi, angi feltnavnet i Active Field, verdien i Active Value og sett Inkluder Active Value to True.</help>
        <label><!-- Active Field --></label>
        <name>ffrr__ActiveField__c</name>
    </fields>
    <fields>
        <help>Verdi i det aktive feltet som angir om kildeoppføringen er aktiv.</help>
        <label><!-- Active Value --></label>
        <name>ffrr__ActiveValue__c</name>
    </fields>
    <fields>
        <help>API-navnet på oppslagsfeltet (til kildeobjektet for denne innstillingen) som administratoren har lagt til Actuals vs Forecast Line-objektet. Dette feltet er obligatorisk når alternativet til Bruk faktisk vs Varselfunksjon er aktivert.</help>
        <label><!-- Actual vs Forecast Line Lookup --></label>
        <name>ffrr__ActualVsForecastRelationship__c</name>
    </fields>
    <fields>
        <help>API-navnet til feltet Nummer eller Valuta på kildeobjektet som lagrer beløpet av kostnaden som er amortisert til dags dato. Hvis befolket, oppdateres verdien på kildeposten automatisk hver gang en inntektsgenkjenningstransaksjon er forpliktet.</help>
        <label><!-- Amortized To Date Value --></label>
        <name>ffrr__AmortizedToDateValue__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller pluklisten som representerer rapporteringsverdi 1. Dette kan enten være et felt på kildeobjektet, eller ved å bruke oppslagforhold kan du referere til et felt på et objekt som er koblet sammen med en bane på opptil fem oppslag.</help>
        <label><!-- Analysis Item 1 --></label>
        <name>ffrr__AnalysisItem1__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller pluklisten som representerer rapporteringsverdi 2. Dette kan enten være et felt på kildeobjektet, eller ved å bruke oppslagforhold kan du referere til et felt på et objekt som er koblet sammen med en bane på opptil fem oppslag.</help>
        <label><!-- Analysis Item 2 --></label>
        <name>ffrr__AnalysisItem2__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller pluklisten som representerer rapporteringsverdi 3. Dette kan enten være et felt på kildeobjektet, eller ved å bruke oppslagforhold kan du referere til et felt på et objekt som er koblet sammen med en bane på opptil fem oppslag.</help>
        <label><!-- Analysis Item 3 --></label>
        <name>ffrr__AnalysisItem3__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller pluklisten som representerer rapporteringsverdien 4. Dette kan enten være et felt på kildeobjektet, eller ved å bruke oppslagforhold kan du referere til et felt på et objekt som er koblet sammen med en bane på opptil fem oppslag.</help>
        <label><!-- Analysis Item 4 --></label>
        <name>ffrr__AnalysisItem4__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller pluklisten på kildeobjektet som representerer balansen GLA som inntektsdagbøkene er lagt ut til.</help>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Billed To Date (Reserved) --></label>
        <name>ffrr__BilledToDate__c</name>
    </fields>
    <fields>
        <label><!-- Chain ID --></label>
        <name>ffrr__ChainID__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller pluklisten som representerer selskapet. Dette kan enten være et felt på kildeobjektet, eller ved å bruke oppslagforhold kan du referere til et felt på et objekt som er koblet sammen med en bane på opptil fem oppslag.</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
    </fields>
    <fields>
        <help>API-navnet til feltet på kildeobjektet som angir ferdigstillingsstatus. Hvis en leveringsmodellmall er opprettet for denne innstillingen, vil inntektene bli gjenkjent når kildeoppføringen er fullført.</help>
        <label><!-- Completed Field --></label>
        <name>ffrr__CompletedField__c</name>
    </fields>
    <fields>
        <help>Verdi på det aktive feltet som angir om kildeoppføringen er fullført. Hvis en leveringsmodellmall er opprettet for denne innstillingen, vil inntektene bli gjenkjent når kildeoppføringen er fullført.</help>
        <label><!-- Completed Value --></label>
        <name>ffrr__CompletedValue__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller pluklisten på kildeobjektet som representerer balansen GLA som kostnadsdagbøkene er lagt ut til.</help>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslag eller valgliste på kildeobjektet som representerer den spesifikke delen av diagrammet over kontoer som administreringskontoene dine er delanalysert av.</help>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslag eller valgliste på kildeobjektet som representerer den spesifikke delen av diagrammet over kontoer som administreringskontoene dine er delanalysert av.</help>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller pluklisten på kildeobjektet som representerer resultatregnskapet GLA som kostnadsdagbøkene er lagt ut til.</help>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>API-navnet til feltet på kildeobjektet som representerer hastigheten som kostnadsenheter belastes på.</help>
        <label><!-- Cost Rate --></label>
        <name>ffrr__CostRate__c</name>
    </fields>
    <fields>
        <help>API-navnet til feltet Nummer på kildeobjektet som representerer det totale antallet kostnadsførte kostnadsenheter. Kostnadsenhetens verdi kan være det totale antall timer eller dager som arbeidet på et prosjekt for eksempel.</help>
        <label><!-- Total Cost Units --></label>
        <name>ffrr__CostTotalUnits__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Tekst, Oppslag eller Picklist på kildeobjektet som representerer valutaen til kildeposten.</help>
        <label><!-- Document Currency --></label>
        <name>ffrr__Currency__c</name>
    </fields>
    <fields>
        <help>Feltmappdefinisjonen Populært på resultatobligasjonslinjeposter som standard når du kobler dem til en kildeoppføring og disse innstillingene.</help>
        <label><!-- Default Field Mapping Definition --></label>
        <name>ffrr__DefaultFieldMappingDefinition__c</name>
        <relationshipLabel><!-- Settings --></relationshipLabel>
    </fields>
    <fields>
        <help>API-navn på nummerfeltet på kildeobjektet som lagrer inntekten som er utsatt til dags dato, i dobbelt valuta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Dual) --></label>
        <name>ffrr__DeferredRevenueToDateDualCurrency__c</name>
    </fields>
    <fields>
        <help>API-navn for nummerfeltet på kildeobjektet som lagrer inntektene som er utsatt til dags dato, i hjemmevaluta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Home) --></label>
        <name>ffrr__DeferredRevenueToDateHomeCurrency__c</name>
    </fields>
    <fields>
        <help>API-navn for nummer-feltet på kildeobjektet som lagrer inntekten som er utsatt til dags dato, i rapporteringsvaluta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Reporting) --></label>
        <name>ffrr__DeferredRevenueToDateReportingCurrency__c</name>
    </fields>
    <fields>
        <help>API-navn på nummerfeltet på kildeobjektet som lagrer inntekten som er utsatt til dags dato, i dokumentvaluta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev --></label>
        <name>ffrr__DeferredRevenueToDate__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Tekst eller Lang tekstområde på kildeobjektet som inneholder en beskrivelse.</help>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <help>API-navnet på feltet Dobbel eller Valuta på kildeobjektet som representerer dokumentet til hjemmekursen.</help>
        <label><!-- Document Rate --></label>
        <name>ffrr__DocumentCurrencyRate__c</name>
    </fields>
    <fields>
        <help>API-navnet på feltet Dobbel eller Valuta på kildeobjektet som representerer regnskapsfirmaets hjemsted for dobbelt valutakurs.</help>
        <label><!-- Dual Rate --></label>
        <name>ffrr__DualCurrencyRate__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Tekst, Oppslag eller Picklist på kildeobjektet som representerer regnskapsfirmaets dobbelte valuta. Dobbelt valuta er vanligvis en bedriftsvaluta som brukes til rapporteringsformål.</help>
        <label><!-- Dual Currency --></label>
        <name>ffrr__DualCurrency__c</name>
    </fields>
    <fields>
        <help>API-navnet til Date eller DateTime-feltet på kildeobjektet som representerer sluttdatoen.</help>
        <label><!-- End Date/Deliverable Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 1 --></label>
        <name>ffrr__Filter1__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 2 --></label>
        <name>ffrr__Filter2__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 3 --></label>
        <name>ffrr__Filter3__c</name>
    </fields>
    <fields>
        <help>Velg for å indikere at den tilsvarende verdien er en fast kode, ikke et API-navn på det relaterte kildeobjektet. Disse avmerkingsboksene kan settes uavhengig av hverandre.</help>
        <label><!-- Fixed Balance Sheet --></label>
        <name>ffrr__FixedBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Velg for å indikere at den tilsvarende verdien er en fast kode, ikke et API-navn på det relaterte kildeobjektet. Disse avmerkingsboksene kan settes uavhengig av hverandre.</help>
        <label><!-- Fixed Balance Sheet (Cost) --></label>
        <name>ffrr__FixedCostBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Velg for å indikere at den tilsvarende verdien er en fast kode, ikke et API-navn på det relaterte kildeobjektet. Disse avmerkingsboksene kan settes uavhengig av hverandre.</help>
        <label><!-- Fixed Cost Center --></label>
        <name>ffrr__FixedCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Velg for å indikere at den tilsvarende verdien er en fast kode, ikke et API-navn på det relaterte kildeobjektet. Disse avmerkingsboksene kan settes uavhengig av hverandre.</help>
        <label><!-- Fixed Cost Center (Cost) --></label>
        <name>ffrr__FixedCostCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Velg for å indikere at den tilsvarende verdien er en fast kode, ikke et API-navn på det relaterte kildeobjektet. Disse avmerkingsboksene kan settes uavhengig av hverandre.</help>
        <label><!-- Fixed Income Statement (Cost) --></label>
        <name>ffrr__FixedCostIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Velg for å indikere at den tilsvarende verdien er en fast kode, ikke et API-navn på det relaterte kildeobjektet. Disse avmerkingsboksene kan settes uavhengig av hverandre.</help>
        <label><!-- Fixed Income Statement --></label>
        <name>ffrr__FixedIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Velg for å indikere at den tilsvarende verdien er en fast kode, ikke et API-navn på det relaterte kildeobjektet. Disse avmerkingsboksene kan angis uavhengig av hverandre.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Dual) --></label>
        <name>ffrr__FixedTrueUpDualAccountCode__c</name>
    </fields>
    <fields>
        <help>Velg for å indikere at den tilsvarende verdien er en fast kode, ikke et API-navn på det relaterte kildeobjektet. Disse avmerkingsboksene kan angis uavhengig av hverandre.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Home) --></label>
        <name>ffrr__FixedTrueUpHomeAccountCode__c</name>
    </fields>
    <fields>
        <help>API-navnet på oppslagsfeltet (til kildeobjektet på dette innstillingsnivået) som administratoren har lagt til i transaksjonsobjektet Revenue Forecast Forecast. Dette må fylles ut hvis innstillings type er Prognose og innstillingsnivået er Primær.</help>
        <label><!-- Revenue Forecast Transaction Lookup --></label>
        <name>ffrr__ForecastHeaderPrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>API-navnet på oppslagsfeltet (til kildeobjektet på dette innstillingsnivået) som administratoren har lagt til i objektet for prognoseprognose-transaksjonslinje. Dette må fylles ut hvis innstillings typen er Forecast.</help>
        <label><!-- Revenue Forecast Transaction Line Lookup --></label>
        <name>ffrr__ForecastTransactionLineRelationship__c</name>
    </fields>
    <fields>
        <label><!-- Group Name --></label>
        <name>ffrr__GroupName__c</name>
    </fields>
    <fields>
        <label><!-- Group --></label>
        <name>ffrr__Group__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller pluklisten på kildeobjektet som representerer den spesifikke komponenten du vil gruppere etter for gjenkjenne alle prosessen.</help>
        <label><!-- Grouped By --></label>
        <name>ffrr__GroupedBy__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Tekst, Oppslag eller Picklist på kildeobjektet som representerer regnskapsfirmaets hjemvaluta. Hjemme valuta er selskapets viktigste arbeid, og rapportering, valuta.</help>
        <label><!-- Home Currency --></label>
        <name>ffrr__HomeCurrency__c</name>
    </fields>
    <fields>
        <help>Inkluder eller ekskluder kildeoppføringer med verdien som er definert i feltet Active Value.</help>
        <label><!-- Include Active Value --></label>
        <name>ffrr__IncludeActiveValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Utelukke</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Inkludere</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Inkluder eller ekskluder kildeoppføringer med verdien som er definert i feltet Fullført verdi.</help>
        <label><!-- Include Completed Value --></label>
        <name>ffrr__IncludeCompletedValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Utelukke</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Inkludere</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller pluklisten på kildeobjektet som representerer resultatregnskapet GLA som inntektsdagbøkene er lagt ut til.</help>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>API-navnet på objektet som fungerer som datakilde på dette nivået. Dette kan ikke endres når innstillingen er brukt på en mal. For ikke-primære innstillinger må objektet være i et hoveddetaljer eller oppslagssammenheng med objektet i foreldreinnstillingen.</help>
        <label><!-- Object --></label>
        <name>ffrr__Object__c</name>
    </fields>
    <fields>
        <help>API-navnet på oppslagsfeltet (til kildeobjektet for denne innstillingen) som administratoren har lagt til objektobjektet Obligasjonslinjepost.</help>
        <label><!-- POLI Source Lookup --></label>
        <name>ffrr__POLISourceField__c</name>
    </fields>
    <fields>
        <help>API-navn på oppslagsfelt på kildeobjektet beskrevet av denne innstillingsposten, eller koblet til kildeobjektet ved en forholdsbane på opptil 5 oppslag, hvis verdier vil fylle inntektsplanplanlinjeoppslag. Når du spesifiserer flere felt, må de være kommaseparert og i samme rekkefølge som feltene i Inntektsplanlinjeoppslag.</help>
        <label><!-- Parent Relationship Paths --></label>
        <name>ffrr__ParentRelationshipPaths__c</name>
    </fields>
    <fields>
        <help>Velg oppføringsregistreringen (innstillingsoppføringen for kildeobjektet ett nivå over dette i hierarkiet).</help>
        <label><!-- Parent Settings --></label>
        <name>ffrr__ParentSettings__c</name>
        <relationshipLabel><!-- ParentSettings --></relationshipLabel>
    </fields>
    <fields>
        <help>API-navnet til feltet Nummer eller prosentandel på kildeobjektet som representerer hvordan fullfør kildeoppføringen er i prosentvise termer.</help>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentageComplete__c</name>
    </fields>
    <fields>
        <help>Skriv inn API-navnet til oppslagsfeltet til kildeobjektet til foreldreinnstillingen.</help>
        <label><!-- Parent Lookup --></label>
        <name>ffrr__PrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>API-navn på oppslaget som representerer produktet. Dette kan enten være et oppslag på kildeobjektet, eller ved å bruke oppslagforhold kan du referere til et oppslag på et objekt som er koblet sammen med en bane på opptil fem oppslag.</help>
        <label><!-- Product --></label>
        <name>ffrr__Product__c</name>
    </fields>
    <fields>
        <help>API-navnet til feltet på kildeobjektet som representerer frekvensen der enhetene belastes.</help>
        <label><!-- Rate --></label>
        <name>ffrr__Rate__c</name>
    </fields>
    <fields>
        <help>API-navn på nummerfeltet på kildeobjektet som lagrer inntektene som hittil er anerkjent i dobbelt valuta. Hvis den er fylt, oppdateres verdien på kildeposten automatisk hver gang en inntektsgjenkjenningstransaksjon blir forpliktet.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Dual) --></label>
        <name>ffrr__RecognizedToDateValueDual__c</name>
    </fields>
    <fields>
        <help>API-navn for nummerfeltet på kildeobjektet som lagrer inntektene som hittil er anerkjent i hjemmevaluta. Hvis den er fylt, oppdateres verdien på kildeposten automatisk hver gang en inntektsgjenkjenningstransaksjon er forpliktet.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Home) --></label>
        <name>ffrr__RecognizedToDateValueHome__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet Nummer eller Valuta på kildeobjektet som lagrer inntektsbeløpet som er gjenkjent til dags dato. Hvis befolket, oppdateres verdien på kildeposten automatisk hver gang en inntektsgenkjenningstransaksjon er forpliktet.</help>
        <label><!-- Recognized To Date Value --></label>
        <name>ffrr__RecognizedToDateValue__c</name>
    </fields>
    <fields>
        <help>API-navnet på det boolske feltet på kildeobjektet som indikerer alle inntekter, er fullt anerkjent og alle kostnader er fullstendig avskrevet. Hvis det er fylt ut, blir kryssboksen for kildeoppføring automatisk aktivert når inntektene er fullt anerkjente og koster fullstendig amortisering.</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionCompleted__c</name>
    </fields>
    <fields>
        <help>API-navn på oppslagsfelt (til kildeobjektet beskrevet av denne innstillingsposten eller relaterte objekter) som administratoren din har lagt til objektet for inntektsplanplanlinjen. Disse feltene er fylt ut fra oppslagene som er identifisert i foreldreforholdsstiene. Når du spesifiserer flere felt, må de være kommaseparert og i samme rekkefølge som feltene i foreldreforholdsbanene.</help>
        <label><!-- Revenue Schedule Line Lookups --></label>
        <name>ffrr__RevenueScheduleLineLookups__c</name>
    </fields>
    <fields>
        <help>API-navn på oppslagsfeltet (til kildeobjektet på dette innstillingsnivået) som administratoren har lagt til i inntektsplanobjektet.</help>
        <label><!-- Revenue Schedule Lookup --></label>
        <name>ffrr__RevenueScheduleSourceLookup__c</name>
    </fields>
    <fields>
        <help>API-navnet til feltet på kildeobjektet som lagrer den frittstående salgsprisen for en ytelsesforpliktelseslinjepost.</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <help>API-navn på oppslaget som representerer Salesforce-kontoen. Dette kan enten være et oppslag på kildeobjektet, eller ved å bruke oppslagforhold kan du referere til et oppslag på et objekt som er koblet sammen med en bane på opptil fem oppslag.</help>
        <label><!-- Account --></label>
        <name>ffrr__SalesforceAccount__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Setting Uniqueness --></label>
        <name>ffrr__SettingUniqueness__c</name>
    </fields>
    <fields>
        <help>Velg nivået for denne innstillingsrekordet for denne kildeobjektet.</help>
        <label><!-- Settings Level --></label>
        <name>ffrr__SettingsLevel__c</name>
        <picklistValues>
            <masterLabel>Level 2</masterLabel>
            <translation>Nivå 2</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 3</masterLabel>
            <translation>Nivå 3</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 4</masterLabel>
            <translation>Nivå 4</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Primary</masterLabel>
            <translation>Primær</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Type prosess denne innstillingsrekorden vil bli brukt til: Faktisk, Forecast eller begge deler. Hvis mappingene er identiske for både faktiske og prognoseprosesser, opprett en innstillingsplate for bruk av begge.</help>
        <label><!-- Settings Type --></label>
        <name>ffrr__SettingsType__c</name>
        <picklistValues>
            <masterLabel>Actual</masterLabel>
            <translation>Faktiske</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Forecast</masterLabel>
            <translation>Prognose</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>API-navnet til dato eller dato-tasten på kildeobjektet som representerer startdatoen.</help>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Nummer eller Valuta på kildeobjektet som representerer totalprisen.</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>API navn på feltet Nummer eller Valuta på kildeobjektet som representerer totalinntektene.</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>API-navnet til feltet Nummer på kildeobjektet som representerer det totale antallet ladede enheter. Den totale enhetens verdi kan være antall timer eller dager arbeidet på et prosjekt for eksempel.</help>
        <label><!-- Total Units --></label>
        <name>ffrr__TotalUnits__c</name>
    </fields>
    <fields>
        <help>API-navnet på oppslagsfeltet (til kildeobjektet på dette innstillingsnivået) som administratoren din har lagt til i transaksjonslinjepostobjektet. Dette må fylles hvis Innstillinger Type er Faktisk.</help>
        <label><!-- Revenue Recognition Txn Line Lookup --></label>
        <name>ffrr__TransactionLineRelationship__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller valglisten på kildeobjektet som representerer GLA som skal brukes til sanne verdier i dobbel valuta.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Dual) --></label>
        <name>ffrr__TrueUpDualAccount__c</name>
    </fields>
    <fields>
        <help>API-navn på tekstfeltet, oppslaget eller valglisten på kildeobjektet som representerer GLA som skal brukes til sanne verdier i hjemmevaluta.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Home) --></label>
        <name>ffrr__TrueUpHomeAccount__c</name>
    </fields>
    <fields>
        <help>Er denne innstillingen brukt i inntektsavtaler.</help>
        <label><!-- Use in Revenue Contract --></label>
        <name>ffrr__UseInRevenueContract__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet Nummer eller prosentandel på kildeobjektet som representerer VSOE-tallet (Vendor Specific Objective Evidence).</help>
        <label><!-- VSOE % --></label>
        <name>ffrr__VSOEPercent__c</name>
    </fields>
    <fields>
        <help>API-navnet på feltet Nummer eller Valuta på kildeobjektet som representerer VSOE (Vendor Specific Objective Evidence) -raten.</help>
        <label><!-- VSOE Rate --></label>
        <name>ffrr__VSOERate__c</name>
    </fields>
    <fields>
        <help>Type behandling av denne innstillingsrekorden vil bli brukt til: Inntekt, Kostnad eller begge deler.</help>
        <label><!-- Value Type --></label>
        <name>ffrr__ValueType__c</name>
        <picklistValues>
            <masterLabel>Cost</masterLabel>
            <translation>Koste</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Revenue</masterLabel>
            <translation>Inntekter</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Innstillinger Navn</nameFieldLabel>
    <webLinks>
        <label><!-- CreateMappings --></label>
        <name>ffrr__CreateMappings</name>
    </webLinks>
    <webLinks>
        <label><!-- CreateMappingsList --></label>
        <name>ffrr__CreateMappingsList</name>
    </webLinks>
    <webLinks>
        <label><!-- DeleteSettings --></label>
        <name>ffrr__DeleteSettings</name>
    </webLinks>
</CustomObjectTranslation>
