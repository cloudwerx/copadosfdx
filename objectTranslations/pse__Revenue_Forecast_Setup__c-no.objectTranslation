<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Prognoseoppsett</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Prognoseoppsett</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Prognoseoppsett</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Prognoseoppsett</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Prognoseoppsett</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Prognoseoppsett</value>
    </caseValues>
    <fields>
        <help>Indikerer inntekter for inntektsvarsel som brukes til beregning av inntektsprognose. Du kan bare ha en aktiv inntektsmeldingsoppgave.</help>
        <label><!-- Active --></label>
        <name>pse__Active__c</name>
    </fields>
    <fields>
        <help>API-navn på feltsettet på inntektsvarsel Versjonsdetaljobjekt som inneholder prosjekt-, mulighets- og milepælfeltene du vil bruke som egendefinerte filtre på siden Gjennomgå prognoseversjon.</help>
        <label><!-- Custom Filter Field Set --></label>
        <name>pse__Custom_Filter_Field_Set__c</name>
    </fields>
    <fields>
        <help>Hvis dette er valgt, er mulighetene ekskludert fra inntektsvarselversjoner.</help>
        <label><!-- Exclude Opportunities for Versions --></label>
        <name>pse__Exclude_Opportunities_For_Versions__c</name>
    </fields>
    <fields>
        <help>Hvis dette er valgt, blir mulighetene ekskludert fra planlagte jobber for inntekt.</help>
        <label><!-- Exclude Opportunities --></label>
        <name>pse__Exclude_Opportunities__c</name>
    </fields>
    <fields>
        <help>Hvis det er valgt, blir sannsynlighet ikke brukt på prognoser for salgsmuligheter.</help>
        <label><!-- Exclude Probability from Opportunities --></label>
        <name>pse__Exclude_Opportunity_Probabilities__c</name>
    </fields>
    <fields>
        <help>Velg for å ekskludere inntekter knyttet til forespurte ressurser om prosjekter og milepæler.</help>
        <label><!-- Exclude Resource Requests on Project --></label>
        <name>pse__Exclude_Resource_Requests_On_Project__c</name>
    </fields>
    <fields>
        <help>Hvis valgt, er ikke inntektsføringstall fra integrasjonen med inntektsadministrasjon lenger inkludert i beregningene av inntektsprognosen.</help>
        <label><!-- Exclude Revenue Recognition Actuals --></label>
        <name>pse__Exclude_Revenue_Recognition_Actuals__c</name>
    </fields>
    <fields>
        <help>Hvis det er valgt, blir ikke planlagte inntekter på prosjekter eller milepæler ekskludert fra inntektsvarselversjoner.</help>
        <label><!-- Exclude Unscheduled Revenue for Versions --></label>
        <name>pse__Exclude_Unscheduled_Revenue_For_Versions__c</name>
    </fields>
    <fields>
        <help>Antall poster som skal behandles samtidig, for eksempel tidskort og utgifter.</help>
        <label><!-- Forecast Factor Batch Size --></label>
        <name>pse__Forecast_Factor_Batch_Size__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet som er angitt på Milestone-objektet som styrer sveverdetaljene som vises for en milepæl i rutenettet for Prognosefordeling. Hvis du ikke angir et feltsett, gjelder standard svevdetaljer.</help>
        <label><!-- Hover Details Field Set on Milestone --></label>
        <name>pse__Hover_Details_Field_Set_On_Milestone__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet som er angitt på Opportunity-objektet som styrer sveveopplysningene som vises for en mulighet i Prognosen sammenbrudd. Hvis du ikke angir et feltsett, gjelder standard svevdetaljer.</help>
        <label><!-- Hover Details Field Set on Opportunity --></label>
        <name>pse__Hover_Details_Field_Set_On_Opportunity__c</name>
    </fields>
    <fields>
        <help>API-navn på feltet som er satt på prosjektobjektet som styrer sveverdetaljene som vises for et prosjekt i rutenettet for Prognosefordeling. Hvis du ikke angir et feltsett, gjelder standard svevdetaljer.</help>
        <label><!-- Hover Details Field Set on Project --></label>
        <name>pse__Hover_Details_Field_Set_On_Project__c</name>
    </fields>
    <fields>
        <help>Hvis valgt, brukes best case terskelen på prognoser for inntekter fra muligheter.</help>
        <label><!-- Include Best Case --></label>
        <name>pse__Include_Best_Case__c</name>
    </fields>
    <fields>
        <help>Hvis valgt, brukes tapsverdien på prognoser for inntekter fra muligheter.</help>
        <label><!-- Include Worst Case --></label>
        <name>pse__Include_Worst_Case__c</name>
    </fields>
    <fields>
        <help>Antall muligheter og ordrelinjer som skal behandles samtidig.</help>
        <label><!-- Opportunity Batch Size --></label>
        <name>pse__Opportunity_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Minste prosentvise sannsynlighet for at en mulighet blir inkludert i best case scenario.</help>
        <label><!-- Opportunity Best Case Threshold (%) --></label>
        <name>pse__Opportunity_Best_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Minste prosentvise sannsynlighet for at en mulighet blir inkludert i det forventede scenariet.</help>
        <label><!-- Opportunity Expected Threshold (%) --></label>
        <name>pse__Opportunity_Expected_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Minste prosentvise sannsynlighet for at en mulighet blir inkludert i verste fall.</help>
        <label><!-- Opportunity Worst Case Threshold (%) --></label>
        <name>pse__Opportunity_Worst_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Antall prosjekter som skal behandles samtidig.</help>
        <label><!-- Project Batch Size --></label>
        <name>pse__Project_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Skriv inn API-navnet til feltet på Opportunity-objektet du vil bruke som forventet sluttdato for prosjektet.</help>
        <label><!-- Expected Project End Date Field on Opp --></label>
        <name>pse__Project_End_Date_on_Opportunity__c</name>
    </fields>
    <fields>
        <help>Skriv inn API-navnet til feltet på Opportunity-objektet du vil bruke som forventet prosjektdato.</help>
        <label><!-- Expected Project Start Date Field on Opp --></label>
        <name>pse__Project_Start_Date_on_Opportunity__c</name>
    </fields>
    <fields>
        <help>Hvis valgt når du ikke bruker integrasjonen mellom PSA og Revenue Management, beholder Revenue Forecasting faktiske forhold fra forrige prognosekjøring i en lukket tidsperiode. Eventuelle gjenværende ventende inntekter flyttes til neste åpne tidsperiode.</help>
        <label><!-- Retain Pending Revenue in Closed Periods --></label>
        <name>pse__Retain_Pending_Revenue_In_Closed_Periods__c</name>
    </fields>
    <fields>
        <help>Velg API-navnet på feltet på prosjektobjektet du vil bruke til å beregne verdien i feltet% timer fullført for anerkjennelse. Om nødvendig kan du opprette ditt eget felt og legge det til i pluklisten.</help>
        <label><!-- Total Hours Field on Project --></label>
        <name>pse__Total_Hours_Field_on_Project__c</name>
        <picklistValues>
            <masterLabel>pse__Estimated_Hours_at_Completion__c</masterLabel>
            <translation>pse__Estimated_Hours_at_Completion__c</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>pse__Planned_Hours__c</masterLabel>
            <translation>pse__Planned_Hours__c</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Antall versjonsdetaljposter som skal behandles samtidig.</help>
        <label><!-- Version Detail Batch Size --></label>
        <name>pse__Version_Detail_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Prognoseperioden dekket av inntektsvarselversjonen.</help>
        <label><!-- Version Forecast Period --></label>
        <name>pse__Version_Forecast_Period__c</name>
        <picklistValues>
            <masterLabel>Current Quarter</masterLabel>
            <translation>Nåværende kvartal</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Current Year</masterLabel>
            <translation>Dette året</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Current and Next Quarter</masterLabel>
            <translation>Nåværende og neste kvartal</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 12 Months</masterLabel>
            <translation>Rullende 12 måneder</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 3 Months</masterLabel>
            <translation>Rullende 3 måneder</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 6 Months</masterLabel>
            <translation>Rullende 6 måneder</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Objektet som prosjekter og muligheter er gruppert under på siden om vurdering av prognose versjon.</help>
        <label><!-- Object for Version Grouping --></label>
        <name>pse__Version_Grouping_Primary__c</name>
        <picklistValues>
            <masterLabel>Group</masterLabel>
            <translation>Gruppe</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Practice</masterLabel>
            <translation>Øve på</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Region</masterLabel>
            <translation>Region</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Navn på oppsett for prognose</nameFieldLabel>
</CustomObjectTranslation>
