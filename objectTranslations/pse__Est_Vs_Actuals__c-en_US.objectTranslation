<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <label><!-- Scheduled Billings --></label>
        <name>FFX_Scheduled_Billings__c</name>
    </fields>
    <fields>
        <label><!-- End Date Is Last Sunday --></label>
        <name>psaws__End_Date_Is_Last_Sunday__c</name>
    </fields>
    <fields>
        <label><!-- High Hours Percent Variance --></label>
        <name>psaws__High_Hours_Percent_Variance__c</name>
    </fields>
    <fields>
        <label><!-- Project Manager Is Current User --></label>
        <name>psaws__Project_Manager_Is_Current_User__c</name>
    </fields>
    <fields>
        <help><!-- Actual Billable Amount divided by Actual Hours or Actual Days as appropriate --></help>
        <label><!-- Actual Average Bill Rate --></label>
        <name>pse__Actual_Average_Bill_Rate__c</name>
    </fields>
    <fields>
        <help><!-- The billable amount of timecard hours --></help>
        <label><!-- Actual Billable Amount --></label>
        <name>pse__Actual_Billable_Amount__c</name>
    </fields>
    <fields>
        <help><!-- Actual number of days worked in the time period based on aggregated timecards by week. --></help>
        <label><!-- Actual Days (Aggregated) --></label>
        <name>pse__Actual_Days_Aggregated__c</name>
    </fields>
    <fields>
        <help><!-- Actual number of days worked in the time period --></help>
        <label><!-- Actual Days --></label>
        <name>pse__Actual_Days__c</name>
    </fields>
    <fields>
        <help><!-- The number of hours entered on timecards --></help>
        <label><!-- Actual Hours --></label>
        <name>pse__Actual_Hours__c</name>
    </fields>
    <fields>
        <label><!-- Assignment --></label>
        <name>pse__Assignment__c</name>
        <relationshipLabel><!-- Est Vs Actuals --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Days Percent Variance --></label>
        <name>pse__Days_Percent_Variance__c</name>
    </fields>
    <fields>
        <label><!-- Days Variance --></label>
        <name>pse__Days_Variance__c</name>
    </fields>
    <fields>
        <help><!-- End date of the time period for this record. This should not be confused with the end date of the associated assignment or timecards. --></help>
        <label><!-- End Date --></label>
        <name>pse__End_Date__c</name>
    </fields>
    <fields>
        <help><!-- The number of days scheduled for work during the time period --></help>
        <label><!-- Estimated Days --></label>
        <name>pse__Estimated_Days__c</name>
    </fields>
    <fields>
        <help><!-- The number of hours scheduled --></help>
        <label><!-- Estimated Hours --></label>
        <name>pse__Estimated_Hours__c</name>
    </fields>
    <fields>
        <label><!-- Hours Percent Variance --></label>
        <name>pse__Percent_Variance__c</name>
    </fields>
    <fields>
        <help><!-- Project (bill rate) exchange rate, relative to corporate currency, calculated on the end date of the schedule. --></help>
        <label><!-- Project Currency Exchange Rate --></label>
        <name>pse__Project_Currency_Exchange_Rate__c</name>
    </fields>
    <fields>
        <label><!-- Project Manager --></label>
        <name>pse__Project_Manager__c</name>
        <relationshipLabel><!-- Est Vs Actuals (Project Manager) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Est Vs Actuals --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- The bill rate amount for the requested resource. --></help>
        <label><!-- Resource Request Bill Rate --></label>
        <name>pse__Resource_Request_Bill_Rate__c</name>
    </fields>
    <fields>
        <help><!-- The number of requested days scheduled for the work on the held resource request during the time period. --></help>
        <label><!-- Resource Request Days --></label>
        <name>pse__Resource_Request_Days__c</name>
    </fields>
    <fields>
        <help><!-- The number of requested hours scheduled on the held resource request. --></help>
        <label><!-- Resource Request Hours --></label>
        <name>pse__Resource_Request_Hours__c</name>
    </fields>
    <fields>
        <help><!-- Lookup to the held resource request relating to this Estimates Versus Actuals record. --></help>
        <label><!-- Resource Request --></label>
        <name>pse__Resource_Request__c</name>
        <relationshipLabel><!-- Estimates Versus Actuals --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>pse__Resource__c</name>
        <relationshipLabel><!-- Est Vs Actuals --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Bill rate from the Assignment --></help>
        <label><!-- Scheduled Bill Rate --></label>
        <name>pse__Scheduled_Bill_Rate__c</name>
    </fields>
    <fields>
        <label><!-- Scheduled Bill Rate is Daily Rate --></label>
        <name>pse__Scheduled_Bill_Rate_is_Daily_Rate__c</name>
    </fields>
    <fields>
        <help><!-- Start date of the time period for this record. This should not be confused with the start date of the associated assignment or timecards. --></help>
        <label><!-- Start Date --></label>
        <name>pse__Start_Date__c</name>
    </fields>
    <fields>
        <help><!-- The type of time period this time span represents --></help>
        <label><!-- Time Period Type --></label>
        <name>pse__Time_Period_Type__c</name>
        <picklistValues>
            <masterLabel>Month</masterLabel>
            <translation><!-- Month --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Quarter</masterLabel>
            <translation><!-- Quarter --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Week</masterLabel>
            <translation><!-- Week --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- The associated time period --></help>
        <label><!-- Time Period --></label>
        <name>pse__Time_Period__c</name>
        <relationshipLabel><!-- Est Vs Actuals --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Is any associated timecard submitted? --></help>
        <label><!-- Timecards Submitted --></label>
        <name>pse__Timecards_Submitted__c</name>
    </fields>
    <fields>
        <help><!-- The amount of variance in hours --></help>
        <label><!-- Hours Variance --></label>
        <name>pse__Variance__c</name>
    </fields>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <validationRules>
        <errorMessage><!-- The estimates versus actuals comparison record requires either an assignment or a resource request. --></errorMessage>
        <name>pse__Assignment_required</name>
    </validationRules>
</CustomObjectTranslation>
