<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Prestationsförpliktelse</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Prestationsförpliktelse</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Prestationsförpliktelse</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Prestationsförpliktelser</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Prestationsförpliktelser</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Prestationsförpliktelser</value>
    </caseValues>
    <fields>
        <help>Information om resultatplikten. Ofta kommer det att vara kontonamnet, men det kan vara vilken information som helst.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>Anger om källposten är aktiv.</help>
        <label><!-- Active --></label>
        <name>ffrr__Active__c</name>
    </fields>
    <fields>
        <help>Justering på grund av skillnad mellan intäkter och avsatta intäkter på intäktskontraktet som orsakats av kumulativa avrundningsfel på Prestationsförpliktelserna.</help>
        <label><!-- Allocated Revenue Rounding Adjustment --></label>
        <name>ffrr__AllocatedRevenueAdjustment__c</name>
    </fields>
    <fields>
        <help>Åsidosätta det beräknade beloppet som tilldelats denna Prestationsförpliktelse genom att ange ett annat värde här.</help>
        <label><!-- Allocated Revenue Override --></label>
        <name>ffrr__AllocatedRevenueOverride__c</name>
    </fields>
    <fields>
        <help>Beloppet som tilldelats denna Prestationsförpliktelse enligt beräkning av intäktsfördelning. Detta värde används för intäktsgenkänning.</help>
        <label><!-- Allocated Revenue --></label>
        <name>ffrr__AllocatedRevenue__c</name>
    </fields>
    <fields>
        <help>Kvoten som används för att fördela intäkter över intäktskontraktet.</help>
        <label><!-- Allocation Ratio --></label>
        <name>ffrr__AllocationRatio__c</name>
    </fields>
    <fields>
        <help>Visar om intäkterna för denna prestationsförpliktelse har tilldelats helt eller behöver omfördelas.</help>
        <label><!-- Allocation Status --></label>
        <name>ffrr__AllocationStatus__c</name>
    </fields>
    <fields>
        <label><!-- Amortized to Date --></label>
        <name>ffrr__AmortizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>Anger om källposten är klar.</help>
        <label><!-- Completed --></label>
        <name>ffrr__Completed__c</name>
    </fields>
    <fields>
        <help>Kontrolkostnadsresultat Obligationslinjeposten. Används för att fylla i fälten på Prestationsförpliktelsen.</help>
        <label><!-- Controlling POLI (Cost) --></label>
        <name>ffrr__ControllingCostPOLI__c</name>
        <relationshipLabel><!-- Controlling POLIs (Cost) --></relationshipLabel>
    </fields>
    <fields>
        <help>Den kontrollerade intäktsförpliktelsen Används för att fylla i fälten på Prestationsförpliktelsen.</help>
        <label><!-- Controlling POLI (Revenue) --></label>
        <name>ffrr__ControllingPOLI__c</name>
        <relationshipLabel><!-- Controlling POLIs (Revenue) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>Kostnad för denna prestationsförpliktelse</help>
        <label><!-- Cost --></label>
        <name>ffrr__Cost__c</name>
    </fields>
    <fields>
        <help>Antal decimaler i postens valuta.</help>
        <label><!-- Currency Decimal Places --></label>
        <name>ffrr__CurrencyDP__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <label><!-- End Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <help>Indikerar att denna Prestationsförpliktelse avser intäkter.</help>
        <label><!-- Has Revenue --></label>
        <name>ffrr__HasRevenue__c</name>
    </fields>
    <fields>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>Antal prestationsobligationslinjeposter som hänför sig till intäkter som har ett Noll SSP-värde.</help>
        <label><!-- Null SSP Count --></label>
        <name>ffrr__NullSSPCount__c</name>
    </fields>
    <fields>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentComplete__c</name>
    </fields>
    <fields>
        <help>Detta kontrolleras om intäkten har tilldelats och källposten är aktiv. Om den inte är markerad kan den här posten inte ingå i intäktsgenkänning.</help>
        <label><!-- Ready for Revenue Recognition --></label>
        <name>ffrr__ReadyForRevenueRecognition__c</name>
    </fields>
    <fields>
        <label><!-- Recognized to Date --></label>
        <name>ffrr__RecognizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Contract --></label>
        <name>ffrr__RevenueContract__c</name>
        <relationshipLabel><!-- Performance Obligations --></relationshipLabel>
    </fields>
    <fields>
        <help>Antalet resultatobligationslinjeposter som avser intäkter.</help>
        <label><!-- Revenue Count --></label>
        <name>ffrr__RevenueCount__c</name>
    </fields>
    <fields>
        <help>När den anges, indikerar att alla intäkter för denna Prestationsförpliktelse har blivit fullt erkända och alla kostnader skrivs av fullt ut.</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionComplete__c</name>
    </fields>
    <fields>
        <help>Intäkter för denna prestationsförpliktelse</help>
        <label><!-- Revenue --></label>
        <name>ffrr__Revenue__c</name>
    </fields>
    <fields>
        <help>Överrätta det fristående försäljningspriset från källposten genom att ange ett annat värde här.</help>
        <label><!-- SSP Override --></label>
        <name>ffrr__SSPOverride__c</name>
    </fields>
    <fields>
        <help>Det fristående försäljningspriset som ska användas för denna prestationsförpliktelse. Använder Total SSP, eller SSP-överstyrning om befolkade.</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>Summan av fristående försäljningspriser från alla länkade prestationsförpliktelser.</help>
        <label><!-- Total SSP --></label>
        <name>ffrr__TotalSSP__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__ffrrTemplate__c</name>
        <relationshipLabel><!-- Performance Obligations --></relationshipLabel>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Prestationsförpliktelse nummer</nameFieldLabel>
    <webLinks>
        <label><!-- AllocateRevenue --></label>
        <name>ffrr__AllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- Update --></label>
        <name>ffrr__Update</name>
    </webLinks>
</CustomObjectTranslation>
