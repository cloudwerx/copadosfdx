<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>收入合同</value>
    </caseValues>
    <fields>
        <help>有关收入合同的信息。通常，这将是帐户名，但可以是任何信息。</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <label><!-- Account --></label>
        <name>ffrr__Account__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Active --></label>
        <name>ffrr__Active__c</name>
    </fields>
    <fields>
        <help>指示是否可以为此收入合同成功运行分配。</help>
        <label><!-- Ready for Allocation --></label>
        <name>ffrr__Allocatable__c</name>
    </fields>
    <fields>
        <help>用于将收入分配到已分配的收入值尚未被覆盖的关联履约义务的比率。</help>
        <label><!-- Allocation Ratio --></label>
        <name>ffrr__AllocationRatio__c</name>
    </fields>
    <fields>
        <help>显示此合同的收入是否已完全分配，或者是否需要重新分配。</help>
        <label><!-- Allocation Status --></label>
        <name>ffrr__AllocationStatus__c</name>
    </fields>
    <fields>
        <help>查找公司对象。</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <fields>
        <help>记录货币的小数位数。</help>
        <label><!-- Currency Decimal Places --></label>
        <name>ffrr__CurrencyDP__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <label><!-- End Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <help>与收入相关且具有至少一个空SSP值的履约义务数。</help>
        <label><!-- Null SSP Count --></label>
        <name>ffrr__NullSSPCount__c</name>
    </fields>
    <fields>
        <help>已输入“已分配收入覆盖”值的已链接“性能义务”的数量。</help>
        <label><!-- PO Allocated Revenue Override Count --></label>
        <name>ffrr__POAllocatedRevenueOverrideCount__c</name>
    </fields>
    <fields>
        <help>本合同的履约义务数量。</help>
        <label><!-- Performance Obligations Count --></label>
        <name>ffrr__PerformanceObligationsCount__c</name>
    </fields>
    <fields>
        <help>指示是否已成功分配此合同的收入。</help>
        <label><!-- Revenue Allocated --></label>
        <name>ffrr__RevenueAllocated__c</name>
    </fields>
    <fields>
        <help>如果合同的收入是在合同级别定义的，而不是从性能义务中计算，则只应填充此字段。填充时，此值将在“收入”字段中使用。</help>
        <label><!-- Revenue Override --></label>
        <name>ffrr__RevenueOverride__c</name>
    </fields>
    <fields>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionComplete__c</name>
    </fields>
    <fields>
        <help>将分配给绩效义务的值。如果已填充，则使用总收入或收入覆盖。</help>
        <label><!-- Revenue --></label>
        <name>ffrr__Revenue__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>所有已链接的性能义务中的已分配收入覆盖值的总和。</help>
        <label><!-- Total Allocated Revenue Override --></label>
        <name>ffrr__TotalAllocatedRevenueOverride__c</name>
    </fields>
    <fields>
        <help>所有已链接的履约义务的已分配收入值总和。</help>
        <label><!-- Total Allocated Revenue --></label>
        <name>ffrr__TotalAllocatedRevenue__c</name>
    </fields>
    <fields>
        <help>所有已链接的履约义务中的摊销日期值总和。</help>
        <label><!-- Total Amortized To Date --></label>
        <name>ffrr__TotalAmortizedToDate__c</name>
    </fields>
    <fields>
        <help>所有关联履约义务的成本价值总和。</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>“分配比率”和“分配的收入覆盖”字段为空的链接的“绩效义务”的数量。</help>
        <label><!-- Null Allocation Ratio Count --></label>
        <name>ffrr__TotalNullAllocationRatioCount__c</name>
    </fields>
    <fields>
        <help>所有链接的履约义务中的已识别日期值总和。</help>
        <label><!-- Total Recognized To Date --></label>
        <name>ffrr__TotalRecognizedToDate__c</name>
    </fields>
    <fields>
        <help>所有链接的履约义务的收入值总和。</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>已分配的收入值尚未被覆盖的已链接履约义务的SSP值总和。</help>
        <label><!-- Total SSP for Allocation --></label>
        <name>ffrr__TotalSSPForAllocation__c</name>
    </fields>
    <fields>
        <help>所有相关履约义务的独立销售价格总和。</help>
        <label><!-- Total SSP --></label>
        <name>ffrr__TotalSSP__c</name>
    </fields>
    <fields>
        <help>与SSP为零的收入相关的履约义务数量。</help>
        <label><!-- Zero SSP Count --></label>
        <name>ffrr__ZeroSSPCount__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__ffrrtemplate__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <nameFieldLabel>收入合同号</nameFieldLabel>
    <webLinks>
        <label><!-- AllocateRevenue --></label>
        <name>ffrr__AllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- ListAllocateRevenue --></label>
        <name>ffrr__ListAllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- ListUpdatePerformanceObligations --></label>
        <name>ffrr__ListUpdatePerformanceObligations</name>
    </webLinks>
    <webLinks>
        <label><!-- ManageObligations --></label>
        <name>ffrr__ManageObligations</name>
    </webLinks>
    <webLinks>
        <label><!-- UpdatePerformanceObligations --></label>
        <name>ffrr__UpdatePerformanceObligations</name>
    </webLinks>
</CustomObjectTranslation>
