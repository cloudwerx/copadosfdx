<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Cálculo de tareas pendientes</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Cálculos de tareas pendientes</value>
    </caseValues>
    <fields>
        <help>Un campo interno utilizado por PSE: no destinado a mostrar a los usuarios.</help>
        <label><!-- Batch Id --></label>
        <name>pse__Batch_Id__c</name>
    </fields>
    <fields>
        <help>Si se debe calcular el retraso acumulado de los proyectos que figuran en la región, la práctica o el grupo.</help>
        <label><!-- Calculate Project Backlog --></label>
        <name>pse__Calculate_Project_Backlog__c</name>
    </fields>
    <fields>
        <help>Actualice los registros de región, práctica, grupo o proyecto con valores de los registros detallados de la cartera de pedidos directamente, además de crear registros de objetos de Cálculo de la reserva y de Detalles de la cartera de pedidos.</help>
        <label><!-- Copy Fields for Current Time Period --></label>
        <name>pse__Copy_Fields_for_Current_Time_Period__c</name>
    </fields>
    <fields>
        <help>La fecha de finalización de los cálculos de retraso acumulado. Si no se especifica una fecha de finalización, se procesarán todos los registros desde la fecha de inicio. Esto se recomienda al calcular la acumulación no programada.</help>
        <label><!-- End Date --></label>
        <name>pse__End_Date__c</name>
    </fields>
    <fields>
        <label><!-- Error Details --></label>
        <name>pse__Error_Details__c</name>
    </fields>
    <fields>
        <label><!-- Group --></label>
        <name>pse__Group__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <help>Si se debe calcular la acumulación de todos los niveles secundarios de la región, práctica o grupo seleccionado. Este campo se ignora si la acumulación se calcula solo para un recurso.</help>
        <label><!-- Include Sublevels --></label>
        <name>pse__Include_Sublevels__c</name>
    </fields>
    <fields>
        <label><!-- Is Report Master --></label>
        <name>pse__Is_Report_Master__c</name>
    </fields>
    <fields>
        <label><!-- Post Process Batch Id --></label>
        <name>pse__Post_Process_Batch_Id__c</name>
    </fields>
    <fields>
        <label><!-- Practice --></label>
        <name>pse__Practice__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Region --></label>
        <name>pse__Region__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>pse__Resource__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <help>Sobrescriba los registros de detalles de la cartera de pedidos existentes para las mismas regiones, prácticas, grupos, proyectos y períodos de tiempo asociados. Le recomendamos que seleccione esta opción para evitar el uso de espacio innecesario.</help>
        <label><!-- Reuse Detail Objects --></label>
        <name>pse__Reuse_Detail_Objects__c</name>
    </fields>
    <fields>
        <help>Establecer este valor establecerá automáticamente el campo de fecha de inicio</help>
        <label><!-- Start Calculating From --></label>
        <name>pse__Start_Calculating_From__c</name>
        <picklistValues>
            <masterLabel>Start of next week</masterLabel>
            <translation>Comienzo de la próxima semana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Start of this week</masterLabel>
            <translation>Comienzo de esta semana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Today</masterLabel>
            <translation>Hoy</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>pse__Start_Date__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Complete</masterLabel>
            <translation>Completar</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Error Occurred</masterLabel>
            <translation>Se produjo un error</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Progress</masterLabel>
            <translation>En progreso</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Not Started</masterLabel>
            <translation>No empezado</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Post Processing</masterLabel>
            <translation>Postprocesamiento</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Time Period Types --></label>
        <name>pse__Time_Period_Types__c</name>
        <picklistValues>
            <masterLabel>Month</masterLabel>
            <translation>Mes</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Other</masterLabel>
            <translation>Otro</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Perpetual</masterLabel>
            <translation>Perpetuo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Quarter</masterLabel>
            <translation>Trimestre</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Week</masterLabel>
            <translation>Semana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Year</masterLabel>
            <translation>Año</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nombre del cálculo de tareas pendientes</nameFieldLabel>
    <validationRules>
        <errorMessage><!-- End date cannot occur before start date --></errorMessage>
        <name>pse__check_end_date</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Only one of Region, Practice, Group, or Project may be specified. --></errorMessage>
        <name>pse__only_one_source_allowed</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- One of Region, Practice, Group, or Project is required --></errorMessage>
        <name>pse__source_required</name>
    </validationRules>
    <webLinks>
        <label><!-- Calculate_Backlog --></label>
        <name>pse__Calculate_Backlog</name>
    </webLinks>
</CustomObjectTranslation>
