<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>false</plural>
        <value>Kosten</value>
    </caseValues>
    <caseValues>
        <caseType>Nominative</caseType>
        <plural>true</plural>
        <value>Kosten</value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>false</plural>
        <value>Kosten</value>
    </caseValues>
    <caseValues>
        <caseType>Accusative</caseType>
        <plural>true</plural>
        <value>Kosten</value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>false</plural>
        <value>Kosten</value>
    </caseValues>
    <caseValues>
        <caseType>Genitive</caseType>
        <plural>true</plural>
        <value>Kosten</value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>false</plural>
        <value>Kosten</value>
    </caseValues>
    <caseValues>
        <caseType>Dative</caseType>
        <plural>true</plural>
        <value>Kosten</value>
    </caseValues>
    <fieldSets>
        <label><!-- Expense Columns For Combine Attachment PDF --></label>
        <name>pse__ExpenseColumnsForCombineAttachmentPDF</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Columns For Combine Attachment Page --></label>
        <name>pse__ExpenseColumnsForCombineAttachmentPage</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Header Row Editable Columns --></label>
        <name>pse__ExpenseHeaderRowEditableColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Header Row Read Only Columns --></label>
        <name>pse__ExpenseHeaderRowReadOnlyColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Notes Fields Editable Columns --></label>
        <name>pse__ExpenseNotesFieldsEditableColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Notes Fields Read Only Columns --></label>
        <name>pse__ExpenseNotesFieldsReadOnlyColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Mobile Expenses: Additional Fields in New and Edit Mode --></label>
        <name>pse__Mobile_Expenses_Additional_Fields</name>
    </fieldSets>
    <fields>
        <label><!-- Expense Notification --></label>
        <name>FFX_Expense_Notification__c</name>
    </fields>
    <fields>
        <help>Wenn diese Option aktiviert ist, kann der Administrator &quot;globale&quot; Änderungen an den Ausgaben vornehmen, einschließlich Änderungen am Projekt, an der Ressource, an der Währung oder am Datum des Aufwands, auch wenn die Option &quot;In Finanzen einschließen&quot; aktiviert ist. Konfigurationsanforderung: Actuals Calculation Mode muss auf &quot;Geplant&quot; eingestellt sein.</help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <label><!-- Amount To Bill --></label>
        <name>pse__Amount_To_Bill__c</name>
    </fields>
    <fields>
        <label><!-- Amount To Reimburse --></label>
        <name>pse__Amount_To_Reimburse__c</name>
    </fields>
    <fields>
        <label><!-- Amount --></label>
        <name>pse__Amount__c</name>
    </fields>
    <fields>
        <help>Im Feld wird die Ausgabenrate für Ausgaben angezeigt.</help>
        <label><!-- Applied Expense Rate --></label>
        <name>pse__Applied_Expense_Rate__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>Dieses Kontrollkästchen sollte aktiviert sein, wenn der Aufwand genehmigt wird. Dieser sollte darauf basieren, ob der übergeordnete Ausgabenbericht (falls vorhanden) genehmigt wurde.</help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Billing --></label>
        <name>pse__Approved_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Vendor Payment --></label>
        <name>pse__Approved_for_Vendor_Payment__c</name>
    </fields>
    <fields>
        <label><!-- Assignment --></label>
        <name>pse__Assignment__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>Wird von der Konfiguration automatisch überprüft, um anzugeben, ob Aufwandsanhänge von der Ausgabenzeile in die Spesenabrechnung verschoben wurden.</help>
        <label><!-- Attachments moved to ER --></label>
        <name>pse__Attachments_moved_to_ER__c</name>
    </fields>
    <fields>
        <help>Speichert den Verlauf der Audit-Notizen. Wenn ein Benutzer ein Projekt oder eine Zuweisung über die Timecard-Benutzerschnittstelle ändert und die Audit-Notizen 255 Zeichen überschreiten, werden ältere Audit-Notizen verschoben und angehängt.</help>
        <label><!-- Audit Notes History --></label>
        <name>pse__Audit_Notes_History__c</name>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Bill Date --></label>
        <name>pse__Bill_Date__c</name>
    </fields>
    <fields>
        <label><!-- Bill Transaction --></label>
        <name>pse__Bill_Transaction__c</name>
        <relationshipLabel><!-- Expense (Bill Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <help>Diese Formel entspricht Betrag (abzüglich nicht abrechenbarem Betrag und Steuer, falls vorhanden) für abrechenbare Kosten und Null für nicht abrechenbare Ausgaben: alles in der angefallenen Währung.</help>
        <label><!-- Billable Amount --></label>
        <name>pse__Billable_Amount__c</name>
    </fields>
    <fields>
        <help>Pauschalgebühr in Projektwährung, um den abrechenbaren Betrag des Aufwands aus dem Projekt zu erhöhen. Bei der Berechnung wird eine prozentuale Aufwandsentschädigung * vor * dieser Pauschalgebühr erhoben.</help>
        <label><!-- Billable Fee Flat Amount --></label>
        <name>pse__Billable_Fee_Flat_Amount__c</name>
    </fields>
    <fields>
        <help>Textformel, die eine Pauschalgebühr in der Projektwährung angibt, um die der abrechenbare Betrag des Aufwands erhöht wird, wird vom Projekt mit der Projektwährung vorbelegt. Bei der Berechnung wird eine prozentuale Aufwandsentschädigung * vor * dieser Pauschalgebühr erhoben.</help>
        <label><!-- Billable Fee Flat --></label>
        <name>pse__Billable_Fee_Flat__c</name>
    </fields>
    <fields>
        <help>Gebührenprozentsatz, um den der abrechenbare Betrag des Aufwands erhöht wird, wird vom Projekt nicht übernommen. 0% bedeutet keine Erhöhung (Standard), während 100% doppelt bedeutet. Bei der Berechnung wird die prozentuale Aufwandsentschädigung * vor * einer Pauschalgebühr erhoben.</help>
        <label><!-- Billable Fee Percentage --></label>
        <name>pse__Billable_Fee_Percentage__c</name>
    </fields>
    <fields>
        <label><!-- Billable --></label>
        <name>pse__Billable__c</name>
    </fields>
    <fields>
        <label><!-- Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Billing Amount (Pre-Fee Subtotal) --></label>
        <name>pse__Billing_Amount_Pre_Fee_Subtotal__c</name>
    </fields>
    <fields>
        <label><!-- Billing Amount --></label>
        <name>pse__Billing_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Billing Currency --></label>
        <name>pse__Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Invoiced --></label>
        <name>pse__Billing_Event_Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Item --></label>
        <name>pse__Billing_Event_Item__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billing Event Released --></label>
        <name>pse__Billing_Event_Released__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Status --></label>
        <name>pse__Billing_Event_Status__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event --></label>
        <name>pse__Billing_Event__c</name>
    </fields>
    <fields>
        <label><!-- Billing Hold --></label>
        <name>pse__Billing_Hold__c</name>
    </fields>
    <fields>
        <label><!-- Cost Transaction --></label>
        <name>pse__Cost_Transaction__c</name>
        <relationshipLabel><!-- Expense (Cost Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <help>Entfernung, die für die automatische Meilenerstattung zurückgelegt wurde.</help>
        <label><!-- Distance --></label>
        <name>pse__Distance__c</name>
    </fields>
    <fields>
        <help>Gibt an, dass sich der Aufwand in einem Status befindet, der für die Generierung von Fakturierungsereignissen geeignet ist (ohne das Flag &quot;Genehmigt für Fakturierung&quot;, das möglicherweise auch für die globale Konfiguration erforderlich ist).</help>
        <label><!-- Eligible for Billing --></label>
        <name>pse__Eligible_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Billing Currency) --></label>
        <name>pse__Exchange_Rate_Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Incurred Currency) --></label>
        <name>pse__Exchange_Rate_Incurred_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Reimbursement Currency) --></label>
        <name>pse__Exchange_Rate_Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <help>Wenn dieses Feld gesetzt ist, wird der * relative * Wechselkurs zwischen der Eintrittsrate und der Erstattungsrate festgelegt, die von der Ressource, für die die Kosten anfallen, festgestellt wird.</help>
        <label><!-- Exchange Rate (Resource-Defined) --></label>
        <name>pse__Exchange_Rate_Resource_Defined__c</name>
    </fields>
    <fields>
        <help>Wenn aktiviert, fakturieren Sie diesen Geschäftsdatensatz niemals. Dieselbe Wirkung wie der Fakturierungshaltevorgang, der jedoch einen permanenten Ausschluss von der Rechnungsgenerierung widerspiegeln soll.</help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Expense Date --></label>
        <name>pse__Expense_Date__c</name>
    </fields>
    <fields>
        <label><!-- Expense Report --></label>
        <name>pse__Expense_Report__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Expense Split Parent --></label>
        <name>pse__Expense_Split_Parent__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <help>Wenn dies für eine abrechenbare Ausgabe geprüft wird, bedeutet dies, dass der Teil des Ausgabenbetrags, der im Feld Steuerereignis angegeben ist, nicht abrechenbar ist. (Dies ist getrennt von und zusätzlich zu jedem Betrag im Feld &quot;Nicht abrechenbarer Betrag&quot;).</help>
        <label><!-- Incurred Tax Non-Billable --></label>
        <name>pse__Incurred_Tax_Non_Billable__c</name>
    </fields>
    <fields>
        <help>Der Betrag der anfallenden Steuer</help>
        <label><!-- Incurred Tax --></label>
        <name>pse__Incurred_Tax__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Date --></label>
        <name>pse__Invoice_Date__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Number --></label>
        <name>pse__Invoice_Number__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Transaction --></label>
        <name>pse__Invoice_Transaction__c</name>
        <relationshipLabel><!-- Expense (Invoice Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <help>Dieses Feld ist auf der Benutzeroberfläche sichtbar, wenn der Benutzer eine Anlage auf Aufwandszeilen benötigt, wenn der Ausgabenbericht übermittelt wird. Wenn dieses Feld auf true gesetzt ist, muss der Benutzer keine Anlage hinzufügen, obwohl die Konfiguration auf &quot;true&quot; gesetzt ist.</help>
        <label><!-- Lost Receipt --></label>
        <name>pse__Lost_Receipt__c</name>
    </fields>
    <fields>
        <label><!-- Milestone --></label>
        <name>pse__Milestone__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Mobile Expense Reference ID --></label>
        <name>pse__Mobile_Expense_Reference_ID__c</name>
    </fields>
    <fields>
        <help>Diese Formel entspricht dem Betrag für nicht abzurechnende Ausgaben und nicht abrechenbarer fälliger Betrag und Steuer (zwischen Null und Aufwand) für abrechenbare Ausgaben: Alle in der angefallenen Währung.</help>
        <label><!-- Non-Billable Amount --></label>
        <name>pse__Non_Billable_Amount__c</name>
    </fields>
    <fields>
        <help>Wenn es für eine abrechenbare Ausgabe vorgesehen ist, wird diese vom Ausgabenbetrag (anfallende Währung) abgezogen, um den abrechenbaren Betrag der Ausgabe zu berechnen. Mindest. Wert Null, max. Wert Ausgabenbetrag. Standardwert: Null oder Null. Keine Auswirkung auf nicht abrechenbare Ausgaben.</help>
        <label><!-- Non-Billable Incurred Amount --></label>
        <name>pse__Non_Billable_Incurred_Amount__c</name>
    </fields>
    <fields>
        <help>Diese Formel fügt alle nicht abrechenbaren Steuereinnahmen (nur, wenn Steuereinnahmen nicht abrechenbar aktiviert sind) zu einem zusätzlichen nicht abrechenbaren Steuerbetrag hinzu, um eine nicht abrechenbare aufgelaufene Zwischensumme mit einem Minimum von 0,00 und einem Maximum des Spesenbetrags zu erhalten.</help>
        <label><!-- Non-Billable Incurred Subtotal --></label>
        <name>pse__Non_Billable_Incurred_Subtotal__c</name>
    </fields>
    <fields>
        <help>Wenn dieses Kontrollkästchen aktiviert ist, wird der Erstattungsbetrag für den Aufwand ungeachtet des anfallenden Betrags gleich Null sein.</help>
        <label><!-- Non-Reimbursable --></label>
        <name>pse__Non_Reimbursible__c</name>
    </fields>
    <fields>
        <label><!-- Notes --></label>
        <name>pse__Notes__c</name>
    </fields>
    <fields>
        <label><!-- Override Group Currency Code --></label>
        <name>pse__Override_Group_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Überschreibt die Gruppe, auf die sich die Kostentransaktionen für Gruppenaktualisierungen aufrollen, selbst wenn sich das Projekt in einer anderen Gruppe befindet. In der Regel rollen die Expense-Transaktionen auf Basis von &quot;following&quot; -Regeln auf die Projekt- oder Ressourcengruppe auf.</help>
        <label><!-- Override Group --></label>
        <name>pse__Override_Group__c</name>
        <relationshipLabel><!-- Override Group For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Practice Currency Code --></label>
        <name>pse__Override_Practice_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Überschreibt die Praxis, in der die Kostentransaktionen für Praxisaktualisierungen ausgeführt werden, selbst wenn sich das Projekt in einer anderen Praxis befindet. In der Regel werden die Transaktionen eines Aufwands auf Grundlage der folgenden Regeln auf die Praxis des Projekts oder der Ressource aufgerollt.</help>
        <label><!-- Override Practice --></label>
        <name>pse__Override_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Rate (Billing Currency) --></label>
        <name>pse__Override_Rate_Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Rate (Incurred Currency) --></label>
        <name>pse__Override_Rate_Incurred_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Rate (Reimbursement Currency) --></label>
        <name>pse__Override_Rate_Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Region Currency Code --></label>
        <name>pse__Override_Region_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Überschreibt die Region, in der die Ausgabentransaktionen für regionale Istwerte ausgeführt werden, auch wenn das Projekt in einer anderen Region liegt. In der Regel rollen die Transaktionen eines Aufwands auf der Basis von &quot;following&quot; -Regeln auf die Region des Projekts oder der Ressource auf.</help>
        <label><!-- Override Region --></label>
        <name>pse__Override_Region__c</name>
        <relationshipLabel><!-- Override Region For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>Suche nach Projektmethodik</help>
        <label><!-- Project Methodology --></label>
        <name>pse__Project_Methodology__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>Nachschlagen in der Projektphase</help>
        <label><!-- Project Phase --></label>
        <name>pse__Project_Phase__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Rate Unit --></label>
        <name>pse__Rate_Unit__c</name>
        <picklistValues>
            <masterLabel>Kilometer</masterLabel>
            <translation>Kilometer</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Mile</masterLabel>
            <translation>Meile</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Die Erkennungsmethode, die für die Umsatzprognose auf diesen Datensatz angewendet werden soll.</help>
        <label><!-- Recognition Method --></label>
        <name>pse__Recognition_Method__c</name>
    </fields>
    <fields>
        <help>In diesem Feld wird ein numerischer Wert für den Rückerstattungsbetrag gespeichert, der unter Verwendung von Systemtarifen (nicht überschrieben) in die Projektwährung (Fakturierungswährung) konvertiert wird, um ihn später bei der Lieferantenfakturierung zu verwenden.</help>
        <label><!-- Reimbursement Amount In Project Currency --></label>
        <name>pse__Reimbursement_Amount_In_Project_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Reimbursement Amount --></label>
        <name>pse__Reimbursement_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Reimbursement Currency --></label>
        <name>pse__Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>pse__Resource__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Revenue Transaction --></label>
        <name>pse__Revenue_Transaction__c</name>
        <relationshipLabel><!-- Expense (Revenue Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Split Expense --></label>
        <name>pse__Split_Expense__c</name>
    </fields>
    <fields>
        <label><!-- Split Notes --></label>
        <name>pse__Split_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation>Genehmigt</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation>Entwurf</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation>Abgelehnt</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Submitted</masterLabel>
            <translation>Eingereicht</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Submitted --></label>
        <name>pse__Submitted__c</name>
    </fields>
    <fields>
        <help>Dieses Kontrollkästchen dient zur Systemnutzung. Es ist vorgesehen, dass es in Verbindung mit einem eventuellen Einfügen / Aktualisieren des Timecard-Splits überprüft wird, das in einer zukünftigen Methode ausgeführt wird, und stellt dann sicher, dass keine nachgelagerten zukünftigen Methoden aufgerufen werden.</help>
        <label><!-- Synchronous Update Required --></label>
        <name>pse__Synchronous_Update_Required__c</name>
    </fields>
    <fields>
        <help>Eine optionale Steuerart, die in den Ausgaben enthalten ist</help>
        <label><!-- Tax Type --></label>
        <name>pse__Tax_Type__c</name>
        <picklistValues>
            <masterLabel>GST</masterLabel>
            <translation>GST</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VAT 1</masterLabel>
            <translation>Mehrwertsteuer 1</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VAT 2</masterLabel>
            <translation>Mehrwertsteuer 2</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Die Kosten-ID aus dem Drittanbieter-Spesenantrag.</help>
        <label><!-- Third-Party Expenses App Expense ID --></label>
        <name>pse__Third_Party_Expenses_App_Expense_ID__c</name>
    </fields>
    <fields>
        <label><!-- Type --></label>
        <name>pse__Type__c</name>
        <picklistValues>
            <masterLabel>Airfare</masterLabel>
            <translation>Flugtickets</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Auto Mileage</masterLabel>
            <translation>Auto-Kilometer</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Business Meals</masterLabel>
            <translation>Geschäftsessen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Car Rental</masterLabel>
            <translation>Autovermietung</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Employee Relations</masterLabel>
            <translation>Arbeitnehmerbeziehungen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Gasoline</masterLabel>
            <translation>Benzin</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>General and Admin Expenses</masterLabel>
            <translation>Allgemeine und Admin-Ausgaben</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>IT Equipment and Services</masterLabel>
            <translation>IT-Ausrüstung und Dienstleistungen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Lodging (Room and Tax)</masterLabel>
            <translation>Unterkunft (Zimmer und Steuern)</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Miscellaneous</masterLabel>
            <translation>Sonstiges</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Office Supplies and Services</masterLabel>
            <translation>Bürobedarf und Dienstleistungen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Personal Meals</masterLabel>
            <translation>Persönliche Mahlzeiten</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Phone</masterLabel>
            <translation>Telefon</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Postage and Shipping</masterLabel>
            <translation>Porto und Versand</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Recruiting</masterLabel>
            <translation>Rekrutierung</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Subway</masterLabel>
            <translation>U-Bahn</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Taxi</masterLabel>
            <translation>Taxi</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Vendor Invoice Item --></label>
        <name>pse__Vendor_Invoice_Item__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Ausgabennummer</nameFieldLabel>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <validationRules>
        <errorMessage><!-- Assignment Project must match Expense Project. --></errorMessage>
        <name>pse__Expense_Asgnmt_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Assignment Resource must match Expense Resource. --></errorMessage>
        <name>pse__Expense_Asgnmt_Resource_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Line Project must match Expense Report Project. --></errorMessage>
        <name>pse__Expense_ER_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Line Resource must match Expense Report Resource. --></errorMessage>
        <name>pse__Expense_ER_Resource_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For an Expense Line to be marked as Billed, it must also be marked as Included in Financials and Approved. --></errorMessage>
        <name>pse__Expense_Line_Billing_Requires_Inclusion</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For an Expense Line to be marked as Invoiced, it must also be marked as Billed. --></errorMessage>
        <name>pse__Expense_Line_Invoicing_Requires_Billing</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- An Expense Line may not be submitted or included in financials unless it belongs to an Expense Report. --></errorMessage>
        <name>pse__Expense_Line_Needs_Parent_For_Submit</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Project field value may not be updated once set (unless parent Expense Report is changed, Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Line_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Expense Report may not be changed if it is submitted or included in financials. --></errorMessage>
        <name>pse__Expense_Line_Report_Fixed_If_Submitted</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Resource field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Line_Resource_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- An Expense may only be marked as Billable if its Project is Billable and its Expense Report and Assignment, if any, are Billable. --></errorMessage>
        <name>pse__Expense_May_Not_Be_Billable</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Milestone Project must match Expense Project. --></errorMessage>
        <name>pse__Expense_Milestone_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Report is required --></errorMessage>
        <name>pse__Expense_Requires_Expense_Report</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If provided, the incurred tax amount for an Expense must not be less than zero or greater than the Expense amount itself. --></errorMessage>
        <name>pse__Incurred_Tax_Amount_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Invoice Number or Date is not allowed on record unless it is invoiced --></errorMessage>
        <name>pse__Invoiced</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If provided, the non-billable incurred amount for an Expense must not be less than zero or greater than the Expense amount itself. --></errorMessage>
        <name>pse__Non_Billable_Incurred_Amount_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The non-billable incurred subtotal for an Expense (any non-billable amount plus any non-billable tax) must not be greater than the Expense amount itself. --></errorMessage>
        <name>pse__Non_Billable_Subtotal_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Resource-defined Exchange Rate can only be set if Incurred and Resource currencies are different.  If the currencies are the same, a relative rate of 1.0 is implied. --></errorMessage>
        <name>pse__Res_Defined_Rate_Diff_Currencies_Only</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Resource-defined Exchange Rate must either be greater than 0.0 (zero) or null/empty. --></errorMessage>
        <name>pse__Res_Defined_Rate_may_not_be_Negative</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The vendor invoice cannot change once set. --></errorMessage>
        <name>pse__Vendor_Invoice_Constant</name>
    </validationRules>
    <webLinks>
        <label><!-- Admin_Edit --></label>
        <name>pse__Admin_Edit</name>
    </webLinks>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
    <webLinks>
        <label><!-- Edit_Expenses --></label>
        <name>pse__Edit_Expenses</name>
    </webLinks>
    <webLinks>
        <label><!-- Multiple_Expense_Entry_UI --></label>
        <name>pse__Multiple_Expense_Entry_UI</name>
    </webLinks>
    <webLinks>
        <label><!-- Ready_to_Submit --></label>
        <name>pse__Ready_to_Submit</name>
    </webLinks>
</CustomObjectTranslation>
