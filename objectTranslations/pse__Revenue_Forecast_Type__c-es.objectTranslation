<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Tipo de pronóstico de ingresos</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Tipos de pronóstico de ingresos</value>
    </caseValues>
    <fields>
        <help>Contiene el valor combinado de los ingresos reconocidos hasta la fecha y los campos de reconocimiento de ingresos pendientes.</help>
        <label><!-- Actuals --></label>
        <name>pse__Actuals__c</name>
    </fields>
    <fields>
        <help>Contiene el valor combinado de los campos Corp: Ingresos reconocidos hasta la fecha y Corp: Reconocimiento de ingresos pendientes.</help>
        <label><!-- Corp: Actuals --></label>
        <name>pse__Corp_Actuals__c</name>
    </fields>
    <fields>
        <help>La moneda corporativa de la organización en el momento en que se ejecutó el pronóstico de ingresos.</help>
        <label><!-- Corp: Currency --></label>
        <name>pse__Corp_Currency__c</name>
    </fields>
    <fields>
        <help>En la moneda corporativa, los ingresos que están listos para el reconocimiento en este pronóstico de ingresos.</help>
        <label><!-- Corp: Revenue Pending Recognition --></label>
        <name>pse__Corp_Revenue_Pending_Recognition__c</name>
    </fields>
    <fields>
        <help>En la moneda corporativa, los ingresos que ya se han reconocido en este pronóstico de ingresos.</help>
        <label><!-- Corp: Revenue Recognized to Date --></label>
        <name>pse__Corp_Revenue_Recognized_To_Date__c</name>
    </fields>
    <fields>
        <help>En la moneda corporativa, los ingresos cuyo reconocimiento está programado para este pronóstico de ingresos.</help>
        <label><!-- Corp: Scheduled Revenue --></label>
        <name>pse__Corp_Scheduled_Revenue__c</name>
    </fields>
    <fields>
        <help>En la moneda corporativa, ingresos que actualmente no están programados.</help>
        <label><!-- Corp: Unscheduled Revenue --></label>
        <name>pse__Corp_Unscheduled_Revenue__c</name>
    </fields>
    <fields>
        <help>Busque el hito con el que se relaciona este tipo de pronóstico de ingresos.</help>
        <label><!-- Milestone --></label>
        <name>pse__Milestone__c</name>
        <relationshipLabel><!-- Revenue Forecast Types --></relationshipLabel>
    </fields>
    <fields>
        <help>Busque la oportunidad con la que se relaciona este tipo de pronóstico de ingresos.</help>
        <label><!-- Opportunity --></label>
        <name>pse__Opportunity__c</name>
        <relationshipLabel><!-- Revenue Forecast Types --></relationshipLabel>
    </fields>
    <fields>
        <help>Busque el proyecto al que se refiere este tipo de pronóstico de ingresos.</help>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Revenue Forecast Types --></relationshipLabel>
    </fields>
    <fields>
        <help>Busque el pronóstico de ingresos relevante.</help>
        <label><!-- Revenue Forecast --></label>
        <name>pse__Revenue_Forecast__c</name>
        <relationshipLabel><!-- Revenue Forecast Types --></relationshipLabel>
    </fields>
    <fields>
        <help>Los ingresos que están listos para el reconocimiento en este pronóstico de ingresos.</help>
        <label><!-- Revenue Pending Recognition --></label>
        <name>pse__Revenue_Pending_Recognition__c</name>
    </fields>
    <fields>
        <help>Los ingresos que ya se han reconocido en este pronóstico de ingresos.</help>
        <label><!-- Revenue Recognized to Date --></label>
        <name>pse__Revenue_Recognized_To_Date__c</name>
    </fields>
    <fields>
        <help>Indica la fuente de los ingresos para este registro.</help>
        <label><!-- Revenue Source --></label>
        <name>pse__Revenue_Source__c</name>
        <picklistValues>
            <masterLabel>% Complete: Milestone</masterLabel>
            <translation>% Completado: hito</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>% Complete: Project</masterLabel>
            <translation>% Completado: Proyecto</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Deliverable: EVA</masterLabel>
            <translation>Entregable: EVA</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Deliverable: Expense</masterLabel>
            <translation>Entregable: Gastos</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Deliverable: Milestone</masterLabel>
            <translation>Entregable: hito</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Deliverable: Miscellaneous Adjustment</masterLabel>
            <translation>Entregable: Ajuste misceláneo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Deliverable: Timecard</masterLabel>
            <translation>Entregable: Tarjeta de tiempo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Equal Split: Milestone</masterLabel>
            <translation>División igual: hito</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Equal Split: Project</masterLabel>
            <translation>División igualitaria: proyecto</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Opportunity</masterLabel>
            <translation>Oportunidad</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Indica si los valores de este registro se aplican a los ingresos reales o previstos.</help>
        <label><!-- Revenue Type --></label>
        <name>pse__Revenue_Type__c</name>
        <picklistValues>
            <masterLabel>Actuals</masterLabel>
            <translation>Informes estadísticos</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Forecast</masterLabel>
            <translation>Pronóstico</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Los ingresos programados para el reconocimiento en este pronóstico de ingresos.</help>
        <label><!-- Scheduled Revenue --></label>
        <name>pse__Scheduled_Revenue__c</name>
    </fields>
    <fields>
        <help>Si hay meses que no contienen ingresos programados o reales, los ingresos no programados se distribuyen uniformemente entre esos meses, excepto los meses vacíos que se encuentran entre los meses que contienen ingresos programados o reales.</help>
        <label><!-- Unscheduled Revenue --></label>
        <name>pse__Unscheduled_Revenue__c</name>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nombre del tipo de pronóstico de ingreso</nameFieldLabel>
</CustomObjectTranslation>
