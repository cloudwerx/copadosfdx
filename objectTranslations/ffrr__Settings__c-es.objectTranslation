<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Configuraciones</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Configuraciones</value>
    </caseValues>
    <fields>
        <help>Nombre de API del campo en el objeto de origen que contiene información para mostrar sobre un registro. A menudo, esta será la cuenta estándar de Salesforce, pero puede ser cualquier texto, lista de selección, búsqueda, número, número automático o campo de fórmula.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>Nombre API del campo en el objeto fuente que indica si está activo. Para incluir registros en Administración de ingresos cuando un campo tiene un valor específico, establezca el nombre del campo en Campo activo, el valor en Valor activo y establezca Incluir valor activo en Verdadero.</help>
        <label><!-- Active Field --></label>
        <name>ffrr__ActiveField__c</name>
    </fields>
    <fields>
        <help>Valor en el campo activo que indica si el registro fuente está activo.</help>
        <label><!-- Active Value --></label>
        <name>ffrr__ActiveValue__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo de búsqueda (al objeto de origen de esta configuración) que su administrador ha agregado al objeto Línea actual frente a Línea pronosticada. Este campo es obligatorio cuando la opción para usar la función real frente a la función de pronóstico está habilitada.</help>
        <label><!-- Actual vs Forecast Line Lookup --></label>
        <name>ffrr__ActualVsForecastRelationship__c</name>
    </fields>
    <fields>
        <help>Nombre API del campo Número o Moneda en el objeto fuente que almacena la cantidad de costo amortizado hasta la fecha. Si se rellena, el valor en el registro fuente se actualiza automáticamente cada vez que se confirma una Transacción de reconocimiento de ingresos.</help>
        <label><!-- Amortized To Date Value --></label>
        <name>ffrr__AmortizedToDateValue__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección que representa el valor de informe 1. Esto puede ser un campo en el objeto de origen, o utilizando relaciones de búsqueda puede hacer referencia a un campo en un objeto vinculado por una ruta de hasta cinco búsquedas.</help>
        <label><!-- Analysis Item 1 --></label>
        <name>ffrr__AnalysisItem1__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección que representa el valor de informe 2. Esto puede ser un campo en el objeto de origen, o utilizando relaciones de búsqueda puede hacer referencia a un campo en un objeto vinculado por una ruta de hasta cinco búsquedas.</help>
        <label><!-- Analysis Item 2 --></label>
        <name>ffrr__AnalysisItem2__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección que representa el valor de informe 3. Esto puede ser un campo en el objeto de origen, o utilizando relaciones de búsqueda puede hacer referencia a un campo en un objeto vinculado por una ruta de hasta cinco búsquedas.</help>
        <label><!-- Analysis Item 3 --></label>
        <name>ffrr__AnalysisItem3__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección que representa el valor de informe 4. Esto puede ser un campo en el objeto de origen, o utilizando relaciones de búsqueda puede hacer referencia a un campo en un objeto vinculado por una ruta de hasta cinco búsquedas.</help>
        <label><!-- Analysis Item 4 --></label>
        <name>ffrr__AnalysisItem4__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección en el objeto de origen que representa el GLA del balance en el que se publican los diarios de ingresos.</help>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Billed To Date (Reserved) --></label>
        <name>ffrr__BilledToDate__c</name>
    </fields>
    <fields>
        <label><!-- Chain ID --></label>
        <name>ffrr__ChainID__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección que representa la empresa. Puede ser un campo en el objeto de origen o, mediante el uso de relaciones de búsqueda, puede hacer referencia a un campo en un objeto vinculado por una ruta de hasta cinco búsquedas.</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo en el objeto de origen que indica su estado de finalización. Si se crea una plantilla tipo entregable para esta configuración, los ingresos se reconocerán cuando se complete el registro fuente.</help>
        <label><!-- Completed Field --></label>
        <name>ffrr__CompletedField__c</name>
    </fields>
    <fields>
        <help>Valor en el campo activo que indica si el registro fuente está completo. Si se crea una plantilla tipo entregable para esta configuración, los ingresos se reconocerán cuando se complete el registro fuente.</help>
        <label><!-- Completed Value --></label>
        <name>ffrr__CompletedValue__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo de texto, búsqueda o lista de selección en el objeto de origen que representa el GLA del balance en el que se publican los diarios de costos.</help>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección en el objeto fuente que representa el componente específico de su plan de cuentas mediante el cual se subanalizan sus cuentas de administración.</help>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección en el objeto fuente que representa el componente específico de su plan de cuentas mediante el cual se subanalizan sus cuentas de administración.</help>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección en el objeto de origen que representa el GLA del estado de resultados en el que se publican los diarios de costos.</help>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>Nombre API del campo en el objeto fuente que representa la velocidad a la que se cobran las unidades de coste.</help>
        <label><!-- Cost Rate --></label>
        <name>ffrr__CostRate__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo Número en el objeto de origen que representa la cantidad total de unidades de costo cargadas. El valor de las unidades de costo podría ser el número total de horas o días trabajados en un proyecto, por ejemplo.</help>
        <label><!-- Total Cost Units --></label>
        <name>ffrr__CostTotalUnits__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo Texto, Búsqueda o Lista de selección en el objeto de origen que representa la moneda del registro de origen.</help>
        <label><!-- Document Currency --></label>
        <name>ffrr__Currency__c</name>
    </fields>
    <fields>
        <help>La definición de asignación de campo se rellena en elementos de línea de obligación de rendimiento de forma predeterminada cuando los vincula a un registro de origen y esta configuración.</help>
        <label><!-- Default Field Mapping Definition --></label>
        <name>ffrr__DefaultFieldMappingDefinition__c</name>
        <relationshipLabel><!-- Settings --></relationshipLabel>
    </fields>
    <fields>
        <help>Nombre de API del campo Número en el objeto de origen que almacena la cantidad de ingresos diferidos hasta la fecha, en moneda dual.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Dual) --></label>
        <name>ffrr__DeferredRevenueToDateDualCurrency__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo Número en el objeto de origen que almacena la cantidad de ingresos diferidos hasta la fecha, en la moneda local.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Home) --></label>
        <name>ffrr__DeferredRevenueToDateHomeCurrency__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo Número en el objeto de origen que almacena la cantidad de ingresos diferidos hasta la fecha, en la moneda del informe.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Reporting) --></label>
        <name>ffrr__DeferredRevenueToDateReportingCurrency__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo Número en el objeto de origen que almacena la cantidad de ingresos diferidos hasta la fecha, en la moneda del documento.</help>
        <label><!-- FOR FUTURE USE Deferred Rev --></label>
        <name>ffrr__DeferredRevenueToDate__c</name>
    </fields>
    <fields>
        <help>Nombre API del campo Texto o Área de texto largo en el objeto fuente que contiene una descripción.</help>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo Doble o Moneda en el objeto de origen que representa el documento al tipo de cambio de la moneda local.</help>
        <label><!-- Document Rate --></label>
        <name>ffrr__DocumentCurrencyRate__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo Doble o Moneda en el objeto de origen que representa la tasa de cambio de la moneda dual de la empresa contable.</help>
        <label><!-- Dual Rate --></label>
        <name>ffrr__DualCurrencyRate__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo Texto, Búsqueda o Lista de selección en el objeto de origen que representa la moneda doble de la empresa de contabilidad. La moneda dual suele ser una moneda corporativa utilizada para fines de informes.</help>
        <label><!-- Dual Currency --></label>
        <name>ffrr__DualCurrency__c</name>
    </fields>
    <fields>
        <help>Nombre API del campo Fecha o Fecha y Hora en el objeto fuente que representa su fecha de finalización.</help>
        <label><!-- End Date/Deliverable Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 1 --></label>
        <name>ffrr__Filter1__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 2 --></label>
        <name>ffrr__Filter2__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 3 --></label>
        <name>ffrr__Filter3__c</name>
    </fields>
    <fields>
        <help>Seleccione para indicar que el valor correspondiente es un código fijo, no un nombre de API en el objeto fuente relacionado. Estas casillas de verificación se pueden configurar de forma independiente.</help>
        <label><!-- Fixed Balance Sheet --></label>
        <name>ffrr__FixedBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Seleccione para indicar que el valor correspondiente es un código fijo, no un nombre de API en el objeto fuente relacionado. Estas casillas de verificación se pueden configurar de forma independiente.</help>
        <label><!-- Fixed Balance Sheet (Cost) --></label>
        <name>ffrr__FixedCostBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Seleccione para indicar que el valor correspondiente es un código fijo, no un nombre de API en el objeto fuente relacionado. Estas casillas de verificación se pueden configurar de forma independiente.</help>
        <label><!-- Fixed Cost Center --></label>
        <name>ffrr__FixedCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Seleccione para indicar que el valor correspondiente es un código fijo, no un nombre de API en el objeto fuente relacionado. Estas casillas de verificación se pueden configurar de forma independiente.</help>
        <label><!-- Fixed Cost Center (Cost) --></label>
        <name>ffrr__FixedCostCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Seleccione para indicar que el valor correspondiente es un código fijo, no un nombre de API en el objeto fuente relacionado. Estas casillas de verificación se pueden configurar de forma independiente.</help>
        <label><!-- Fixed Income Statement (Cost) --></label>
        <name>ffrr__FixedCostIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Seleccione para indicar que el valor correspondiente es un código fijo, no un nombre de API en el objeto fuente relacionado. Estas casillas de verificación se pueden configurar de forma independiente.</help>
        <label><!-- Fixed Income Statement --></label>
        <name>ffrr__FixedIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Seleccione para indicar que el valor correspondiente es un código fijo, no un nombre de API en el objeto fuente relacionado. Estas casillas de verificación se pueden establecer de forma independiente entre sí.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Dual) --></label>
        <name>ffrr__FixedTrueUpDualAccountCode__c</name>
    </fields>
    <fields>
        <help>Seleccione para indicar que el valor correspondiente es un código fijo, no un nombre de API en el objeto fuente relacionado. Estas casillas de verificación se pueden establecer de forma independiente entre sí.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Home) --></label>
        <name>ffrr__FixedTrueUpHomeAccountCode__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo de búsqueda (al objeto de origen en este nivel de configuración) que su administrador ha agregado al objeto Transacción de previsión de ingresos. Esto debe completarse si el tipo de configuración es Previsión y el nivel de configuración es Primario.</help>
        <label><!-- Revenue Forecast Transaction Lookup --></label>
        <name>ffrr__ForecastHeaderPrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo de búsqueda (al objeto de origen en este nivel de configuración) que su administrador ha agregado al objeto Línea de transacción del pronóstico de ingresos. Esto debe completarse si el tipo de configuración es Pronóstico.</help>
        <label><!-- Revenue Forecast Transaction Line Lookup --></label>
        <name>ffrr__ForecastTransactionLineRelationship__c</name>
    </fields>
    <fields>
        <label><!-- Group Name --></label>
        <name>ffrr__GroupName__c</name>
    </fields>
    <fields>
        <label><!-- Group --></label>
        <name>ffrr__Group__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección en el objeto de origen que representa el componente específico por el que desea agrupar para el proceso Reconocer todo.</help>
        <label><!-- Grouped By --></label>
        <name>ffrr__GroupedBy__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo Texto, Búsqueda o Lista de selección en el objeto de origen que representa la moneda local de la empresa de contabilidad. La moneda nacional es la moneda principal de trabajo y de informes de la compañía.</help>
        <label><!-- Home Currency --></label>
        <name>ffrr__HomeCurrency__c</name>
    </fields>
    <fields>
        <help>Incluir o excluir registros de origen con el valor definido en el campo Valor activo.</help>
        <label><!-- Include Active Value --></label>
        <name>ffrr__IncludeActiveValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Excluir</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Incluir</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Incluir o excluir registros fuente con el valor definido en el campo Valor Completado.</help>
        <label><!-- Include Completed Value --></label>
        <name>ffrr__IncludeCompletedValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Excluir</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Incluir</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección en el objeto de origen que representa el GLA del estado de resultados en el que se publican los diarios de ingresos.</help>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del objeto que actúa como fuente de datos en este nivel. Esto no puede cambiar una vez que se usa la configuración en una plantilla. Para configuraciones no primarias, el objeto debe estar en una relación maestra de detalles o búsqueda con el objeto en su configuración principal.</help>
        <label><!-- Object --></label>
        <name>ffrr__Object__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo de búsqueda (al objeto de origen de esta configuración) que su administrador ha agregado al objeto Elemento de línea de obligación de rendimiento.</help>
        <label><!-- POLI Source Lookup --></label>
        <name>ffrr__POLISourceField__c</name>
    </fields>
    <fields>
        <help>Nombres de API de campos de búsqueda en el objeto de origen descrito por este registro de configuración, o vinculados al objeto de origen por una ruta de relación de hasta 5 búsquedas, cuyos valores completarán las búsquedas de línea de programación de ingresos. Al especificar varios campos, deben estar separados por comas y en el mismo orden que los campos de las búsquedas de líneas de programación de ingresos.</help>
        <label><!-- Parent Relationship Paths --></label>
        <name>ffrr__ParentRelationshipPaths__c</name>
    </fields>
    <fields>
        <help>Seleccione el registro de configuración principal (el registro de configuración para el objeto fuente un nivel por encima de este en la jerarquía).</help>
        <label><!-- Parent Settings --></label>
        <name>ffrr__ParentSettings__c</name>
        <relationshipLabel><!-- ParentSettings --></relationshipLabel>
    </fields>
    <fields>
        <help>Nombre de API del campo Número o Porcentaje en el objeto fuente que representa cómo se completa el registro fuente en términos de porcentaje.</help>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentageComplete__c</name>
    </fields>
    <fields>
        <help>Ingrese el nombre de API del campo de búsqueda en el objeto de origen de la configuración principal.</help>
        <label><!-- Parent Lookup --></label>
        <name>ffrr__PrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>Nombre de API de la búsqueda que representa el producto. Esto puede ser una búsqueda en el objeto de origen o, mediante el uso de relaciones de búsqueda, puede hacer referencia a una búsqueda en un objeto vinculado por una ruta de hasta cinco búsquedas.</help>
        <label><!-- Product --></label>
        <name>ffrr__Product__c</name>
    </fields>
    <fields>
        <help>Nombre API del campo en el objeto fuente que representa la velocidad a la que se cargan las unidades.</help>
        <label><!-- Rate --></label>
        <name>ffrr__Rate__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo Número en el objeto de origen que almacena la cantidad de ingresos reconocidos hasta la fecha en moneda dual. Si se completa, el valor del registro de origen se actualiza automáticamente cada vez que se confirma una Transacción de reconocimiento de ingresos.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Dual) --></label>
        <name>ffrr__RecognizedToDateValueDual__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo Número en el objeto de origen que almacena la cantidad de ingresos reconocidos hasta la fecha en la moneda local. Si se completa, el valor del registro de origen se actualiza automáticamente cada vez que se confirma una Transacción de reconocimiento de ingresos.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Home) --></label>
        <name>ffrr__RecognizedToDateValueHome__c</name>
    </fields>
    <fields>
        <help>Nombre API del campo Número o Moneda en el objeto fuente que almacena la cantidad de ingresos reconocidos hasta la fecha. Si se rellena, el valor en el registro fuente se actualiza automáticamente cada vez que se confirma una Transacción de reconocimiento de ingresos.</help>
        <label><!-- Recognized To Date Value --></label>
        <name>ffrr__RecognizedToDateValue__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo booleano en el objeto de origen que indica que todos los ingresos se reconocen completamente y que todos los costos se amortizan completamente. Si se completa, la casilla de verificación del registro de origen se habilita automáticamente cuando los ingresos se reconocen completamente y el costo se amortiza completamente.</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionCompleted__c</name>
    </fields>
    <fields>
        <help>Nombres de API de campos de búsqueda (al objeto de origen descrito por este registro de configuración u objetos relacionados) que su administrador ha agregado al objeto Línea de programación de ingresos. Estos campos se completan a partir de las búsquedas identificadas en las Rutas de relación de padres. Al especificar varios campos, deben estar separados por comas y en el mismo orden que los campos en las Rutas de relación de padres.</help>
        <label><!-- Revenue Schedule Line Lookups --></label>
        <name>ffrr__RevenueScheduleLineLookups__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de búsqueda (al objeto de origen en este nivel de configuración) que su administrador ha agregado al objeto Programa de ingresos.</help>
        <label><!-- Revenue Schedule Lookup --></label>
        <name>ffrr__RevenueScheduleSourceLookup__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo en el objeto de origen que almacena el precio de venta independiente para una línea de pedido de obligación de rendimiento.</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <help>Nombre de API de la búsqueda que representa la cuenta de Salesforce. Esto puede ser una búsqueda en el objeto de origen, o utilizando relaciones de búsqueda puede hacer referencia a una búsqueda en un objeto vinculado por una ruta de hasta cinco búsquedas.</help>
        <label><!-- Account --></label>
        <name>ffrr__SalesforceAccount__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Setting Uniqueness --></label>
        <name>ffrr__SettingUniqueness__c</name>
    </fields>
    <fields>
        <help>Seleccione el nivel de este registro de configuración de objeto fuente.</help>
        <label><!-- Settings Level --></label>
        <name>ffrr__SettingsLevel__c</name>
        <picklistValues>
            <masterLabel>Level 2</masterLabel>
            <translation>Nivel 2</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 3</masterLabel>
            <translation>Nivel 3</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 4</masterLabel>
            <translation>Nivel 4</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Primary</masterLabel>
            <translation>Primario</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>El tipo de proceso para el que se utilizará este registro de configuración: real, pronóstico o ambos. Si las asignaciones son idénticas para los procesos reales y de pronóstico, cree un registro de configuración para que ambos lo utilicen.</help>
        <label><!-- Settings Type --></label>
        <name>ffrr__SettingsType__c</name>
        <picklistValues>
            <masterLabel>Actual</masterLabel>
            <translation>Real</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Forecast</masterLabel>
            <translation>Predicción</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Nombre de API del campo Fecha o Fecha y hora en el objeto de origen que representa la fecha de inicio.</help>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>Nombre API del campo Número o Moneda en el objeto fuente que representa el costo total.</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo Número o Moneda en el objeto fuente que representa el ingreso total.</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>Nombre API del campo Número en el objeto fuente que representa el número total de unidades cargadas. El valor total de las unidades podría ser el número de horas o días trabajados en un proyecto, por ejemplo.</help>
        <label><!-- Total Units --></label>
        <name>ffrr__TotalUnits__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo de búsqueda (al objeto de origen en este nivel de configuración) que su administrador ha agregado al objeto Línea de transacción de reconocimiento de ingresos. Esto se debe completar si el tipo de configuración es real.</help>
        <label><!-- Revenue Recognition Txn Line Lookup --></label>
        <name>ffrr__TransactionLineRelationship__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo de texto, búsqueda o lista de selección en el objeto de origen que representa el GLA que se utilizará para valores reales en moneda dual.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Dual) --></label>
        <name>ffrr__TrueUpDualAccount__c</name>
    </fields>
    <fields>
        <help>Nombre de API del campo de texto, búsqueda o lista de selección en el objeto de origen que representa el GLA que se utilizará para los valores reales en la moneda local.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Home) --></label>
        <name>ffrr__TrueUpHomeAccount__c</name>
    </fields>
    <fields>
        <help>Es esta configuración utilizada en los contratos de ingresos.</help>
        <label><!-- Use in Revenue Contract --></label>
        <name>ffrr__UseInRevenueContract__c</name>
    </fields>
    <fields>
        <help>Nombre de la API del campo Número o Porcentaje en el objeto fuente que representa el porcentaje VSOE (Evidencia de Objetivo Específico del Proveedor).</help>
        <label><!-- VSOE % --></label>
        <name>ffrr__VSOEPercent__c</name>
    </fields>
    <fields>
        <help>Nombre API del campo Número o Moneda en el objeto fuente que representa la tasa VSOE (Evidencia de Objetivo Específico del Proveedor).</help>
        <label><!-- VSOE Rate --></label>
        <name>ffrr__VSOERate__c</name>
    </fields>
    <fields>
        <help>El tipo de procesamiento que se utilizará para este registro de configuración será: Ingresos, Costo o ambos.</help>
        <label><!-- Value Type --></label>
        <name>ffrr__ValueType__c</name>
        <picklistValues>
            <masterLabel>Cost</masterLabel>
            <translation>Costo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Revenue</masterLabel>
            <translation>Ingresos</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nombre de configuración</nameFieldLabel>
    <webLinks>
        <label><!-- CreateMappings --></label>
        <name>ffrr__CreateMappings</name>
    </webLinks>
    <webLinks>
        <label><!-- CreateMappingsList --></label>
        <name>ffrr__CreateMappingsList</name>
    </webLinks>
    <webLinks>
        <label><!-- DeleteSettings --></label>
        <name>ffrr__DeleteSettings</name>
    </webLinks>
</CustomObjectTranslation>
