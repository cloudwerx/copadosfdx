<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Udgiftsrapport</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Udgiftsrapport</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Udgiftsrapport</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Udgiftsrapporter</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Udgiftsrapporter</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Udgiftsrapporter</value>
    </caseValues>
    <fieldSets>
        <label><!-- ExpenseReportApprovalColumns --></label>
        <name>pse__ExpenseReportApprovalColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Report Editable Columns --></label>
        <name>pse__ExpenseReportEditableColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Report Read Only Columns --></label>
        <name>pse__ExpenseReportReadOnlyColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Report Grid --></label>
        <name>pse__ExpenseRptGridFieldSet</name>
    </fieldSets>
    <fields>
        <label><!-- Expense Report Number --></label>
        <name>FFX_Expense_Report_Number__c</name>
    </fields>
    <fields>
        <label><!-- Is Resource Current User --></label>
        <name>psaws__Is_Resource_Current_User__c</name>
    </fields>
    <fields>
        <label><!-- Action: Update Include In Financials --></label>
        <name>pse__Action_Check_Include_In_Financials__c</name>
    </fields>
    <fields>
        <help>Hvis markeret, tillader Admin at foretage globale ændringer i udgiftsrapporten, herunder redigeringer til ERs projekt, ressource, valuta eller dato, selvom Include In Financials er markeret. Config krav: Aktuals Beregning Mode skal indstilles til &apos;Planlagt&apos;.</help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <help>Denne afkrydsningsfelt skal kontrolleres, når udgiftsrapporten er godkendt - typisk baseret på statusfeltet.</help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approver --></label>
        <name>pse__Approver__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Assignment --></label>
        <name>pse__Assignment__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Opbevarer revisionsnotater historie. Når en bruger ændrer et projekt eller en opgave på en udgiftsrapport og revisionsnotaterne overstiger 255 tegn, flyttes ældre revisionsnotater og tilføjes her.</help>
        <label><!-- Audit Notes History --></label>
        <name>pse__Audit_Notes_History__c</name>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Billable --></label>
        <name>pse__Billable__c</name>
    </fields>
    <fields>
        <label><!-- Lines Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <help>Hvis markeret, bør dette deaktivere enhver PSA-triggerbaseret automatisk indsendelse til godkendelse til udgiftsrapporten.</help>
        <label><!-- Disable Approval Auto Submit --></label>
        <name>pse__Disable_Approval_Auto_Submit__c</name>
    </fields>
    <fields>
        <help>Hvis markeret, vil du som standard kontrollere afkrydsningsfeltet Undgå fra fakturering på børneudgifter, som har samme virkning som Billing Hold, men som er beregnet til at afspejle en permanent udelukkelse fra Billing Generation.</help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Expense Report Reference --></label>
        <name>pse__Expense_Report_Reference__c</name>
    </fields>
    <fields>
        <label><!-- First Expense Date --></label>
        <name>pse__First_Expense_Date__c</name>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <label><!-- Lines Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Last Expense Date --></label>
        <name>pse__Last_Expense_Date__c</name>
    </fields>
    <fields>
        <label><!-- Milestone --></label>
        <name>pse__Milestone__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Overstyrer den gruppe, som børns Expense Transactions vil kaste op for Gruppelovgivninger, selvom projektet er i en anden gruppe. En Expense&apos;s Transactions kører typisk op til projektets eller ressourcegruppens gruppe baseret på &quot;følger&quot; regler.</help>
        <label><!-- Override Group --></label>
        <name>pse__Override_Group__c</name>
        <relationshipLabel><!-- Override Group For Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Overstyrer den praksis, som barnets udgiftstransaktioner vil kaste op for praksishandlinger, selvom projektet er i en anden praksis. Typisk går en Expenses Transactions op til Projektets eller Ressurs Practice baseret på &quot;følger&quot; regler.</help>
        <label><!-- Override Practice --></label>
        <name>pse__Override_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Overstyrer regionen, som børns udgiftstransaktioner vil kaste op for regionale aktører, selvom projektet ligger i en anden region. Typisk går en Expenses Transactions op til Projektets eller Ressourcens Region baseret på &quot;følger&quot; regler.</help>
        <label><!-- Override Region --></label>
        <name>pse__Override_Region__c</name>
        <relationshipLabel><!-- Override Region For Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project Expense Notes --></label>
        <name>pse__Project_Expense_Notes__c</name>
    </fields>
    <fields>
        <help>Opslag til projektmetodologi</help>
        <label><!-- Project Methodology --></label>
        <name>pse__Project_Methodology__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Opslag til projektfase</help>
        <label><!-- Project Phase --></label>
        <name>pse__Project_Phase__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Dette felt viser valutakoden for 3 tegn, for at Ressourcen skal refunderes.</help>
        <label><!-- Reimbursement Currency --></label>
        <name>pse__Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>pse__Resource__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation>Godkendt</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation>Udkast</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation>Afvist</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Submitted</masterLabel>
            <translation>Indsendt</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Submitted --></label>
        <name>pse__Submitted__c</name>
    </fields>
    <fields>
        <help>ID for udgiftsrapporten fra Tredjeparts Udgifter Applikation.</help>
        <label><!-- Third-Party Expenses App Report ID --></label>
        <name>pse__Third_Party_Expenses_App_Report_ID__c</name>
    </fields>
    <fields>
        <label><!-- Total Billable Amount --></label>
        <name>pse__Total_Billable_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Total Non-Billable Amount --></label>
        <name>pse__Total_Non_Billable_Amount__c</name>
    </fields>
    <fields>
        <help>Dette tal viser det samlede tilbagebetalingsbeløb i Ressurs valuta.</help>
        <label><!-- Total Reimbursement Amount --></label>
        <name>pse__Total_Reimbursement_Amount__c</name>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Udgiftsrapportnavn</nameFieldLabel>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <validationRules>
        <errorMessage><!-- Assignment Project must match Expense Report Project. --></errorMessage>
        <name>pse__ExpenseReport_Asgnmt_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Assignment Resource must match Expense Report Resource. --></errorMessage>
        <name>pse__ExpenseReport_Asgnmt_Resource_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Milestone Project must match Expense Report Project. --></errorMessage>
        <name>pse__ExpenseReport_Milestone_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Cannot associate the methodology with this expense report. The methodology must belong to the same project as the expense report. Select a methodology that belongs to the same project as the expense report. --></errorMessage>
        <name>pse__ExpenseRpt_Methodology_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Cannot associate the phase with this expense report. The phase must belong to the same project as the expense report. Select a phase that belongs to the same project as the expense report. --></errorMessage>
        <name>pse__ExpenseRpt_Phase_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For an Expense Report to be marked as Approved, it must also be marked as Submitted. --></errorMessage>
        <name>pse__Expense_Report_Approval_Requires_Submit</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- An Expense Report may only be marked as Billable if its Project is Billable and its Assignment, if any, is Billable. --></errorMessage>
        <name>pse__Expense_Report_May_Not_Be_Billable</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Report&apos;s Project field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Report_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Report&apos;s Resource field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Report_Resource_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Reports for this Project require an Assignment. --></errorMessage>
        <name>pse__Project_Requires_Exp_Report_Assignment</name>
    </validationRules>
    <webLinks>
        <label><!-- Admin_Edit --></label>
        <name>pse__Admin_Edit</name>
    </webLinks>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
    <webLinks>
        <label><!-- Combine_Attachments --></label>
        <name>pse__Combine_Attachments</name>
    </webLinks>
    <webLinks>
        <label><!-- Multiple_Expense_Entry_UI --></label>
        <name>pse__Multiple_Expense_Entry_UI</name>
    </webLinks>
</CustomObjectTranslation>
