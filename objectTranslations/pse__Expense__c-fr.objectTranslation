<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Frais</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Dépenses</value>
    </caseValues>
    <fieldSets>
        <label><!-- Expense Columns For Combine Attachment PDF --></label>
        <name>pse__ExpenseColumnsForCombineAttachmentPDF</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Columns For Combine Attachment Page --></label>
        <name>pse__ExpenseColumnsForCombineAttachmentPage</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Header Row Editable Columns --></label>
        <name>pse__ExpenseHeaderRowEditableColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Header Row Read Only Columns --></label>
        <name>pse__ExpenseHeaderRowReadOnlyColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Notes Fields Editable Columns --></label>
        <name>pse__ExpenseNotesFieldsEditableColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Notes Fields Read Only Columns --></label>
        <name>pse__ExpenseNotesFieldsReadOnlyColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Mobile Expenses: Additional Fields in New and Edit Mode --></label>
        <name>pse__Mobile_Expenses_Additional_Fields</name>
    </fieldSets>
    <fields>
        <label><!-- Expense Notification --></label>
        <name>FFX_Expense_Notification__c</name>
    </fields>
    <fields>
        <help>Si cette case est cochée, l&apos;administrateur peut modifier «globalement» les dépenses, y compris les modifications apportées au projet, à la ressource, à la devise ou à la date de la dépense, même si l&apos;option Inclure dans les états financiers est cochée. Configuration requise: le mode de calcul réel doit être défini sur &quot;Planifié&quot;.</help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <label><!-- Amount To Bill --></label>
        <name>pse__Amount_To_Bill__c</name>
    </fields>
    <fields>
        <label><!-- Amount To Reimburse --></label>
        <name>pse__Amount_To_Reimburse__c</name>
    </fields>
    <fields>
        <label><!-- Amount --></label>
        <name>pse__Amount__c</name>
    </fields>
    <fields>
        <help>Le champ contiendra le taux de dépense appliqué aux dépenses.</help>
        <label><!-- Applied Expense Rate --></label>
        <name>pse__Applied_Expense_Rate__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>Cette case à cocher doit être cochée lorsque la dépense est approuvée, ce qui doit être basé sur l’approbation ou non du rapport de dépenses parent (le cas échéant).</help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Billing --></label>
        <name>pse__Approved_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Vendor Payment --></label>
        <name>pse__Approved_for_Vendor_Payment__c</name>
    </fields>
    <fields>
        <label><!-- Assignment --></label>
        <name>pse__Assignment__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>Vérifié automatiquement par configuration pour indiquer si les pièces jointes de dépenses ont été déplacées de la ligne de dépense vers la note de frais.</help>
        <label><!-- Attachments moved to ER --></label>
        <name>pse__Attachments_moved_to_ER__c</name>
    </fields>
    <fields>
        <help>Stocke l&apos;historique des notes d&apos;audit. Lorsqu&apos;un utilisateur modifie un projet ou une affectation à l&apos;aide de l&apos;interface Timecard et que les notes d&apos;audit dépassent 255 caractères, les anciennes notes d&apos;audit sont déplacées et ajoutées ici.</help>
        <label><!-- Audit Notes History --></label>
        <name>pse__Audit_Notes_History__c</name>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Bill Date --></label>
        <name>pse__Bill_Date__c</name>
    </fields>
    <fields>
        <label><!-- Bill Transaction --></label>
        <name>pse__Bill_Transaction__c</name>
        <relationshipLabel><!-- Expense (Bill Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <help>Cette formule est égale à la somme (moins le montant incombant non facturé et la taxe, le cas échéant) pour les dépenses facturables et à zéro pour les dépenses non facturables: toutes dans la devise utilisée.</help>
        <label><!-- Billable Amount --></label>
        <name>pse__Billable_Amount__c</name>
    </fields>
    <fields>
        <help>Frais forfaitaires dans la devise du projet pour augmenter le montant facturable de la dépense, par défaut du projet. Le calcul appliquera tout pourcentage de frais * avant * ce montant forfaitaire.</help>
        <label><!-- Billable Fee Flat Amount --></label>
        <name>pse__Billable_Fee_Flat_Amount__c</name>
    </fields>
    <fields>
        <help>Formule de texte indiquant des frais fixes dans la devise du projet pour augmenter le montant facturable de la dépense, par défaut du projet avec la devise du projet. Le calcul appliquera tout pourcentage de frais * avant * ce montant forfaitaire.</help>
        <label><!-- Billable Fee Flat --></label>
        <name>pse__Billable_Fee_Flat__c</name>
    </fields>
    <fields>
        <help>Pourcentage d&apos;honoraires pour augmenter le montant facturable de la dépense, impayé par le projet. 0% signifie aucune augmentation (par défaut), alors que 100% signifie double. Le calcul appliquera les frais de pourcentage * avant * toute commission de dépense fixe.</help>
        <label><!-- Billable Fee Percentage --></label>
        <name>pse__Billable_Fee_Percentage__c</name>
    </fields>
    <fields>
        <label><!-- Billable --></label>
        <name>pse__Billable__c</name>
    </fields>
    <fields>
        <label><!-- Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Billing Amount (Pre-Fee Subtotal) --></label>
        <name>pse__Billing_Amount_Pre_Fee_Subtotal__c</name>
    </fields>
    <fields>
        <label><!-- Billing Amount --></label>
        <name>pse__Billing_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Billing Currency --></label>
        <name>pse__Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Invoiced --></label>
        <name>pse__Billing_Event_Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Item --></label>
        <name>pse__Billing_Event_Item__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billing Event Released --></label>
        <name>pse__Billing_Event_Released__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Status --></label>
        <name>pse__Billing_Event_Status__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event --></label>
        <name>pse__Billing_Event__c</name>
    </fields>
    <fields>
        <label><!-- Billing Hold --></label>
        <name>pse__Billing_Hold__c</name>
    </fields>
    <fields>
        <label><!-- Cost Transaction --></label>
        <name>pse__Cost_Transaction__c</name>
        <relationshipLabel><!-- Expense (Cost Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <help>Distance parcourue pour le remboursement du kilométrage automatique.</help>
        <label><!-- Distance --></label>
        <name>pse__Distance__c</name>
    </fields>
    <fields>
        <help>Indique que les dépenses sont dans un état éligible pour la génération d&apos;événements de facturation (à l&apos;exception de l&apos;indicateur Approuvé pour la facturation, qui peut également être requis par configuration globale).</help>
        <label><!-- Eligible for Billing --></label>
        <name>pse__Eligible_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Billing Currency) --></label>
        <name>pse__Exchange_Rate_Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Incurred Currency) --></label>
        <name>pse__Exchange_Rate_Incurred_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Exchange Rate (Reimbursement Currency) --></label>
        <name>pse__Exchange_Rate_Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <help>Lorsqu&apos;il est défini, ce champ définit le taux de change * relatif * entre le taux encouru et le taux de remboursement que la ressource subit.</help>
        <label><!-- Exchange Rate (Resource-Defined) --></label>
        <name>pse__Exchange_Rate_Resource_Defined__c</name>
    </fields>
    <fields>
        <help>Si cette case est cochée, ne jamais facturer cet enregistrement professionnel. Même effet que le blocage de la facturation, mais destiné à refléter une exclusion permanente de la génération de facturation.</help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Expense Date --></label>
        <name>pse__Expense_Date__c</name>
    </fields>
    <fields>
        <label><!-- Expense Report --></label>
        <name>pse__Expense_Report__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Expense Split Parent --></label>
        <name>pse__Expense_Split_Parent__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <help>Si cette case est cochée pour une dépense facturable, cela signifie que la partie du montant de la dépense indiquée dans le champ Taxe encourue n&apos;est pas facturable. (Ceci est distinct de et en plus de tout montant dans le champ Montant non facturé imputable).</help>
        <label><!-- Incurred Tax Non-Billable --></label>
        <name>pse__Incurred_Tax_Non_Billable__c</name>
    </fields>
    <fields>
        <help>Le montant de la taxe encourue</help>
        <label><!-- Incurred Tax --></label>
        <name>pse__Incurred_Tax__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Date --></label>
        <name>pse__Invoice_Date__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Number --></label>
        <name>pse__Invoice_Number__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Transaction --></label>
        <name>pse__Invoice_Transaction__c</name>
        <relationshipLabel><!-- Expense (Invoice Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <help>Ce champ est visible sur l&apos;interface utilisateur si l&apos;utilisateur nécessite une pièce jointe sur les lignes de dépense si le rapport de dépenses est soumis. Si ce champ est défini sur true, l&apos;utilisateur n&apos;a pas besoin d&apos;ajouter de pièce jointe alors que la configuration est définie sur true.</help>
        <label><!-- Lost Receipt --></label>
        <name>pse__Lost_Receipt__c</name>
    </fields>
    <fields>
        <label><!-- Milestone --></label>
        <name>pse__Milestone__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Mobile Expense Reference ID --></label>
        <name>pse__Mobile_Expense_Reference_ID__c</name>
    </fields>
    <fields>
        <help>Cette formule correspond au montant des dépenses non facturables et au montant et à la taxe imputables non facturables (compris entre zéro et le montant de la dépense) pour les dépenses facturables: le tout dans la devise utilisée.</help>
        <label><!-- Non-Billable Amount --></label>
        <name>pse__Non_Billable_Amount__c</name>
    </fields>
    <fields>
        <help>Si une dépense facturée est fournie, elle est soustraite du montant de la dépense (monnaie encourue) pour calculer le montant facturable de la dépense. Min. valeur zéro, max. valeur Montant des dépenses. Valeur par défaut: zéro ou null. Aucun effet sur les dépenses non facturables.</help>
        <label><!-- Non-Billable Incurred Amount --></label>
        <name>pse__Non_Billable_Incurred_Amount__c</name>
    </fields>
    <fields>
        <help>Cette formule ajoute toute taxe d&apos;encaissement non facturable (uniquement si la taxe engagée non facturée est cochée) à tout montant incombant non facturable supplémentaire pour obtenir un sous-total engagé non facturable avec un minimum de 0,00 et un maximum du montant de la dépense.</help>
        <label><!-- Non-Billable Incurred Subtotal --></label>
        <name>pse__Non_Billable_Incurred_Subtotal__c</name>
    </fields>
    <fields>
        <help>Si cette case est cochée, le montant du remboursement pour la dépense sera nul quel que soit le montant engagé.</help>
        <label><!-- Non-Reimbursable --></label>
        <name>pse__Non_Reimbursible__c</name>
    </fields>
    <fields>
        <label><!-- Notes --></label>
        <name>pse__Notes__c</name>
    </fields>
    <fields>
        <label><!-- Override Group Currency Code --></label>
        <name>pse__Override_Group_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Remplace le groupe auquel les transactions de dépenses seront cumulées pour les comptes réels de groupe, même si le projet appartient à un groupe différent. En règle générale, les transactions de dépenses sont reportées sur le groupe de projets ou de ressources en fonction des règles «suivantes».</help>
        <label><!-- Override Group --></label>
        <name>pse__Override_Group__c</name>
        <relationshipLabel><!-- Override Group For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Practice Currency Code --></label>
        <name>pse__Override_Practice_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Remplace la pratique à laquelle les transactions de dépenses seront cumulées pour les données réelles, même si le projet est dans une pratique différente. En règle générale, les transactions d&apos;une dépense sont cumulées avec la pratique de son projet ou de sa ressource selon les règles suivantes.</help>
        <label><!-- Override Practice --></label>
        <name>pse__Override_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Rate (Billing Currency) --></label>
        <name>pse__Override_Rate_Billing_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Rate (Incurred Currency) --></label>
        <name>pse__Override_Rate_Incurred_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Rate (Reimbursement Currency) --></label>
        <name>pse__Override_Rate_Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Override Region Currency Code --></label>
        <name>pse__Override_Region_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Remplace la région à laquelle les transactions de dépenses seront cumulées pour les statistiques régionales, même si le projet se trouve dans une autre région. En règle générale, les transactions de dépenses s’appliquent à la région de son projet ou de sa ressource en fonction des règles suivantes.</help>
        <label><!-- Override Region --></label>
        <name>pse__Override_Region__c</name>
        <relationshipLabel><!-- Override Region For Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>Rechercher dans la méthodologie du projet</help>
        <label><!-- Project Methodology --></label>
        <name>pse__Project_Methodology__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <help>Recherche à la phase de projet</help>
        <label><!-- Project Phase --></label>
        <name>pse__Project_Phase__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Rate Unit --></label>
        <name>pse__Rate_Unit__c</name>
        <picklistValues>
            <masterLabel>Kilometer</masterLabel>
            <translation>Kilomètre</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Mile</masterLabel>
            <translation>Mile</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>La méthode de comptabilisation à appliquer à cet enregistrement pour la prévision des revenus.</help>
        <label><!-- Recognition Method --></label>
        <name>pse__Recognition_Method__c</name>
    </fields>
    <fields>
        <help>Ce champ stocke une valeur numérique pour le montant du remboursement converti dans la devise du projet (facturation) à l&apos;aide de taux système (non remplacés), pour une utilisation ultérieure dans la facturation fournisseur.</help>
        <label><!-- Reimbursement Amount In Project Currency --></label>
        <name>pse__Reimbursement_Amount_In_Project_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Reimbursement Amount --></label>
        <name>pse__Reimbursement_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Reimbursement Currency --></label>
        <name>pse__Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>pse__Resource__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Revenue Transaction --></label>
        <name>pse__Revenue_Transaction__c</name>
        <relationshipLabel><!-- Expense (Revenue Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Split Expense --></label>
        <name>pse__Split_Expense__c</name>
    </fields>
    <fields>
        <label><!-- Split Notes --></label>
        <name>pse__Split_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation>Approuvé</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation>Brouillon</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation>Rejeté</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Submitted</masterLabel>
            <translation>Soumis</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Submitted --></label>
        <name>pse__Submitted__c</name>
    </fields>
    <fields>
        <help>Cette case à cocher est destinée au système. Il est destiné à être contrôlé conjointement avec toute insertion / mise à jour du fractionnement de la carte de temps exécutée dans une méthode future, puis garantit qu&apos;aucune méthode future en aval n&apos;est appelée.</help>
        <label><!-- Synchronous Update Required --></label>
        <name>pse__Synchronous_Update_Required__c</name>
    </fields>
    <fields>
        <help>Un type de taxe facultatif qui a été inclus dans la dépense</help>
        <label><!-- Tax Type --></label>
        <name>pse__Tax_Type__c</name>
        <picklistValues>
            <masterLabel>GST</masterLabel>
            <translation>TPS</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VAT 1</masterLabel>
            <translation>TVA 1</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>VAT 2</masterLabel>
            <translation>TVA 2</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>L&apos;identifiant de dépense de l&apos;application tierce partie.</help>
        <label><!-- Third-Party Expenses App Expense ID --></label>
        <name>pse__Third_Party_Expenses_App_Expense_ID__c</name>
    </fields>
    <fields>
        <label><!-- Type --></label>
        <name>pse__Type__c</name>
        <picklistValues>
            <masterLabel>Airfare</masterLabel>
            <translation>Billet d&apos;avion</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Auto Mileage</masterLabel>
            <translation>Kilométrage automatique</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Business Meals</masterLabel>
            <translation>Repas d&apos;affaires</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Car Rental</masterLabel>
            <translation>Location de voiture</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Employee Relations</masterLabel>
            <translation>Relations avec les employés</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Gasoline</masterLabel>
            <translation>De l&apos;essence</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>General and Admin Expenses</masterLabel>
            <translation>Frais généraux et administratifs</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>IT Equipment and Services</masterLabel>
            <translation>Équipement et services informatiques</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Lodging (Room and Tax)</masterLabel>
            <translation>Hébergement (chambre et taxes)</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Miscellaneous</masterLabel>
            <translation>Divers</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Office Supplies and Services</masterLabel>
            <translation>Fournitures de bureau et services</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Personal Meals</masterLabel>
            <translation>Repas personnels</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Phone</masterLabel>
            <translation>Téléphone</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Postage and Shipping</masterLabel>
            <translation>Affranchissement et expédition</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Recruiting</masterLabel>
            <translation>Recrutement</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Subway</masterLabel>
            <translation>Métro</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Taxi</masterLabel>
            <translation>Taxi</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Vendor Invoice Item --></label>
        <name>pse__Vendor_Invoice_Item__c</name>
        <relationshipLabel><!-- Expenses --></relationshipLabel>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Numéro de dépense</nameFieldLabel>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <errorMessage><!-- Assignment Project must match Expense Project. --></errorMessage>
        <name>pse__Expense_Asgnmt_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Assignment Resource must match Expense Resource. --></errorMessage>
        <name>pse__Expense_Asgnmt_Resource_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Line Project must match Expense Report Project. --></errorMessage>
        <name>pse__Expense_ER_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Line Resource must match Expense Report Resource. --></errorMessage>
        <name>pse__Expense_ER_Resource_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For an Expense Line to be marked as Billed, it must also be marked as Included in Financials and Approved. --></errorMessage>
        <name>pse__Expense_Line_Billing_Requires_Inclusion</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For an Expense Line to be marked as Invoiced, it must also be marked as Billed. --></errorMessage>
        <name>pse__Expense_Line_Invoicing_Requires_Billing</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- An Expense Line may not be submitted or included in financials unless it belongs to an Expense Report. --></errorMessage>
        <name>pse__Expense_Line_Needs_Parent_For_Submit</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Project field value may not be updated once set (unless parent Expense Report is changed, Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Line_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Expense Report may not be changed if it is submitted or included in financials. --></errorMessage>
        <name>pse__Expense_Line_Report_Fixed_If_Submitted</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Line&apos;s Resource field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Line_Resource_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- An Expense may only be marked as Billable if its Project is Billable and its Expense Report and Assignment, if any, are Billable. --></errorMessage>
        <name>pse__Expense_May_Not_Be_Billable</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Milestone Project must match Expense Project. --></errorMessage>
        <name>pse__Expense_Milestone_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Report is required --></errorMessage>
        <name>pse__Expense_Requires_Expense_Report</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If provided, the incurred tax amount for an Expense must not be less than zero or greater than the Expense amount itself. --></errorMessage>
        <name>pse__Incurred_Tax_Amount_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Invoice Number or Date is not allowed on record unless it is invoiced --></errorMessage>
        <name>pse__Invoiced</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If provided, the non-billable incurred amount for an Expense must not be less than zero or greater than the Expense amount itself. --></errorMessage>
        <name>pse__Non_Billable_Incurred_Amount_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The non-billable incurred subtotal for an Expense (any non-billable amount plus any non-billable tax) must not be greater than the Expense amount itself. --></errorMessage>
        <name>pse__Non_Billable_Subtotal_Invalid</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Resource-defined Exchange Rate can only be set if Incurred and Resource currencies are different.  If the currencies are the same, a relative rate of 1.0 is implied. --></errorMessage>
        <name>pse__Res_Defined_Rate_Diff_Currencies_Only</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Resource-defined Exchange Rate must either be greater than 0.0 (zero) or null/empty. --></errorMessage>
        <name>pse__Res_Defined_Rate_may_not_be_Negative</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The vendor invoice cannot change once set. --></errorMessage>
        <name>pse__Vendor_Invoice_Constant</name>
    </validationRules>
    <webLinks>
        <label><!-- Admin_Edit --></label>
        <name>pse__Admin_Edit</name>
    </webLinks>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
    <webLinks>
        <label><!-- Edit_Expenses --></label>
        <name>pse__Edit_Expenses</name>
    </webLinks>
    <webLinks>
        <label><!-- Multiple_Expense_Entry_UI --></label>
        <name>pse__Multiple_Expense_Entry_UI</name>
    </webLinks>
    <webLinks>
        <label><!-- Ready_to_Submit --></label>
        <name>pse__Ready_to_Submit</name>
    </webLinks>
</CustomObjectTranslation>
