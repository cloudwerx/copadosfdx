<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Impostazioni</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Impostazioni</value>
    </caseValues>
    <fields>
        <help>Nome API del campo sull’oggetto di origine che contiene informazioni da visualizzare su un record. Spesso questo sarà l’account Salesforce standard, ma può essere qualsiasi campo di testo, elenco di selezione, ricerca, numero, numero automatico o formula.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>Nome API del campo sull&apos;oggetto di origine che indica se è attivo. Per includere record in Revenue Management quando un campo ha un valore specifico, impostare il nome del campo in Campo attivo, il valore in Valore attivo e impostare Includi valore attivo su Vero.</help>
        <label><!-- Active Field --></label>
        <name>ffrr__ActiveField__c</name>
    </fields>
    <fields>
        <help>Valore sul campo attivo che indica se il record sorgente è attivo.</help>
        <label><!-- Active Value --></label>
        <name>ffrr__ActiveValue__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di ricerca (all&apos;oggetto di origine di questa impostazione) che l&apos;amministratore ha aggiunto all&apos;oggetto Linea Actuals vs Forecast. Questo campo è obbligatorio quando è abilitata l&apos;opzione Utilizza la funzione effettiva vs previsione.</help>
        <label><!-- Actual vs Forecast Line Lookup --></label>
        <name>ffrr__ActualVsForecastRelationship__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero o Valuta sull&apos;oggetto di origine che memorizza l&apos;ammontare del costo ammortizzato fino ad oggi. Se popolato, il valore sul record di origine viene automaticamente aggiornato ogni volta che viene eseguita una transazione di Riconoscimento delle entrate.</help>
        <label><!-- Amortized To Date Value --></label>
        <name>ffrr__AmortizedToDateValue__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, della ricerca o dell’elenco di selezione che rappresenta il valore del report 1. Questo può essere un campo sull’oggetto di origine oppure, utilizzando le relazioni di ricerca, è possibile fare riferimento a un campo su un oggetto collegato da un percorso di massimo cinque ricerche.</help>
        <label><!-- Analysis Item 1 --></label>
        <name>ffrr__AnalysisItem1__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione che rappresenta il valore di report 2. Questo può essere un campo sull’oggetto di origine oppure, utilizzando le relazioni di ricerca, è possibile fare riferimento a un campo su un oggetto collegato da un percorso di massimo cinque ricerche.</help>
        <label><!-- Analysis Item 2 --></label>
        <name>ffrr__AnalysisItem2__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione che rappresenta il valore di report 3. Questo può essere un campo sull’oggetto di origine oppure, utilizzando le relazioni di ricerca, è possibile fare riferimento a un campo su un oggetto collegato da un percorso di un massimo di cinque ricerche.</help>
        <label><!-- Analysis Item 3 --></label>
        <name>ffrr__AnalysisItem3__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione che rappresenta il valore di report 4. Questo può essere un campo sull’oggetto di origine oppure, utilizzando le relazioni di ricerca, è possibile fare riferimento a un campo su un oggetto collegato da un percorso di massimo cinque ricerche.</help>
        <label><!-- Analysis Item 4 --></label>
        <name>ffrr__AnalysisItem4__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione sull’oggetto di origine che rappresenta il GLA di bilancio in cui sono registrate le registrazioni delle entrate.</help>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Billed To Date (Reserved) --></label>
        <name>ffrr__BilledToDate__c</name>
    </fields>
    <fields>
        <label><!-- Chain ID --></label>
        <name>ffrr__ChainID__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione che rappresenta la società. Può essere un campo sull’oggetto di origine oppure, utilizzando le relazioni di ricerca, è possibile fare riferimento a un campo su un oggetto collegato da un percorso di massimo cinque ricerche.</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
    </fields>
    <fields>
        <help>Nome API del campo sull&apos;oggetto di origine che indica il suo stato di completamento. Se per questa impostazione viene creato un modello di tipo Deliverable, le entrate verranno riconosciute al completamento del record di origine.</help>
        <label><!-- Completed Field --></label>
        <name>ffrr__CompletedField__c</name>
    </fields>
    <fields>
        <help>Valore sul campo attivo che indica se il record di origine è completo. Se per questa impostazione viene creato un modello di tipo Deliverable, le entrate verranno riconosciute al completamento del record di origine.</help>
        <label><!-- Completed Value --></label>
        <name>ffrr__CompletedValue__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione sull’oggetto di origine che rappresenta il GLA di bilancio in cui vengono registrate le registrazioni dei costi.</help>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione sull&apos;oggetto di origine che rappresenta il componente specifico del tuo piano dei conti con cui i tuoi account di gestione sono analizzati in modo sub-analogo.</help>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione sull&apos;oggetto di origine che rappresenta il componente specifico del tuo piano dei conti con cui i tuoi account di gestione sono analizzati in modo sub-analogo.</help>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione sull’oggetto di origine che rappresenta il GLA del conto economico in cui sono registrate le registrazioni dei costi.</help>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>Nome API del campo sull&apos;oggetto di origine che rappresenta la velocità di addebito delle unità di costo.</help>
        <label><!-- Cost Rate --></label>
        <name>ffrr__CostRate__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero sull&apos;oggetto di origine che rappresenta il numero totale di unità di costo addebitate. Il valore delle unità di costo potrebbe essere il numero totale di ore o giorni lavorati su un progetto, ad esempio.</help>
        <label><!-- Total Cost Units --></label>
        <name>ffrr__CostTotalUnits__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Testo, Ricerca o Elenco di prelievo sull&apos;oggetto di origine che rappresenta la valuta del record di origine.</help>
        <label><!-- Document Currency --></label>
        <name>ffrr__Currency__c</name>
    </fields>
    <fields>
        <help>La definizione di mappatura dei campi popolata in base agli elementi pubblicitari con obbligo di rendimento per impostazione predefinita quando li si collega a un record sorgente e a queste impostazioni.</help>
        <label><!-- Default Field Mapping Definition --></label>
        <name>ffrr__DefaultFieldMappingDefinition__c</name>
        <relationshipLabel><!-- Settings --></relationshipLabel>
    </fields>
    <fields>
        <help>Nome API del campo Numero sull’oggetto di origine che memorizza l’importo delle entrate differite fino alla data, in doppia valuta.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Dual) --></label>
        <name>ffrr__DeferredRevenueToDateDualCurrency__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero sull’oggetto di origine che memorizza l’importo delle entrate differite fino alla data, nella valuta locale.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Home) --></label>
        <name>ffrr__DeferredRevenueToDateHomeCurrency__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero sull’oggetto di origine che memorizza l’importo delle entrate differite fino alla data, nella valuta dei rapporti.</help>
        <label><!-- FOR FUTURE USE Deferred Rev (Reporting) --></label>
        <name>ffrr__DeferredRevenueToDateReportingCurrency__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero sull’oggetto di origine che memorizza l’importo delle entrate differite fino alla data, nella valuta del documento.</help>
        <label><!-- FOR FUTURE USE Deferred Rev --></label>
        <name>ffrr__DeferredRevenueToDate__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Testo o Area di testo lungo sull&apos;oggetto di origine che contiene una descrizione.</help>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Double o Currency sull&apos;oggetto di origine che rappresenta il documento rispetto al tasso di cambio della valuta locale.</help>
        <label><!-- Document Rate --></label>
        <name>ffrr__DocumentCurrencyRate__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Doppio o Valuta sull&apos;oggetto di origine che rappresenta la casa della società di contabilità al tasso di cambio della doppia valuta.</help>
        <label><!-- Dual Rate --></label>
        <name>ffrr__DualCurrencyRate__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Testo, Ricerca o Elenco di prelievo sull&apos;oggetto di origine che rappresenta la doppia valuta della società di contabilità. La doppia valuta è in genere una valuta aziendale utilizzata a fini di reportistica.</help>
        <label><!-- Dual Currency --></label>
        <name>ffrr__DualCurrency__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Data o DateTime sull&apos;oggetto di origine che rappresenta la sua data di fine.</help>
        <label><!-- End Date/Deliverable Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 1 --></label>
        <name>ffrr__Filter1__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 2 --></label>
        <name>ffrr__Filter2__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Filter 3 --></label>
        <name>ffrr__Filter3__c</name>
    </fields>
    <fields>
        <help>Selezionare per indicare che il valore corrispondente è un codice fisso, non un nome API sull&apos;oggetto di origine correlato. Queste caselle di controllo possono essere impostate indipendentemente l&apos;una dall&apos;altra.</help>
        <label><!-- Fixed Balance Sheet --></label>
        <name>ffrr__FixedBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Selezionare per indicare che il valore corrispondente è un codice fisso, non un nome API sull&apos;oggetto di origine correlato. Queste caselle di controllo possono essere impostate indipendentemente l&apos;una dall&apos;altra.</help>
        <label><!-- Fixed Balance Sheet (Cost) --></label>
        <name>ffrr__FixedCostBalanceSheetAccountCode__c</name>
    </fields>
    <fields>
        <help>Selezionare per indicare che il valore corrispondente è un codice fisso, non un nome API sull&apos;oggetto di origine correlato. Queste caselle di controllo possono essere impostate indipendentemente l&apos;una dall&apos;altra.</help>
        <label><!-- Fixed Cost Center --></label>
        <name>ffrr__FixedCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Selezionare per indicare che il valore corrispondente è un codice fisso, non un nome API sull&apos;oggetto di origine correlato. Queste caselle di controllo possono essere impostate indipendentemente l&apos;una dall&apos;altra.</help>
        <label><!-- Fixed Cost Center (Cost) --></label>
        <name>ffrr__FixedCostCostCenterCode__c</name>
    </fields>
    <fields>
        <help>Selezionare per indicare che il valore corrispondente è un codice fisso, non un nome API sull&apos;oggetto di origine correlato. Queste caselle di controllo possono essere impostate indipendentemente l&apos;una dall&apos;altra.</help>
        <label><!-- Fixed Income Statement (Cost) --></label>
        <name>ffrr__FixedCostIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Selezionare per indicare che il valore corrispondente è un codice fisso, non un nome API sull&apos;oggetto di origine correlato. Queste caselle di controllo possono essere impostate indipendentemente l&apos;una dall&apos;altra.</help>
        <label><!-- Fixed Income Statement --></label>
        <name>ffrr__FixedIncomeStatementAccountCode__c</name>
    </fields>
    <fields>
        <help>Selezionare per indicare che il valore corrispondente è un codice fisso, non un nome API sull’oggetto di origine correlato. Queste caselle di controllo possono essere impostate indipendentemente l’una dall’altra.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Dual) --></label>
        <name>ffrr__FixedTrueUpDualAccountCode__c</name>
    </fields>
    <fields>
        <help>Selezionare per indicare che il valore corrispondente è un codice fisso, non un nome API sull’oggetto di origine correlato. Queste caselle di controllo possono essere impostate indipendentemente l’una dall’altra.</help>
        <label><!-- FOR FUTURE USE Fixed True-up (Home) --></label>
        <name>ffrr__FixedTrueUpHomeAccountCode__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di ricerca (all&apos;oggetto di origine a questo livello di impostazioni) che l&apos;amministratore ha aggiunto all&apos;oggetto Transaction Forecast Transaction. Questo deve essere compilato se il tipo di impostazioni è Previsione e il livello delle impostazioni è Primario.</help>
        <label><!-- Revenue Forecast Transaction Lookup --></label>
        <name>ffrr__ForecastHeaderPrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di ricerca (per l&apos;oggetto di origine a questo livello di impostazioni) che l&apos;amministratore ha aggiunto all&apos;oggetto Linea di transazione previsione ricavi. Questo deve essere compilato se il tipo di impostazioni è Previsione.</help>
        <label><!-- Revenue Forecast Transaction Line Lookup --></label>
        <name>ffrr__ForecastTransactionLineRelationship__c</name>
    </fields>
    <fields>
        <label><!-- Group Name --></label>
        <name>ffrr__GroupName__c</name>
    </fields>
    <fields>
        <label><!-- Group --></label>
        <name>ffrr__Group__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione sull&apos;&apos;oggetto di origine che rappresenta il componente specifico che si desidera raggruppare per il processo Riconosci tutto.</help>
        <label><!-- Grouped By --></label>
        <name>ffrr__GroupedBy__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Testo, Ricerca o Picklist sull&apos;oggetto di origine che rappresenta la valuta locale della società di contabilità. La valuta locale è la principale attività dell&apos;azienda, e la segnalazione, valuta.</help>
        <label><!-- Home Currency --></label>
        <name>ffrr__HomeCurrency__c</name>
    </fields>
    <fields>
        <help>Includi o Escludi record di origine con il valore definito nel campo Valore attivo.</help>
        <label><!-- Include Active Value --></label>
        <name>ffrr__IncludeActiveValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Escludere</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Includere</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Includi o Escludi record di origine con il valore definito nel campo Valore completato.</help>
        <label><!-- Include Completed Value --></label>
        <name>ffrr__IncludeCompletedValue__c</name>
        <picklistValues>
            <masterLabel>Exclude</masterLabel>
            <translation>Escludere</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Include</masterLabel>
            <translation>Includere</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione sull’oggetto di origine che rappresenta il GLA del conto economico in cui sono registrate le registrazioni delle entrate.</help>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>Nome API dell&apos;oggetto che funge da origine dati a questo livello. Questo non può cambiare una volta che l&apos;impostazione è stata utilizzata su un modello. Per le impostazioni non primarie, l&apos;oggetto deve trovarsi in una relazione principale o di ricerca con l&apos;oggetto sulla sua impostazione genitore.</help>
        <label><!-- Object --></label>
        <name>ffrr__Object__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di ricerca (all&apos;oggetto di origine di questa impostazione) che l&apos;amministratore ha aggiunto all&apos;oggetto Elemento pubblicitario di Performance Obligation.</help>
        <label><!-- POLI Source Lookup --></label>
        <name>ffrr__POLISourceField__c</name>
    </fields>
    <fields>
        <help>Nomi API dei campi di ricerca sull’oggetto di origine descritti da questo record di impostazioni o collegati all’oggetto di origine da un percorso di relazione fino a 5 ricerche, i cui valori popoleranno le Ricerche linea pianificazione entrate. Quando si specificano più campi, questi devono essere separati da virgole e nello stesso ordine dei campi nelle ricerche riga pianificazione ricavi.</help>
        <label><!-- Parent Relationship Paths --></label>
        <name>ffrr__ParentRelationshipPaths__c</name>
    </fields>
    <fields>
        <help>Seleziona il record delle impostazioni genitore (il record delle impostazioni per l&apos;oggetto sorgente di un livello sopra questo nella gerarchia).</help>
        <label><!-- Parent Settings --></label>
        <name>ffrr__ParentSettings__c</name>
        <relationshipLabel><!-- ParentSettings --></relationshipLabel>
    </fields>
    <fields>
        <help>Nome dell&apos;API del campo Numero o percentuale sull&apos;oggetto di origine che rappresenta quanto è completo il record sorgente in termini percentuali.</help>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentageComplete__c</name>
    </fields>
    <fields>
        <help>Immettere il nome dell&apos;API del campo di ricerca sull&apos;oggetto di origine dell&apos;impostazione principale.</help>
        <label><!-- Parent Lookup --></label>
        <name>ffrr__PrimaryRelationship__c</name>
    </fields>
    <fields>
        <help>Nome API della ricerca che rappresenta il prodotto. Può essere una ricerca sull’oggetto di origine oppure, utilizzando le relazioni di ricerca, è possibile fare riferimento a una ricerca su un oggetto collegato da un percorso di massimo cinque ricerche.</help>
        <label><!-- Product --></label>
        <name>ffrr__Product__c</name>
    </fields>
    <fields>
        <help>Nome API del campo sull&apos;oggetto di origine che rappresenta la velocità di addebito delle unità.</help>
        <label><!-- Rate --></label>
        <name>ffrr__Rate__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero sull’oggetto di origine che memorizza l’importo dei ricavi riconosciuti fino ad oggi in doppia valuta. Se compilato, il valore sul record di origine viene aggiornato automaticamente ogni volta che viene eseguita una transazione di riconoscimento dei ricavi.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Dual) --></label>
        <name>ffrr__RecognizedToDateValueDual__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero sull’oggetto di origine che memorizza l’importo dei ricavi riconosciuti fino ad oggi nella valuta locale. Se compilato, il valore sul record di origine viene aggiornato automaticamente ogni volta che viene eseguita una transazione di riconoscimento dei ricavi.</help>
        <label><!-- FOR FUTURE USE Rec To Date Value (Home) --></label>
        <name>ffrr__RecognizedToDateValueHome__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero o Valuta sull&apos;oggetto di origine che memorizza l&apos;ammontare delle entrate riconosciute fino ad oggi. Se popolato, il valore sul record di origine viene automaticamente aggiornato ogni volta che viene eseguita una transazione di Riconoscimento delle entrate.</help>
        <label><!-- Recognized To Date Value --></label>
        <name>ffrr__RecognizedToDateValue__c</name>
    </fields>
    <fields>
        <help>Nome API del campo booleano sull&apos;oggetto di origine che indica che tutte le entrate sono state completamente riconosciute e tutti i costi sono stati completamente ammortizzati. Se popolata, la casella di controllo del record di origine viene abilitata automaticamente quando le entrate sono completamente riconosciute e ammortizzate completamente.</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionCompleted__c</name>
    </fields>
    <fields>
        <help>Nomi API dei campi di ricerca (all’oggetto di origine descritto da questo record di impostazioni o da oggetti correlati) che l’amministratore ha aggiunto all’oggetto Riga pianificazione ricavi. Questi campi vengono popolati dalle ricerche identificate nei percorsi di relazione padre. Quando si specificano più campi, devono essere separati da virgole e nello stesso ordine dei campi nei percorsi delle relazioni padre.</help>
        <label><!-- Revenue Schedule Line Lookups --></label>
        <name>ffrr__RevenueScheduleLineLookups__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di ricerca (all’oggetto di origine a questo livello di impostazioni) che l’amministratore ha aggiunto all’oggetto Pianificazione entrate.</help>
        <label><!-- Revenue Schedule Lookup --></label>
        <name>ffrr__RevenueScheduleSourceLookup__c</name>
    </fields>
    <fields>
        <help>Nome API del campo sull&apos;oggetto di origine che memorizza il prezzo di vendita standalone per un elemento pubblicitario con obbligo di rendimento.</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <help>Nome API della ricerca che rappresenta l’account Salesforce. Può essere una ricerca sull’oggetto di origine oppure, utilizzando le relazioni di ricerca, è possibile fare riferimento a una ricerca su un oggetto collegato da un percorso di massimo cinque ricerche.</help>
        <label><!-- Account --></label>
        <name>ffrr__SalesforceAccount__c</name>
    </fields>
    <fields>
        <label><!-- Deprecated Setting Uniqueness --></label>
        <name>ffrr__SettingUniqueness__c</name>
    </fields>
    <fields>
        <help>Seleziona il livello di questo record di impostazioni dell&apos;oggetto di origine.</help>
        <label><!-- Settings Level --></label>
        <name>ffrr__SettingsLevel__c</name>
        <picklistValues>
            <masterLabel>Level 2</masterLabel>
            <translation>Livello 2</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 3</masterLabel>
            <translation>Livello 3</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Level 4</masterLabel>
            <translation>Livello 4</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Primary</masterLabel>
            <translation>Primario</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Il tipo di processo utilizzato da questo record di impostazioni per: Effettivo, Previsione o entrambi. Se i mapping sono identici sia per i processi effettivi che per quelli di previsione, creare un record delle impostazioni da utilizzare da entrambi.</help>
        <label><!-- Settings Type --></label>
        <name>ffrr__SettingsType__c</name>
        <picklistValues>
            <masterLabel>Actual</masterLabel>
            <translation>Effettivo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Forecast</masterLabel>
            <translation>Previsione</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Nome API del campo Data o DateTime sull&apos;oggetto di origine che rappresenta la data di inizio.</help>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero o Valuta sull&apos;oggetto di origine che rappresenta il costo totale.</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero o Valuta sull&apos;oggetto di origine che rappresenta le entrate totali.</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero sull&apos;oggetto di origine che rappresenta il numero totale di unità caricate. Il valore totale delle unità potrebbe essere il numero di ore o giorni lavorati su un progetto, ad esempio.</help>
        <label><!-- Total Units --></label>
        <name>ffrr__TotalUnits__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di ricerca (all&apos;oggetto di origine a questo livello di impostazioni) che l&apos;amministratore ha aggiunto all&apos;oggetto Linea di transazione Riconoscimento dei ricavi. Questo deve essere compilato se il tipo di impostazioni è reale.</help>
        <label><!-- Revenue Recognition Txn Line Lookup --></label>
        <name>ffrr__TransactionLineRelationship__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione sull’oggetto di origine che rappresenta il GLA da utilizzare per i valori di true-up in doppia valuta.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Dual) --></label>
        <name>ffrr__TrueUpDualAccount__c</name>
    </fields>
    <fields>
        <help>Nome API del campo di testo, ricerca o elenco di selezione sull’oggetto di origine che rappresenta il GLA da utilizzare per i valori di allineamento nella valuta locale.</help>
        <label><!-- FOR FUTURE USE True-Up GLA (Home) --></label>
        <name>ffrr__TrueUpHomeAccount__c</name>
    </fields>
    <fields>
        <help>Questa impostazione è utilizzata nei contratti di entrate.</help>
        <label><!-- Use in Revenue Contract --></label>
        <name>ffrr__UseInRevenueContract__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero o percentuale sull&apos;oggetto di origine che rappresenta la percentuale VSOE (Evidenza obiettiva specifica del fornitore).</help>
        <label><!-- VSOE % --></label>
        <name>ffrr__VSOEPercent__c</name>
    </fields>
    <fields>
        <help>Nome API del campo Numero o Valuta sull&apos;oggetto sorgente che rappresenta il tasso VSOE (Vendic Specical Objective Evidence).</help>
        <label><!-- VSOE Rate --></label>
        <name>ffrr__VSOERate__c</name>
    </fields>
    <fields>
        <help>Il tipo di elaborazione di questo record di impostazioni verrà utilizzato per: Entrate, Costo o entrambi.</help>
        <label><!-- Value Type --></label>
        <name>ffrr__ValueType__c</name>
        <picklistValues>
            <masterLabel>Cost</masterLabel>
            <translation>Costo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Revenue</masterLabel>
            <translation>Reddito</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nome impostazioni</nameFieldLabel>
    <startsWith>Vowel</startsWith>
    <webLinks>
        <label><!-- CreateMappings --></label>
        <name>ffrr__CreateMappings</name>
    </webLinks>
    <webLinks>
        <label><!-- CreateMappingsList --></label>
        <name>ffrr__CreateMappingsList</name>
    </webLinks>
    <webLinks>
        <label><!-- DeleteSettings --></label>
        <name>ffrr__DeleteSettings</name>
    </webLinks>
</CustomObjectTranslation>
