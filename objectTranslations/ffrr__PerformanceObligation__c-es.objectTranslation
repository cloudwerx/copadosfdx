<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Obligación de rendimiento</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Obligaciones de rendimiento</value>
    </caseValues>
    <fields>
        <help>Información sobre la obligación de desempeño. A menudo, este será el nombre de la cuenta, pero puede ser cualquier información.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>Indica si el registro fuente está activo.</help>
        <label><!-- Active --></label>
        <name>ffrr__Active__c</name>
    </fields>
    <fields>
        <help>Ajuste debido a una diferencia entre los ingresos y los ingresos asignados en el Contrato de ingresos causados ​​por errores acumulativos de redondeo en las Obligaciones de rendimiento.</help>
        <label><!-- Allocated Revenue Rounding Adjustment --></label>
        <name>ffrr__AllocatedRevenueAdjustment__c</name>
    </fields>
    <fields>
        <help>Anule la cantidad calculada asignada a esta Obligación de rendimiento ingresando un valor diferente aquí.</help>
        <label><!-- Allocated Revenue Override --></label>
        <name>ffrr__AllocatedRevenueOverride__c</name>
    </fields>
    <fields>
        <help>El monto asignado a esta Obligación de Desempeño por el cálculo de Asignación de Ingresos. Este valor se usa para el reconocimiento de ingresos.</help>
        <label><!-- Allocated Revenue --></label>
        <name>ffrr__AllocatedRevenue__c</name>
    </fields>
    <fields>
        <help>La proporción utilizada para asignar ingresos a través del Contrato de Ingresos.</help>
        <label><!-- Allocation Ratio --></label>
        <name>ffrr__AllocationRatio__c</name>
    </fields>
    <fields>
        <help>Muestra si los Ingresos para esta obligación de rendimiento se han asignado por completo o si es necesario reasignarlos.</help>
        <label><!-- Allocation Status --></label>
        <name>ffrr__AllocationStatus__c</name>
    </fields>
    <fields>
        <label><!-- Amortized to Date --></label>
        <name>ffrr__AmortizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>Indica si el registro fuente está completo.</help>
        <label><!-- Completed --></label>
        <name>ffrr__Completed__c</name>
    </fields>
    <fields>
        <help>Elemento de la línea de obligación de rendimiento de costes de control. Se usa para rellenar los campos en la Obligación de rendimiento.</help>
        <label><!-- Controlling POLI (Cost) --></label>
        <name>ffrr__ControllingCostPOLI__c</name>
        <relationshipLabel><!-- Controlling POLIs (Cost) --></relationshipLabel>
    </fields>
    <fields>
        <help>Elemento de la línea de obligación de rendimiento de los ingresos de control. Se usa para rellenar los campos en la Obligación de rendimiento.</help>
        <label><!-- Controlling POLI (Revenue) --></label>
        <name>ffrr__ControllingPOLI__c</name>
        <relationshipLabel><!-- Controlling POLIs (Revenue) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>Costo de esta obligación de desempeño</help>
        <label><!-- Cost --></label>
        <name>ffrr__Cost__c</name>
    </fields>
    <fields>
        <help>Número de posiciones decimales en la moneda del registro.</help>
        <label><!-- Currency Decimal Places --></label>
        <name>ffrr__CurrencyDP__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <label><!-- End Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <help>Indica que esta Obligación de desempeño se relaciona con los ingresos.</help>
        <label><!-- Has Revenue --></label>
        <name>ffrr__HasRevenue__c</name>
    </fields>
    <fields>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>El número de elementos de línea de obligación de rendimiento relacionados con los ingresos que tienen un valor de SSP nulo.</help>
        <label><!-- Null SSP Count --></label>
        <name>ffrr__NullSSPCount__c</name>
    </fields>
    <fields>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentComplete__c</name>
    </fields>
    <fields>
        <help>Esto se verifica si los Ingresos han sido asignados y el Registro de origen está activo. Si no se selecciona, este registro no se puede incluir en Reconocimiento de ingresos.</help>
        <label><!-- Ready for Revenue Recognition --></label>
        <name>ffrr__ReadyForRevenueRecognition__c</name>
    </fields>
    <fields>
        <label><!-- Recognized to Date --></label>
        <name>ffrr__RecognizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Contract --></label>
        <name>ffrr__RevenueContract__c</name>
        <relationshipLabel><!-- Performance Obligations --></relationshipLabel>
    </fields>
    <fields>
        <help>El número de elementos de línea de obligación de rendimiento que se relacionan con los ingresos.</help>
        <label><!-- Revenue Count --></label>
        <name>ffrr__RevenueCount__c</name>
    </fields>
    <fields>
        <help>Cuando se establece, indica que todos los ingresos de esta Obligación de rendimiento se han reconocido completamente y todos los costos están totalmente amortizados.</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionComplete__c</name>
    </fields>
    <fields>
        <help>Ingresos por esta obligación de desempeño</help>
        <label><!-- Revenue --></label>
        <name>ffrr__Revenue__c</name>
    </fields>
    <fields>
        <help>Anule el precio de venta independiente del registro fuente ingresando un valor diferente aquí.</help>
        <label><!-- SSP Override --></label>
        <name>ffrr__SSPOverride__c</name>
    </fields>
    <fields>
        <help>El precio de venta independiente que se utilizará para esta obligación de rendimiento. Utiliza el SSP total o la anulación de SSP si se rellena.</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>Suma de precios de venta independientes de todos los artículos de línea de obligación de rendimiento vinculados.</help>
        <label><!-- Total SSP --></label>
        <name>ffrr__TotalSSP__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__ffrrTemplate__c</name>
        <relationshipLabel><!-- Performance Obligations --></relationshipLabel>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Número de obligación de rendimiento</nameFieldLabel>
    <webLinks>
        <label><!-- AllocateRevenue --></label>
        <name>ffrr__AllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- Update --></label>
        <name>ffrr__Update</name>
    </webLinks>
</CustomObjectTranslation>
