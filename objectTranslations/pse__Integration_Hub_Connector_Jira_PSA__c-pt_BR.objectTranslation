<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value><!-- Conector do hub de integração: Jira - PSA --></value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value><!-- Conector do hub de integração: Jira - PSA --></value>
    </caseValues>
    <fields>
        <help>Endereço de e-mail para receber notificações de erro se a integração com Jira falhar.</help>
        <label><!-- Email --></label>
        <name>pse__Email__c</name>
    </fields>
    <fields>
        <help>O URL para o aplicativo Jira. Usado para autenticação.</help>
        <label><!-- Jira Application URL --></label>
        <name>pse__Jira_Application_URL__c</name>
    </fields>
    <fields>
        <help>Quando o campo Mapear Projetos PSA para Problemas Jira é selecionado, determina a categoria de status de problemas Jira que atualizam o campo do projeto mapeado com o valor definido em Valor do Campo PSA para Projetos Atualizados.</help>
        <label><!-- Jira Issue Status Category --></label>
        <name>pse__Jira_Issue_Status_Category__c</name>
    </fields>
    <fields>
        <help>Quando o campo Ativar Sincronização de Projetos com Problemas Jira é selecionado, determina o tipo de problemas Jira com os quais os projetos PSA são sincronizados. Este valor faz distinção entre maiúsculas e minúsculas e o tipo de problema deve existir em seu aplicativo Jira.</help>
        <label><!-- Jira Issue Type --></label>
        <name>pse__Jira_Issue_Type__c</name>
    </fields>
    <fields>
        <help>Indica se os recursos PSA sincronizados com Jira recebem uma notificação por email com seu nome de usuário Jira e um link para definir sua senha.</help>
        <label><!-- Jira Welcome Email for Synced Resources --></label>
        <name>pse__Jira_Welcome_Email_for_Synced_Resources__c</name>
    </fields>
    <fields>
        <help>Quando selecionados, os projetos PSA são mapeados para problemas Jira, em vez de projetos Jira. O tipo de problema Jira é determinado pelo valor do campo Tipo de problema Jira.</help>
        <label><!-- Map PSA Projects to Jira Issues --></label>
        <name>pse__Map_PSA_Projects_to_Jira_Issues__c</name>
    </fields>
    <fields>
        <help>Número máximo de dias do cartão de ponto, começando com segunda-feira, o tempo pode ser registrado em cada semana para um recurso. Você pode inserir um número inteiro entre 1 e 7. Por exemplo, insira 5 para habilitar o registro de horas de segunda a sexta-feira.</help>
        <label><!-- Max Resource Days Per Week --></label>
        <name>pse__Max_Resource_Days_Per_Week__c</name>
    </fields>
    <fields>
        <help>Número máximo de horas de cartão de ponto que podem ser registradas a cada dia para um recurso. Você pode inserir um valor entre 1 e 24.</help>
        <label><!-- Max Resource Hours Per Day --></label>
        <name>pse__Max_Resource_Hours_Per_Day__c</name>
    </fields>
    <fields>
        <help>Quando o campo Mapear Projetos PSA para Problemas Jira é selecionado, determina o valor com o qual o campo do projeto mapeado é atualizado quando um problema Jira tem o status definido na Categoria de Status de Problema Jira. Este valor diferencia maiúsculas de minúsculas.</help>
        <label><!-- PSA Field Value for Updating Projects --></label>
        <name>pse__PSA_Field_Value_for_Updating_Projects__c</name>
    </fields>
    <fields>
        <help>Nome do campo personalizado Jira usado para vincular um problema a um tipo de problema pai, por exemplo, Link pai. O relacionamento pode ter vários níveis, mas o tipo de problema definido em Jira Issue Type é o mais alto considerado.</help>
        <label><!-- Parent Issue Field --></label>
        <name>pse__Parent_Issue_Field__c</name>
    </fields>
    <fields>
        <help>Determina o tipo de problemas do Jira que são elegíveis para sincronização com as tarefas do projeto PSA. Você pode separar vários tipos de problemas com vírgulas. Os tipos de problema devem existir em seu aplicativo Jira.</help>
        <label><!-- Project Task Jira Issue Types --></label>
        <name>pse__Project_Task_Jira_Issue_Types__c</name>
    </fields>
    <fields>
        <help>Define o tamanho do lote usado ao tentar sincronizar as tarefas do projeto PSA com os problemas do Jira. O valor padrão é 200.</help>
        <label><!-- Retry Project Task Sync Batch Size --></label>
        <name>pse__Retry_Project_Task_Sync_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Indica se os problemas do Jira devem ser sincronizados com as tarefas do projeto PSA. É preenchido automaticamente quando os problemas de sincronização de Jira para tarefas de projeto no fluxo de trabalho PSA são ativados usando o Console de recursos.</help>
        <label><!-- Sync Jira Issues to PSA Project Tasks --></label>
        <name>pse__Sync_Jira_Issues_to_PSA_Project_Tasks__c</name>
    </fields>
    <fields>
        <help>Indica se os logs de trabalho do Jira devem ser sincronizados com cartões de ponto PSA. É preenchido automaticamente quando os Logs de trabalho de sincronização de Jira para cartões de ponto no fluxo de trabalho PSA são ativados usando o console de recursos.</help>
        <label><!-- Sync Jira Work Logs to PSA Timecards --></label>
        <name>pse__Sync_Jira_Work_Logs_to_PSA_Timecards__c</name>
    </fields>
    <fields>
        <help>Indica se as tarefas do projeto PSA devem ser sincronizadas com os problemas do Jira. É preenchido automaticamente quando o fluxo de trabalho Sincronizar tarefas do projeto PSA com problemas do Jira é habilitado usando o console de recursos.</help>
        <label><!-- Sync PSA Project Tasks to Jira Issues --></label>
        <name>pse__Sync_PSA_Project_Tasks_to_Jira_Issues__c</name>
    </fields>
    <fields>
        <help>Indica se os projetos PSA devem ser sincronizados com os problemas do Jira. É preenchido automaticamente quando o fluxo de trabalho Sincronizar projetos de PSA para problemas no Jira é ativado usando o Console de recursos.</help>
        <label><!-- Sync PSA Projects to Jira Issues --></label>
        <name>pse__Sync_PSA_Projects_to_Jira_Issues__c</name>
    </fields>
    <fields>
        <help>Indica se os projetos PSA devem ser sincronizados com os projetos Jira. É preenchido automaticamente quando o fluxo de trabalho Sincronizar projetos de PSA para projetos no Jira é ativado usando o Console de recursos.</help>
        <label><!-- Sync PSA Projects to Jira Projects --></label>
        <name>pse__Sync_PSA_Projects_to_Jira_Projects__c</name>
    </fields>
    <fields>
        <help>Indica se os recursos PSA devem ser sincronizados para usuários Jira. É preenchido automaticamente quando o fluxo de trabalho Sincronizar recursos do PSA para usuários no Jira é ativado usando o Console de recursos.</help>
        <label><!-- Sync PSA Resources to Jira Users --></label>
        <name>pse__Sync_PSA_Resources_to_Jira_Users__c</name>
    </fields>
    <fields>
        <help>Status em que os cartões de ponto são criados.</help>
        <label><!-- Timecard Status --></label>
        <name>pse__Timecard_Status__c</name>
    </fields>
    <fields>
        <help>Indica se os Projetos PSA devem ser atualizados com base no status do problema Jira.</help>
        <label><!-- Update Projects Based on Issue Status --></label>
        <name>pse__Update_Projects_Based_on_Issue_Status__c</name>
    </fields>
    <fields>
        <help>Indica se o status das tarefas do projeto PSA deve ser atualizado com base no status dos problemas relacionados do Jira.</help>
        <label><!-- Update Task Status Based on Issue Status --></label>
        <name>pse__Update_Task_Status_Based_On_Issue_Status__c</name>
    </fields>
    <fields>
        <help>Indica se o aplicativo Jira fornecido na URL do aplicativo Jira é uma versão do Jira Cloud. Deixe desmarcado se você estiver usando o servidor Jira.</help>
        <label><!-- Use Jira Cloud --></label>
        <name>pse__Use_Jira_Cloud__c</name>
    </fields>
    <fields>
        <help>Determina a direção da sincronização de tarefas do projeto (desde que o fluxo de trabalho apropriado esteja ativado). Quando selecionado, as tarefas do projeto PSA são sincronizadas com os problemas do Jira. Quando desmarcado, os problemas do Jira são sincronizados com as tarefas do projeto PSA.</help>
        <label><!-- Use PSA as Source for Project Task Sync --></label>
        <name>pse__Use_PSA_as_Source_for_Project_Task_Sync__c</name>
    </fields>
    
</CustomObjectTranslation>
