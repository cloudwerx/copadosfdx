<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Línea de transacción de reconocimiento de ingresos</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Líneas de transacción de reconocimiento de ingresos</value>
    </caseValues>
    <fieldSets>
        <label><!-- Summarization Fields --></label>
        <name>ffrr__Summarization_Fields</name>
    </fieldSets>
    <fields>
        <label><!-- GLA Type --></label>
        <name>ffrr__AccountType__c</name>
    </fields>
    <fields>
        <label><!-- GLA Revenue --></label>
        <name>ffrr__Account__c</name>
    </fields>
    <fields>
        <label><!-- Actual vs. Forecast Record --></label>
        <name>ffrr__ActualVsForecastRelationship__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Amortized Opening Balance Dual --></label>
        <name>ffrr__AmortizedOpeningBalanceDual__c</name>
    </fields>
    <fields>
        <label><!-- Amortized Opening Balance Home --></label>
        <name>ffrr__AmortizedOpeningBalanceHome__c</name>
    </fields>
    <fields>
        <help>Cantidad copiada desde el campo Amortizado hasta la fecha de este registro fuente antes de que existiera cualquier transacción de reconocimiento de ingresos. Normalmente, se establece un saldo de apertura al migrar datos al gestor de ingresos.</help>
        <label><!-- Amortized Opening Balance --></label>
        <name>ffrr__AmortizedOpeningBalance__c</name>
    </fields>
    <fields>
        <help>El importe acumulado amortizado hasta la fecha, en la moneda del documento, para el registro de origen en esta línea de transacción.</help>
        <label><!-- Amortized To Date --></label>
        <name>ffrr__AmortizedToDate__c</name>
    </fields>
    <fields>
        <help>Importe amortizado en doble moneda.</help>
        <label><!-- Amount Amortized (Dual) --></label>
        <name>ffrr__AmountAmortizedDual__c</name>
    </fields>
    <fields>
        <help>Importe amortizado en moneda local.</help>
        <label><!-- Amount Amortized (Home) --></label>
        <name>ffrr__AmountAmortizedHome__c</name>
    </fields>
    <fields>
        <label><!-- Amount Amortized --></label>
        <name>ffrr__AmountAmortized__c</name>
    </fields>
    <fields>
        <help>Importe reconocido en doble moneda.</help>
        <label><!-- Amount Recognized (Dual) --></label>
        <name>ffrr__AmountRecognizedDual__c</name>
    </fields>
    <fields>
        <help>Importe reconocido en moneda local</help>
        <label><!-- Amount Recognized (Home) --></label>
        <name>ffrr__AmountRecognizedHome__c</name>
    </fields>
    <fields>
        <label><!-- Amount Recognized --></label>
        <name>ffrr__AmountRecognized__c</name>
    </fields>
    <fields>
        <help>Valor de informe 1.</help>
        <label><!-- Analysis Item 1 --></label>
        <name>ffrr__AnalysisItem1__c</name>
    </fields>
    <fields>
        <help>Valor de informe 2.</help>
        <label><!-- Analysis Item 2 --></label>
        <name>ffrr__AnalysisItem2__c</name>
    </fields>
    <fields>
        <help>Valor de informe 3.</help>
        <label><!-- Analysis Item 3 --></label>
        <name>ffrr__AnalysisItem3__c</name>
    </fields>
    <fields>
        <help>Valor de reporte 4.</help>
        <label><!-- Analysis Item 4 --></label>
        <name>ffrr__AnalysisItem4__c</name>
    </fields>
    <fields>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
    </fields>
    <fields>
        <label><!-- GLA Cost --></label>
        <name>ffrr__CostAccount__c</name>
    </fields>
    <fields>
        <label><!-- Cost Amended --></label>
        <name>ffrr__CostAmended__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Document Currency --></label>
        <name>ffrr__Currency__c</name>
    </fields>
    <fields>
        <help>El tipo de cambio que se convierte de la moneda del documento a la moneda local.</help>
        <label><!-- Document Rate --></label>
        <name>ffrr__DocumentCurrencyRate__c</name>
    </fields>
    <fields>
        <help>El tipo de cambio que se convierte de moneda local a doble moneda.</help>
        <label><!-- Dual Rate --></label>
        <name>ffrr__DualCurrencyRate__c</name>
    </fields>
    <fields>
        <help>La moneda dual.</help>
        <label><!-- Dual Currency --></label>
        <name>ffrr__DualCurrency__c</name>
    </fields>
    <fields>
        <help>La moneda de origen.</help>
        <label><!-- Home Currency --></label>
        <name>ffrr__HomeCurrency__c</name>
    </fields>
    <fields>
        <label><!-- Internal Amount (Amortized) --></label>
        <name>ffrr__InternalAmortizedAmount__c</name>
    </fields>
    <fields>
        <label><!-- Internal Amount --></label>
        <name>ffrr__InternalAmount__c</name>
    </fields>
    <fields>
        <help>El importe amortizado con su signo invertido.</help>
        <label><!-- Journal Amount (Amortized) --></label>
        <name>ffrr__JournalAmortizedAmount__c</name>
    </fields>
    <fields>
        <help>El importe amortizado en moneda dual, con su signo invertido.</help>
        <label><!-- Journal Amount Dual (Amortized) --></label>
        <name>ffrr__JournalAmortizedDualAmount__c</name>
    </fields>
    <fields>
        <help>El importe amortizado en moneda local, con su signo invertido.</help>
        <label><!-- Journal Amount Home (Amortized) --></label>
        <name>ffrr__JournalAmortizedHomeAmount__c</name>
    </fields>
    <fields>
        <help>El importe reconocido en moneda dual, con su signo invertido.</help>
        <label><!-- Journal Amount Dual --></label>
        <name>ffrr__JournalAmountDual__c</name>
    </fields>
    <fields>
        <help>El importe reconocido en moneda local, con su signo invertido.</help>
        <label><!-- Journal Amount Home --></label>
        <name>ffrr__JournalAmountHome__c</name>
    </fields>
    <fields>
        <help>El importe reconocido con su signo invertido.</help>
        <label><!-- Journal Amount --></label>
        <name>ffrr__JournalAmount__c</name>
    </fields>
    <fields>
        <label><!-- Level 3 Object Record Name --></label>
        <name>ffrr__Level3ObjectRecordName__c</name>
    </fields>
    <fields>
        <label><!-- Level 4 Object Record Name --></label>
        <name>ffrr__Level4ObjectRecordName__c</name>
    </fields>
    <fields>
        <label><!-- Performance Obligation --></label>
        <name>ffrr__PerformanceObligation__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Period --></label>
        <name>ffrr__Period__c</name>
    </fields>
    <fields>
        <label><!-- Primary Object Record Name --></label>
        <name>ffrr__PrimaryObjectRecordName__c</name>
    </fields>
    <fields>
        <help>Busque el producto en la línea de transacción.</help>
        <label><!-- Product --></label>
        <name>ffrr__Product__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Recognized Date --></label>
        <name>ffrr__RecognizedDate__c</name>
    </fields>
    <fields>
        <label><!-- Recognized Opening Balance Dual --></label>
        <name>ffrr__RecognizedOpeningBalanceDual__c</name>
    </fields>
    <fields>
        <label><!-- Recognized Opening Balance Home --></label>
        <name>ffrr__RecognizedOpeningBalanceHome__c</name>
    </fields>
    <fields>
        <help>Cantidad copiada del campo Fecha de Reconocimiento de este registro fuente antes de que existiera cualquier transacción de reconocimiento de ingresos. Normalmente, se establece un saldo de apertura al migrar datos al gestor de ingresos.</help>
        <label><!-- Recognized Opening Balance --></label>
        <name>ffrr__RecognizedOpeningBalance__c</name>
    </fields>
    <fields>
        <label><!-- Recognized Record ID --></label>
        <name>ffrr__RecognizedRecordID__c</name>
    </fields>
    <fields>
        <help>El importe acumulado reconocido hasta la fecha, en la moneda del documento, para el registro de origen en esta línea de transacción.</help>
        <label><!-- Recognized To Date --></label>
        <name>ffrr__RecognizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Amended --></label>
        <name>ffrr__RevenueAmended__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Contract --></label>
        <name>ffrr__RevenueContract__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Revenue Recognition Transaction --></label>
        <name>ffrr__RevenueRecognitionMaster__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <help>Busque la cuenta de Salesforce en la línea de transacción.</help>
        <label><!-- Account --></label>
        <name>ffrr__SalesforceAccount__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Level 2 Object Record Name --></label>
        <name>ffrr__SecondaryObjectRecordName__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__Template__c</name>
        <relationshipLabel><!-- Revenue Recognition Transaction Lines --></relationshipLabel>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Número de línea de transacción</nameFieldLabel>
</CustomObjectTranslation>
