<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Budget</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Budget</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Budget</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Budgetar</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Budgetar</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Budgetar</value>
    </caseValues>
    <fieldSets>
        <label><!-- Create Project From Opp And Template Budget Columns --></label>
        <name>pse__CreateProjectFromOppAndTempBudgetColumns</name>
    </fieldSets>
    <fields>
        <label><!-- Account --></label>
        <name>pse__Account__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <help>Om markerad gör det möjligt för Admin att göra globala ändringar i budgeten, inklusive ändringar i budgetprojektet, resurs, valuta eller datum, även om Inkludera i ekonomi är markerat. Konfigeringskrav: Aktuellt beräkningsläge måste ställas in på &quot;Schemalagt&quot;.</help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <help>Det monetära beloppet för budgeten, inklusive inget belopp som anges separat i fältet Expense Amount. Standard och måste alltid vara samma valuta som Project.</help>
        <label><!-- Amount --></label>
        <name>pse__Amount__c</name>
    </fields>
    <fields>
        <help>Den här kryssrutan ska kontrolleras när budgeten är godkänd - vanligtvis baserat på fältet Status.</help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approved for Billing --></label>
        <name>pse__Approved_for_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Approver --></label>
        <name>pse__Approver__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Bill Date --></label>
        <name>pse__Bill_Date__c</name>
    </fields>
    <fields>
        <label><!-- Billable --></label>
        <name>pse__Billable__c</name>
    </fields>
    <fields>
        <label><!-- Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Invoiced --></label>
        <name>pse__Billing_Event_Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Item --></label>
        <name>pse__Billing_Event_Item__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billing Event Released --></label>
        <name>pse__Billing_Event_Released__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event Status --></label>
        <name>pse__Billing_Event_Status__c</name>
    </fields>
    <fields>
        <label><!-- Billing Event --></label>
        <name>pse__Billing_Event__c</name>
    </fields>
    <fields>
        <label><!-- Billing Hold --></label>
        <name>pse__Billing_Hold__c</name>
    </fields>
    <fields>
        <label><!-- Budget Header --></label>
        <name>pse__Budget_Header__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <label><!-- Effective Date --></label>
        <name>pse__Effective_Date__c</name>
    </fields>
    <fields>
        <help>Indikerar att budgeten är i ett tillstånd som är berättigat till fakturering av händelsegenerering (inte inklusive flaggan Godkända för fakturering, vilket också kan krävas per global config).</help>
        <label><!-- Eligible for Billing --></label>
        <name>pse__Eligible_for_Billing__c</name>
    </fields>
    <fields>
        <help>Om det är markerat, fakturerar du aldrig denna företagsrekord. Samma effekt som Billing Hold, men avsedd att spegla en permanent uteslutning från Billing Generation.</help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <help>Det monetära beloppet (om någon) uttryckligen avsatts för utgifter i budgeten. Detta är separat från och förutom fältet Budgetbelopp. Standard och måste alltid vara samma valuta som Project.</help>
        <label><!-- Expense Amount --></label>
        <name>pse__Expense_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Expense Transaction --></label>
        <name>pse__Expense_Transaction__c</name>
        <relationshipLabel><!-- Budget (Expense Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Date --></label>
        <name>pse__Invoice_Date__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Number --></label>
        <name>pse__Invoice_Number__c</name>
    </fields>
    <fields>
        <label><!-- Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity --></label>
        <name>pse__Opportunity__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Group Currency Code --></label>
        <name>pse__Override_Project_Group_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Om det här fältet är inställt, åsidosätter det den grupp till vilken en budgetransaktioner kommer att rulla upp för gruppaktörer, även om projektet ligger i en annan grupp. Typiskt rullar en budgettransaktioner upp till projektets grupp.</help>
        <label><!-- Override Project Group --></label>
        <name>pse__Override_Project_Group__c</name>
        <relationshipLabel><!-- Override Group For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Practice Currency Code --></label>
        <name>pse__Override_Project_Practice_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Om det här fältet är inställt, åsidosätter det den praxis som en budgettransaktioner kommer att rulla upp för övningsaktörer, även om projektet ligger i en annan praxis. Vanligtvis rullar en budgettransaktioner upp till sitt projekt.</help>
        <label><!-- Override Project Practice --></label>
        <name>pse__Override_Project_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Override Project Region Currency Code --></label>
        <name>pse__Override_Project_Region_Currency_Code__c</name>
    </fields>
    <fields>
        <help>Om det här fältet är inställt, överstyr det det område till vilket en budgettransaktioner kommer att rulla upp för regionala aktörer, även om projektet ligger i en annan region. Vanligtvis rullar en budgettransaktioner upp till projektets region.</help>
        <label><!-- Override Project Region --></label>
        <name>pse__Override_Project_Region__c</name>
        <relationshipLabel><!-- Override Region For Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- DEPRECATED: Pre-Billed Amount --></label>
        <name>pse__PreBilledAmount__c</name>
    </fields>
    <fields>
        <help>Den del av de kombinerade budget- / utgiftsbelopp som ska debiteras. Standard och måste alltid vara samma valuta som Project. Det här fältet ersätter det gamla fältet Före fakturerat belopp (PreBilledAmount__c) som inte tillåter icke-heltal decimaler.</help>
        <label><!-- Pre-Billed Amount --></label>
        <name>pse__Pre_Billed_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Pre-Billed Transaction --></label>
        <name>pse__Pre_Billed_Transaction__c</name>
        <relationshipLabel><!-- Budget (Pre-Billed Transaction) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Budgets --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation>Godkänd</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation>Förslag</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Open</masterLabel>
            <translation>Öppen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation>Avvisade</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Total Amount --></label>
        <name>pse__Total_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Transaction --></label>
        <name>pse__Transaction__c</name>
        <relationshipLabel><!-- Budget --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Type --></label>
        <name>pse__Type__c</name>
        <picklistValues>
            <masterLabel>Customer Purchase Order</masterLabel>
            <translation>Beställningsköp</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Customer Purchase Order Change Request</masterLabel>
            <translation>Beställningsköp för beställning</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Budget</masterLabel>
            <translation>Intern budget</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Internal Budget Change Request</masterLabel>
            <translation>Intern budgetändringsbegäran</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Vendor Purchase Order</masterLabel>
            <translation>Leverantörs beställning</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Vendor Purchase Order Change Request</masterLabel>
            <translation>Beställningsändringsförfrågan för leverantör</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Work Order</masterLabel>
            <translation>Arbetsorder</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Work Order Change Request</masterLabel>
            <translation>Beställ beställningsändringsbegäran</translation>
        </picklistValues>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Budgetnamn</nameFieldLabel>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s account must match the account on this budget --></errorMessage>
        <name>pse__Budget_Header_Account_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s project does not match the project on this budget --></errorMessage>
        <name>pse__Budget_Header_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The budget header&apos;s type must match the type on this budget --></errorMessage>
        <name>pse__Budget_Header_Type_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Budget&apos;s Project field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Budget_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Amount should be a positive value --></errorMessage>
        <name>pse__Has_non_negative_budget_amount</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Pre Billed Amount should be a positive value --></errorMessage>
        <name>pse__Has_non_negative_pre_billed_amount</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Master Budget cannot be changed once set. --></errorMessage>
        <name>pse__Master_Budget_is_Constant</name>
    </validationRules>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
</CustomObjectTranslation>
