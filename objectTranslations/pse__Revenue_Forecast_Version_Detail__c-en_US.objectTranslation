<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- The corporate currency of the org at the time the revenue forecast was run. --></help>
        <label><!-- Corp: Currency --></label>
        <name>pse__Corp_Currency__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is ready for recognition within the relevant time period for the Milestone revenue source, when using the Equal Split recognition method. --></help>
        <label><!-- Corp: Equal Split: Milestone: Pending --></label>
        <name>pse__Corp_Equal_Split_Milestone_P__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that has already been recognized within the relevant time period for the Milestone revenue source, when using the Equal Split recognition method. --></help>
        <label><!-- Corp: Equal Split: Milestone: Recognized --></label>
        <name>pse__Corp_Equal_Split_Milestone_R__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is scheduled for recognition within the relevant time period for the Milestone revenue source, when using the Equal Split recognition method. --></help>
        <label><!-- Corp: Equal Split: Milestone: Scheduled --></label>
        <name>pse__Corp_Equal_Split_Milestone_S__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is ready for recognition within the relevant time period for the Project revenue source, when using the Equal Split recognition method. --></help>
        <label><!-- Corp: Equal Split: Project: Pending --></label>
        <name>pse__Corp_Equal_Split_Project_P__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that has already been recognized within the relevant time period for the Project revenue source, when using the Equal Split recognition method. --></help>
        <label><!-- Corp: Equal Split: Project: Recognized --></label>
        <name>pse__Corp_Equal_Split_Project_R__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is scheduled for recognition within the relevant time period for the Project revenue source, when using the Equal Split recognition method. --></help>
        <label><!-- Corp: Equal Split: Project: Scheduled --></label>
        <name>pse__Corp_Equal_Split_Project_S__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is scheduled for recognition within the relevant time period for the EVA revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Corp: Deliverable: EVA: Scheduled --></label>
        <name>pse__Corp_Eva_S__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is ready for recognition within the relevant time period for the Expense revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Corp: Deliverable: Expense: Pending --></label>
        <name>pse__Corp_Expense_P__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that has already been recognized within the relevant time period for the Expense revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Corp: Deliverable: Expense: Recognized --></label>
        <name>pse__Corp_Expense_R__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is ready for recognition for the Milestone revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Corp: Deliverable: Milestone: Pending --></label>
        <name>pse__Corp_Milestone_P__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that has already been recognized within the relevant time period for the Milestone revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Corp: Deliverable: Milestone: Recognized --></label>
        <name>pse__Corp_Milestone_R__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is scheduled for recognition within the relevant time period for the Milestone revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Corp: Deliverable: Milestone: Scheduled --></label>
        <name>pse__Corp_Milestone_S__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is ready for recognition for the Miscellaneous Adjustment revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Corp: Deliverable: MA: Pending --></label>
        <name>pse__Corp_Misc_Adjustment_P__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that has already been recognized within the relevant time period for the Miscellaneous Adjustment revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Corp: Deliverable: MA: Recognized --></label>
        <name>pse__Corp_Misc_Adjustment_R__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of unscheduled revenue within the relevant time period for the Opportunity revenue source, using the best case scenario. --></help>
        <label><!-- Corp: Opportunity: Unscheduled Best --></label>
        <name>pse__Corp_Opportunity_U_Best__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of unscheduled revenue within the relevant time period for the Opportunity revenue source, using the worst case scenario. --></help>
        <label><!-- Corp: Opportunity: Unscheduled Worst --></label>
        <name>pse__Corp_Opportunity_U_Worst__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of unscheduled revenue within the relevant time period for the Opportunity revenue source, using the expected scenario. --></help>
        <label><!-- Corp: Opportunity: Unscheduled --></label>
        <name>pse__Corp_Opportunity_U__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is ready for recognition within the relevant time period for the Milestone revenue source, when using the % Complete recognition method. --></help>
        <label><!-- Corp: % Complete: Milestone: Pending --></label>
        <name>pse__Corp_Percent_Complete_Milestone_P__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that has already been recognized within the relevant time period for the Milestone revenue source, when using the % Complete recognition method. --></help>
        <label><!-- Corp: % Complete: Milestone: Recognized --></label>
        <name>pse__Corp_Percent_Complete_Milestone_R__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is scheduled for recognition within the relevant time period for the Milestone revenue source, when using the % Complete recognition method. --></help>
        <label><!-- Corp: % Complete: Milestone: Scheduled --></label>
        <name>pse__Corp_Percent_Complete_Milestone_S__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of unscheduled revenue within the relevant time period for the Milestone revenue source, when using the % Complete recognition method. --></help>
        <label><!-- Corp: % Complete: Milestone: Unscheduled --></label>
        <name>pse__Corp_Percent_Complete_Milestone_U__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is ready for recognition within the relevant time period for the Project revenue source, when using the % Complete recognition method. --></help>
        <label><!-- Corp: % Complete: Project: Pending --></label>
        <name>pse__Corp_Percent_Complete_Project_P__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that has already been recognized within the relevant time period for the Project revenue source, when using the % Complete recognition method. --></help>
        <label><!-- Corp: % Complete: Project: Recognized --></label>
        <name>pse__Corp_Percent_Complete_Project_R__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is scheduled for recognition within the relevant time period for the Project revenue source, when using the % Complete recognition method. --></help>
        <label><!-- Corp: % Complete: Project: Scheduled --></label>
        <name>pse__Corp_Percent_Complete_Project_S__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of unscheduled revenue within the relevant time period for the Project revenue source, when using the % Complete recognition method. --></help>
        <label><!-- Corp: % Complete: Project: Unscheduled --></label>
        <name>pse__Corp_Percent_Complete_Project_U__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that is ready for recognition for the Timecard revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Corp: Deliverable: Timecard: Pending --></label>
        <name>pse__Corp_Timecard_P__c</name>
    </fields>
    <fields>
        <help><!-- In the corporate currency, the amount of revenue that has already been recognized within the relevant time period for the Timecard revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Corp: Deliverable: Timecard: Recognized --></label>
        <name>pse__Corp_Timecard_R__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is ready for recognition within the relevant time period for the Milestone revenue source, when using the Equal Split recognition method. Populated by the revenue forecast version batch Apex job. --></help>
        <label><!-- Equal Split: Milestone: Pending --></label>
        <name>pse__Equal_Split_Milestone_Pending__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that has already been recognized within the relevant time period for the Milestone revenue source, when using the Equal Split recognition method. Populated using data from Revenue Management, if you are using the integration. --></help>
        <label><!-- Equal Split: Milestone: Recognized --></label>
        <name>pse__Equal_Split_Milestone_Recognized__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is scheduled for recognition within the relevant time period for the Milestone revenue source, when using the Equal Split recognition method. --></help>
        <label><!-- Equal Split: Milestone: Scheduled --></label>
        <name>pse__Equal_Split_Milestone_Scheduled__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is ready for recognition within the relevant time period for the Project revenue source, when using the Equal Split recognition method. Populated by the revenue forecast version batch Apex job. --></help>
        <label><!-- Equal Split: Project: Pending --></label>
        <name>pse__Equal_Split_Project_Pending__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that has already been recognized within the relevant time period for the Project revenue source, when using the Equal Split recognition method. Populated using data from Revenue Management, if you are using the integration. --></help>
        <label><!-- Equal Split: Project: Recognized --></label>
        <name>pse__Equal_Split_Project_Recognized__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is scheduled for recognition within the relevant time period for the Project revenue source, when using the Equal Split recognition method. --></help>
        <label><!-- Equal Split: Project: Scheduled --></label>
        <name>pse__Equal_Split_Project_Scheduled__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is scheduled for recognition within the relevant time period for the EVA revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Deliverable: EVA: Scheduled --></label>
        <name>pse__Eva_Scheduled__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is ready for recognition within the relevant time period for the Expense revenue source, when using the Deliverable recognition method. Populated by the revenue forecast version batch Apex job. --></help>
        <label><!-- Deliverable: Expense: Pending --></label>
        <name>pse__Expense_Pending__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that has already been recognized within the relevant time period for the Expense revenue source, when using the Deliverable recognition method. Populated using data from Revenue Management, if you are using the integration. --></help>
        <label><!-- Deliverable: Expense: Recognized --></label>
        <name>pse__Expense_Recognized__c</name>
    </fields>
    <fields>
        <help><!-- Lookup to the group this revenue forecast version detail relates to. --></help>
        <label><!-- Group --></label>
        <name>pse__Group__c</name>
        <relationshipLabel><!-- Revenue Forecast Version Details --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is ready for recognition within the relevant time period for the Milestone revenue source, when using the Deliverable recognition method. Populated by the revenue forecast version batch Apex job. --></help>
        <label><!-- Deliverable: Milestone: Pending --></label>
        <name>pse__Milestone_Pending__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that has already been recognized within the relevant time period for the Milestone revenue source, when using the Deliverable recognition method. Populated using data from Revenue Management, if you are using the integration. --></help>
        <label><!-- Deliverable: Milestone: Recognized --></label>
        <name>pse__Milestone_Recognized__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is scheduled for recognition within the relevant time period for the Milestone revenue source, when using the Deliverable recognition method. --></help>
        <label><!-- Deliverable: Milestone: Scheduled --></label>
        <name>pse__Milestone_Scheduled__c</name>
    </fields>
    <fields>
        <help><!-- Lookup to the milestone this revenue forecast version detail relates to. --></help>
        <label><!-- Milestone --></label>
        <name>pse__Milestone__c</name>
        <relationshipLabel><!-- Revenue Forecast Version Details --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is ready for recognition within the relevant time period for the Miscellaneous Adjustment revenue source, when using the Deliverable recognition method. Populated by the revenue forecast version batch Apex job. --></help>
        <label><!-- Deliverable: Misc Adjustment: Pending --></label>
        <name>pse__Misc_Adjustment_Pending__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue already recognized within the relevant time period for the Miscellaneous Adjustment revenue source, when using the Deliverable recognition method. Populated using data from Revenue Management, if you are using the integration. --></help>
        <label><!-- Deliverable: Misc Adjustment: Recognized --></label>
        <name>pse__Misc_Adjustment_Recognized__c</name>
    </fields>
    <fields>
        <help><!-- The probability of the opportunity that this revenue forecast version detail relates to. --></help>
        <label><!-- Opportunity Probability (%) --></label>
        <name>pse__Opportunity_Probability__c</name>
    </fields>
    <fields>
        <help><!-- Amount of unscheduled revenue within the relevant time period for the Opportunity revenue source, using the best case scenario. --></help>
        <label><!-- Opportunity: Unscheduled Best --></label>
        <name>pse__Opportunity_Unscheduled_Best__c</name>
    </fields>
    <fields>
        <help><!-- Amount of unscheduled revenue within the relevant time period for the Opportunity revenue source, using the worst case scenario. --></help>
        <label><!-- Opportunity: Unscheduled Worst --></label>
        <name>pse__Opportunity_Unscheduled_Worst__c</name>
    </fields>
    <fields>
        <help><!-- Amount of unscheduled revenue within the relevant time period for the Opportunity revenue source, using the expected scenario. --></help>
        <label><!-- Opportunity: Unscheduled --></label>
        <name>pse__Opportunity_Unscheduled__c</name>
    </fields>
    <fields>
        <help><!-- Lookup to the opportunity this revenue forecast version detail relates to. --></help>
        <label><!-- Opportunity --></label>
        <name>pse__Opportunity__c</name>
        <relationshipLabel><!-- Revenue Forecast Version Details --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is ready for recognition within the relevant time period for the Milestone revenue source, when using the % Complete recognition method. Populated by the revenue forecast version batch Apex job. --></help>
        <label><!-- % Complete: Milestone: Pending --></label>
        <name>pse__Percent_Complete_Milestone_Pending__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that has already been recognized within the relevant time period for the Milestone revenue source, when using the % Complete recognition method. Populated using data from Revenue Management, if you are using the integration. --></help>
        <label><!-- % Complete: Milestone: Recognized --></label>
        <name>pse__Percent_Complete_Milestone_Recognized__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is scheduled for recognition within the relevant time period for the Milestone revenue source, when using the % Complete recognition method. --></help>
        <label><!-- % Complete: Milestone: Scheduled --></label>
        <name>pse__Percent_Complete_Milestone_Scheduled__c</name>
    </fields>
    <fields>
        <help><!-- Amount of unscheduled revenue within the relevant time period for the Milestone revenue source, when using the % Complete recognition method. --></help>
        <label><!-- % Complete: Milestone: Unscheduled --></label>
        <name>pse__Percent_Complete_Milestone_Unscheduled__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is ready for recognition within the relevant time period for the Project revenue source, when using the % Complete recognition method. Populated by the revenue forecast version batch Apex job. --></help>
        <label><!-- % Complete: Project: Pending --></label>
        <name>pse__Percent_Complete_Project_Pending__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that has already been recognized within the relevant time period for the Project revenue source, when using the % Complete recognition method. Populated using data from Revenue Management, if you are using the integration. --></help>
        <label><!-- % Complete: Project: Recognized --></label>
        <name>pse__Percent_Complete_Project_Recognized__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is scheduled for recognition within the relevant time period for the Project revenue source, when using the % Complete recognition method. --></help>
        <label><!-- % Complete: Project: Scheduled --></label>
        <name>pse__Percent_Complete_Project_Scheduled__c</name>
    </fields>
    <fields>
        <help><!-- Amount of unscheduled revenue within the relevant time period for the Project revenue source, when using the % Complete recognition method. --></help>
        <label><!-- % Complete: Project: Unscheduled --></label>
        <name>pse__Percent_Complete_Project_Unscheduled__c</name>
    </fields>
    <fields>
        <help><!-- Lookup to the practice this revenue forecast version detail relates to. --></help>
        <label><!-- Practice --></label>
        <name>pse__Practice__c</name>
        <relationshipLabel><!-- Revenue Forecast Version Details --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Lookup to the project this revenue forecast version detail relates to. --></help>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Revenue Forecast Version Details --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Lookup to the region this revenue forecast version detail relates to. --></help>
        <label><!-- Region --></label>
        <name>pse__Region__c</name>
        <relationshipLabel><!-- Revenue Forecast Version Details --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Lookup to the revenue forecast version this record relates to. --></help>
        <label><!-- Revenue Forecast Version --></label>
        <name>pse__Revenue_Forecast_Version__c</name>
        <relationshipLabel><!-- Revenue Forecast Version Details --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- End date of the time period this record relates to. --></help>
        <label><!-- Time Period End --></label>
        <name>pse__Time_Period_End__c</name>
    </fields>
    <fields>
        <help><!-- Start date of the time period this record relates to. --></help>
        <label><!-- Time Period Start --></label>
        <name>pse__Time_Period_Start__c</name>
    </fields>
    <fields>
        <help><!-- Lookup to the time period this revenue forecast version detail relates to. --></help>
        <label><!-- Time Period --></label>
        <name>pse__Time_Period__c</name>
        <relationshipLabel><!-- Revenue Forecast Version Details --></relationshipLabel>
    </fields>
    <fields>
        <help><!-- Amount of revenue that is ready for recognition within the relevant time period for the Timecard revenue source, when using the Deliverable recognition method. Populated by the revenue forecast version batch Apex job. --></help>
        <label><!-- Deliverable: Timecard: Pending --></label>
        <name>pse__Timecard_Pending__c</name>
    </fields>
    <fields>
        <help><!-- Amount of revenue that has already been recognized within the relevant time period for the Timecard revenue source, when using the Deliverable recognition method. Populated using data from Revenue Management, if you are using the integration. --></help>
        <label><!-- Deliverable: Timecard: Recognized --></label>
        <name>pse__Timecard_Recognized__c</name>
    </fields>
</CustomObjectTranslation>
