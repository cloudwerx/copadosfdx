<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Contrat de revenu</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Contrats de revenus</value>
    </caseValues>
    <fields>
        <help>Informations sur le contrat de revenus. Ce sera souvent le nom du compte, mais il peut s’agir de n’importe quelle information.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <label><!-- Account --></label>
        <name>ffrr__Account__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Active --></label>
        <name>ffrr__Active__c</name>
    </fields>
    <fields>
        <help>Indique si l&apos;allocation peut être exécutée avec succès pour ce contrat de revenu.</help>
        <label><!-- Ready for Allocation --></label>
        <name>ffrr__Allocatable__c</name>
    </fields>
    <fields>
        <help>Le ratio utilisé pour affecter les revenus aux obligations de performance liées lorsque la valeur des revenus alloués n&apos;a pas été remplacée.</help>
        <label><!-- Allocation Ratio --></label>
        <name>ffrr__AllocationRatio__c</name>
    </fields>
    <fields>
        <help>Indique si les revenus de ce contrat ont été entièrement alloués ou doivent être réaffectés.</help>
        <label><!-- Allocation Status --></label>
        <name>ffrr__AllocationStatus__c</name>
    </fields>
    <fields>
        <help>Recherche de l’objet société.</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <fields>
        <help>Nombre de décimales dans la devise de l’enregistrement.</help>
        <label><!-- Currency Decimal Places --></label>
        <name>ffrr__CurrencyDP__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <label><!-- End Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <help>Le nombre d&apos;obligations de performance relatives au chiffre d&apos;affaires ayant au moins une valeur SSP nulle.</help>
        <label><!-- Null SSP Count --></label>
        <name>ffrr__NullSSPCount__c</name>
    </fields>
    <fields>
        <help>Nombre d&apos;obligations de performance liées dans lesquelles une valeur de remplacement des revenus alloués a été saisie.</help>
        <label><!-- PO Allocated Revenue Override Count --></label>
        <name>ffrr__POAllocatedRevenueOverrideCount__c</name>
    </fields>
    <fields>
        <help>Le nombre d&apos;obligations de performance sur ce contrat.</help>
        <label><!-- Performance Obligations Count --></label>
        <name>ffrr__PerformanceObligationsCount__c</name>
    </fields>
    <fields>
        <help>Indique si le revenu de ce contrat a été alloué avec succès.</help>
        <label><!-- Revenue Allocated --></label>
        <name>ffrr__RevenueAllocated__c</name>
    </fields>
    <fields>
        <help>Ce champ ne doit être renseigné que si les revenus du contrat sont définis au niveau du contrat au lieu d&apos;être calculés à partir des obligations de performance. Lorsqu&apos;elle est renseignée, cette valeur est utilisée dans le champ Revenu.</help>
        <label><!-- Revenue Override --></label>
        <name>ffrr__RevenueOverride__c</name>
    </fields>
    <fields>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionComplete__c</name>
    </fields>
    <fields>
        <help>La valeur qui sera affectée aux obligations de performance. Utilise le chiffre d&apos;affaires total ou le remplacement des revenus s&apos;il est rempli.</help>
        <label><!-- Revenue --></label>
        <name>ffrr__Revenue__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>Somme des valeurs de substitution des revenus alloués de toutes les obligations de performance liées.</help>
        <label><!-- Total Allocated Revenue Override --></label>
        <name>ffrr__TotalAllocatedRevenueOverride__c</name>
    </fields>
    <fields>
        <help>Somme des valeurs de revenus alloués de toutes les obligations de performance liées.</help>
        <label><!-- Total Allocated Revenue --></label>
        <name>ffrr__TotalAllocatedRevenue__c</name>
    </fields>
    <fields>
        <help>Somme des valeurs amorties à la date de toutes les obligations de performance liées.</help>
        <label><!-- Total Amortized To Date --></label>
        <name>ffrr__TotalAmortizedToDate__c</name>
    </fields>
    <fields>
        <help>Somme des valeurs de coût de toutes les obligations de performance liées.</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>Le nombre d&apos;obligations de performance liées lorsque les zones Ratio d&apos;allocation et Recettes imputées sont nulles.</help>
        <label><!-- Null Allocation Ratio Count --></label>
        <name>ffrr__TotalNullAllocationRatioCount__c</name>
    </fields>
    <fields>
        <help>Somme des valeurs Recognised to Date de toutes les obligations de performance liées.</help>
        <label><!-- Total Recognized To Date --></label>
        <name>ffrr__TotalRecognizedToDate__c</name>
    </fields>
    <fields>
        <help>Somme des valeurs de revenus de toutes les obligations de performance liées.</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>Somme des valeurs du fournisseur de services partagés pour les obligations de performance liées lorsque la valeur des revenus alloués n&apos;a pas été remplacée.</help>
        <label><!-- Total SSP for Allocation --></label>
        <name>ffrr__TotalSSPForAllocation__c</name>
    </fields>
    <fields>
        <help>Somme des prix de vente autonomes de toutes les obligations de performance liées.</help>
        <label><!-- Total SSP --></label>
        <name>ffrr__TotalSSP__c</name>
    </fields>
    <fields>
        <help>Le nombre d&apos;obligations de performance relatives au chiffre d&apos;affaires où le SSP est nul.</help>
        <label><!-- Zero SSP Count --></label>
        <name>ffrr__ZeroSSPCount__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__ffrrtemplate__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Numéro de contrat de revenu</nameFieldLabel>
    <startsWith>Vowel</startsWith>
    <webLinks>
        <label><!-- AllocateRevenue --></label>
        <name>ffrr__AllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- ListAllocateRevenue --></label>
        <name>ffrr__ListAllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- ListUpdatePerformanceObligations --></label>
        <name>ffrr__ListUpdatePerformanceObligations</name>
    </webLinks>
    <webLinks>
        <label><!-- ManageObligations --></label>
        <name>ffrr__ManageObligations</name>
    </webLinks>
    <webLinks>
        <label><!-- UpdatePerformanceObligations --></label>
        <name>ffrr__UpdatePerformanceObligations</name>
    </webLinks>
</CustomObjectTranslation>
