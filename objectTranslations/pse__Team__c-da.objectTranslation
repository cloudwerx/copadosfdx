<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Hold</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Hold</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Hold</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Hold</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Hold</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Hold</value>
    </caseValues>
    <fields>
        <help>Når det er sandt, sendes tidsplanen til alle holdmedlemmer, når du klikker på knappen Send planlægning.</help>
        <label><!-- All Team Members --></label>
        <name>pse__All_Team_Members__c</name>
    </fields>
    <fields>
        <help>Når det er sandt, sendes en meddelelse til alle holdmedlemmer med redigeringstilgang, når en holdplan skiftes.</help>
        <label><!-- Anyone with Edit Access --></label>
        <name>pse__Anyone_With_Edit_Access__c</name>
    </fields>
    <fields>
        <help>Beskrivelsen for holdet.</help>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <help>Vælg for at lade holdmedlemmer bytte skift på den tilhørende holdplanlægning. Dette felt er deaktiveret som standard.</help>
        <label><!-- Enable Swapping --></label>
        <name>pse__Enable_Swapping__c</name>
    </fields>
    <fields>
        <help>Når det er sandt, sendes en meddelelse til det nyligt tildelte holdmedlem, når en holdplan skiftes.</help>
        <label><!-- Currently Assigned Team Member --></label>
        <name>pse__Now_Assigned__c</name>
    </fields>
    <fields>
        <help>Når det er sandt, sendes en meddelelse til det tidligere tildelte holdmedlem, når en holdplan skiftes.</help>
        <label><!-- Previously Assigned Team Member --></label>
        <name>pse__Previously_Assigned__c</name>
    </fields>
    <fields>
        <help>Projektet er forbundet med holdet.</help>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Teams --></relationshipLabel>
    </fields>
    <fields>
        <help>Angiv den brugerdefinerede e-mail-skabelon til brug for afsendelse af holdplaner.</help>
        <label><!-- Custom Template Name (Schedule) --></label>
        <name>pse__Schedule_Custom_Template_Name__c</name>
    </fields>
    <fields>
        <help>Angiver e-mailadresser for at modtage holdplanen. Værdien er en kommasepareret liste over e-mailadresser.</help>
        <label><!-- Other Email Addresses (Schedule) --></label>
        <name>pse__Schedule_Other_Email_Addresses__c</name>
    </fields>
    <fields>
        <help>Når det er sandt, sendes tidsplanen til e-mail-adresser, der er angivet i feltet Andre e-mail-adresser, når du klikker på knappen Send planlægning.</help>
        <label><!-- Send Email to Others (Schedule) --></label>
        <name>pse__Schedule_Send_Email_To_Others__c</name>
    </fields>
    <fields>
        <help>Når det er sandt, bruger skemaet email den e-mail-skabelon, der er defineret i feltet Brugerdefineret skabelon (Schedule).</help>
        <label><!-- Use Custom Template (Schedule) --></label>
        <name>pse__Schedule_Use_Custom_Template__c</name>
    </fields>
    <fields>
        <help>Angiver den brugerdefinerede e-mail-skabelon, der skal bruges til afsendelse af holdplanlægningsudvekslingsmeddelelser.</help>
        <label><!-- Custom Template Name (Swap) --></label>
        <name>pse__Swap_Custom_Template_Name__c</name>
    </fields>
    <fields>
        <help>Angiver e-mailadresser for at modtage en anmeldelse, når en holdplan skiftes. Værdien er en kommasepareret liste over e-mailadresser.</help>
        <label><!-- Other Email Addresses (Swap) --></label>
        <name>pse__Swap_Other_Email_Addresses__c</name>
    </fields>
    <fields>
        <help>Når det er sandt, sendes en meddelelse til alle e-mailadresser, der er defineret i feltet Send email to Others (Swap), når en holdplan skiftes.</help>
        <label><!-- Send Email to Others (Swap) --></label>
        <name>pse__Swap_Send_Email_To_Others__c</name>
    </fields>
    <fields>
        <help>Når det er sandt, bruger swap notification emailen den e-mail-skabelon, der er defineret i feltet Custom Template Name (swap).</help>
        <label><!-- Use Custom Template (Swap) --></label>
        <name>pse__Swap_Use_Custom_Template__c</name>
    </fields>
    <fields>
        <help>Den ressource, der ejer teamet.</help>
        <label><!-- Team Owner --></label>
        <name>pse__Team_Owner__c</name>
        <relationshipLabel><!-- Team --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Time Zone --></label>
        <name>pse__Time_Zone__c</name>
        <picklistValues>
            <masterLabel>Africa/Algiers</masterLabel>
            <translation>Afrika / Algiers</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Cairo</masterLabel>
            <translation>Afrika / Cairo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Casablanca</masterLabel>
            <translation>Afrika / Casablanca</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Johannesburg</masterLabel>
            <translation>Afrika / Johannesburg</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Nairobi</masterLabel>
            <translation>Afrika / Nairobi</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Adak</masterLabel>
            <translation>Amerika / Adak</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Anchorage</masterLabel>
            <translation>Amerika / Anchorage</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Argentina/Buenos_Aires</masterLabel>
            <translation>Amerika / Argentina / Buenos_ Aires</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Bogota</masterLabel>
            <translation>Amerika / Bogota</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Caracas</masterLabel>
            <translation>Amerika / Caracas</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Chicago</masterLabel>
            <translation>Amerika / Chicago</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Denver</masterLabel>
            <translation>Amerika / Denver</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/El_Salvador</masterLabel>
            <translation>Amerika / El_Salvador</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Halifax</masterLabel>
            <translation>Amerika / Halifax</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Indiana/Indianapolis</masterLabel>
            <translation>Amerika / Indiana / Indianapolis</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Lima</masterLabel>
            <translation>Amerika / Lima</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Los_Angeles</masterLabel>
            <translation>Amerika / Los_Angeles</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Mazatlan</masterLabel>
            <translation>Amerika / Mazatlan</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Mexico_City</masterLabel>
            <translation>Amerika / Mexico_City</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/New_York</masterLabel>
            <translation>Amerika / New_York</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Panama</masterLabel>
            <translation>Amerika / Panama</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Phoenix</masterLabel>
            <translation>Amerika / Phoenix</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Puerto_Rico</masterLabel>
            <translation>Amerika / Puerto_Rico</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Santiago</masterLabel>
            <translation>Amerika / Santiago</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Sao_Paulo</masterLabel>
            <translation>Amerika / Sao_Paulo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Scoresbysund</masterLabel>
            <translation>Amerika / Scoresbysund</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/St_Johns</masterLabel>
            <translation>Amerika / St_Johns</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Tijuana</masterLabel>
            <translation>Amerika / Tijuana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Baghdad</masterLabel>
            <translation>Asien / Bagdad</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Baku</masterLabel>
            <translation>Asien / Baku</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Bangkok</masterLabel>
            <translation>Asien / Bangkok</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Beirut</masterLabel>
            <translation>Asien / Beirut</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Colombo</masterLabel>
            <translation>Asien / Colombo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Dhaka</masterLabel>
            <translation>Asien / Dhaka</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Dubai</masterLabel>
            <translation>Asien / Dubai</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Ho_Chi_Minh</masterLabel>
            <translation>Asien / Ho_Chi_Minh</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Hong_Kong</masterLabel>
            <translation>Asien / Hong_Kong</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Jakarta</masterLabel>
            <translation>Asien / Jakarta</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Jerusalem</masterLabel>
            <translation>Asien / Jerusalem</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kabul</masterLabel>
            <translation>Asien / Kabul</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kamchatka</masterLabel>
            <translation>Asien / Kamchatka</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Karachi</masterLabel>
            <translation>Asien / Karachi</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kathmandu</masterLabel>
            <translation>Asien / Kathmandu</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kolkata</masterLabel>
            <translation>Asien / Kolkata</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kuala_Lumpur</masterLabel>
            <translation>Asien / Kuala_Lumpur</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kuwait</masterLabel>
            <translation>Asien / Kuwait</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Manila</masterLabel>
            <translation>Asien / Manila</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Rangoon</masterLabel>
            <translation>Asien / Rangoon</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Riyadh</masterLabel>
            <translation>Asien / Riyadh</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Seoul</masterLabel>
            <translation>Asien / Seoul</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Shanghai</masterLabel>
            <translation>Asien / Shanghai</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Singapore</masterLabel>
            <translation>Asien / Singapore</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Taipei</masterLabel>
            <translation>Asien / Taipei</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tashkent</masterLabel>
            <translation>Asien / Tashkent</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tbilisi</masterLabel>
            <translation>Asien / Tbilisi</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tehran</masterLabel>
            <translation>Asien / Tehran</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tokyo</masterLabel>
            <translation>Asien / Tokyo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Yekaterinburg</masterLabel>
            <translation>Asien / Jekaterinburg</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Yerevan</masterLabel>
            <translation>Asien / Yerevan</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/Azores</masterLabel>
            <translation>Atlantic / Azores</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/Bermuda</masterLabel>
            <translation>Atlantic / Bermuda</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/Cape_Verde</masterLabel>
            <translation>Atlantic / Cape_Verde</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/South_Georgia</masterLabel>
            <translation>Atlantic / South_</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Adelaide</masterLabel>
            <translation>Australien / Adelaide</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Brisbane</masterLabel>
            <translation>Australien / Brisbane</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Darwin</masterLabel>
            <translation>Australien / Darwin</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Lord_Howe</masterLabel>
            <translation>Australien / Lord_Howe</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Perth</masterLabel>
            <translation>Australien / Perth</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Sydney</masterLabel>
            <translation>Australien / Sydney</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Amsterdam</masterLabel>
            <translation>Europa / Amsterdam</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Athens</masterLabel>
            <translation>Europa / Athen</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Berlin</masterLabel>
            <translation>Europe / Berlin</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Brussels</masterLabel>
            <translation>Europa / Bruxelles</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Bucharest</masterLabel>
            <translation>Europa / Bukarest</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Dublin</masterLabel>
            <translation>Europa / Dublin</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Helsinki</masterLabel>
            <translation>Europa / Helsinki</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Istanbul</masterLabel>
            <translation>Europa / Istanbul</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Lisbon</masterLabel>
            <translation>Europa / Lissabon</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/London</masterLabel>
            <translation>Europa / London</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Minsk</masterLabel>
            <translation>Europa / Minsk</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Moscow</masterLabel>
            <translation>Europa / Moskva</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Paris</masterLabel>
            <translation>Europa / Paris</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Prague</masterLabel>
            <translation>Europa / Prag</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Rome</masterLabel>
            <translation>Europa / Rom</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GMT</masterLabel>
            <translation>GMT</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Auckland</masterLabel>
            <translation>Pacific / Auckland</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Chatham</masterLabel>
            <translation>Pacific / Chatham</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Enderbury</masterLabel>
            <translation>Pacific / Enderbury</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Fiji</masterLabel>
            <translation>Pacific / Fiji</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Gambier</masterLabel>
            <translation>Stillehavet / Gambier</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Guadalcanal</masterLabel>
            <translation>Stillehavet / Guadalcanal</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Honolulu</masterLabel>
            <translation>Pacific / Honolulu</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Kiritimati</masterLabel>
            <translation>Stillehavet / Kiritimati</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Marquesas</masterLabel>
            <translation>Pacific / Marquesas</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Niue</masterLabel>
            <translation>Pacific / Niue</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Norfolk</masterLabel>
            <translation>Pacific / Norfolk</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Pago_Pago</masterLabel>
            <translation>Pacific / Pago_Pago</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Pitcairn</masterLabel>
            <translation>Pacific / Pitcairn</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Tongatapu</masterLabel>
            <translation>Stillehavet / Tongatapu</translation>
        </picklistValues>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Hold navn</nameFieldLabel>
    <validationRules>
        <errorMessage><!-- A project must be active to associate it with a team. --></errorMessage>
        <name>pse__Active_Project</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- One or both of the email address fields do not contain a value. --></errorMessage>
        <name>pse__Other_Email_Address_Fields_are_Blank</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- One or both of the email address fields contains one or more invalid email addresses. --></errorMessage>
        <name>pse__Validate_Other_Email_Address_Fields</name>
    </validationRules>
    <webLinks>
        <label><!-- Manage_Team --></label>
        <name>pse__Manage_Team</name>
    </webLinks>
    <webLinks>
        <label><!-- Open_Team_Scheduler --></label>
        <name>pse__Open_Team_Scheduler</name>
    </webLinks>
</CustomObjectTranslation>
