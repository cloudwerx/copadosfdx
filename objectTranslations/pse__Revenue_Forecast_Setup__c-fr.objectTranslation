<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Configuration des prévisions de revenus</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Configuration des prévisions de revenus</value>
    </caseValues>
    <fields>
        <help>Indique l’enregistrement de configuration de prévision de revenu utilisé pour les calculs de prévision de revenu. Vous ne pouvez avoir qu’un seul enregistrement de configuration de prévision de revenu actif.</help>
        <label><!-- Active --></label>
        <name>pse__Active__c</name>
    </fields>
    <fields>
        <help>Nom d’API du champ défini sur l’objet Détail de la version de prévision de revenus qui contient les champs de projet, d’opportunité et de jalon que vous souhaitez utiliser comme filtres personnalisés sur la page Vérifier la version de prévision.</help>
        <label><!-- Custom Filter Field Set --></label>
        <name>pse__Custom_Filter_Field_Set__c</name>
    </fields>
    <fields>
        <help>Si cette option est sélectionnée, les opportunités sont exclues des versions des prévisions de revenus.</help>
        <label><!-- Exclude Opportunities for Versions --></label>
        <name>pse__Exclude_Opportunities_For_Versions__c</name>
    </fields>
    <fields>
        <help>Si cette option est sélectionnée, les opportunités sont exclues des travaux planifiés des prévisions de revenus.</help>
        <label><!-- Exclude Opportunities --></label>
        <name>pse__Exclude_Opportunities__c</name>
    </fields>
    <fields>
        <help>Si cette option est sélectionnée, les probabilités ne sont pas appliquées aux prévisions de revenus des opportunités.</help>
        <label><!-- Exclude Probability from Opportunities --></label>
        <name>pse__Exclude_Opportunity_Probabilities__c</name>
    </fields>
    <fields>
        <help>Sélectionnez cette option pour exclure les revenus liés aux demandes de ressources détenues sur les projets et les jalons.</help>
        <label><!-- Exclude Resource Requests on Project --></label>
        <name>pse__Exclude_Resource_Requests_On_Project__c</name>
    </fields>
    <fields>
        <help>Si cette option est sélectionnée, les chiffres réels de reconnaissance des revenus issus de l’intégration avec Revenue Management ne sont plus inclus dans les calculs des prévisions de revenus.</help>
        <label><!-- Exclude Revenue Recognition Actuals --></label>
        <name>pse__Exclude_Revenue_Recognition_Actuals__c</name>
    </fields>
    <fields>
        <help>Si cette option est sélectionnée, tous les revenus non planifiés sur les projets ou les jalons sont exclus des versions de prévision des revenus.</help>
        <label><!-- Exclude Unscheduled Revenue for Versions --></label>
        <name>pse__Exclude_Unscheduled_Revenue_For_Versions__c</name>
    </fields>
    <fields>
        <help>Nombre d’enregistrements à traiter en même temps, tels que les fiches de présence et les dépenses.</help>
        <label><!-- Forecast Factor Batch Size --></label>
        <name>pse__Forecast_Factor_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Nom d’API du champ défini sur l’objet Jalon qui contrôle les détails de survol affichés pour un jalon dans la grille Répartition des prévisions. Si vous n’entrez pas de jeu de champs, les détails de survol par défaut s’appliquent.</help>
        <label><!-- Hover Details Field Set on Milestone --></label>
        <name>pse__Hover_Details_Field_Set_On_Milestone__c</name>
    </fields>
    <fields>
        <help>Nom d’API du champ défini sur l’objet Opportunité qui contrôle les détails de survol affichés pour une opportunité dans la grille Répartition des prévisions. Si vous n’entrez pas de jeu de champs, les détails de survol par défaut s’appliquent.</help>
        <label><!-- Hover Details Field Set on Opportunity --></label>
        <name>pse__Hover_Details_Field_Set_On_Opportunity__c</name>
    </fields>
    <fields>
        <help>Nom d’API du champ défini sur l’objet Projet qui contrôle les détails de survol affichés pour un projet dans la grille Répartition des prévisions. Si vous n’entrez pas de jeu de champs, les détails de survol par défaut s’appliquent.</help>
        <label><!-- Hover Details Field Set on Project --></label>
        <name>pse__Hover_Details_Field_Set_On_Project__c</name>
    </fields>
    <fields>
        <help>Si cette option est sélectionnée, le seuil du meilleur cas est appliqué aux prévisions de revenus d’opportunité.</help>
        <label><!-- Include Best Case --></label>
        <name>pse__Include_Best_Case__c</name>
    </fields>
    <fields>
        <help>Si cette option est sélectionnée, le seuil le plus défavorable est appliqué aux prévisions de revenus d’opportunité.</help>
        <label><!-- Include Worst Case --></label>
        <name>pse__Include_Worst_Case__c</name>
    </fields>
    <fields>
        <help>Nombre d’opportunités et d’éléments de campagne d’opportunité à traiter simultanément.</help>
        <label><!-- Opportunity Batch Size --></label>
        <name>pse__Opportunity_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Probabilité en pourcentage minimum pour qu’une opportunité soit incluse dans le meilleur scénario.</help>
        <label><!-- Opportunity Best Case Threshold (%) --></label>
        <name>pse__Opportunity_Best_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Le pourcentage de probabilité minimum pour qu’une opportunité soit incluse dans le scénario attendu.</help>
        <label><!-- Opportunity Expected Threshold (%) --></label>
        <name>pse__Opportunity_Expected_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Probabilité en pourcentage minimum pour qu’une opportunité soit incluse dans le pire des cas.</help>
        <label><!-- Opportunity Worst Case Threshold (%) --></label>
        <name>pse__Opportunity_Worst_Case_Threshold__c</name>
    </fields>
    <fields>
        <help>Nombre de projets à traiter en même temps.</help>
        <label><!-- Project Batch Size --></label>
        <name>pse__Project_Batch_Size__c</name>
    </fields>
    <fields>
        <help>Entrez le nom d’API du champ sur l’objet Opportunité que vous souhaitez utiliser comme date de fin de projet attendue.</help>
        <label><!-- Expected Project End Date Field on Opp --></label>
        <name>pse__Project_End_Date_on_Opportunity__c</name>
    </fields>
    <fields>
        <help>Entrez le nom d’API du champ sur l’objet Opportunité que vous souhaitez utiliser comme date de début de projet attendue.</help>
        <label><!-- Expected Project Start Date Field on Opp --></label>
        <name>pse__Project_Start_Date_on_Opportunity__c</name>
    </fields>
    <fields>
        <help>Si cette option est sélectionnée lorsque vous n’utilisez pas l’intégration entre PSA et Revenue Management, la prévision des revenus conserve les chiffres réels de la précédente exécution des prévisions pour toute période fermée. Tout revenu en attente restant est déplacé vers la période d’ouverture suivante.</help>
        <label><!-- Retain Pending Revenue in Closed Periods --></label>
        <name>pse__Retain_Pending_Revenue_In_Closed_Periods__c</name>
    </fields>
    <fields>
        <help>Sélectionnez le nom d’API du champ sur l’objet Projet que vous souhaitez utiliser pour calculer la valeur dans le champ% heures complétées pour la reconnaissance. Si nécessaire, vous pouvez créer votre propre champ et l’ajouter à la liste de sélection.</help>
        <label><!-- Total Hours Field on Project --></label>
        <name>pse__Total_Hours_Field_on_Project__c</name>
        <picklistValues>
            <masterLabel>pse__Estimated_Hours_at_Completion__c</masterLabel>
            <translation>pse__Estimated_Hours_at_Completion__c</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>pse__Planned_Hours__c</masterLabel>
            <translation>pse__Planned_Hours__c</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Nombre d’enregistrements de détails de version à traiter en même temps.</help>
        <label><!-- Version Detail Batch Size --></label>
        <name>pse__Version_Detail_Batch_Size__c</name>
    </fields>
    <fields>
        <help>La période de prévision couverte par la version des prévisions de revenus.</help>
        <label><!-- Version Forecast Period --></label>
        <name>pse__Version_Forecast_Period__c</name>
        <picklistValues>
            <masterLabel>Current Quarter</masterLabel>
            <translation>Trimestre en cours</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Current Year</masterLabel>
            <translation>Année actuelle</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Current and Next Quarter</masterLabel>
            <translation>Trimestre actuel et prochain</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 12 Months</masterLabel>
            <translation>Rolling 12 mois</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 3 Months</masterLabel>
            <translation>Rolling 3 mois</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rolling 6 Months</masterLabel>
            <translation>Rolling 6 mois</translation>
        </picklistValues>
    </fields>
    <fields>
        <help>Objet sous lequel les projets et opportunités sont regroupés sur la page Vérifier la version de prévision.</help>
        <label><!-- Object for Version Grouping --></label>
        <name>pse__Version_Grouping_Primary__c</name>
        <picklistValues>
            <masterLabel>Group</masterLabel>
            <translation>Groupe</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Practice</masterLabel>
            <translation>Entraine toi</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Region</masterLabel>
            <translation>Région</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nom d’installation de prévision de reven</nameFieldLabel>
    <startsWith>Vowel</startsWith>
</CustomObjectTranslation>
