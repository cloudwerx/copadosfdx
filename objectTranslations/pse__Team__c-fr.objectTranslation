<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Équipe</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Équipes</value>
    </caseValues>
    <fields>
        <help>Lorsque la valeur est true, la planification est envoyée à tous les membres de l&apos;équipe lorsque vous cliquez sur le bouton Envoyer la planification.</help>
        <label><!-- All Team Members --></label>
        <name>pse__All_Team_Members__c</name>
    </fields>
    <fields>
        <help>Lorsqu&apos;elle est vraie, une notification est envoyée à tous les membres de l&apos;équipe avec un accès en modification lorsqu&apos;un programme d&apos;équipe est échangé.</help>
        <label><!-- Anyone with Edit Access --></label>
        <name>pse__Anyone_With_Edit_Access__c</name>
    </fields>
    <fields>
        <help>La description de l&apos;équipe.</help>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <help>Sélectionnez cette option pour permettre aux membres de l&apos;équipe d&apos;échanger les équipes sur le calendrier d&apos;équipe associé. Ce champ est désélectionné par défaut.</help>
        <label><!-- Enable Swapping --></label>
        <name>pse__Enable_Swapping__c</name>
    </fields>
    <fields>
        <help>Lorsqu&apos;elle est vraie, une notification est envoyée au membre de l&apos;équipe nouvellement affecté lorsqu&apos;un programme d&apos;équipe est échangé.</help>
        <label><!-- Currently Assigned Team Member --></label>
        <name>pse__Now_Assigned__c</name>
    </fields>
    <fields>
        <help>Lorsqu&apos;elle est définie sur true, une notification est envoyée au membre de l&apos;équipe précédemment affecté lorsqu&apos;un programme d&apos;équipe est échangé.</help>
        <label><!-- Previously Assigned Team Member --></label>
        <name>pse__Previously_Assigned__c</name>
    </fields>
    <fields>
        <help>Le projet associé à l&apos;équipe.</help>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Teams --></relationshipLabel>
    </fields>
    <fields>
        <help>Spécifie le modèle de courrier électronique personnalisé à utiliser pour l&apos;envoi de programmes d&apos;équipe.</help>
        <label><!-- Custom Template Name (Schedule) --></label>
        <name>pse__Schedule_Custom_Template_Name__c</name>
    </fields>
    <fields>
        <help>Spécifie les adresses e-mail pour recevoir le planning de l&apos;équipe. La valeur est une liste d&apos;adresses de messagerie séparées par des virgules.</help>
        <label><!-- Other Email Addresses (Schedule) --></label>
        <name>pse__Schedule_Other_Email_Addresses__c</name>
    </fields>
    <fields>
        <help>Lorsque la valeur est true, la planification est envoyée aux adresses électroniques spécifiées dans le champ Autres adresses électroniques lorsque vous cliquez sur le bouton Envoyer la planification.</help>
        <label><!-- Send Email to Others (Schedule) --></label>
        <name>pse__Schedule_Send_Email_To_Others__c</name>
    </fields>
    <fields>
        <help>Lorsque la valeur est true, l&apos;e-mail de planification utilise le modèle d&apos;e-mail défini dans le champ Nom du modèle personnalisé (Planification).</help>
        <label><!-- Use Custom Template (Schedule) --></label>
        <name>pse__Schedule_Use_Custom_Template__c</name>
    </fields>
    <fields>
        <help>Spécifie le modèle de courrier électronique personnalisé à utiliser pour envoyer des notifications d&apos;échange d&apos;horaires d&apos;équipe.</help>
        <label><!-- Custom Template Name (Swap) --></label>
        <name>pse__Swap_Custom_Template_Name__c</name>
    </fields>
    <fields>
        <help>Spécifie les adresses e-mail pour recevoir une notification lorsqu&apos;un programme d&apos;équipe est échangé. La valeur est une liste d&apos;adresses de messagerie séparées par des virgules.</help>
        <label><!-- Other Email Addresses (Swap) --></label>
        <name>pse__Swap_Other_Email_Addresses__c</name>
    </fields>
    <fields>
        <help>Lorsqu&apos;elle est vraie, une notification est envoyée à toutes les adresses électroniques définies dans le champ Envoyer un courrier électronique à d&apos;autres personnes (échange) lorsqu&apos;un programme d&apos;équipe est échangé.</help>
        <label><!-- Send Email to Others (Swap) --></label>
        <name>pse__Swap_Send_Email_To_Others__c</name>
    </fields>
    <fields>
        <help>Lorsque la valeur est true, l&apos;e-mail de notification d&apos;échange utilise le modèle d&apos;e-mail défini dans le champ Nom du modèle personnalisé (Swap).</help>
        <label><!-- Use Custom Template (Swap) --></label>
        <name>pse__Swap_Use_Custom_Template__c</name>
    </fields>
    <fields>
        <help>La ressource à laquelle appartient l&apos;équipe.</help>
        <label><!-- Team Owner --></label>
        <name>pse__Team_Owner__c</name>
        <relationshipLabel><!-- Team --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Time Zone --></label>
        <name>pse__Time_Zone__c</name>
        <picklistValues>
            <masterLabel>Africa/Algiers</masterLabel>
            <translation>Afrique / Alger</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Cairo</masterLabel>
            <translation>Afrique / Le Caire</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Casablanca</masterLabel>
            <translation>Afrique / Casablanca</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Johannesburg</masterLabel>
            <translation>Afrique / Johannesburg</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Africa/Nairobi</masterLabel>
            <translation>Afrique / Nairobi</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Adak</masterLabel>
            <translation>Amérique / Adak</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Anchorage</masterLabel>
            <translation>Amérique / Anchorage</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Argentina/Buenos_Aires</masterLabel>
            <translation>Amérique / Argentine / Buenos_Aires</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Bogota</masterLabel>
            <translation>Amérique / Bogota</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Caracas</masterLabel>
            <translation>Amérique / Caracas</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Chicago</masterLabel>
            <translation>Amérique / Chicago</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Denver</masterLabel>
            <translation>Amérique / Denver</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/El_Salvador</masterLabel>
            <translation>Amérique / El_Salvador</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Halifax</masterLabel>
            <translation>Amérique / Halifax</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Indiana/Indianapolis</masterLabel>
            <translation>Amérique / Indiana / Indianapolis</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Lima</masterLabel>
            <translation>Amérique / Lima</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Los_Angeles</masterLabel>
            <translation>Amérique / Los_Angeles</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Mazatlan</masterLabel>
            <translation>Amérique / Mazatlan</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Mexico_City</masterLabel>
            <translation>Amérique / Mexico_City</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/New_York</masterLabel>
            <translation>Amérique / New_York</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Panama</masterLabel>
            <translation>Amérique / Panama</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Phoenix</masterLabel>
            <translation>Amérique / Phoenix</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Puerto_Rico</masterLabel>
            <translation>Amérique / Puerto_Rico</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Santiago</masterLabel>
            <translation>Amérique / Santiago</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Sao_Paulo</masterLabel>
            <translation>Amérique / Sao_Paulo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Scoresbysund</masterLabel>
            <translation>America / Scoresbysund</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/St_Johns</masterLabel>
            <translation>America / St_Johns</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>America/Tijuana</masterLabel>
            <translation>Amérique / Tijuana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Baghdad</masterLabel>
            <translation>Asie / Bagdad</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Baku</masterLabel>
            <translation>Asie / Bakou</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Bangkok</masterLabel>
            <translation>Asie / Bangkok</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Beirut</masterLabel>
            <translation>Asie / Beyrouth</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Colombo</masterLabel>
            <translation>Asie / Colombo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Dhaka</masterLabel>
            <translation>Asie / Dhaka</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Dubai</masterLabel>
            <translation>Asie / Dubaï</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Ho_Chi_Minh</masterLabel>
            <translation>Asie / Ho_Chi_Minh</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Hong_Kong</masterLabel>
            <translation>Asie / Hong_Kong</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Jakarta</masterLabel>
            <translation>Asie / Jakarta</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Jerusalem</masterLabel>
            <translation>Asie / Jérusalem</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kabul</masterLabel>
            <translation>Asie / Kaboul</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kamchatka</masterLabel>
            <translation>Asie / Kamtchatka</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Karachi</masterLabel>
            <translation>Asie / Karachi</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kathmandu</masterLabel>
            <translation>Asie / Katmandou</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kolkata</masterLabel>
            <translation>Asie / Kolkata</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kuala_Lumpur</masterLabel>
            <translation>Asie / Kuala_Lumpur</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Kuwait</masterLabel>
            <translation>Asie / Koweït</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Manila</masterLabel>
            <translation>Asie / Manille</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Rangoon</masterLabel>
            <translation>Asie / Rangoon</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Riyadh</masterLabel>
            <translation>Asie / Riyad</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Seoul</masterLabel>
            <translation>Asie / Séoul</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Shanghai</masterLabel>
            <translation>Asie / Shanghai</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Singapore</masterLabel>
            <translation>Asie / Singapour</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Taipei</masterLabel>
            <translation>Asie / Taipei</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tashkent</masterLabel>
            <translation>Asie / Tachkent</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tbilisi</masterLabel>
            <translation>Asie / Tbilissi</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tehran</masterLabel>
            <translation>Asie / Téhéran</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Tokyo</masterLabel>
            <translation>Asie / Tokyo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Yekaterinburg</masterLabel>
            <translation>Asie / Iekaterinbourg</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Asia/Yerevan</masterLabel>
            <translation>Asie / Erevan</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/Azores</masterLabel>
            <translation>Atlantique / Açores</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/Bermuda</masterLabel>
            <translation>Atlantique / Bermudes</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/Cape_Verde</masterLabel>
            <translation>Atlantique / Cape_Verde</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Atlantic/South_Georgia</masterLabel>
            <translation>Atlantic / South_Georgia</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Adelaide</masterLabel>
            <translation>Australie / Adelaide</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Brisbane</masterLabel>
            <translation>Australie / Brisbane</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Darwin</masterLabel>
            <translation>Australie / Darwin</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Lord_Howe</masterLabel>
            <translation>Australie / Lord_Howe</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Perth</masterLabel>
            <translation>Australie / Perth</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Australia/Sydney</masterLabel>
            <translation>Australie / Sydney</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Amsterdam</masterLabel>
            <translation>Europe / Amsterdam</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Athens</masterLabel>
            <translation>Europe / Athènes</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Berlin</masterLabel>
            <translation>Europe / Berlin</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Brussels</masterLabel>
            <translation>Europe / Bruxelles</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Bucharest</masterLabel>
            <translation>Europe / Bucarest</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Dublin</masterLabel>
            <translation>Europe / Dublin</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Helsinki</masterLabel>
            <translation>Europe / Helsinki</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Istanbul</masterLabel>
            <translation>Europe / Istanbul</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Lisbon</masterLabel>
            <translation>Europe / Lisbonne</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/London</masterLabel>
            <translation>Europe / Londres</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Minsk</masterLabel>
            <translation>Europe / Minsk</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Moscow</masterLabel>
            <translation>Europe / Moscou</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Paris</masterLabel>
            <translation>Europe / Paris</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Prague</masterLabel>
            <translation>Europe / Prague</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Europe/Rome</masterLabel>
            <translation>Europe / Rome</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GMT</masterLabel>
            <translation>GMT</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Auckland</masterLabel>
            <translation>Pacifique / Auckland</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Chatham</masterLabel>
            <translation>Pacifique / Chatham</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Enderbury</masterLabel>
            <translation>Pacifique / Enderbury</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Fiji</masterLabel>
            <translation>Pacifique / Fidji</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Gambier</masterLabel>
            <translation>Pacifique / Gambier</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Guadalcanal</masterLabel>
            <translation>Pacifique / Guadalcanal</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Honolulu</masterLabel>
            <translation>Pacifique / Honolulu</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Kiritimati</masterLabel>
            <translation>Pacifique / Kiritimati</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Marquesas</masterLabel>
            <translation>Pacifique / Marquises</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Niue</masterLabel>
            <translation>Pacifique / Nioué</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Norfolk</masterLabel>
            <translation>Pacifique / Norfolk</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Pago_Pago</masterLabel>
            <translation>Pacifique / Pago_Pago</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Pitcairn</masterLabel>
            <translation>Pacifique / Pitcairn</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Pacific/Tongatapu</masterLabel>
            <translation>Pacifique / Tongatapu</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nom de l&apos;équipe</nameFieldLabel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <errorMessage><!-- A project must be active to associate it with a team. --></errorMessage>
        <name>pse__Active_Project</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- One or both of the email address fields do not contain a value. --></errorMessage>
        <name>pse__Other_Email_Address_Fields_are_Blank</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- One or both of the email address fields contains one or more invalid email addresses. --></errorMessage>
        <name>pse__Validate_Other_Email_Address_Fields</name>
    </validationRules>
    <webLinks>
        <label><!-- Manage_Team --></label>
        <name>pse__Manage_Team</name>
    </webLinks>
    <webLinks>
        <label><!-- Open_Team_Scheduler --></label>
        <name>pse__Open_Team_Scheduler</name>
    </webLinks>
</CustomObjectTranslation>
