<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>履约义务</value>
    </caseValues>
    <fields>
        <help>有关性能义务的信息。通常，这将是帐户名，但可以是任何信息。</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <help>指示源记录是否处于活动状态。</help>
        <label><!-- Active --></label>
        <name>ffrr__Active__c</name>
    </fields>
    <fields>
        <help>由于绩效义务的累积舍入错误导致的收入和收入合同的分配收入之间的差异进行调整。</help>
        <label><!-- Allocated Revenue Rounding Adjustment --></label>
        <name>ffrr__AllocatedRevenueAdjustment__c</name>
    </fields>
    <fields>
        <help>通过在此处输入不同的值来覆盖分配给此Performance Obligation的计算金额。</help>
        <label><!-- Allocated Revenue Override --></label>
        <name>ffrr__AllocatedRevenueOverride__c</name>
    </fields>
    <fields>
        <help>通过收入分配计算分配给此绩效义务的金额。此值用于收入确认。</help>
        <label><!-- Allocated Revenue --></label>
        <name>ffrr__AllocatedRevenue__c</name>
    </fields>
    <fields>
        <help>用于在收入合同中分配收入的比率。</help>
        <label><!-- Allocation Ratio --></label>
        <name>ffrr__AllocationRatio__c</name>
    </fields>
    <fields>
        <help>显示此履约义务的收入是否已完全分配，或需要重新分配。</help>
        <label><!-- Allocation Status --></label>
        <name>ffrr__AllocationStatus__c</name>
    </fields>
    <fields>
        <label><!-- Amortized to Date --></label>
        <name>ffrr__AmortizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Balance Sheet GLA --></label>
        <name>ffrr__BalanceSheetAccount__c</name>
    </fields>
    <fields>
        <help>指示源记录是否完整。</help>
        <label><!-- Completed --></label>
        <name>ffrr__Completed__c</name>
    </fields>
    <fields>
        <help>控制成本绩效义务项目。用于填充Performance Obligation上的字段。</help>
        <label><!-- Controlling POLI (Cost) --></label>
        <name>ffrr__ControllingCostPOLI__c</name>
        <relationshipLabel><!-- Controlling POLIs (Cost) --></relationshipLabel>
    </fields>
    <fields>
        <help>控制收入绩效义务项目。用于填充Performance Obligation上的字段。</help>
        <label><!-- Controlling POLI (Revenue) --></label>
        <name>ffrr__ControllingPOLI__c</name>
        <relationshipLabel><!-- Controlling POLIs (Revenue) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Balance Sheet GLA (Cost) --></label>
        <name>ffrr__CostBalanceSheetAccount__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center --></label>
        <name>ffrr__CostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Cost Center (Cost) --></label>
        <name>ffrr__CostCostCenter__c</name>
    </fields>
    <fields>
        <label><!-- Income Statement GLA (Cost) --></label>
        <name>ffrr__CostIncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>此履约义务的成本</help>
        <label><!-- Cost --></label>
        <name>ffrr__Cost__c</name>
    </fields>
    <fields>
        <help>记录货币的小数位数。</help>
        <label><!-- Currency Decimal Places --></label>
        <name>ffrr__CurrencyDP__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <label><!-- End Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <help>表示此性能义务与收入相关。</help>
        <label><!-- Has Revenue --></label>
        <name>ffrr__HasRevenue__c</name>
    </fields>
    <fields>
        <label><!-- Income Statement GLA --></label>
        <name>ffrr__IncomeStatementAccount__c</name>
    </fields>
    <fields>
        <help>与SSP价值为零的收入相关的履约义务项目数。</help>
        <label><!-- Null SSP Count --></label>
        <name>ffrr__NullSSPCount__c</name>
    </fields>
    <fields>
        <label><!-- % Complete --></label>
        <name>ffrr__PercentComplete__c</name>
    </fields>
    <fields>
        <help>如果已分配收入且源记录处于活动状态，则会检查此情况。如果未选中，则此记录不能包含在收入确认中。</help>
        <label><!-- Ready for Revenue Recognition --></label>
        <name>ffrr__ReadyForRevenueRecognition__c</name>
    </fields>
    <fields>
        <label><!-- Recognized to Date --></label>
        <name>ffrr__RecognizedToDate__c</name>
    </fields>
    <fields>
        <label><!-- Revenue Contract --></label>
        <name>ffrr__RevenueContract__c</name>
        <relationshipLabel><!-- Performance Obligations --></relationshipLabel>
    </fields>
    <fields>
        <help>与收入相关的履约义务项目数。</help>
        <label><!-- Revenue Count --></label>
        <name>ffrr__RevenueCount__c</name>
    </fields>
    <fields>
        <help>设置后，表示此履约义务的所有收入均已得到全面确认，且所有成本均已全额摊销。</help>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionComplete__c</name>
    </fields>
    <fields>
        <help>此履约义务的收入</help>
        <label><!-- Revenue --></label>
        <name>ffrr__Revenue__c</name>
    </fields>
    <fields>
        <help>通过在此输入不同的值来覆盖源记录中的独立销售价格。</help>
        <label><!-- SSP Override --></label>
        <name>ffrr__SSPOverride__c</name>
    </fields>
    <fields>
        <help>将用于此履约义务的独立销售价格。如果已填充，则使用Total SSP或SSP Override。</help>
        <label><!-- SSP --></label>
        <name>ffrr__SSP__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>所有关联的履约义务项目的独立销售价格总和。</help>
        <label><!-- Total SSP --></label>
        <name>ffrr__TotalSSP__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__ffrrTemplate__c</name>
        <relationshipLabel><!-- Performance Obligations --></relationshipLabel>
    </fields>
    <nameFieldLabel>履约义务编号</nameFieldLabel>
    <webLinks>
        <label><!-- AllocateRevenue --></label>
        <name>ffrr__AllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- Update --></label>
        <name>ffrr__Update</name>
    </webLinks>
</CustomObjectTranslation>
