<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Cálculo do Backlog</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Cálculos de atraso</value>
    </caseValues>
    <fields>
        <help>Um campo interno usado pelo PSE - não se destina a exibição aos usuários.</help>
        <label><!-- Batch Id --></label>
        <name>pse__Batch_Id__c</name>
    </fields>
    <fields>
        <help>Se deve calcular o backlog para projetos contidos na região, prática ou grupo.</help>
        <label><!-- Calculate Project Backlog --></label>
        <name>pse__Calculate_Project_Backlog__c</name>
    </fields>
    <fields>
        <help>Atualize os registros de região, prática, grupo ou projeto com valores diretamente dos registros de detalhes da lista de pendências, além de criar registros de objeto de cálculo e de detalhe da lista de pendências.</help>
        <label><!-- Copy Fields for Current Time Period --></label>
        <name>pse__Copy_Fields_for_Current_Time_Period__c</name>
    </fields>
    <fields>
        <help>A data de término para cálculos de backlog. Se nenhuma data de término for especificada, todos os registros da data de início serão processados. Isso é recomendado ao calcular a carteira não programada.</help>
        <label><!-- End Date --></label>
        <name>pse__End_Date__c</name>
    </fields>
    <fields>
        <label><!-- Error Details --></label>
        <name>pse__Error_Details__c</name>
    </fields>
    <fields>
        <label><!-- Group --></label>
        <name>pse__Group__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <help>Para calcular a carteira de pedidos para todos os subníveis da região, prática ou grupo selecionado. Este campo será ignorado se o backlog for calculado apenas para um recurso.</help>
        <label><!-- Include Sublevels --></label>
        <name>pse__Include_Sublevels__c</name>
    </fields>
    <fields>
        <label><!-- Is Report Master --></label>
        <name>pse__Is_Report_Master__c</name>
    </fields>
    <fields>
        <label><!-- Post Process Batch Id --></label>
        <name>pse__Post_Process_Batch_Id__c</name>
    </fields>
    <fields>
        <label><!-- Practice --></label>
        <name>pse__Practice__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Region --></label>
        <name>pse__Region__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>pse__Resource__c</name>
        <relationshipLabel><!-- Backlog Calculations --></relationshipLabel>
    </fields>
    <fields>
        <help>Substitua os registros de detalhes do backlog existentes para as mesmas regiões, práticas, grupos, projetos e períodos de tempo associados. Recomendamos que você selecione esta opção para evitar o uso de espaço desnecessário.</help>
        <label><!-- Reuse Detail Objects --></label>
        <name>pse__Reuse_Detail_Objects__c</name>
    </fields>
    <fields>
        <help>Definir este valor definirá o campo da data de início automaticamente</help>
        <label><!-- Start Calculating From --></label>
        <name>pse__Start_Calculating_From__c</name>
        <picklistValues>
            <masterLabel>Start of next week</masterLabel>
            <translation>Começo da próxima semana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Start of this week</masterLabel>
            <translation>Começo desta semana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Today</masterLabel>
            <translation>Hoje</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>pse__Start_Date__c</name>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Complete</masterLabel>
            <translation>Completo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Error Occurred</masterLabel>
            <translation>Ocorreu um erro</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>In Progress</masterLabel>
            <translation>Em progresso</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Not Started</masterLabel>
            <translation>não foi iniciado</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Post Processing</masterLabel>
            <translation>Pós-processamento</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Time Period Types --></label>
        <name>pse__Time_Period_Types__c</name>
        <picklistValues>
            <masterLabel>Month</masterLabel>
            <translation>Mês</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Other</masterLabel>
            <translation>De outros</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Perpetual</masterLabel>
            <translation>Perpétuo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Quarter</masterLabel>
            <translation>Trimestre</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Week</masterLabel>
            <translation>Semana</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Year</masterLabel>
            <translation>Ano</translation>
        </picklistValues>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nome de cálculo do backlog</nameFieldLabel>
    <validationRules>
        <errorMessage><!-- End date cannot occur before start date --></errorMessage>
        <name>pse__check_end_date</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Only one of Region, Practice, Group, or Project may be specified. --></errorMessage>
        <name>pse__only_one_source_allowed</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- One of Region, Practice, Group, or Project is required --></errorMessage>
        <name>pse__source_required</name>
    </validationRules>
    <webLinks>
        <label><!-- Calculate_Backlog --></label>
        <name>pse__Calculate_Backlog</name>
    </webLinks>
</CustomObjectTranslation>
