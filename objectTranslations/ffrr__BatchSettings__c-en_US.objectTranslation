<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- The number of committed Revenue Recognition Transaction Lines per batch processed for which Actual vs. Forecast Lines will be created (default:200). --></help>
        <label><!-- Actual vs. Forecast Actuals Batch Size --></label>
        <name>ffrr__AVFCreateFromActualsBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Revenue Forecast Transaction Lines per batch processed for which Actual vs. Forecast Lines will be created (default:200). --></help>
        <label><!-- Actual vs. Forecast Forecast Batch Size --></label>
        <name>ffrr__AVFCreateFromForecastBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Actual vs. Forecast Lines per batch processed (default:1000). --></help>
        <label><!-- Actual vs. Forecast Delete Batch Size --></label>
        <name>ffrr__AVFDeleteLinesBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- Deprecated: The number of source/staging records to be processed/deleted per batch processed (default:1000). --></help>
        <label><!-- Deprecated: Old Staging Batch Size --></label>
        <name>ffrr__ActualsRetrieveLinesBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Revenue Recognition Transactions committed per batch processed (default:1). --></help>
        <label><!-- Actuals Transaction Commit Batch Size --></label>
        <name>ffrr__ActualsTransactionCommitBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of source records for which Revenue Recognition Transaction Lines are created per batch processed (default:500). --></help>
        <label><!-- Actuals Transaction Save Batch Size --></label>
        <name>ffrr__ActualsTransactionSaveBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Revenue Recognition Transactions summarized per batch processed (default:1). --></help>
        <label><!-- Actuals Transaction Summary Batch Size --></label>
        <name>ffrr__ActualsTransactionSummaryBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Draft Forecast records that will be deleted per batch processed when running the DeleteDraftForecasts batch process (default:50). --></help>
        <label><!-- Forecast Draft Transaction Delete Batch --></label>
        <name>ffrr__ForecastDraftTransactionDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- This is the batch size for the save batch that creates Forecast and Forecast Lines from Draft Lines (default:200). --></help>
        <label><!-- Forecast Transaction Save Batch Size --></label>
        <name>ffrr__ForecastTransactionSaveBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Forecast records that will be processed per batch when generating from source records (default:200). --></help>
        <label><!-- Generate Forecasts Batch Size --></label>
        <name>ffrr__GenerateForecastsBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Batch Tracking Control records with Action &apos;Create Grouping&apos; that will be deleted per batch processed when running the GroupingDeleteBatch batch process (default:1000). --></help>
        <label><!-- Grouping BTC Delete Batch Size --></label>
        <name>ffrr__GroupingBTCDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Staging records that will be processed to create the Grouping Summary records per batch processed (default:1000). --></help>
        <label><!-- Grouping Create Batch Size --></label>
        <name>ffrr__GroupingCreateBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The maximum number of source records that can be grouped synchronously (default:3000). --></help>
        <label><!-- Grouping Create Synchronous Limit --></label>
        <name>ffrr__GroupingCreateSynchronousLimit__c</name>
    </fields>
    <fields>
        <help><!-- The maximum number of grouping records that can be deleted synchronously (default:2000). --></help>
        <label><!-- Grouping Delete Synchronous Limit --></label>
        <name>ffrr__GroupingDeleteSynchronousLimit__c</name>
    </fields>
    <fields>
        <help><!-- The number of Grouping Summary records that will be deleted per batch processed when running the GroupingDeleteBatch batch process (default:1000). --></help>
        <label><!-- Grouping Summary Delete Batch Size --></label>
        <name>ffrr__GroupingSummaryDeleteBatchSize__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity Product Scheduler Date --></label>
        <name>ffrr__LastOpportunityProductMirrorBatch__c</name>
    </fields>
    <fields>
        <help><!-- The maximum number of source records processed in a single batch when creating or updating a revenue contract asynchronously (default:500). --></help>
        <label><!-- Manage RC Batch Size --></label>
        <name>ffrr__ManageRevenueContractBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The maximum number of source records that can be processed synchronously when creating or updating a revenue contract. If the number of records is equal to or greater than this value, all the records are processed in the background. The default is 100. --></help>
        <label><!-- Manage RC Synchronous Limit --></label>
        <name>ffrr__ManageRevenueContractSynchronousLimit__c</name>
    </fields>
    <fields>
        <help><!-- The number of Opportunity Product records for which Opportunity Product Mirror records will be created or updated per batch processed (default:1000). --></help>
        <label><!-- Opportunity Product Mirror Batch Size --></label>
        <name>ffrr__OpportunityProductMirrorBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Opportunity Product Mirror records to be deleted per batch processed (default:2000). --></help>
        <label><!-- Opp Product Mirror Delete Batch Size --></label>
        <name>ffrr__OpportunityProductMirrorDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Process Control records that will be deleted per batch processed when running the Staging Delete batch process (default:2000). --></help>
        <label><!-- Process Control Delete Batch Size --></label>
        <name>ffrr__ProcessControlDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Revenue Recognition Transactions per batch processed when creating FinancialForce Accounting journals (default:1). --></help>
        <label><!-- RRTs for Journal Creation Batch Size --></label>
        <name>ffrr__RRTToJournalCreationBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Batch Tracking Control records with Action &apos;Create Staging&apos; that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:1000). --></help>
        <label><!-- Staging BTC Delete Batch Size --></label>
        <name>ffrr__StagingBTCDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of staging records that will be processed per batch processed to create grouping summaries (default:1000). --></help>
        <label><!-- Staging Create Batch Size --></label>
        <name>ffrr__StagingCreateBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The maximum number of source records that can be processed synchronously (default:1000). --></help>
        <label><!-- Staging Create Synchronous Limit --></label>
        <name>ffrr__StagingCreateSynchronousLimit__c</name>
    </fields>
    <fields>
        <help><!-- The maximum number of staging records and grouping records that can be deleted synchronously. At and above this limit Batch Apex is used (default:1000). --></help>
        <label><!-- Staging Delete Synchronous Limit --></label>
        <name>ffrr__StagingDeleteSynchronousLimit__c</name>
    </fields>
    <fields>
        <help><!-- The number of Staging Detail records that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:1000). --></help>
        <label><!-- Staging Detail Delete Batch Size --></label>
        <name>ffrr__StagingDetailDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of records that will be processed to create Revenue Recognition Transaction from staging data per batch processed (default:1000). --></help>
        <label><!-- Staging Save Batch Size --></label>
        <name>ffrr__StagingSaveBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The maximum number of staging records that can be saved synchronously (default:500). --></help>
        <label><!-- Staging Save Synchronous Limit --></label>
        <name>ffrr__StagingSaveSynchronousLimit__c</name>
    </fields>
    <fields>
        <help><!-- The number of Staging Summary records that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:600). --></help>
        <label><!-- Staging Summary Delete Batch Size --></label>
        <name>ffrr__StagingSummaryDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of Staging Version records that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:1000). --></help>
        <label><!-- Staging Version Delete Batch Size --></label>
        <name>ffrr__StagingVersionDeleteBatchSize__c</name>
    </fields>
    <fields>
        <help><!-- The number of performance obligation line items that will be processed per batch when the Transfer Previously Recognized process runs (default:1000). --></help>
        <label><!-- Transfer Prev. Recognized Batch Size --></label>
        <name>ffrr__TransferPreviouslyRecognizedBatchSize__c</name>
    </fields>
</CustomObjectTranslation>
