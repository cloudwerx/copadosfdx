<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Utgiftsrapport</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Utgiftsrapport</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Utgiftsrapport</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Utgiftsrapporter</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Utgiftsrapporter</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Utgiftsrapporter</value>
    </caseValues>
    <fieldSets>
        <label><!-- ExpenseReportApprovalColumns --></label>
        <name>pse__ExpenseReportApprovalColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Report Editable Columns --></label>
        <name>pse__ExpenseReportEditableColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Report Read Only Columns --></label>
        <name>pse__ExpenseReportReadOnlyColumns</name>
    </fieldSets>
    <fieldSets>
        <label><!-- Expense Report Grid --></label>
        <name>pse__ExpenseRptGridFieldSet</name>
    </fieldSets>
    <fields>
        <label><!-- Expense Report Number --></label>
        <name>FFX_Expense_Report_Number__c</name>
    </fields>
    <fields>
        <label><!-- Is Resource Current User --></label>
        <name>psaws__Is_Resource_Current_User__c</name>
    </fields>
    <fields>
        <label><!-- Action: Update Include In Financials --></label>
        <name>pse__Action_Check_Include_In_Financials__c</name>
    </fields>
    <fields>
        <help>Hvis markert, tillater Admin å gjøre globale endringer i kostnadsrapporten, inkludert endringer i ER-prosjektet, ressurs, valuta eller dato, selv om Inkluder i finans er merket. Konfigeringskrav: Aktualsberegningsmodus må settes til &apos;Planlagt&apos;.</help>
        <label><!-- Admin Global Edit --></label>
        <name>pse__Admin_Global_Edit__c</name>
    </fields>
    <fields>
        <help>Denne avkrysningsruten bør kontrolleres når utgiftsrapporten er godkjent - vanligvis basert på statusfeltet.</help>
        <label><!-- Approved --></label>
        <name>pse__Approved__c</name>
    </fields>
    <fields>
        <label><!-- Approver --></label>
        <name>pse__Approver__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Assignment --></label>
        <name>pse__Assignment__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Lagrer revisjonsnotathistorikk. Når en bruker endrer et prosjekt eller en oppgave på en kostnadsrapport og revisjonsnotatene overstiger 255 tegn, blir eldre revisjonsnotater flyttet og lagt til her.</help>
        <label><!-- Audit Notes History --></label>
        <name>pse__Audit_Notes_History__c</name>
    </fields>
    <fields>
        <label><!-- Audit Notes --></label>
        <name>pse__Audit_Notes__c</name>
    </fields>
    <fields>
        <label><!-- Billable --></label>
        <name>pse__Billable__c</name>
    </fields>
    <fields>
        <label><!-- Lines Billed --></label>
        <name>pse__Billed__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>pse__Description__c</name>
    </fields>
    <fields>
        <help>Hvis det er merket, bør dette deaktivere noen PSA-triggerbasert automatisk innlevering for godkjenning for kostnadsrapporten.</help>
        <label><!-- Disable Approval Auto Submit --></label>
        <name>pse__Disable_Approval_Auto_Submit__c</name>
    </fields>
    <fields>
        <help>Hvis det er merket av, vil du som standard merke avkryssingsfeltet Ekskluder fra fakturering på barnekostnader, som har samme effekt som Faktura Hold, men som er ment å gjenspeile permanent utelukkelse fra Faktureringsgenerering.</help>
        <label><!-- Exclude from Billing --></label>
        <name>pse__Exclude_from_Billing__c</name>
    </fields>
    <fields>
        <label><!-- Expense Report Reference --></label>
        <name>pse__Expense_Report_Reference__c</name>
    </fields>
    <fields>
        <label><!-- First Expense Date --></label>
        <name>pse__First_Expense_Date__c</name>
    </fields>
    <fields>
        <label><!-- Include In Financials --></label>
        <name>pse__Include_In_Financials__c</name>
    </fields>
    <fields>
        <label><!-- Lines Invoiced --></label>
        <name>pse__Invoiced__c</name>
    </fields>
    <fields>
        <label><!-- Last Expense Date --></label>
        <name>pse__Last_Expense_Date__c</name>
    </fields>
    <fields>
        <label><!-- Milestone --></label>
        <name>pse__Milestone__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Overstyrer gruppen som barnets utgiftstransaksjoner vil kaste opp for gruppespørsmål, selv om prosjektet er i en annen gruppe. Vanligvis går en utgiftstransaksjoner opp til prosjektets eller ressursgruppens konsern basert på &quot;følger&quot; -regler.</help>
        <label><!-- Override Group --></label>
        <name>pse__Override_Group__c</name>
        <relationshipLabel><!-- Override Group For Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Overstyrer praksisen som barnets utgiftstransaksjoner vil rulle opp for praksisaktører, selv om prosjektet er i en annen praksis. Vanligvis går en utgiftstransaksjoner opp til prosjektets eller ressursens praksis basert på &quot;følger&quot; regler.</help>
        <label><!-- Override Practice --></label>
        <name>pse__Override_Practice__c</name>
        <relationshipLabel><!-- Override Practice For Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Overstyrer regionen som barnets utgiftstransaksjoner vil rulle opp for regionale aktører, selv om prosjektet ligger i en annen region. Vanligvis går en utgiftstransaksjoner opp til prosjektets eller ressursregionen basert på &quot;følger&quot; -reglene.</help>
        <label><!-- Override Region --></label>
        <name>pse__Override_Region__c</name>
        <relationshipLabel><!-- Override Region For Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project Expense Notes --></label>
        <name>pse__Project_Expense_Notes__c</name>
    </fields>
    <fields>
        <help>Oppsøk til prosjektmetodikk</help>
        <label><!-- Project Methodology --></label>
        <name>pse__Project_Methodology__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Oppslag til prosjektfase</help>
        <label><!-- Project Phase --></label>
        <name>pse__Project_Phase__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Project --></label>
        <name>pse__Project__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <help>Dette feltet viser valutakoden for 3 tegn for at ressursen skal refunderes.</help>
        <label><!-- Reimbursement Currency --></label>
        <name>pse__Reimbursement_Currency__c</name>
    </fields>
    <fields>
        <label><!-- Resource --></label>
        <name>pse__Resource__c</name>
        <relationshipLabel><!-- Expense Reports --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Status --></label>
        <name>pse__Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation>Godkjent</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation>Utkast</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation>Avvist</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Submitted</masterLabel>
            <translation>Sendt inn</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Submitted --></label>
        <name>pse__Submitted__c</name>
    </fields>
    <fields>
        <help>ID for utgiftsrapporten fra Tredjeparts Utgifter Applikasjon.</help>
        <label><!-- Third-Party Expenses App Report ID --></label>
        <name>pse__Third_Party_Expenses_App_Report_ID__c</name>
    </fields>
    <fields>
        <label><!-- Total Billable Amount --></label>
        <name>pse__Total_Billable_Amount__c</name>
    </fields>
    <fields>
        <label><!-- Total Non-Billable Amount --></label>
        <name>pse__Total_Non_Billable_Amount__c</name>
    </fields>
    <fields>
        <help>Dette tallet viser totalbeløpet i Ressurs valuta.</help>
        <label><!-- Total Reimbursement Amount --></label>
        <name>pse__Total_Reimbursement_Amount__c</name>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Utgiftsrapportnavn</nameFieldLabel>
    <sharingReasons>
        <label><!-- PSE Approver Share --></label>
        <name>pse__PSE_Approver_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE Member Share --></label>
        <name>pse__PSE_Member_Share</name>
    </sharingReasons>
    <sharingReasons>
        <label><!-- PSE PM Share --></label>
        <name>pse__PSE_PM_Share</name>
    </sharingReasons>
    <validationRules>
        <errorMessage><!-- Assignment Project must match Expense Report Project. --></errorMessage>
        <name>pse__ExpenseReport_Asgnmt_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Assignment Resource must match Expense Report Resource. --></errorMessage>
        <name>pse__ExpenseReport_Asgnmt_Resource_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Milestone Project must match Expense Report Project. --></errorMessage>
        <name>pse__ExpenseReport_Milestone_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Cannot associate the methodology with this expense report. The methodology must belong to the same project as the expense report. Select a methodology that belongs to the same project as the expense report. --></errorMessage>
        <name>pse__ExpenseRpt_Methodology_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Cannot associate the phase with this expense report. The phase must belong to the same project as the expense report. Select a phase that belongs to the same project as the expense report. --></errorMessage>
        <name>pse__ExpenseRpt_Phase_Project_Mismatch</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- For an Expense Report to be marked as Approved, it must also be marked as Submitted. --></errorMessage>
        <name>pse__Expense_Report_Approval_Requires_Submit</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- An Expense Report may only be marked as Billable if its Project is Billable and its Assignment, if any, is Billable. --></errorMessage>
        <name>pse__Expense_Report_May_Not_Be_Billable</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Report&apos;s Project field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Report_Project_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- A Expense Report&apos;s Resource field value may not be blank and may not be updated once set (unless Admin Global Edit is checked, Audit Notes are provided, and Actuals Calculation Mode is Scheduled). --></errorMessage>
        <name>pse__Expense_Report_Resource_is_Constant</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Expense Reports for this Project require an Assignment. --></errorMessage>
        <name>pse__Project_Requires_Exp_Report_Assignment</name>
    </validationRules>
    <webLinks>
        <label><!-- Admin_Edit --></label>
        <name>pse__Admin_Edit</name>
    </webLinks>
    <webLinks>
        <label><!-- Clear_Billing_Data --></label>
        <name>pse__Clear_Billing_Data</name>
    </webLinks>
    <webLinks>
        <label><!-- Combine_Attachments --></label>
        <name>pse__Combine_Attachments</name>
    </webLinks>
    <webLinks>
        <label><!-- Multiple_Expense_Entry_UI --></label>
        <name>pse__Multiple_Expense_Entry_UI</name>
    </webLinks>
</CustomObjectTranslation>
