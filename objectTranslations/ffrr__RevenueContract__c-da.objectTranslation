<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <article>Definite</article>
        <plural>false</plural>
        <value>Indkomstkontrakt</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>false</plural>
        <value>Indkomstkontrakt</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>false</plural>
        <value>Indkomstkontrakt</value>
    </caseValues>
    <caseValues>
        <article>Definite</article>
        <plural>true</plural>
        <value>Indkomstkontrakter</value>
    </caseValues>
    <caseValues>
        <article>Indefinite</article>
        <plural>true</plural>
        <value>Indkomstkontrakter</value>
    </caseValues>
    <caseValues>
        <article>None</article>
        <plural>true</plural>
        <value>Indkomstkontrakter</value>
    </caseValues>
    <fields>
        <help>Oplysninger om indtægtskontrakten. Ofte vil dette være kontonavnet, men det kan være enhver information.</help>
        <label><!-- Display Information --></label>
        <name>ffrr__AccountName__c</name>
    </fields>
    <fields>
        <label><!-- Account --></label>
        <name>ffrr__Account__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Active --></label>
        <name>ffrr__Active__c</name>
    </fields>
    <fields>
        <help>Angiver om tildeling kan køres med succes for denne indtjeningskontrakt.</help>
        <label><!-- Ready for Allocation --></label>
        <name>ffrr__Allocatable__c</name>
    </fields>
    <fields>
        <help>Forholdet anvendt til at allokere indtægter til tilknyttede præstationsforpligtelser, hvor værdien Allocated Revenue ikke er overskredet.</help>
        <label><!-- Allocation Ratio --></label>
        <name>ffrr__AllocationRatio__c</name>
    </fields>
    <fields>
        <help>Viser, om indtægterne for denne kontrakt er blevet fuldt allokeret eller skal omfordeles.</help>
        <label><!-- Allocation Status --></label>
        <name>ffrr__AllocationStatus__c</name>
    </fields>
    <fields>
        <help>Opslag til virksomhedsobjektet.</help>
        <label><!-- Company --></label>
        <name>ffrr__Company__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <fields>
        <help>Antal decimaler i postens valuta.</help>
        <label><!-- Currency Decimal Places --></label>
        <name>ffrr__CurrencyDP__c</name>
    </fields>
    <fields>
        <label><!-- Description --></label>
        <name>ffrr__Description__c</name>
    </fields>
    <fields>
        <label><!-- End Date --></label>
        <name>ffrr__EndDate__c</name>
    </fields>
    <fields>
        <help>Antal præstationsforpligtelser vedrørende indtægter, der har mindst en null SSP-værdi.</help>
        <label><!-- Null SSP Count --></label>
        <name>ffrr__NullSSPCount__c</name>
    </fields>
    <fields>
        <help>Antallet af tilknyttede præstationsforpligtelser, hvor der er indtastet en Tilladt indtjeningsoverstyringsværdi.</help>
        <label><!-- PO Allocated Revenue Override Count --></label>
        <name>ffrr__POAllocatedRevenueOverrideCount__c</name>
    </fields>
    <fields>
        <help>Antallet af præstationsforpligtelser på denne kontrakt.</help>
        <label><!-- Performance Obligations Count --></label>
        <name>ffrr__PerformanceObligationsCount__c</name>
    </fields>
    <fields>
        <help>Angiver, om indtægterne for denne kontrakt er blevet tildelt succesfuldt.</help>
        <label><!-- Revenue Allocated --></label>
        <name>ffrr__RevenueAllocated__c</name>
    </fields>
    <fields>
        <help>Dette felt bør kun udfyldes, hvis kontraktens indtægter er defineret på kontraktniveau i stedet for at blive beregnet ud fra Performance Obligations. Når befolket, bruges denne værdi i feltet Indtægter.</help>
        <label><!-- Revenue Override --></label>
        <name>ffrr__RevenueOverride__c</name>
    </fields>
    <fields>
        <label><!-- Fully Recognized and Amortized --></label>
        <name>ffrr__RevenueRecognitionComplete__c</name>
    </fields>
    <fields>
        <help>Den værdi, der vil blive allokeret til præstationsforpligtelserne. Anvender samlet indkomst eller indtægtsovergreb, hvis den er befolket.</help>
        <label><!-- Revenue --></label>
        <name>ffrr__Revenue__c</name>
    </fields>
    <fields>
        <label><!-- Start Date --></label>
        <name>ffrr__StartDate__c</name>
    </fields>
    <fields>
        <help>Summen af ​​tildelte indtægter Overstyr værdier fra alle sammenknyttede præstationsforpligtelser.</help>
        <label><!-- Total Allocated Revenue Override --></label>
        <name>ffrr__TotalAllocatedRevenueOverride__c</name>
    </fields>
    <fields>
        <help>Summen af ​​tildelte indtægtsværdier fra alle sammenknyttede præstationsforpligtelser.</help>
        <label><!-- Total Allocated Revenue --></label>
        <name>ffrr__TotalAllocatedRevenue__c</name>
    </fields>
    <fields>
        <help>Summen af ​​amortiseret til dato-værdier fra alle sammenknyttede præstationsforpligtelser.</help>
        <label><!-- Total Amortized To Date --></label>
        <name>ffrr__TotalAmortizedToDate__c</name>
    </fields>
    <fields>
        <help>Summen af ​​omkostningsværdier fra alle sammenknyttede præstationsforpligtelser.</help>
        <label><!-- Total Cost --></label>
        <name>ffrr__TotalCost__c</name>
    </fields>
    <fields>
        <help>Antallet af tilknyttede præstationsforpligtelser, hvor felterne for tildelingsforhold og tildelte indtægter er null.</help>
        <label><!-- Null Allocation Ratio Count --></label>
        <name>ffrr__TotalNullAllocationRatioCount__c</name>
    </fields>
    <fields>
        <help>Summen af ​​indregnet til datoværdier fra alle sammenknyttede præstationsforpligtelser.</help>
        <label><!-- Total Recognized To Date --></label>
        <name>ffrr__TotalRecognizedToDate__c</name>
    </fields>
    <fields>
        <help>Summen af ​​indtjeningsværdier fra alle sammenknyttede præstationsforpligtelser.</help>
        <label><!-- Total Revenue --></label>
        <name>ffrr__TotalRevenue__c</name>
    </fields>
    <fields>
        <help>Summen af ​​SSP-værdier for tilknyttede præstationsforpligtelser, hvor værdien for tildelte indtægter ikke er overskredet.</help>
        <label><!-- Total SSP for Allocation --></label>
        <name>ffrr__TotalSSPForAllocation__c</name>
    </fields>
    <fields>
        <help>Summen af ​​selvstændige salgspriser fra alle sammenknyttede præstationsforpligtelser.</help>
        <label><!-- Total SSP --></label>
        <name>ffrr__TotalSSP__c</name>
    </fields>
    <fields>
        <help>Antallet af præstationsforpligtelser vedrørende omsætning, hvor SSP er nul.</help>
        <label><!-- Zero SSP Count --></label>
        <name>ffrr__ZeroSSPCount__c</name>
    </fields>
    <fields>
        <label><!-- Template --></label>
        <name>ffrr__ffrrtemplate__c</name>
        <relationshipLabel><!-- Revenue Contracts --></relationshipLabel>
    </fields>
    <gender>Feminine</gender>
    <nameFieldLabel>Indtægtsaftalens nummer</nameFieldLabel>
    <webLinks>
        <label><!-- AllocateRevenue --></label>
        <name>ffrr__AllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- ListAllocateRevenue --></label>
        <name>ffrr__ListAllocateRevenue</name>
    </webLinks>
    <webLinks>
        <label><!-- ListUpdatePerformanceObligations --></label>
        <name>ffrr__ListUpdatePerformanceObligations</name>
    </webLinks>
    <webLinks>
        <label><!-- ManageObligations --></label>
        <name>ffrr__ManageObligations</name>
    </webLinks>
    <webLinks>
        <label><!-- UpdatePerformanceObligations --></label>
        <name>ffrr__UpdatePerformanceObligations</name>
    </webLinks>
</CustomObjectTranslation>
