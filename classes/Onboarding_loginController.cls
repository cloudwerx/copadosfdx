/**
* @name         Onboarding_loginController 
* @author       Girish Baviskar
* @date         19-01-2021
* @description  Controller for login lwc
* @testClass    Onboarding_loginControllerTest
**/ 
public with sharing class Onboarding_loginController {
    

    /**
    * @description Generates 6 digit otp if valid email matches and calls send email method
    * @param       officialEmailId official email of contact, email will be sent here.
    * @return     `LoginResponse`
    **/
    @AuraEnabled
    public static LoginResponse generateAndEmailOtp(string officialEmailId){
        LoginResponse res = new LoginResponse();
        try {
            
            System.debug('inside generateAndEmailOtp');
            //To always generate 6 digit number 
            Integer randomNumber = Math.round((Math.random() * (900000) + 100000));
            //Integer randomNumber = Integer.valueof((Math.random() * 1000000));
            res.otp = randomNumber;
            System.debug('otp: ' + res.otp);
            contact objContact =    [SELECT Id,
                                            Name,
                                            Email,
                                            Official_email__C,
                                            Employee_Type__c,
                                            Onboarding_Feedback_Rating__c,
                                            Is_Employee_Onboarding_Complete__c,
                                            Status__c
                                    FROM Contact 
                                    WHERE Email =: officialEmailId 
                                    WITH SECURITY_ENFORCED
                                    LIMIT 1];
            System.debug('retrieved contact is: ' + objContact);
            res.objContact = objContact;
            System.debug('res objContact: ' + res.objContact);
            if (objContact.Status__c == 'Active Employee') {
                sendEmailWithOtp(randomNumber, objContact);
                return res;
            } else {
                return null;
            }
            
            
        } catch (Exception e) {
            System.debug('inside catch of generateAndEmailOtp');
            return null ;
        }
    }
    
    /**
    * @description sends otp to the employees on official email id.
    * @param       otp one time password to login.
    * @param       objContact contact object who is receiving email.
    * @return     ``
    **/
    public static void sendEmailWithOtp(Integer otp, Contact objContact) {
        if (objContact != null) {
            if (objContact.Email != null) {
                System.debug('inside sendEmailWithOtp');
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[]{objContact.Email}; 
                System.debug('toAddresses : ' + toAddresses);
                mail.setToAddresses(toAddresses);
                mail.setSenderDisplayName( 'Cloudwerx Self Service Support');
                mail.setSubject('OTP : Cloudwerx Self Service Portal');
                mail.setPlainTextBody('Your One Time Password to login into Cloudwerx Self Service Portal is : ' + otp);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
        }
        
    }
    public with sharing class LoginResponse {
        @AuraEnabled
        public Integer otp;
        @AuraEnabled
        public Contact objContact;
    }
}