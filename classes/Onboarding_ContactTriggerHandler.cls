/**
* @name         Onboarding_ContactTriggerHandler 
* @author       Girish Baviskar
* @date         15-01-2021
* @description  This is Handler Class for Trigger on contact object. 
* @testClass    Onboarding_ContactTriggerTest
**/

/* MODIFICATION LOG
* Version   Developer           Date         Ticket Number       Description
*------------------------------------------------------------------------------------------------
*  1        Yash Bhalerao       28.01.2021                          handlerAfterInsert => Added after insert handler for task creation
*  
*/


public with sharing class Onboarding_ContactTriggerHandler {
    
    public static String WORK_COUNTRY = 'India';
    
    /**
    * @description This method handles all afer update logic of trigger
    * @param       newContactList updated list of contactsa
    * @param       oldContactMap old map of contacts
    * @return     ``
    **/
    public static void handlerAfterUpdate(List<Contact> newContactList, Map<Id,Contact> oldContactMap) {
        List<Contact> lstOfContactsToMessage = new List<Contact>();
        List<Contact> lstOfContactsSendingMsg = new List<Contact>();
        List<Contact> lstOfContactsToFetchSlackInfo = new List<Contact>();
        List<Contact> lstOfContactsToAddIntoChannels = new List<Contact>();
        List<Contact> lstOfContactsToInviteToExperience = new List<Contact>();
        List<Contact> lstOfContactsOnboardingCompleted = new List<Contact>();

        List<Onboarding_App_Slack_Setting__mdt> metaDataSettings = [SELECT  Notify_Employee__c,
                                                                            Office_Location__c,
                                                                            Notify_Employer__c, 
                                                                            Employers_Channel__c,
                                                                            Channels__c,
                                                                            Task_Complete_Message_To_Employee__c,
                                                                            Task_Complete_Message_To_Employer__c,
                                                                            Bot_Named_Credentials__c,
                                                                            User_Named_Credentials__c
                                                                    FROM Onboarding_App_Slack_Setting__mdt
                                                                    WITH SECURITY_ENFORCED
                                                                    LIMIT 1000];  
        Map< Id, Onboarding_App_Slack_Setting__mdt > mapOfContactAndMetadata = new Map<Id, Onboarding_App_Slack_Setting__mdt>();
        // creating map of contact id and slack setting applicable for it.
        for (Contact objContact : newContactList) {
            
            for (Onboarding_App_Slack_Setting__mdt metaData : metaDataSettings) {
                if(metadata.Office_Location__c == objContact.Work_Location__c) {
                    mapOfContactAndMetadata.put(objContact.Id, metaData); 
                }
            }

            if(objContact.Is_Slack_User__c != oldContactMap.get(objContact.id).Is_Slack_User__c 
                && objContact.Is_Slack_User__c == true) {
                lstOfContactsToFetchSlackInfo.add(objContact);
            }
            if(objContact.Is_Employer_Onboarding_Complete__c != oldContactMap.get(objContact.id).Is_Employer_Onboarding_Complete__c 
                && objContact.Is_Employer_Onboarding_Complete__c == true 
               ) { //&& objContact.Slack_Profile_Id__c != null
                    
                    lstOfContactsToMessage.add(objContact);
                }
            if(objContact.Is_Employee_Onboarding_Complete__c != oldContactMap.get(objContact.id).Is_Employee_Onboarding_Complete__c 
                && objContact.Is_Employee_Onboarding_Complete__c == true) {
                    
                    lstOfContactsSendingMsg.add(objContact);
            }
            if (objContact.Slack_Profile_Id__c != oldContactMap.get(objContact.id).Slack_Profile_Id__c 
                && objContact.Slack_Profile_Id__c != null) {
                    lstOfContactsToAddIntoChannels.add(objContact);   
            }
            if(objContact.Status__c != oldContactMap.get(objContact.id).Status__c 
                && objContact.Status__c == 'Active Employee') {
                    lstOfContactsToInviteToExperience.add(objContact);
            }
        
            if(
                (
                    (objContact.Is_Employee_Onboarding_Complete__c != oldContactMap.get(objContact.id).Is_Employee_Onboarding_Complete__c)
                    && (objContact.Is_Employee_Onboarding_Complete__c == true) 
                    && (objContact.Is_Employer_Onboarding_Complete__c == oldContactMap.get(objContact.id).Is_Employer_Onboarding_Complete__c)
                    && (oldContactMap.get(objContact.id).Is_Employer_Onboarding_Complete__c == true)
                )
                ||
                (
                    (objContact.Is_Employer_Onboarding_Complete__c != oldContactMap.get(objContact.id).Is_Employer_Onboarding_Complete__c)
                    && (objContact.Is_Employer_Onboarding_Complete__c == true)
                    &&(objContact.Is_Employee_Onboarding_Complete__c == oldContactMap.get(objContact.id).Is_Employee_Onboarding_Complete__c)
                    && (oldContactMap.get(objContact.id).Is_Employee_Onboarding_Complete__c == true)
                )
            ) {
                lstOfContactsOnboardingCompleted.add(objContact);
            }

        }

        if(lstOfContactsToFetchSlackInfo.size() > 0) {
            fetchSlackInfoOfContacts(lstOfContactsToFetchSlackInfo, mapOfContactAndMetadata);
        }
        if(lstOfContactsToMessage.size() > 0) {
            sendSlackMessageToEmployee(lstOfContactsToMessage, mapOfContactAndMetadata, 'taskComplete');
        } 
        if (lstOfContactsSendingMsg.size() > 0) {
            sendSlackMessageToEmployer(lstOfContactsSendingMsg, mapOfContactAndMetadata , 'taskComplete');
        }
        if (lstOfContactsToAddIntoChannels.size() > 0) {
            addUsersToSlackChannels(lstOfContactsToAddIntoChannels, mapOfContactAndMetadata);
        }   
        if(lstOfContactsToInviteToExperience.size() > 0) {
            inviteNewEmployeeToOnboardingExperirnce(lstOfContactsToInviteToExperience);
        }
        if(lstOfContactsOnboardingCompleted.size() > 0) {
            finalOnboardingEmail(lstOfContactsOnboardingCompleted);
        }


        
        
    }
    
    /**
    * @description This method fetches details of all the members of slack workspace and updates 
    *              the fetched details on contact object in queueable call.
    * @param       newContactList updated list of contactsa
    * @param       mapOfContactAndMetadata map of contact and applicable metadata of slack setting
    * @return     ``
    **/
    public static void fetchSlackInfoOfContacts(List<contact> newContactList, Map< Id, Onboarding_App_Slack_Setting__mdt > mapOfContactAndMetadata) {
        System.debug('inside fetchSlackInfoOfContacts');
        List<Onboarding_SlackRequestWrapper> lstOfReq = new list<Onboarding_SlackRequestWrapper>();
        string namedCred= mapOfContactAndMetadata.get(newContactList[0].Id).User_Named_Credentials__c;
        Onboarding_SlackRequestWrapper objReq = Onboarding_SlackWebApi.getAllSlackUserProfileDetails(namedCred); 
        lstOfReq.add(objReq); 
        if (lstOfReq.size() > 0) {
        system.enqueueJob(new Onboarding_QueueableSlackCall(lstOfReq, newContactList)); // contact list is further passed to queueable class to update details like slack profile id on contact.
        }
    }

    /**
    * @description This method sends message(Direct Message) to employee(contact) from user who made
    *              slack app for integration.
    *              This method forms list of request wrapper class and enqueues queueable slack job.
    * @param       newContactList updated list of contactsa
    * @param       mapOfContactAndMetadata map of contact and applicable metadata of slack setting
    * @return     ``
    **/
    public static void sendSlackMessageToEmployee(List<contact> newContactList, Map< Id, Onboarding_App_Slack_Setting__mdt > mapOfContactAndMetadata, string messageType) {
    string message = '';  
    Onboarding_App_Slack_Setting__mdt metaData;
    System.debug('inside  sendSlackMessageToEmployee first for');
    List<Onboarding_SlackRequestWrapper> lstOfReq = new list<Onboarding_SlackRequestWrapper>();  
    for (contact objContact : newContactList) {
        if (objContact.Is_Slack_User__c != NULL) {
            metaData = mapOfContactAndMetadata.get(objContact.Id);
            if (metaData != NUll && metadata.Notify_Employee__c == true) { 
            switch on messageType {
                when 'taskComplete' {
                    message = metadata.Task_Complete_Message_To_Employee__c; // message after all task completed
                }
                when 'taskReminder' {
                    message = metaData.Reminder_Message_To_Employee__c; //  reminder message.
                }
             }
            message = message.replaceAll('firstName', objContact.FirstName);
            message = message.replaceAll('lastName', objContact.LastName); 
            System.debug('message formed: ' + message);
            Onboarding_SlackRequestWrapper objReq = Onboarding_SlackWebApi.postMessageAsUser( metaData.User_Named_Credentials__c, objContact.Slack_Profile_Id__c , message);// form request.
            lstOfReq.add(objReq);  // adding callout request to list.
            System.debug('inside second if inside second for'); 
            
            } 
        }
        
          
    }
    System.debug(lstOfReq);
    if (lstOfReq.size() > 0) {
        System.enqueueJob(new Onboarding_QueueableSlackCall(lstOfReq)); 
    }
    
}

    /**
    * @description This methods posts message onbehalf of employee(contact) into employers channels
    *              Channels are specified in slack setting metadata.
    *              In this method actually bot posts message disguised as employee.
    * @param       newContactList updated list of contactsa
    * @param       mapOfContactAndMetadata map of contact and applicable metadata of slack setting
    * @return     ``
    **/
    public static void sendSlackMessageToEmployer(List<contact> newContactList, Map< Id, Onboarding_App_Slack_Setting__mdt > mapOfContactAndMetadata, string messageType) {
       System.debug('inside trigger handler sendSlackMessageToEmployer method');
        List<string> channelList = new List<String>();
        System.debug(newContactList);
        string message = '';
        // string contactNames = '';
        Onboarding_App_Slack_Setting__mdt metaData; 
        List<Onboarding_SlackRequestWrapper> lstOfReq = new list<Onboarding_SlackRequestWrapper>(); //list of requests
            for (contact objContact : newContactList) {
                System.debug('inside sendSlackMessageToEmployer first for');
                metaData = mapOfContactAndMetadata.get(objContact.id);
                // contactNames += objContact.Name + '\n';

                if ( metaData != NULL && metadata.Notify_Employer__c == true 
                    && metadata.Employers_Channel__c != null) {
                        
                        System.debug('inside second if inside second for'); 
                        channelList = metadata.Employers_Channel__c.split(',');
                        for (string channel : channelList) {
                            System.debug('inside second if and second for');
                            Onboarding_SlackRequestWrapper objReq = new Onboarding_SlackRequestWrapper(); //creating list of requests.
                            objReq.url = 'callout:' + metadata.Bot_Named_Credentials__c + '/chat.postMessage';
                            objReq.method = 'POST';
                            Map<String,Object> msg = new Map<String,Object>();
                            switch on messageType {
                                when 'taskComplete' {
                                    message = metadata.Task_Complete_Message_To_Employer__c;
                                    msg.put('username', objContact.Slack_Display_Name__c);
                                    msg.put('icon_url', objContact.Slack_Profile_Picture_Url__c);
                                }
                                when 'taskReminder' {
                                    message = metaData.Reminder_Message_To_Employer__c;
                                }
                             }
                            message = message.replaceAll('firstName', objContact.FirstName);
                            message = message.replaceAll('lastName', objContact.LastName); 
                            msg.put('channel',  channel);
                            msg.put('text',  message);
                            
                            objReq.body = JSON.serialize(msg);
                            lstOfReq.add(objReq); 
                        }
                        System.debug('channel list :'+channelList);
                        
                }   
            }
        System.debug(lstOfReq);
        if (lstOfReq.size() > 0) {
        System.enqueueJob(new Onboarding_QueueableSlackCall(lstOfReq)); 
        } 
    }
// TODO complete this method.

//******************************************New Method*********************** */

   /*  public static void sendTaskCompletionReminderToEmployer(List<contact> newContactList, Map< Id, Onboarding_App_Slack_Setting__mdt > mapOfContactAndMetadata) {
        Set<Onboarding_App_Slack_Setting__mdt> setOfMetadataSettings = new Set<Onboarding_App_Slack_Setting__mdt>();
        setOfMetadataSettings.addAll(mapOfContactAndMetadata.values());
        List<Onboarding_SlackRequestWrapper> lstOfReq = new list<Onboarding_SlackRequestWrapper>();
        List<string> channelList = new List<String>();
        for (setOfMetadataSettings metadata : setOfMetadataSettings) {
            channelList = metadata.Employers_Channel__c.split(',');
            for (string channel : channelList) {
                Onboarding_SlackRequestWrapper objReq = new Onboarding_SlackRequestWrapper(); //creating list of requests.
                objReq.url = 'callout:' + metadata.Bot_Named_Credentials__c + '/chat.postMessage';
                objReq.method = 'POST';
                string contactNames = '';
                Map<String,Object> msg = new Map<String,Object>();
                message = metadata.Task_Complete_Message_To_Employer__c;
                msg.put('username', objContact.Slack_Display_Name__c);
                msg.put('icon_url', objContact.Slack_Profile_Picture_Url__c);
                for (contact variable : List_or_set) {
                    
                }
            }
            for (Contact objContact : newContactList) {
                if (mapOfContactAndMetadata.get(objContact.Id) == metadata) {
                    for (string channel : channelList) {
                        
                    }
                }
            }
            channelList = metadata.Employers_Channel__c.split(',');

        }
    } */

    /**
    * @description This method adds employee(contact) into set of specified channels
    * @param       newContactList updated list of contactsa
    * @param       mapOfContactAndMetadata map of contact and applicable metadata of slack setting
    * @return     ``
    **/
    public static void addUsersToSlackChannels(List<contact> newContactList, Map< Id, Onboarding_App_Slack_Setting__mdt > mapOfContactAndMetadata) {
        System.debug('inside addUsersToSlackChannels ');
        System.debug('newContactList : ' + newContactList);
        string userIdsToAdd = '';
        map<string, string> mapOfChannelAndUsersToAdd = new Map<string, string>();
        List<string> lstOfChannels = new List<String>();
        List<Onboarding_SlackRequestWrapper> lstOfReq = new list<Onboarding_SlackRequestWrapper>();
        Onboarding_App_Slack_Setting__mdt metaData; 
        for (Contact objContact : newContactList) {
            metaData = mapOfContactAndMetadata.get(objContact.id);
            if (metadata.Channels__c != null) {
                lstOfChannels = metadata.Channels__c.split(',');
                for (string channel : lstOfChannels) {
                    mapOfChannelAndUsersToAdd.put(channel, mapOfChannelAndUsersToAdd.get(channel) + objContact.Slack_Profile_Id__c + ','); // append users to add to channels
                    System.debug(mapOfChannelAndUsersToAdd);
                }
            }
            
        }
        
        for (string channel : mapOfChannelAndUsersToAdd.keySet()) {
            Onboarding_SlackRequestWrapper objReq = Onboarding_SlackWebApi.addUsersToChannels(metadata.User_Named_Credentials__c, channel,  mapOfChannelAndUsersToAdd.get(channel).remove('null')); //creating list of requests.
            lstOfReq.add(objReq); 
        }
        
        System.debug(lstOfReq);
        if (lstOfReq.size() > 0) {
        System.enqueueJob(new Onboarding_QueueableSlackCall(lstOfReq)); 
        }
    }

    /**
    * @description This method sends welcome email to employees with link to onboarding public community.
    *              When a employee(contact) is marked as active employee this method gets invoked.
    * @param       newContactList list of contacts who will receive email.
    * @return     ``
    **/
    public static void inviteNewEmployeeToOnboardingExperirnce(List<contact> newContactList) {
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        System.debug('newContactList : ' + newContactList);
        String emailTemplateName = 'Onboarding_Community_Invite';
        EmailTemplate template = [SELECT Id, Subject, Body FROM EmailTemplate WHERE DeveloperName =:emailTemplateName];
        
        for (Contact objContact : newContactList) {
            if(objContact.Official_Email__c != null && objContact.Email != null) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                //List<string> toAddress = new List<string>();
                String[] toAddress = new String[]{objContact.Email}; 
                //String[] toAddress = new String[]{'gbaviskar@clodwerx.co'};
                //System.debug('obj offial email : ' + objContact.Official_email__C);
                //toAddress.add(objContact.Official_email__C);
                mail.setToAddresses(toAddress);
                System.debug('toAddress' + toAddress);
                System.debug('mail : '+ mail);
                mail.setTemplateId(template.Id);
                mail.setTreatTargetObjectAsRecipient(false);
                mail.setTargetObjectId(objContact.Id);
                mail.setSaveAsActivity(false);
                // mail.setWhatId(objContact.Id);
                emails.add(mail);
            }
            
        }
        
        //EmailMessage messageObj = new EmailMessage(ToAddress = candidate.Email, Subject = template.Subject, HtmlBody = template.Body, RelatedToId = candidate.Id);
        try {
            Messaging.sendEmail(emails);
            //insert messageObj;
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
    }
    /**
    * @description This method sends final email to employers with all onboarding tasks completed by employer 
    *               and employee.
    *               When both employee onboarding tasks and employer onboarding task are completed this method is called.
    * @param       newContactList list of contacts who will receive email.
    * @return     ``
    **/
    public static void finalOnboardingEmail(List<contact> newContactList) {
        System.debug('inside finalOnboardingEmail');
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        System.debug('newContactList : ' + newContactList);
        String emailTemplateName = 'final_onboarding_complete_email';
        EmailTemplate template = [SELECT Id, Subject, Body FROM EmailTemplate WHERE DeveloperName =:emailTemplateName];
        List<String> listOfRecipients = Label.Onboarding_Final_Email_Recipients.split(',');
        System.debug('listOfRecipients' + listOfRecipients);
        for (Contact objContact : newContactList) {
            if(listOfRecipients != null) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                //List<string> toAddress = new List<string>();
                //String[] toAddress = new String[]{objContact.Official_email__C}; 
                //String[] toAddress = new String[]{'gbaviskar@clodwerx.co'};
                //System.debug('obj offial email : ' + objContact.Official_email__C);
                //toAddress.add(objContact.Official_email__C);
                mail.setToAddresses(listOfRecipients);
                System.debug('toAddress' + listOfRecipients);
                System.debug('mail : '+ mail);
                mail.setTreatTargetObjectAsRecipient(false);
                mail.setTemplateId(template.Id);
                mail.setTargetObjectId(objContact.Id);
                mail.setSaveAsActivity(false);
                mail.setWhatId(objContact.Id);
                emails.add(mail);
            }
            
        }
        
        //EmailMessage messageObj = new EmailMessage(ToAddress = candidate.Email, Subject = template.Subject, HtmlBody = template.Body, RelatedToId = candidate.Id);
        try {
            Messaging.sendEmail(emails);
            //insert messageObj;
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
    }
    

    /**
    * @description This method handles after insert logic of trigger.
                    This method creates onboarding task for newly inserted employee(contact) from metadata.
    * @param       insertedContactList list of inserted contacts. 
    * @return     ``
    **/
    public static void handlerAfterInsert(List<Contact> insertedContactList)
    {
        Id IndianContactRecordTypeId;
        Id EmployeeId;
        Id EmployerId;

        list<Onboarding_Task__mdt> MetadataTaskList = [SELECT Id, AssignedToId__c, Description_of_Task__c, Employee_Role__c, Employer_Comments__c, IsRequired__c, Is_Slack_msg_Required__c, Is_Upload_Required__c, Is_Information_Form_required__c, Office_Location__c, Subject_of_Task__c, Task_Sequence_Number__c, Type_of_Document__c, Type_of_Task__c, Help_Doc_URL__c, Help_Doc_ID__c, Detail_Button__c, Is_Downloadable__c FROM Onboarding_Task__mdt ORDER BY Task_Sequence_Number__c ASC LIMIT 10000];

        /*List<RecordType> RecordTypeIds = [SELECT Id FROM RecordType WHERE DeveloperName = 'India_Employee' LIMIT 1];

        for(RecordType r : RecordTypeIds)
        {
            IndianContactRecordTypeId = r.Id;
        }*/

        List<Onboarding_Task_Assigned_Id__mdt> UserIds = [SELECT Label, User_Id__c FROM Onboarding_Task_Assigned_Id__mdt LIMIT 100];

        for(Onboarding_Task_Assigned_Id__mdt u : UserIds)
        {
            if(u.Label == 'Platform Integration User')
            {
                EmployeeId = u.User_Id__c;
            }
            else 
            {
                EmployerId = u.User_Id__c;
            }
        }

        list<Task> TasksToInsert = new list<Task>();

        for(Contact con : insertedContactList)
        {
            if(con.WorkCountry__c == WORK_COUNTRY)
            {
                for(Onboarding_Task__mdt mdtTask : MetadataTaskList)
                {
                    Task newTask = new Task();
                    if(mdtTask.Type_of_Task__c == 'Employee')
                    {
                        newTask.OwnerId = EmployeeId;
                    }
                    else
                    {
                        newTask.OwnerId = EmployerId;
                    }

                    if(mdtTask.Detail_Button__c == 'Show')
                    {
                        newTask.Detail_Button__c = 'slds-show';
                    }
                    else 
                    {
                        newTask.Detail_Button__c = 'slds-hide';
                    }
                    
                    newTask.WhoId = con.Id;
                    newTask.Description = mdtTask.Description_of_Task__c;
                    newTask.Employer_Comments__c = mdtTask.Employer_Comments__c;
                    newTask.Is_Required__c = mdtTask.IsRequired__c;
                    newTask.Is_Slack_msg_Required__c = mdtTask.Is_Slack_msg_Required__c;
                    newTask.Is_Upload_Required__c = mdtTask.Is_Upload_Required__c;
                    newTask.Is_Information_Form_required__c = mdtTask.Is_Information_Form_required__c;
                    newTask.Subject = mdtTask.Subject_of_Task__c;
                    newTask.Task_Sequence_Number__c = mdtTask.Task_Sequence_Number__c;
                    newTask.Type_of_Document__c = mdtTask.Type_of_Document__c;
                    newTask.Task_Type__c = mdtTask.Type_of_Task__c;
                    newTask.Help_Doc_URL__c = mdtTask.Help_Doc_URL__c;
                    newTask.Help_Doc_ID__c = mdtTask.Help_Doc_ID__c;
                    newTask.Is_Downloadable__c = mdtTask.Is_Downloadable__c;
                    TasksToInsert.add(newTask);
                }
            }
        }
        insert TasksToInsert;
        system.debug('TasksToInsert : ' + TasksToInsert);
    }

}