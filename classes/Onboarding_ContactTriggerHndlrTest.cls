@isTest
public class Onboarding_ContactTriggerHndlrTest {
    
    @TestSetup
    public static void makeData(){
        List<Account> accList = Onboarding_UtilityClass.createAccounts(1, true);
        list<Contact> cntList = Onboarding_UtilityClass.createContacts(1, true, accList[0].Id);
    }
    @IsTest
    public static void checkFectchInfoAddToChannels(){
        
        Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());
        Test.startTest();
        Contact cnt =[ SELECT Is_Slack_User__c, Slack_Display_Name__c, Slack_Profile_Id__c,
                            Slack_Profile_Picture_Url__c, Work_Location__c FROM Contact];
        cnt.Is_Slack_User__c = true;
        update cnt;
        Test.stopTest();
       /*  System.debug(cnt);
        Test.startTest();
        cnt.Is_Employee_Onboarding_Complete__c = true;
        update cnt;
        Test.stopTest();
        Test.startTest();
        cnt.Is_Employer_Onboarding_Complete__c = true;
        update cnt;
        Test.stopTest();
         */
        
    }

    @IsTest
    static void testEmployeeOnboarding(){
        Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());
        Test.startTest();
        Contact cnt =[ SELECT Is_Slack_User__c, Slack_Display_Name__c, Slack_Profile_Id__c,
                            Slack_Profile_Picture_Url__c FROM Contact];
        cnt.Is_Slack_User__c = true;
        cnt.Is_Employee_Onboarding_Complete__c = true;
        update cnt;
        Test.stopTest();
        
    }

    @IsTest
    static void testEmployerOnboarding(){
        
        Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());
        Test.startTest();
        Contact cnt =[ SELECT Is_Slack_User__c, Slack_Display_Name__c, Slack_Profile_Id__c,
                            Slack_Profile_Picture_Url__c FROM Contact];
        cnt.Is_Slack_User__c = true;
        cnt.Is_Employer_Onboarding_Complete__c = true;
        update cnt;
        Test.stopTest();
        
    }

    @IsTest
    static void testPostMessageAsUserViaBot(){
        
        Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());
        string message = 'hi im new hire';
        Contact cnt =[ SELECT Is_Slack_User__c, Slack_Display_Name__c, Slack_Profile_Id__c,
        Slack_Profile_Picture_Url__c, Work_Location__c FROM Contact];
        Test.startTest();
        Onboarding_SlackWebApi.postMessageAsUserViaBot(message, cnt);
        Test.stopTest();
        
    }

    @IsTest
    static void testFinalEmailAfterOnboarding(){
        
        Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());
        
        Contact cnt =[ SELECT Is_Employee_Onboarding_Complete__c, Is_Employer_Onboarding_Complete__c, Slack_Profile_Id__c,
        Slack_Profile_Picture_Url__c, Work_Location__c FROM Contact];
        cnt.Is_Employee_Onboarding_Complete__c = true;
        //update cnt;
        //cnt.Is_Employer_Onboarding_Complete__c = true;
        Test.startTest();
        update cnt;
        Test.stopTest();
    }
    @IsTest
    static void testFinalEmailAfterOnboardingToEmployer(){
        
        Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());
        
        Contact cnt =[ SELECT Is_Employee_Onboarding_Complete__c, Is_Employer_Onboarding_Complete__c, Slack_Profile_Id__c,
        Slack_Profile_Picture_Url__c, Work_Location__c FROM Contact];
        //cnt.Is_Employee_Onboarding_Complete__c = true;
        //update cnt;
        cnt.Is_Employer_Onboarding_Complete__c = true;
        Test.startTest();
        update cnt;
        Test.stopTest();
    }
    @isTest
  public static void handlerAfterInsertTest()
    {
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'PSA Resource' AND isActive = true LIMIT 1];

        Contact con = new Contact();
        con.FirstName = 'Yash';
        con.LastName = 'Bhalerao';
        con.Email = 'ybhalerao@cloudwerx.co';
        con.Work_Location__c = 'Pune Office';
        con.WorkCountry__c = 'India';
        con.Employee_Role__c = 'Salesforce Developer';
        con.Employee_Type__c = 'Employee';
        con.Date_Of_Joining__c = date.parse('11/02/2021');
        con.RecordTypeId = rt.Id;
        insert con;
        
        list<Task> TaskList = [SELECT Id FROM Task WHERE WhoId =:con.Id];
        system.assertNotEquals(0, TaskList.size());
    }
}