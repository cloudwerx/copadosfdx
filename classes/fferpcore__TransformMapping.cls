/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global interface TransformMapping {
    List<fferpcore.MessageDescription.Node> getNodes(fferpcore.Context param0);
    fferpcore.SubscriptionMapping getSubscriptionMapping(List<String> param0);
}
