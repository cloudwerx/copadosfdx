public with sharing class LandingComponentController {
    

    @AuraEnabled
    public static string getUserType(Id loggedInUserId){
        System.debug('requestingContactId : ' + loggedInUserId);

        Contact loggedInContact = [ SELECT  Work_Location__c,
                                            Employee_Type__c 
                                    FROM Contact    
                                    WHERE id =: loggedInUserId
                                    WITH SECURITY_ENFORCED
                                    LIMIT 1];
        
        
            return loggedInContact.Employee_Type__c;
        
    }
}