/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class GSP_D_TriggerContext {
    global static Boolean DisableAccountTriggers;
    global static Boolean DisableAllTriggers;
    global static Boolean DisableCaseTriggers;
    global static Boolean DisableLeadTriggers;
    global static Boolean DisableMonthlySalesTargetTriggers;
    global static Boolean DisableOpportunityLineItemTriggers;
    global static Boolean DisableOpportunityTriggers;
    global GSP_D_TriggerContext() {

    }
}
