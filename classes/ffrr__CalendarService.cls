/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CalendarService {
    global static Map<Id,List<Id>> createYears(List<ffrr.CalendarService.RecognitionYear> years) {
        return null;
    }
global class RecognitionYear {
    global Id Id {
        get;
        set;
    }
    global String Name {
        get;
        set;
    }
    global Decimal NumberOfMonths {
        get;
        set;
    }
    global Decimal NumberOfWeeks {
        get;
        set;
    }
    global String PeriodCalculationBasis {
        get;
        set;
    }
    global Date StartDate {
        get;
        set;
    }
    global RecognitionYear() {

    }
}
}
