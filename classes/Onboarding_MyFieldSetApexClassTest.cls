/************************************************************************************************
* @Name         Onboarding_MyFieldSetApexClassTest
* @Author       Yash Bhalerao
* @Date         11-02-2021
* @Description  Test class for MyFieldSetApexClass
* @TestClass    
*************************************************************************************************/

@isTest
public class Onboarding_MyFieldSetApexClassTest 
{
    public Onboarding_MyFieldSetApexClassTest() 
    {

    }

    @testSetup
    public static void setup()
    {

        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'India Employee' AND isActive = true LIMIT 1];

        Contact con = new Contact();
        con.FirstName = 'Yash';
        con.LastName = 'Bhalerao';
        con.Email = 'ybhalerao@cloudwerx.co';
        con.Work_Location__c = 'Pune Office';
        con.Employee_Role__c = 'Salesforce Developer';
        con.Employee_Type__c = 'Employee';
        con.Date_Of_Joining__c = date.parse('11/02/2021');
        con.RecordTypeId = rt.Id;
        
        insert con;
    }

    @isTest
    public static void getFieldsTest()
    {
        MyFieldSetApexClass.getFields();
    }

    @isTest
    public static void getContactListTest()
    {
        Contact con = [SELECT Id FROM Contact LIMIT 10];

        MyFieldSetApexClass.getContactList(con.Id);
    }

    @isTest
    public static void updateContactTest()
    {
        Contact con = [SELECT Id FROM Contact LIMIT 10];

        MyFieldSetApexClass.updateContact(con);

        con.LastName = '';
        MyFieldSetApexClass.updateContact(con);
    }
}