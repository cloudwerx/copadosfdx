/**
* @name         Onboarding_SlackReminderTest 
* @author       Girish Baviskar
* @date         15-01-2021
* @description  Test class for Onboarding_SlackReminder. 
* @testClass    
**/

@isTest
public class Onboarding_SlackReminderTest 
{
    public Onboarding_SlackReminderTest() 
    {
        
    }

    @TestSetup
    static void setup()
    {
        List<Onboarding_App_Slack_Setting__mdt> metaDataSettings = [SELECT  Notify_Employee__c,
                                                                            Office_Location__c,
                                                                            Notify_Employer__c, 
                                                                            Employers_Channel__c,
                                                                            Reminder_Message_To_Employee__c,
                                                                            Reminder_Message_To_Employer__c,
                                                                            Channels__c,
                                                                            Bot_Named_Credentials__c,
                                                                            User_Named_Credentials__c
                                                                    FROM Onboarding_App_Slack_Setting__mdt
                                                                    WITH SECURITY_ENFORCED
                                                                    LIMIT 1000];   
                                                                    
        Contact con = new Contact();
        con.FirstName = 'Yash';
        con.LastName = 'Bhalerao';
        con.Email = 'ybhalerao@cloudwerx.co';
        con.Work_Location__c = 'Pune Office';
        con.Employee_Role__c = 'Salesforce Developer';
        con.Employee_Type__c = 'Employee';
        con.Date_Of_Joining__c = date.today();
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'India Employee' AND isActive = true LIMIT 1];
        con.RecordTypeId = rt.id;
        
        insert con;
    }

    @isTest
    public static void test()
    {
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());  
        Onboarding_SlackReminder obj = new Onboarding_SlackReminder();      
        String sch = '0 32 21 1/1 * ? *';
        system.schedule('Test check', sch, obj);
        obj.execute(null);
        Test.stopTest();
    }
}