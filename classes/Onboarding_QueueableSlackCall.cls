/**
* @name         Onboarding_QueueableSlackCall 
* @author       Girish Baviskar
* @date         18-01-2021
* @description  Queueable class to make slack callouts. 
* @testClass    
**/

public Without sharing class Onboarding_QueueableSlackCall implements System.Queueable, Database.AllowsCallouts{
    
    /* Receives list of Onboarding_SlackRequestWrapper (wrapper class) and list of contact to update 
    slack information such as slack id, slack display name, slack profile url.
    (list of HttpRequest is not allowed because not serealizable, hence wrapper object) 
    
    */
    List<Onboarding_SlackRequestWrapper> lstOfRequests;
    List<Contact> lstOfContacts;
    
    public Onboarding_QueueableSlackCall(List<Onboarding_SlackRequestWrapper> lstOfRequests) {
        this.lstOfRequests = lstOfRequests; 
    }

    public Onboarding_QueueableSlackCall(List<Onboarding_SlackRequestWrapper> lstOfRequests, List<Contact> lstOfContacts) {
        this.lstOfRequests = lstOfRequests; 
        this.lstOfContacts = lstOfContacts.deepClone(true, true, true);
    }

    public void execute(System.QueueableContext ctx) {
        System.debug('inside execute of queueable');
        System.debug(lstOfRequests);
        for (Onboarding_SlackRequestWrapper objReqWrapper  : lstOfRequests) {
            
            HttpRequest req = new HttpRequest();
            req.setMethod(objReqWrapper.method);
            System.debug('formed request:' + req);
            System.debug('body of request:' + objReqWrapper.body);
            req.setBody(objReqWrapper.body);
            System.debug('formed request body:' + req.getBody());
            req.setHeader('Content-type', 'application/json');
            System.debug('formed request:' + req);
            Http http = new Http();
            HttpResponse res;
            // if (!Test.isRunningTest()) {
                req.setEndpoint(objReqWrapper.url);
                System.debug('formed request:' + req);
                res = http.send(req);
                System.debug('callout Response code :' + res.getStatusCode());
                System.debug('callout Response body :' + res.getBody());
                //if request was to get list of all the users in slack workspace update their details
                // on contact.
                if (objReqWrapper.url.contains('users.list')  && res.getStatusCode() == 200) {
                    Onboarding_SlackBulkResponseWrapper slackUsersInfo =  Onboarding_SlackBulkResponseWrapper.parse(res.getBody());
                    System.debug('inside bulk response');
                    for (Contact objContact : lstOfContacts) {
                        for (Onboarding_SlackBulkResponseWrapper.Members member : slackUsersInfo.members) {
                            if (member.deleted == false && objContact.Official_Email__c == member.profile.email) {
                                System.debug('found match');
                                objContact.Slack_Profile_Id__c = member.id;
                                objContact.Slack_Profile_Picture_Url__c = member.profile.image_512;
                                objContact.Slack_Display_Name__c = member.profile.display_name;
                                System.debug('contact object now:' + objContact);
                            }
                        }
                    }

                    try {
                        System.debug('inside try block');
                        // if (!Test.isRunningTest()) {
                            update lstOfContacts;
                        // }
                    } catch (Exception e) {
                        System.debug('inside catch block');
                        System.debug('Update failed: ' + e.getMessage());
                    }
                }
            //}
              

        }
        
        
    }
}