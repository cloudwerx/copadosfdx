/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PopulateFromSourceRecordsAction {
    global PopulateFromSourceRecordsAction() {

    }
    @InvocableMethod(label='Populate Performance Obligations Lines from source records.' description='Populate Performance Obligations line items that are linked to the provided source records.')
    global static void populatePerformanceObligationLineItemFromSourceRecords(List<ffrr.PopulateFromSourceRecordsAction.SourceRecord> input) {

    }
global class SourceRecord {
    @InvocableVariable(label='Source record Id' description='Source record Id' required=true)
    global Id sourceID;
    global SourceRecord() {

    }
}
}
