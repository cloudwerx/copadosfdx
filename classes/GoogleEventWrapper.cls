public with sharing class GoogleEventWrapper {
    
    public startDate start;
    public endDate endDate;
    public conferenceData conferenceData;
    public String summary;
    public String description;
    public List<Attendee> attendees;
    
    public GoogleEventWrapper() {
    }

    public class Attendee {
        public String email;
        public Attendee(String emailId) {
            email = emailId;
        }
    }

    public class startDate {
        public DateTime interviewStart;
        public String timeZone;
    }

    public class endDate {
        public DateTime interviewEnd;
        public String timeZone;
    }

    public class conferenceData {
        public createRequest createRequest;
    }

    public class createRequest {
        public String requestId;
    }
}