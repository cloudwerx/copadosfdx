/**
* @name         ReviewTimeSheetSlackMthReminderBatch
* @author       Girish
* @date         30/08/2021
* @description  This is Util class for slack time-sheet reminders.
**/
global with sharing class TimeSheetReminderUtils {

    public static Date todayDate {get {return todayDate == null ? Date.today() : todayDate;} set;}
    
    //This method forms query used for both week and month batch.
    global static string startInit(Review_Timesheet_Slack_Reminder_Settings__mdt mdtRec) {
        
        //Query to get all the active cloudwerx employees(Contacts).
        String query = 'SELECT Id, FirstName, LastName,  Slack_Profile_Id__c, Is_Slack_User__c, Work_Location__c ';
        query += 'FROM Contact ';
        query += 'WHERE Account.Name = \'Cloudwerx\' AND Status__c = \'Active Employee\' AND Id NOT IN :contactsToExclude';
        return query;
    }

    /**
    * @description This method queries and returns metadata records for configuration.
    * @return     `Review_Timesheet_Slack_Reminder_Settings__mdt`
    **/
    global static Review_Timesheet_Slack_Reminder_Settings__mdt getTimesheetReminderConfigDetails() {
        return [SELECT  DeveloperName,
                        Send_Weekly_Reminders__c, 
                        Send_Monthly_Reminders__c, 
                        Mothly_Reminder_Message__c, 
                        Weekly_Reminder_Message__c, 
                        Batch_Run_Emails__c,
                        Good_Bye_Messages__c,
                        Pune_User_Access_Token__c,
                        Sydney_User_Access_Token__c,
                        Users_to_Exclude_from_reminders__c 
                FROM Review_Timesheet_Slack_Reminder_Settings__mdt
                WITH SECURITY_ENFORCED
                LIMIT 1];
    }

    /**
    * @description This method sends batch run confirmation email.
    * @param       asyncJob
    **/
    public static void sendConfirmationEmail(AsyncApexJob asyncJob, Review_Timesheet_Slack_Reminder_Settings__mdt mdtRec, Integer msgCount, Integer contactsWithNoSlackId){    
        if (mdtRec != null && mdtRec.Batch_Run_Emails__c != null) {
            List <String> listOfEmails = new List <String>();
            listOfEmails = mdtRec.Batch_Run_Emails__c.split(',');
            String createdDate = asyncJob.CreatedDate.format('dd-MM-yyyy HH:mm:ss','Australia/Adelaide');
            String completedDate = asyncJob.CompletedDate.format('dd-MM-yyyy HH:mm:ss','Australia/Adelaide');

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(listOfEmails);
            mail.setSubject('Review Timesheet Slack Reminder Batch - ' + asyncJob.Status);
            String plainText = '<html><body> The batch process that automatically sends slack message to all employees to review timesheets has been successfully processed on ';
            plainText += completedDate + '. Further details are provided below: <br/><br/>';
            plainText += '<b>Batch Start Time:</b> ' + createdDate + '<br/>';
            plainText += '<b>Batch End Time:</b> ' + completedDate + '<br/>';
            plainText += '<b>Number of Batches Processed:</b> ' + asyncJob.TotalJobItems+ '<br/>'; 
            plainText += '<b>Number of Slack Reminders Sent Successfully:</b> ' + msgCount+ '<br/>';
            plainText += '<b>Number of Slack Reminders Not Sent Because of slack id unavailable:</b> ' + contactsWithNoSlackId+ '<br/>';
            if(asyncJob.ExtendedStatus != null){
                plainText += '<b>Failure Reason:</b>' + asyncJob.ExtendedStatus+ '<br/>';
            }
            plainText += ' </body></html>';
            mail.setHtmlBody(plainText);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }

    //This method forms and http request to send to the users.
    public static HttpRequest formHttpRequestToSend(Review_Timesheet_Slack_Reminder_Settings__mdt mdtRec, Contact con,  boolean isWeekly) {
        Map<String,Object> body = new Map<String,Object>();
        List<String> listOfGoodByeMessages = new List<String>();
        if (mdtRec.Good_Bye_Messages__c != null) {
            listOfGoodByeMessages = mdtRec.Good_Bye_Messages__c.split(';');
        }
        
        string message = ''; 
        if (isWeekly) {
            message = mdtRec.Weekly_Reminder_Message__c;
        } else {
            message = mdtRec.Mothly_Reminder_Message__c;
        }
        
        message = message.replaceAll('firstName', con.FirstName);
        message = message.replaceAll('lastName', con.LastName); 
        message = message.replaceAll('randomGoodByMessage', listOfGoodByeMessages[(Integer) (math.random() * (listOfGoodByeMessages.size()-1))]);
        body.put('channel',  con.Slack_Profile_Id__c);
        body.put('text',  message);
        //send message as actual user.
        body.put('as-user',  true);
        //Create HttpRequest
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        //for sydney and pune location senders are different, hence two different named credentials.
        req.setEndpoint('https://slack.com/api/chat.postMessage');
        if (con.Work_Location__c != null) {
            if (con.Work_Location__c == 'Sydney Office') {
                req.setHeader('Authorization', 'Bearer ' + mdtRec.Sydney_User_Access_Token__c);
            } else if (con.Work_Location__c == 'Pune Office') {
                req.setHeader('Authorization', 'Bearer ' + mdtRec.Pune_User_Access_Token__c);
            }  
        }
        
        req.setHeader('Content-type', 'application/json');
        req.setBody(JSON.serialize(body));
        return req;
    }
}