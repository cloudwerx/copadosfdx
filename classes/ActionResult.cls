/**
 * Description 	 : ActionResult class for Lightning Component
 * Author		 : Cloudwerx
 * Created Date	 : 9 December 2020
 * Code Coverage : CloudwerxCertificationsController_Test
 * */
public class ActionResult {
    @AuraEnabled
    public Boolean isSuccess;
    
    @AuraEnabled
    public String error;
    
    @AuraEnabled
    public Object data;
    
    public void setSuccess(Map<String, Object> mapData) {
        isSuccess = true;
        error = null;
        data = mapData;
    }
    
    public void setFailure(String errorMessage) {
        isSuccess = false;
        error = errorMessage;
        data = null;
    }
    
    public void setFailure(Exception ex) {
        isSuccess = false;
        error = ex.getMessage();
        data = new Map<String, String> {
            'errorDetails' => ex.getStackTraceString()
                };
                    }
}