/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ChildTransformMapping implements fferpcore.TransformMapping {
    global ChildTransformMapping(String sourceKey, String targetKey, fferpcore.ChildCorrelationStrategy correlationStrategy) {

    }
    global fferpcore.ChildTransformMapping withMapping(fferpcore.TransformMapping mapping) {
        return null;
    }
}
