/************************************************************************************************
* @Name         OnboardingUIController
* @Author       Yash Bhalerao
* @Date         19-01-2021
* @Description  Controller for Onboarding Employee LWC
* @TestClass    OnboardingUIControllerTest
*************************************************************************************************/

public without sharing class OnboardingUIController 
{

    /*public OnboardingUIController() 
    {

    }*/

    /*@AuraEnabled()
    public static List<string> showPicklistValues()
    {
        try 
        {
            Schema.DescribeFieldResult fieldResult = Onboarding_Task__mdt.Type_of_Action__c .getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            List<string> picklistValues = new List<string>();
            for(Schema.PicklistEntry s : ple)
            {
                System.debug('value : ' + s.getLabel());
                picklistValues.add(s.getLabel());
                System.debug('picklistValues : ' + picklistValues);
            }

            return picklistValues;
        } 
        catch (Exception e) 
        {
            throw new AuraHandledException(e.getMessage());
        }
    }*/

    @AuraEnabled()
    public static List<Task> showAllTasks(Id contactId)
    {
        return [SELECT Id, OwnerId, Subject, Status, Priority, Description, Employer_Comments__c, Is_Required__c, Is_Completed__c, Is_Upload_Required__c, Is_Slack_msg_Required__c, Is_Information_Form_required__c, Task_Sequence_Number__c, Uploaded_File_Id__c, Uploaded_File_URL__c, Help_Doc_URL__c, Detail_Button__c, Is_Downloadable__c FROM Task WHERE WhoId =: contactId AND Task_Type__c = 'Employee' ORDER BY Task_Sequence_Number__c ASC LIMIT 10000];
    }

    @AuraEnabled
    public static Contact updateTaskStatus(Id taskToUpdateId)
    {
        try 
        {
            system.debug('taskToUpdateId : ' + taskToUpdateId);

            Task taskToUpdate = [SELECT Id, Status, Is_Completed__c, WhoId, Is_Information_Form_required__c, Is_Slack_msg_Required__c, Is_Upload_Required__c FROM Task WHERE Id =: taskToUpdateId LIMIT 1];
            system.debug('taskToUpdate : ' + taskToUpdate);
            if(taskToUpdate.Is_Completed__c == true)
            {
                system.debug('in if : ' + taskToUpdate);
                taskToUpdate.Is_Completed__c = false;
                taskToUpdate.Status = 'In Progress';
            }
            else
            {
                system.debug('in else : ' + taskToUpdate);
                taskToUpdate.Is_Completed__c = true;
                taskToUpdate.Status = 'Completed';
            }

            update taskToUpdate;

            Contact cont = [SELECT Id, Is_Employee_Onboarding_Complete__c FROM Contact WHERE Id =: taskToUpdate.WhoId];
            return cont;
        } 
        catch (Exception e) 
        {
            //throw new AuraHandledException(e.getMessage());
            return null;
        }
    }

    @AuraEnabled
    public static boolean sendSlackMessage(string slackMessage, Id contactId)
    {
        try 
        {
            /*Id userId = UserInfo.getUserId();
            User u = [select Id, ContactId from User where Id = : userId];
            Id getContactId = u.ContactId;*/
            Contact contactToSend = [SELECT Id, Slack_Display_Name__c, Slack_Profile_Picture_Url__c, Work_Location__c FROM Contact WHERE Id =: contactId];
            //system.debug('getContactId' + getContactId);
            if (!Test.isRunningTest()) 
            {
                Onboarding_SlackWebApi.postMessageAsUserViaBot(slackMessage, contactToSend);
            }
            return true;
        } 
        catch (Exception e) 
        {
            return false;
            //throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static boolean saveOnboardingFeedback(Contact contactToUpdate)
    {
        try
        {
            update contactToUpdate;
            return true;
        } 
        catch (Exception e) 
        {
            return false;
            //throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Contact OnboardingStatus(Id contactId)
    {
        Contact cont = [SELECT Is_Employee_Onboarding_Complete__c, Onboarding_Feedback_Rating__c, Onboarding_Feedback_Text__c FROM Contact WHERE Id =: contactId];
        return cont;
    }

    @AuraEnabled
    public static ContentVersion saveFile(Id idParent, String strFileName, String base64Data, Id contactId) 
    {
        system.debug('idParent : ' + idParent);
        system.debug('base64Data : ' + base64Data);
        system.debug('strFileName : ' + strFileName);

        // Decoding base64Data
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

        // Get current user name
        //string userName = UserInfo.getName();//Returns the context user's full name.
        //string filename = userName;

        Contact cont = [SELECT Id, Name FROM Contact WHERE Id =: contactId];
        string filename = cont.Name;
        
        // inserting file
        ContentVersion cv = new ContentVersion();
        //cv.Title = strFileName;
        cv.Title = filename;
        cv.PathOnClient = '/' + strFileName;
        cv.FirstPublishLocationId = idParent;
        cv.VersionData = EncodingUtil.base64Decode(base64Data);
        cv.IsMajorVersion = true;
        insert cv;

        ContentVersion cv1 = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1];
        ContentWorkspace ws = [SELECT Id, RootContentFolderId FROM ContentWorkspace WHERE Name = 'Onboarding Employee Documents' LIMIT 1];
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = cv1.ContentDocumentId;
        cdl.ShareType = 'I';
        cdl.Visibility = 'AllUsers';
        cdl.LinkedEntityId = ws.Id; //Magic happens here
        insert cdl;

        /*ContentDistribution cd = new ContentDistribution();
        cd.Name = 'Test';
        cd.ContentVersionId = cv.Id;
        cd.PreferencesAllowViewInBrowser= true;
        cd.PreferencesLinkLatestVersion=true;
        cd.PreferencesNotifyOnVisit=false;
        cd.PreferencesPasswordRequired=false;
        cd.PreferencesAllowOriginalDownload= true;
        insert cd;

        system.debug('cd : ' + cd);*/

        //Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cv.Id].ContentDocumentId;
 
        //Create ContentDocumentLink 
        /*ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = idParent;
        cdl.ContentDocumentId = conDocId;
        cdl.shareType = 'V';
        cdl.Visibility = 'AllUsers';
        Insert cdl;*/

        if (!Test.isRunningTest()) 
        {
            getAccessToken(cv.Id, contactId, strFileName);
        }
        return cv;
    }

    @future(callout=true)
    public static void getAccessToken(Id cv, Id contactId, string strFileName)
    {
        Onboarding_App_Google_Drive_Setting__mdt googleSettings = [SELECT Client_Id__c, Client_Secret__c, Refresh_Token__c FROM Onboarding_App_Google_Drive_Setting__mdt LIMIT 1];    
        string key = googleSettings.Client_Id__c; //'136243563566-32gs0scnfvcvgnn7jcthaonbkqp82ogj.apps.googleusercontent.com';
        string secret = googleSettings.Client_Secret__c; //'bAQnpN-TrbaKHuQbVV8nzsL9';
        string refresh_token = googleSettings.Refresh_Token__c;
        string redirect_uri = 'https://cloudwerx3-dev-ed--c.visualforce.com/apex/GDriveIntegrationPage';
        string accesstoken;
        integer expiresIn;
        string tokentype;

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('https://www.googleapis.com/oauth2/v4/token');
        req.setHeader('content-type', 'application/x-www-form-urlencoded');
        String messageBody = 'client_id=' + key + '&client_secret=' + secret + '&refresh_token=' + refresh_token + '&redirect_uri=' + redirect_uri + '&grant_type=refresh_token';
        req.setHeader('Content-length', String.valueOf(messageBody.length()));
        req.setBody(messageBody);
        req.setTimeout(60 * 1000);
        Http h = new Http();
        String resp;
        HttpResponse res = h.send(req);
        resp = res.getBody();
        system.debug(resp);

        JSONParser parser = JSON.createParser(resp);
        while (parser.nextToken() != null) 
        {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) 
            {
                String fieldName = parser.getText();
                parser.nextToken();
                if (fieldName == 'access_token') 
                {
                    accesstoken = parser.getText();
                }
                else if (fieldName == 'expires_in') 
                {
                    expiresIn = parser.getIntegerValue();
                }
                else if (fieldname == 'token_type') 
                {
                    tokentype = parser.getText();
                }
            }
        }

        uploadFile(accesstoken, cv, contactId, strFileName);
    }

    public static void uploadFile(string accessToken, Id cv, Id contactId, string strFileName)
    {
        string taskId;
        string folderId;

        ContentVersion cv1 = [SELECT Id,ContentDocumentId FROM ContentVersion WHERE Id =: cv];
        string cdId = cv1.ContentDocumentId;

        ContentDocument cds = [SELECT Id,(SELECT LinkedEntityId FROM ContentDocumentLinks) FROM ContentDocument WHERE Id =: cdId];
        String sObjName = '';
        system.debug(cds);    
        for (ContentDocumentLink cdl : cds.ContentDocumentLinks)
        {
            sObjName = cdl.LinkedEntityId.getSObjectType().getDescribe().getName(); 
            //system.debug(sObjName + ' ' + c.Id);
            if(sobjName == 'Task')
            {
                taskId = cdl.LinkedEntityId;
                system.debug('Task : ' + cdl.LinkedEntityId);
            }
            else
            {
                system.debug('Not task : ' + cdl.LinkedEntityId);
            }
        }

        system.debug('Task : ' + taskId);

        Task t = [SELECT Id, Type_of_Document__c, Uploaded_File_Id__c FROM Task WHERE Id =: taskId LIMIT 1];
        string documentType = t.Type_of_Document__c;
        string uploadedFileId = t.Uploaded_File_Id__c;
        system.debug('uploadedFileId : ' + uploadedFileId);
        system.debug('Task t : ' + t);

        if(uploadedFileId != null)
        {
            system.debug('uploadedFileId : ' + uploadedFileId);
            deleteExisitingFile(accessToken, uploadedFileId);
        }

        List<Onboarding_App_Google_Drive_Folder_Id__mdt> allFolders = [SELECT Folder_Id__c, Folder_Name__c FROM Onboarding_App_Google_Drive_Folder_Id__mdt LIMIT 10000];
        Onboarding_App_Google_Drive_Folder_Id__mdt other = [SELECT Folder_Id__c, Folder_Name__c FROM Onboarding_App_Google_Drive_Folder_Id__mdt WHERE Folder_Name__c = 'Other Documents'];
        string otherFolderId = other.Folder_Id__c;

        system.debug('documentType : ' + documentType);

        for(Onboarding_App_Google_Drive_Folder_Id__mdt fId : allFolders)
        {
            if(documentType == fId.Folder_Name__c)
            {
                folderId = fId.Folder_Id__c;
                system.debug('documentType : ' + documentType);
                system.debug('folderId : ' + folderId);
                system.debug('fId.Folder_Name__c : ' + fId.Folder_Name__c);
                break;
            }
            else
            {
                folderId = otherFolderId;
                system.debug('inside else');
            }
        }

        string filetype;

        if(strFileName.contains('.jpg'))
        {
            filetype = 'image/jpeg';
        }
        else if(strFileName.contains('.png'))
        {
            filetype = 'image/png';
        }
        else if(strFileName.contains('.doc'))
        {
            filetype = 'application/msword';
        }
        else if(strFileName.contains('.gif'))
        {
            filetype = 'image/gif';
        }
        else
        {
            filetype = 'application/pdf';
        }

        string filename;
        integer expiresIn;
        string tokentype;

        //Get the file uploaded to Salesforce files as a blob to process it and insert it to Google Drive
        ContentVersion fileRecord = [SELECT Id, ContentDocumentId, VersionData FROM Contentversion WHERE Id =:cv];
        Blob csvFileBody = fileRecord.VersionData;
        system.debug('csvFileBody : ' + csvFileBody);

        // Get current user name and assign it to file name with document type
        //string userName = UserInfo.getName(); //Returns the context user's first name.

        Contact cont = [SELECT Id, Name FROM Contact WHERE Id =: contactId];
        string userName = cont.Name;
        
        if(documentType != null)
        {
            filename = userName + '_' + documentType;
        }
        else
        {
            filename = userName;
        }
        

        String boundary = '----------9889464542212';
        String delimiter = '\r\n--' + boundary + '\r\n';
        String close_delim = '\r\n--' + boundary + '--';
        String bodyEncoded = EncodingUtil.base64Encode(csvFileBody);

        Onboarding_App_Google_Drive_Setting__mdt googleFolderId = [SELECT Client_Id__c, Client_Secret__c, Refresh_Token__c FROM Onboarding_App_Google_Drive_Setting__mdt LIMIT 1];
        
        //To drive
        //String body = delimiter + 'Content-Type: application/json\r\n\r\n' + '{ "title" : "' + filename + '",' + ' "mimeType" : "' + filetype + '" }' + delimiter + 'Content-Type: ' + filetype + '\r\n' + 'Content-Transfer-Encoding: base64\r\n' + '\r\n' + bodyEncoded + close_delim;

        //To folder
        String body=delimiter+'Content-Type: application/json\r\n\r\n'+'{ "title" : "'+ filename+'",'+ ' "mimeType" : "'+ filetype+ '",' + '"parents":[{"id":"'+ folderId +'"}] }'+delimiter+'Content-Type: ' + filetype + '\r\n'+'Content-Transfer-Encoding: base64\r\n'+'\r\n'+bodyEncoded+close_delim;
        Http http = new Http();
        HttpRequest requ = new HttpRequest();
        requ.setEndpoint('https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart');
        requ.setHeader('Authorization', 'Bearer ' + accessToken);
        requ.setHeader('Content-Type', 'multipart/mixed; boundary="' + boundary + '"');
        requ.setHeader('Content-length', String.valueOf(body.length()));
        requ.setBody(body);
        requ.setMethod('POST');
        requ.setTimeout(60 * 1000);
        string resp2;
        HttpResponse res2 = http.send(requ);
        resp2 = res2.getBody();
        //system.debug('resp2 : ' + resp2);

        string fileId;

        JSONParser parser2 = JSON.createParser(resp2);
        system.debug('parser 2 : ' + parser2);
        while (parser2.nextToken() != null) 
        {
            if ((parser2.getCurrentToken() == JSONToken.FIELD_NAME)) 
            {
                String fieldName = parser2.getText();
                parser2.nextToken();
                if (fieldName == 'id') 
                {
                    fileId = parser2.getText();
                    system.debug('parser2.getText() : ' + parser2.getText());
                    break;
                }
            }
        }

        system.debug('fileId : ' + fileId);

        //Update uploaded file id field once file is uploaded
        Task taskToUpdate = [SELECT Id, Uploaded_File_Id__c, Uploaded_File_URL__c FROM Task WHERE Id =: taskId LIMIT 1];
        taskToUpdate.Uploaded_File_Id__c = fileId;
        taskToUpdate.Uploaded_File_URL__c = 'https://drive.google.com/file/d/' + fileId;
        update taskToUpdate;


        cv = null;
        filetype = '';
        filename = '';
    }

    public static void deleteExisitingFile(string accessToken, string uploadedFileId)
    {
        Http http = new Http();
        HttpRequest requ = new HttpRequest();
        requ.setEndpoint('https://www.googleapis.com/drive/v2/files/' + uploadedFileId);
        requ.setHeader('Authorization', 'Bearer ' + accessToken);
        requ.setMethod('DELETE');
        requ.setTimeout(60 * 1000);
        string resp3;
        if (!Test.isRunningTest()) 
        {
            HttpResponse res2 = http.send(requ);
        }
        //resp3 = res2.getBody();
        system.debug('resp3 : ' + resp3);
    }
}