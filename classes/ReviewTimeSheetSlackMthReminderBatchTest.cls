/**
* @name         ReviewTimeSheetSlackMthReminderBatchTest
* @author       Girish
* @date         30/08/2021
* @description  This is test class for ReviewTimeSheetSlackMthReminderBatch
**/
@isTest
public with sharing class ReviewTimeSheetSlackMthReminderBatchTest {
    
    @TestSetup
    static void setup()
    {
        Account acc = new Account();
        acc.Name = 'Cloudwerx';
        insert acc;                                                            
        Contact con = new Contact();
        con.FirstName = 'test';
        con.LastName = 'contact';
        con.Email = 'testcontact@test.co';
        con.Work_Location__c = 'Pune Office';
        con.Employee_Role__c = 'Salesforce Developer';
        con.Employee_Type__c = 'Employee';
        con.Official_Email__c = 'test@test.co';
        con.Status__c = 'Active Employee';
        con.Is_Slack_User__c = true;
        con.Date_Of_Joining__c = date.today().addYears(-1);
        con.Slack_Profile_Id__c = 'UASDFLKLJLFD';
        con.AccountId = acc.Id;
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE Name = 'India Employee' AND isActive = true LIMIT 1];
        con.RecordTypeId = rt.id;
        
        insert con;
    }

    @isTest
    public static void test()
    {
        TimeSheetReminderUtils.todayDate = DateTime.now().addMonths(1).date().toStartOfMonth().addDays(-1);
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new Onboarding_SlackResponseMock());  
        ReviewTimeSheetSlackMthReminderBatch obj = new ReviewTimeSheetSlackMthReminderBatch();      
        String sch = '0 32 21 1/1 * ? *';
        system.schedule('Test check', sch, obj);
        obj.execute(null);
        Test.stopTest();
    }
}