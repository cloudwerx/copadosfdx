/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Exceptions {
global virtual class AppException extends Exception {
}
global virtual class InvalidArgumentException extends Exception {
}
global class InvalidIdException extends Exception {
}
global class InvalidPeriodException extends Exception {
}
global class InvalidSettingsException extends Exception {
}
global class InvalidTemplateException extends Exception {
}
global class MalformedCallException extends Exception {
}
global class PackageNotInstalledException extends Exception {
}
}
