/**
 * Description	: Test Class for CloudwerxCertificationsController and CertificationDependency Classes
 * Author		: Cloudwerx
 * Created Date	: 16 December 2020
 **/
@isTest
public class CloudwerxCertificationsController_Test {
		
    @TestSetup
    static void createTestData(){
        //Create Account
        Account acc = new Account(Name = 'TestAccount', Active__c = 'Active');
        insert acc;
        //Create Contact
        Contact cnt = new Contact(FirstName = 'Dummy', LastName='Contact', Status__c = 'Active', Email ='dummy@cloudwerx.co', accountId = acc.Id, Work_Location__c = 'Pune Office', Employee_Role__c = 'Salesforce Developer', Date_Of_Joining__c = date.parse('11/02/2021'), Employee_Type__c = 'Employee');
        insert cnt;
        //Create Certification
        Certification__c cert = new Certification__c(Certification_Name__c = 'Administrator', Date_Achieved__c = System.today(), Contact__c = cnt.Id);
        insert cert;
    }
    
    //Test Method to check Scenario of Existing Certification
    static testMethod void getCertification(){
        Contact cntObj = [SELECT Id, name From Contact LIMIT 1];
        ActionResult result = new ActionResult();
        Test.startTest();
        	result = CloudwerxCertificationsController.getCertifications(cntObj.Id);
        	System.assertNotEquals(null, result);
        	System.assertEquals(true, result.isSuccess);
        Test.stopTest();
    }
    
    //Test Method to check Scenario of No Existing Certification
    static testMethod void getCertificationNoExisting(){
        Contact cntObj = [SELECT Id, name From Contact LIMIT 1];
        ActionResult result = new ActionResult();
        Certification__c cert = [SELECT Id FROM Certification__c LIMIT 1];
        delete cert;
        Test.startTest();
        	result = CloudwerxCertificationsController.getCertifications('dummy@cloudwerx.co');
        	System.assertNotEquals(null, result);
        	System.assertEquals(true, result.isSuccess);
        Test.stopTest();
    }
    
    //Test Method to check Scenario of adding new Certifications
    static testMethod void addNewCertifications(){
        Contact cnt = [SELECT Id,Email FROM Contact LIMIT 1];
        List<CloudwerxCertificationsController.wrapcertification> certList  = new List<CloudwerxCertificationsController.wrapcertification>();
        CloudwerxCertificationsController.wrapcertification mywrap = new CloudwerxCertificationsController.wrapcertification();
        mywrap.certification = new Certification__c(Certification_Name__c = 'Advanced Administrator', 
                                                    Date_Achieved__c = System.today(),Contact__c = cnt.Id);
        mywrap.isSelected = true;
        certList.add(mywrap);
        ActionResult result = new ActionResult();
        Test.startTest();
        	result = CloudwerxCertificationsController.insertNewCertifications(certList,cnt.Id);
        Test.stopTest();
    }
    
    //Test Method to check Scenario of adding new Certifications without Contact
    static testMethod void addNewCertificationsNegative(){
        List<CloudwerxCertificationsController.wrapcertification> certList  = new List<CloudwerxCertificationsController.wrapcertification>();
        CloudwerxCertificationsController.wrapcertification mywrap = new CloudwerxCertificationsController.wrapcertification();
        mywrap.certification = new Certification__c(Certification_Name__c = 'Advanced Administrator', 
                                                    Date_Achieved__c = System.today());
        mywrap.isSelected = true;
        certList.add(mywrap);
        ActionResult result = new ActionResult();
        Test.startTest();
        	result = CloudwerxCertificationsController.insertNewCertifications(certList,'dummy@cloudwerx.com');
        	//No Significance Just to cover code as the method has not been used anywhere
        	result.setFailure('dummyErrorMessage');
        Test.stopTest();
    }
    
    //Test Method to check invalid Email Address Scenario
    static testMethod void invalidEmailAddress(){
        ActionResult result = new ActionResult();
        Test.startTest();
        	result = CloudwerxCertificationsController.getCertifications('dummy@cloudwerx.com');
        Test.stopTest();
    }
    
    
}