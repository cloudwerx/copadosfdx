/**
* Description   : Wrapper class for CloudwerxCertificationsController Class keeping all the constant value
* Author		: Cloudwerx
* Created Date	: 09 December 2020
* Code Coverage : CloudwerxCertificationsController_Test
* */
public class CertificationDependency {
    
    public class MyWrapper{
        public String specializationCertification{get;set;}
        public String specializationCategory{get;set;}
        public List<String> relevantSpecializationList {get;set;}
        
        public MyWrapper(String specializationCertification, string specializationCategory, List<String> relevantSpecializationList ) {
            this.specializationCertification = specializationCertification;
            this.specializationCategory = specializationCategory;
            this.relevantSpecializationList = relevantSpecializationList;
        }
        
    }
    
    
    // method to return dependent picklist values
    public static Map<String, MyWrapper> certDependencyMap() {
        Map<String, MyWrapper> certMap = new Map<String, MyWrapper>{
            	'Community Cloud Consultant' => new MyWrapper('Community Cloud Consultant', 'Community Cloud', new List<String>{'Partner Portals (PRM)', 'Customer Portals (incl Self Service)'}),
                'CPQ Specialist' => new MyWrapper('CPQ Specialist', 'Sales', new List<String>{'CPQ'}),
                'Education Cloud Consultant' => new MyWrapper('Education Cloud Consultant', 'Education', new List<String>{'Recruiting', 'Student Success', 'Advancement', 'Engagement', 'K-12'}),
                'Field Service Lightning Consultant' => new MyWrapper('Field Service Lightning Consultant', 'Service', new List<String>{'Field Service Lightning', 'ClickSoftware FSE'}),
                'Marketing Cloud Consultant' => new MyWrapper('Marketing Cloud Consultant', 'Marketing', new List<String>{'Core Messaging/Journeys', 'Social Studio', 'Advertising Studio', 'Einstein for Marketing'}),
                'Nonprofit Cloud Consultant' => new MyWrapper('Nonprofit Cloud Consultant', 'Nonprofit', new List<String>{'Engagement', 'Fundraising', 'Program Management', 'Grantmaking'}),
                'Pardot Consultant' => new MyWrapper('Pardot Consultant', 'Marketing', new List<String>{'Pardot', 'Account Based Marketing', 'Business Units'}),
                'Pardot Specialist' => new MyWrapper('Pardot Specialist', 'Marketing', new List<String>{'Pardot', 'Account Based Marketing', 'Business Units'}),
                'Platform Developer I' => new MyWrapper('Platform Dev I', 'Customer 360 Platform', new List<String>{'Platform', 'Heroku', 'Shield', 'Flow'}),
                'Platform Developer II' => new MyWrapper('Platform Dev II', 'Customer 360 Platform', new List<String>{'Platform', 'Heroku', 'Shield', 'Flow'}),
                'Sales Cloud Consultant' => new MyWrapper('Sales Cloud Consultant', 'Sales', new List<String>{'Core Salesforce Maps', 'Sales Cloud', 'Einstein for Sales'}),
                'Service Cloud Consultant' => new MyWrapper('Service Cloud Consultant', 'Service', new List<String>{'Service Cloud', 'Digital Engagement'})
                };
                
        return certMap;
    }
}