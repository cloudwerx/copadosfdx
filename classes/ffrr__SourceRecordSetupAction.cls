/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SourceRecordSetupAction {
    global SourceRecordSetupAction() {

    }
    @InvocableMethod(label='Setup Revenue Cloud source records for FF Revenue Management' description='Map ffrr__templates and GLAs from the appropriate revenue recognition and GLA treatments to source records.')
    global static void setup(List<ffrr.SourceRecordSetupAction.SourceRecord> input) {

    }
global class SourceRecord {
    @InvocableVariable(label='Source Record ID' description='Source record ID' required=true)
    global Id sourceId;
    global SourceRecord() {

    }
}
}
