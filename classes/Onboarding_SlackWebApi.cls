/************************************************************************************************
* @name         Onboarding_SlackWebApi 
* @author       Girish Baviskar
* @date         15-01-2021
* @description  This class has methods to callout slack's REST api. 
* @testClass    
*************************************************************************************************/

public with sharing class Onboarding_SlackWebApi {
    /**
     * @description Method to post a message to slack as bot.
     * @param message  message to send on above channel.
     * @param objContact contact to send message on behalf of.
     * @return 
     **/
    public static void postMessageAsUserViaBot(string message, Contact objContact) {
        string objLocation = objContact.Work_Location__c;
        Onboarding_App_Slack_Setting__mdt metadata =   [SELECT Bot_Named_Credentials__c,
                                                            Introductory_Message_Channel__c 
                                                        FROM Onboarding_App_Slack_Setting__mdt 
                                                        WHERE Office_Location__c = :objLocation 
                                                        WITH SECURITY_ENFORCED
                                                        LIMIT 1];
        List<Onboarding_SlackRequestWrapper> lstOfReq = new list<Onboarding_SlackRequestWrapper>();  
        Onboarding_SlackRequestWrapper objReq = new Onboarding_SlackRequestWrapper();
        objReq.url = 'callout:' + metadata.Bot_Named_Credentials__c + '/chat.postMessage';
        objReq.method = 'POST';
        Map<String,Object> msg = new Map<String,Object>();
        msg.put('channel',  metadata.Introductory_Message_Channel__c);
        msg.put('text', message);
        msg.put('username', objContact.Slack_Display_Name__c);
        msg.put('icon_url', objContact.Slack_Profile_Picture_Url__c);
        objReq.body = JSON.serialize(msg);
        lstOfReq.add(objReq);
        System.enqueueJob(new Onboarding_QueueableSlackCall(lstOfReq));  
    }

    /**
     * @description Constructs request wrapper object to post a message to slack as real user.
     * @param namedCreds   named creds to make callout.
     * @param channel Channel id to send message to.
     * @param message  message to send on above channel.
     * @return  `Onboarding_SlackRequestWrapper`
     **/
    public static Onboarding_SlackRequestWrapper postMessageAsUser(string namedCred, string channel, string message) {
        
        Onboarding_SlackRequestWrapper objReq = new Onboarding_SlackRequestWrapper();
        objReq.url = 'callout:' + namedCred + '/chat.postMessage';
        objReq.method   = 'POST';
        Map<String,Object> msg = new Map<String,Object>();
        msg.put('channel',  channel);
        msg.put('text',  message);
        msg.put('as-user',  'true');
        objReq.body = JSON.serialize(msg);
        return objReq;
    }

   
    /**
     * @description Constructs request wrapper object to add users into given slack channels
     * @param namedCreds   named creds to make callout.
     * @param channel channel id to add user.
     * @param users   comma seperated value of profile ids.
     * @return   `Onboarding_SlackRequestWrapper`
     **/
    public static Onboarding_SlackRequestWrapper addUsersToChannels(string namedCreds, string channel, string users) {
        
        Onboarding_SlackRequestWrapper objReq = new Onboarding_SlackRequestWrapper(); //creating list of requests.
        objReq.url = 'callout:' + namedCreds + '/conversations.invite'; //TODO test extentensively for different locations and diff credentials.
        objReq.method   = 'POST';
        Map<String,Object> msg = new Map<String,Object>();
        msg.put('channel',  channel);
        msg.put('users',  users);
        objReq.body = JSON.serialize(msg);
        return objReq;
    }
 
    /**
     * @description Constructs request wrapper object to get all users profile 
     * @param namedCreds   named creds to make callout.
     * @return   `Onboarding_SlackRequestWrapper`
     **/
    public static Onboarding_SlackRequestWrapper getAllSlackUserProfileDetails(string namedCred) {
        
        Onboarding_SlackRequestWrapper objReq = new Onboarding_SlackRequestWrapper(); //creating list of requests.
        objReq.url = 'callout:' + namedCred + '/users.list'; // slack method to get list of all members of workspace a
        objReq.method = 'GET';
        objReq.body = '';
        return objReq;
    }
    

   /*  public static void notifyEmployerAndEmployeeIfTasksNotCompleted() {
      
        List<Onboarding_App_Slack_Setting__mdt> metaDataSettings = [SELECT  Notify_Employee__c,
                                                                            Office_Location__c,
                                                                            Notify_Employer__c, 
                                                                            Employers_Channel__c,
                                                                            Reminder_Message_To_Employee__c,
                                                                            Reminder_Message_To_Employer__c,
                                                                            Channels__c,
                                                                            Bot_Named_Credentials__c,
                                                                            User_Named_Credentials__c
                                                                    FROM Onboarding_App_Slack_Setting__mdt
                                                                    WITH SECURITY_ENFORCED
                                                                    LIMIT 1000];  
        
        Map< Id, Onboarding_App_Slack_Setting__mdt > mapOfContactAndMetadata = new Map<Id, Onboarding_App_Slack_Setting__mdt>();
         
        
        List<contact> onboardingNotCompletedList = [SELECT Name
                                                            
                                                    FROM Contact
                                                    WHERE Is_Employee_Onboarding_Complete__c = false
                                                    OR Is_Employer_Onboarding_Complete__c = false
                                                    WITH SECURITY_ENFORCED
                                                    LIMIT 10000];

        for (Contact objContact : onboardingNotCompletedList) {
        
            for (Onboarding_App_Slack_Setting__mdt metaData : metaDataSettings) {
                if(metadata.Office_Location__c == objContact.Work_Location__c) {
                    mapOfContactAndMetadata.put(objContact.Id, metaData); // creating map of contact and slack setting applicable for it.
                }
            }
        }
       
        List<contact> employeeOnboardingIncompleteLst = new List<contact>();
        List<contact> employerOnboardingIncompleteLst = new List<contact>();    
        
        for (contact objContact : onboardingNotCompletedList) {
            if (objContact.Is_Employee_Onboarding_Complete__c == false) {
                employeeOnboardingIncompleteLst.add(objContact);
            } 
            if(objContact.Is_Employer_Onboarding_Complete__c == false) {
                employerOnboardingIncompleteLst.add(objContact);
            }
        }
        Onboarding_ContactTriggerHandler.sendSlackMessageToEmployee(employeeOnboardingIncompleteLst,mapOfContactAndMetadata, 'taskReminder');
        Onboarding_ContactTriggerHandler.sendSlackMessageToEmployer(employerOnboardingIncompleteLst,mapOfContactAndMetadata, 'taskReminder');
        List<Onboarding_SlackRequestWrapper> lstOfRequests = new List<Onboarding_SlackRequestWrapper>(); 
        List<Id> contactIds = new List<Id>();
        
    } */
    
    

}