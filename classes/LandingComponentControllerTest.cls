@isTest
public with sharing class LandingComponentControllerTest {
    
    
    @TestSetup
    static void makeData(){
        List<Account> accountList = Onboarding_UtilityClass.createAccounts(1, true);
        List<Contact> employeeList = Onboarding_UtilityClass.createContacts(10, true, accountList[0].Id);
    }
    @IsTest
    static void testGetUserType(){
        List<Contact> employeeList = [SELECT id, Employee_Type__c FROM contact ];
        Contact contactToPass ;
        for (Contact objContact : employeeList) {
            if (objContact.Employee_Type__c == 'Employer') {
                contactToPass = objContact;
                break;
            }
        }
        Test.startTest();
        String type = LandingComponentController.getUserType(contactToPass.Id);
        System.assertEquals('Employer', type, 'user matched');
        Test.stopTest();
        
    }

    /* @IsTest
    static void exceptionGetUserType(){
        
        Test.startTest();
        String type = LandingComponentController.getUserType('98378579348579');
        Test.stopTest();
        
    } */
}