public without sharing class FileUploader {

    @AuraEnabled()
    public static String fetchAttachment(Id taskId) 
    {
        system.debug('taskId : ' + taskId);
        Task t = [SELECT Help_Doc_ID__c FROM Task WHERE Id =: taskId LIMIT 1];
        Id cdId = t.Help_Doc_ID__c;
        system.debug('cdId : ' + cdId);
        ContentVersion a = [SELECT VersionData FROM ContentVersion WHERE ContentDocumentId=:cdId LIMIT 1];
        return EncodingUtil.Base64Encode(a.VersionData);
    }
}