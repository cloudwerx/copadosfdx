<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>Hierarchy</customSettingsType>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>ffrr__AVFCreateFromActualsBatchSize__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of committed Revenue Recognition Transaction Lines per batch processed for which Actual vs. Forecast Lines will be created (default:200).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of committed Revenue Recognition Transaction Lines per batch processed for which Actual vs. Forecast Lines will be created (default:200).</inlineHelpText>
        <label>Actual vs. Forecast Actuals Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__AVFCreateFromForecastBatchSize__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Revenue Forecast Transaction Lines per batch processed for which Actual vs. Forecast Lines will be created (default:200).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Revenue Forecast Transaction Lines per batch processed for which Actual vs. Forecast Lines will be created (default:200).</inlineHelpText>
        <label>Actual vs. Forecast Forecast Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__AVFDeleteLinesBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Actual vs. Forecast Lines per batch processed (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Actual vs. Forecast Lines per batch processed (default:1000).</inlineHelpText>
        <label>Actual vs. Forecast Delete Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__ActualsRetrieveLinesBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>Deprecated: The number of source/staging records to be processed/deleted per batch processed (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>Deprecated: The number of source/staging records to be processed/deleted per batch processed (default:1000).</inlineHelpText>
        <label>Deprecated: Old Staging Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__ActualsTransactionCommitBatchSize__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Revenue Recognition Transactions committed per batch processed (default:1).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Revenue Recognition Transactions committed per batch processed (default:1).</inlineHelpText>
        <label>Actuals Transaction Commit Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__ActualsTransactionSaveBatchSize__c</fullName>
        <defaultValue>500</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of source records for which Revenue Recognition Transaction Lines are created per batch processed (default:500).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of source records for which Revenue Recognition Transaction Lines are created per batch processed (default:500).</inlineHelpText>
        <label>Actuals Transaction Save Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__ActualsTransactionSummaryBatchSize__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Revenue Recognition Transactions summarized per batch processed (default:1).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Revenue Recognition Transactions summarized per batch processed (default:1).</inlineHelpText>
        <label>Actuals Transaction Summary Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__ForecastDraftTransactionDeleteBatchSize__c</fullName>
        <defaultValue>50</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Draft Forecast records that will be deleted per batch processed when running the DeleteDraftForecasts batch process (default:50).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Draft Forecast records that will be deleted per batch processed when running the DeleteDraftForecasts batch process (default:50).</inlineHelpText>
        <label>Forecast Draft Transaction Delete Batch</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__ForecastTransactionSaveBatchSize__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>This is the batch size for the save batch that creates Forecast and Forecast Lines from Draft Lines (default:200).</description>
        <externalId>false</externalId>
        <inlineHelpText>This is the batch size for the save batch that creates Forecast and Forecast Lines from Draft Lines (default:200).</inlineHelpText>
        <label>Forecast Transaction Save Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__GenerateForecastsBatchSize__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Forecast records that will be processed per batch when generating from source records (default:200).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Forecast records that will be processed per batch when generating from source records (default:200).</inlineHelpText>
        <label>Generate Forecasts Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__GroupingBTCDeleteBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Batch Tracking Control records with Action &apos;Create Grouping&apos; that will be deleted per batch processed when running the GroupingDeleteBatch batch process (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Batch Tracking Control records with Action &apos;Create Grouping&apos; that will be deleted per batch processed when running the GroupingDeleteBatch batch process (default:1000).</inlineHelpText>
        <label>Grouping BTC Delete Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__GroupingCreateBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Staging records that will be processed to create the Grouping Summary records per batch processed (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Staging records that will be processed to create the Grouping Summary records per batch processed (default:1000).</inlineHelpText>
        <label>Grouping Create Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__GroupingCreateSynchronousLimit__c</fullName>
        <defaultValue>3000</defaultValue>
        <deprecated>false</deprecated>
        <description>The maximum number of source records that can be grouped synchronously (default:3000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The maximum number of source records that can be grouped synchronously (default:3000).</inlineHelpText>
        <label>Grouping Create Synchronous Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__GroupingDeleteSynchronousLimit__c</fullName>
        <defaultValue>2000</defaultValue>
        <deprecated>false</deprecated>
        <description>The maximum number of grouping records that can be deleted synchronously (default:2000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The maximum number of grouping records that can be deleted synchronously (default:2000).</inlineHelpText>
        <label>Grouping Delete Synchronous Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__GroupingSummaryDeleteBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Grouping Summary records that will be deleted per batch processed when running the GroupingDeleteBatch batch process (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Grouping Summary records that will be deleted per batch processed when running the GroupingDeleteBatch batch process (default:1000).</inlineHelpText>
        <label>Grouping Summary Delete Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__LastOpportunityProductMirrorBatch__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Opportunity Product Scheduler Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>ffrr__ManageRevenueContractBatchSize__c</fullName>
        <defaultValue>500</defaultValue>
        <deprecated>false</deprecated>
        <description>The maximum number of source records processed in a single batch when creating or updating a revenue contract asynchronously (default:500).</description>
        <externalId>false</externalId>
        <inlineHelpText>The maximum number of source records processed in a single batch when creating or updating a revenue contract asynchronously (default:500).</inlineHelpText>
        <label>Manage RC Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__ManageRevenueContractSynchronousLimit__c</fullName>
        <defaultValue>100</defaultValue>
        <deprecated>false</deprecated>
        <description>The maximum number of source records that can be processed synchronously when creating or updating a revenue contract. If the number of records is equal to or greater than this value, all the records are processed in the background. The default is 100.</description>
        <externalId>false</externalId>
        <inlineHelpText>The maximum number of source records that can be processed synchronously when creating or updating a revenue contract. If the number of records is equal to or greater than this value, all the records are processed in the background. The default is 100.</inlineHelpText>
        <label>Manage RC Synchronous Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__OpportunityProductMirrorBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Opportunity Product records for which Opportunity Product Mirror records will be created or updated per batch processed (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Opportunity Product records for which Opportunity Product Mirror records will be created or updated per batch processed (default:1000).</inlineHelpText>
        <label>Opportunity Product Mirror Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__OpportunityProductMirrorDeleteBatchSize__c</fullName>
        <defaultValue>2000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Opportunity Product Mirror records to be deleted per batch processed (default:2000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Opportunity Product Mirror records to be deleted per batch processed (default:2000).</inlineHelpText>
        <label>Opp Product Mirror Delete Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__ProcessControlDeleteBatchSize__c</fullName>
        <defaultValue>2000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Process Control records that will be deleted per batch processed when running the Staging Delete batch process (default:2000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Process Control records that will be deleted per batch processed when running the Staging Delete batch process (default:2000).</inlineHelpText>
        <label>Process Control Delete Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__RRTToJournalCreationBatchSize__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Revenue Recognition Transactions per batch processed when creating FinancialForce Accounting journals (default:1).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Revenue Recognition Transactions per batch processed when creating FinancialForce Accounting journals (default:1).</inlineHelpText>
        <label>RRTs for Journal Creation Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__StagingBTCDeleteBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Batch Tracking Control records with Action &apos;Create Staging&apos; that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Batch Tracking Control records with Action &apos;Create Staging&apos; that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:1000).</inlineHelpText>
        <label>Staging BTC Delete Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__StagingCreateBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of staging records that will be processed per batch processed to create grouping summaries (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of staging records that will be processed per batch processed to create grouping summaries (default:1000).</inlineHelpText>
        <label>Staging Create Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__StagingCreateSynchronousLimit__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The maximum number of source records that can be processed synchronously (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The maximum number of source records that can be processed synchronously (default:1000).</inlineHelpText>
        <label>Staging Create Synchronous Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__StagingDeleteSynchronousLimit__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The maximum number of staging records and grouping records that can be deleted synchronously. At and above this limit Batch Apex is used (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The maximum number of staging records and grouping records that can be deleted synchronously. At and above this limit Batch Apex is used (default:1000).</inlineHelpText>
        <label>Staging Delete Synchronous Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__StagingDetailDeleteBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Staging Detail records that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Staging Detail records that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:1000).</inlineHelpText>
        <label>Staging Detail Delete Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__StagingSaveBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of records that will be processed to create Revenue Recognition Transaction from staging data per batch processed (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of records that will be processed to create Revenue Recognition Transaction from staging data per batch processed (default:1000).</inlineHelpText>
        <label>Staging Save Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__StagingSaveSynchronousLimit__c</fullName>
        <defaultValue>500</defaultValue>
        <deprecated>false</deprecated>
        <description>The maximum number of staging records that can be saved synchronously (default:500).</description>
        <externalId>false</externalId>
        <inlineHelpText>The maximum number of staging records that can be saved synchronously (default:500).</inlineHelpText>
        <label>Staging Save Synchronous Limit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__StagingSummaryDeleteBatchSize__c</fullName>
        <defaultValue>600</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Staging Summary records that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:600).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Staging Summary records that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:600).</inlineHelpText>
        <label>Staging Summary Delete Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__StagingVersionDeleteBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of Staging Version records that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Staging Version records that will be deleted per batch processed when running the StagingDeleteBatch batch process (default:1000).</inlineHelpText>
        <label>Staging Version Delete Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__TransferPreviouslyRecognizedBatchSize__c</fullName>
        <defaultValue>1000</defaultValue>
        <deprecated>false</deprecated>
        <description>The number of performance obligation line items that will be processed per batch when the Transfer Previously Recognized process runs (default:1000).</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of performance obligation line items that will be processed per batch when the Transfer Previously Recognized process runs (default:1000).</inlineHelpText>
        <label>Transfer Prev. Recognized Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Revenue Management Batch Settings</label>
    <visibility>Public</visibility>
</CustomObject>
