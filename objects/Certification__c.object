<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object to track employee certifications</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Certification_Name__c</fullName>
        <description>Picklist of all salesforce certifications</description>
        <externalId>false</externalId>
        <label>Certification Name</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Administrator</fullName>
                    <default>false</default>
                    <label>Administrator</label>
                </value>
                <value>
                    <fullName>Advanced Administrator</fullName>
                    <default>false</default>
                    <label>Advanced Administrator (EK)</label>
                </value>
                <value>
                    <fullName>App Builder</fullName>
                    <default>false</default>
                    <label>App Builder (EK)</label>
                </value>
                <value>
                    <fullName>Platform Developer I</fullName>
                    <default>false</default>
                    <label>Platform Developer I (CO)</label>
                </value>
                <value>
                    <fullName>Platform Developer II</fullName>
                    <default>false</default>
                    <label>Platform Developer II (EK)</label>
                </value>
                <value>
                    <fullName>Commerce Cloud Digital Developer</fullName>
                    <default>false</default>
                    <label>Commerce Cloud Digital Developer (CO)</label>
                </value>
                <value>
                    <fullName>Marketing Cloud Developer</fullName>
                    <default>false</default>
                    <label>Marketing Cloud Developer (CO)</label>
                </value>
                <value>
                    <fullName>Sales Cloud Consultant</fullName>
                    <default>false</default>
                    <label>Sales Cloud Consultant (CO)</label>
                </value>
                <value>
                    <fullName>Service Cloud Consultant</fullName>
                    <default>false</default>
                    <label>Service Cloud Consultant (CO)</label>
                </value>
                <value>
                    <fullName>Community Cloud Consultant</fullName>
                    <default>false</default>
                    <label>Community Cloud Consultant (CO)</label>
                </value>
                <value>
                    <fullName>Field Service Lightning Consultant</fullName>
                    <default>false</default>
                    <label>Field Service Lightning Consultant (EK)</label>
                </value>
                <value>
                    <fullName>Pardot Consultant</fullName>
                    <default>false</default>
                    <label>Pardot Consultant (CO)</label>
                </value>
                <value>
                    <fullName>Marketing Cloud Consultant</fullName>
                    <default>false</default>
                    <label>Marketing Cloud Consultant (CO)</label>
                </value>
                <value>
                    <fullName>Einstein Analytics and Discovery Consultant</fullName>
                    <default>false</default>
                    <label>Einstein Analytics and Discovery Consultant (CO)</label>
                </value>
                <value>
                    <fullName>Pardot Specialist</fullName>
                    <default>false</default>
                    <label>Pardot Specialist</label>
                </value>
                <value>
                    <fullName>Marketing Cloud Email Specialist</fullName>
                    <default>false</default>
                    <label>Marketing Cloud Email Specialist</label>
                </value>
                <value>
                    <fullName>Marketing Cloud Social Specialist</fullName>
                    <default>false</default>
                    <label>Marketing Cloud Social Specialist (EK)</label>
                </value>
                <value>
                    <fullName>CPQ Specialist</fullName>
                    <default>false</default>
                    <label>CPQ Specialist (EK)</label>
                </value>
                <value>
                    <fullName>Heroku Architecture Designer</fullName>
                    <default>false</default>
                    <label>Heroku Architecture Designer (CO)</label>
                </value>
                <value>
                    <fullName>Data Architecture and Management Designer</fullName>
                    <default>false</default>
                    <label>Data Architecture and Management Designer</label>
                </value>
                <value>
                    <fullName>Development Lifecycle and Deployment Designer</fullName>
                    <default>false</default>
                    <label>Development Lifecycle and Deployment Designer</label>
                </value>
                <value>
                    <fullName>Identity and Access Management Designer</fullName>
                    <default>false</default>
                    <label>Identity and Access Management Designer</label>
                </value>
                <value>
                    <fullName>Integration Architecture Designer</fullName>
                    <default>false</default>
                    <label>Integration Architecture Designer</label>
                </value>
                <value>
                    <fullName>Mobile Solutions Architecture Designer</fullName>
                    <default>false</default>
                    <label>Mobile Solutions Architecture Designer</label>
                </value>
                <value>
                    <fullName>Sharing and Visibility Designer</fullName>
                    <default>false</default>
                    <label>Sharing and Visibility Designer</label>
                </value>
                <value>
                    <fullName>Application Architect</fullName>
                    <default>false</default>
                    <label>Application Architect (AR)</label>
                </value>
                <value>
                    <fullName>System Architect</fullName>
                    <default>false</default>
                    <label>System Architect (AR)</label>
                </value>
                <value>
                    <fullName>Technical Architect</fullName>
                    <default>false</default>
                    <label>Technical Architect (AR)</label>
                </value>
                <value>
                    <fullName>Nonprofit Cloud Consultant</fullName>
                    <default>false</default>
                    <label>Nonprofit Cloud Consultant (CO)</label>
                </value>
                <value>
                    <fullName>Education Cloud Consultant</fullName>
                    <default>false</default>
                    <label>Education Cloud Consultant (CO)</label>
                </value>
                <value>
                    <fullName>Javascript Developer I</fullName>
                    <default>false</default>
                    <label>Javascript Developer I</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <externalId>false</externalId>
        <label>Employee Name</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Certifications</relationshipLabel>
        <relationshipName>Certifications</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Date_Achieved__c</fullName>
        <description>Date at which a certification was achieved</description>
        <externalId>false</externalId>
        <label>Date Achieved</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Relevant_Specialization__c</fullName>
        <description>This picklist valueset represents the relavant specializations for the specific Specialization</description>
        <externalId>false</externalId>
        <label>Relevant Specialization</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <controllingField>Specialization_Certification__c</controllingField>
            <restricted>true</restricted>
            <valueSetName>Relavant_Specialization</valueSetName>
            <valueSettings>
                <controllingFieldValue>Nonprofit Cloud Consultant</controllingFieldValue>
                <controllingFieldValue>Education Cloud Consultant</controllingFieldValue>
                <valueName>Engagement</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Nonprofit Cloud Consultant</controllingFieldValue>
                <valueName>Fundraising</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Nonprofit Cloud Consultant</controllingFieldValue>
                <valueName>Program Management</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Nonprofit Cloud Consultant</controllingFieldValue>
                <valueName>Grantmaking</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pardot Consultant</controllingFieldValue>
                <controllingFieldValue>Pardot Specialist</controllingFieldValue>
                <valueName>Pardot</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pardot Consultant</controllingFieldValue>
                <controllingFieldValue>Pardot Specialist</controllingFieldValue>
                <valueName>Account Based Marketing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pardot Consultant</controllingFieldValue>
                <controllingFieldValue>Pardot Specialist</controllingFieldValue>
                <valueName>Business Units</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Platform Dev I</controllingFieldValue>
                <controllingFieldValue>Platform Dev I &amp; Platform Dev II</controllingFieldValue>
                <controllingFieldValue>Platform Dev II</controllingFieldValue>
                <valueName>Platform</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Platform Dev I</controllingFieldValue>
                <controllingFieldValue>Platform Dev I &amp; Platform Dev II</controllingFieldValue>
                <controllingFieldValue>Platform Dev II</controllingFieldValue>
                <valueName>Heroku</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Platform Dev I</controllingFieldValue>
                <controllingFieldValue>Platform Dev I &amp; Platform Dev II</controllingFieldValue>
                <controllingFieldValue>Platform Dev II</controllingFieldValue>
                <valueName>Shield</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Platform Dev I</controllingFieldValue>
                <controllingFieldValue>Platform Dev I &amp; Platform Dev II</controllingFieldValue>
                <controllingFieldValue>Platform Dev II</controllingFieldValue>
                <valueName>Flow</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Sales Cloud Consultant</controllingFieldValue>
                <valueName>Core Salesforce Maps</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Sales Cloud Consultant</controllingFieldValue>
                <valueName>Sales Cloud</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Sales Cloud Consultant</controllingFieldValue>
                <valueName>Einstein for Sales</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Service Cloud Consultant</controllingFieldValue>
                <valueName>Service Cloud</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Service Cloud Consultant</controllingFieldValue>
                <valueName>Digital Engagement</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2B Commerce Admin Accreditation</controllingFieldValue>
                <controllingFieldValue>B2B Commerce Developer Accreditation</controllingFieldValue>
                <valueName>B2B Commerce</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2C Commerce Digital Developer</controllingFieldValue>
                <valueName>B2C Commerce</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2C Commerce Digital Developer</controllingFieldValue>
                <valueName>Salesforce Order Management</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2C Commerce Digital Developer</controllingFieldValue>
                <valueName>Commerce Portals</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2C Commerce Digital Developer</controllingFieldValue>
                <valueName>Einstein for Commerce</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Billing Specialist and Billing Advanced Specialist Superbadge</controllingFieldValue>
                <valueName>Billing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Community Cloud Consultant</controllingFieldValue>
                <valueName>Partner Portals (PRM)</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Community Cloud Consultant</controllingFieldValue>
                <valueName>Customer Portals (incl Self Service)</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>CPQ Specialist</controllingFieldValue>
                <valueName>CPQ</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education Cloud Consultant</controllingFieldValue>
                <valueName>Recruiting</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education Cloud Consultant</controllingFieldValue>
                <valueName>Student Success</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education Cloud Consultant</controllingFieldValue>
                <valueName>Advancement</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education Cloud Consultant</controllingFieldValue>
                <valueName>K-12</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Field Service Lightning Consultant</controllingFieldValue>
                <valueName>Field Service Lightning</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Field Service Lightning Consultant</controllingFieldValue>
                <valueName>ClickSoftware FSE</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing Cloud Consultant</controllingFieldValue>
                <valueName>Core Messaging/Journeys</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing Cloud Consultant</controllingFieldValue>
                <valueName>Social Studio</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing Cloud Consultant</controllingFieldValue>
                <valueName>Advertising Studio</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing Cloud Consultant</controllingFieldValue>
                <valueName>Einstein for Marketing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>MuleSoft Certified Platform Architect - Level 1</controllingFieldValue>
                <valueName>Mulesoft Anypoint Platform</valueName>
            </valueSettings>
        </valueSet>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>ReturnOne__c</fullName>
        <description>Always returns 1. Used by a report to calculate the average number of certs per person</description>
        <externalId>false</externalId>
        <formula>1</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>ReturnOne</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Specialization_Category__c</fullName>
        <description>This picklist value set is for the Specialization Categories for Salesforce</description>
        <externalId>false</externalId>
        <label>Specialization Category</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>Specialization_Certification__c</controllingField>
            <restricted>true</restricted>
            <valueSetName>Specialization_Category</valueSetName>
            <valueSettings>
                <controllingFieldValue>Nonprofit Cloud Consultant</controllingFieldValue>
                <valueName>Nonprofit</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pardot Consultant</controllingFieldValue>
                <controllingFieldValue>Pardot Specialist</controllingFieldValue>
                <controllingFieldValue>Marketing Cloud Consultant</controllingFieldValue>
                <valueName>Marketing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Platform Dev I</controllingFieldValue>
                <controllingFieldValue>Platform Dev I &amp; Platform Dev II</controllingFieldValue>
                <controllingFieldValue>Platform Dev II</controllingFieldValue>
                <valueName>Customer 360 Platform</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Sales Cloud Consultant</controllingFieldValue>
                <controllingFieldValue>Billing Specialist and Billing Advanced Specialist Superbadge</controllingFieldValue>
                <controllingFieldValue>CPQ Specialist</controllingFieldValue>
                <valueName>Sales</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Service Cloud Consultant</controllingFieldValue>
                <controllingFieldValue>Field Service Lightning Consultant</controllingFieldValue>
                <valueName>Service</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2B Commerce Admin Accreditation</controllingFieldValue>
                <controllingFieldValue>B2B Commerce Developer Accreditation</controllingFieldValue>
                <valueName>B2B Commerce</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2C Commerce Digital Developer</controllingFieldValue>
                <valueName>B2C Commerce</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Community Cloud Consultant</controllingFieldValue>
                <valueName>Community Cloud</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education Cloud Consultant</controllingFieldValue>
                <valueName>Education</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>MuleSoft Certified Platform Architect - Level 1</controllingFieldValue>
                <valueName>Integration_MuleSoft</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>Specialization_Certification__c</fullName>
        <description>This picklist field has the names of the certifications linked to specific Specialization</description>
        <externalId>false</externalId>
        <label>Specialization Certification</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Specialization_Certification</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>is_Offshore_Employee__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Contact__r.Work_Location__c) = &apos;Pune Office&apos;</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>is Offshore Employee</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Certification</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Certification_Name__c</columns>
        <columns>Contact__c</columns>
        <columns>Date_Achieved__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>Offshore_Employee_Certification</fullName>
        <columns>NAME</columns>
        <columns>Certification_Name__c</columns>
        <columns>Contact__c</columns>
        <columns>Specialization_Certification__c</columns>
        <columns>Date_Achieved__c</columns>
        <columns>Specialization_Category__c</columns>
        <columns>Relevant_Specialization__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>is_Offshore_Employee__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>Offshore Employee Certification</label>
    </listViews>
    <listViews>
        <fullName>Onshore_Employee_Certifications</fullName>
        <columns>NAME</columns>
        <columns>Certification_Name__c</columns>
        <columns>Contact__c</columns>
        <columns>Specialization_Certification__c</columns>
        <columns>Date_Achieved__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>is_Offshore_Employee__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>Onshore Employee Certifications</label>
    </listViews>
    <nameField>
        <displayFormat>C-{00000}</displayFormat>
        <label>Certification ID</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Certifications</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
