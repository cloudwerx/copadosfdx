<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Temporary storage for Grouping summary information</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>ffrr__BatchTrackingControl__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This field looks up to the Batch Tracking Control that creates child groupings for this grouping summary, if any.</description>
        <externalId>false</externalId>
        <label>Batch Tracking Control</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ffrr__BatchTrackingControl__c.ffrr__Action__c</field>
                <operation>equals</operation>
                <value>Create Grouping</value>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>ffrr__BatchTrackingControl__c</referenceTo>
        <relationshipLabel>Grouping Summaries</relationshipLabel>
        <relationshipName>GroupingSummaries</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ffrr__Expanded__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This flag indicates if Child Groupings have been created for this Grouping</description>
        <externalId>false</externalId>
        <label>Expanded</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__FieldLabel__c</fullName>
        <deprecated>false</deprecated>
        <description>Label of field used to create grouping summary</description>
        <externalId>false</externalId>
        <label>Grouping Field Label</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__Field__c</fullName>
        <deprecated>false</deprecated>
        <description>Field used to create grouping summary</description>
        <externalId>false</externalId>
        <label>Grouping Field</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__GroupingOrder__c</fullName>
        <deprecated>false</deprecated>
        <description>The position of this Grouping Summary relative to the Grouping Summary at the highest level.</description>
        <externalId>false</externalId>
        <label>Grouping Summary Order</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__GroupingParent__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Parent Grouping</label>
        <referenceTo>ffrr__GroupingSummary__c</referenceTo>
        <relationshipLabel>Parent Grouping Summary</relationshipLabel>
        <relationshipName>ParentGroupingSummary</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ffrr__GroupingValueHash__c</fullName>
        <deprecated>false</deprecated>
        <description>The hash of the path of all field values used to create this Grouping</description>
        <externalId>false</externalId>
        <label>Grouping Value Hash</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__IsExpandable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This flag indicates if Child Groupings can be created for this Grouping</description>
        <externalId>false</externalId>
        <label>Is Expandable</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__PreviouslyAmortized__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Previously Amortized</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__PreviouslyRecognized__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Previously Recognized</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__SObjectType__c</fullName>
        <deprecated>false</deprecated>
        <description>The SObjectType of the grouped data.</description>
        <externalId>false</externalId>
        <inlineHelpText>The SObjectType of the grouped data.</inlineHelpText>
        <label>SObjectType</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__ToAmortizeThisPeriod__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>To Amortize This Period</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__ToRecognizeThisPeriod__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>To Recognize This Period</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__TotalCost__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__TotalLeafRecordCount__c</fullName>
        <deprecated>false</deprecated>
        <description>The total number of leaf records under this grouping summary.</description>
        <externalId>false</externalId>
        <label>Total Leaf Record Count</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__TotalRevenue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Revenue</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__Version__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The version from the staging table used for this grouping summary</description>
        <externalId>false</externalId>
        <label>Staging Table Version</label>
        <referenceTo>ffrr__StagingVersion__c</referenceTo>
        <relationshipLabel>Staging Table Version</relationshipLabel>
        <relationshipName>StagingTableVersion</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Grouping Summary</label>
    <nameField>
        <label>Grouping Field Value</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Grouping Summaries</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
