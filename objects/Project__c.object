<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This custom object has data related to the projects in Cloudwerx</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Client_Admin__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field points to contact record that represents the Client Admin.</description>
        <externalId>false</externalId>
        <label>Client Admin</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Projects (Client Admin)</relationshipLabel>
        <relationshipName>Projects1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Client_Executive__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field points to the contact record representing the Client executive.</description>
        <externalId>false</externalId>
        <label>Client Executive</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Projects</relationshipLabel>
        <relationshipName>Projects</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Client__c</fullName>
        <description>This field represents the Client associated with this particular Project record</description>
        <externalId>false</externalId>
        <label>Client</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Projects</relationshipLabel>
        <relationshipName>Projects</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>End_Date__c</fullName>
        <description>This field represents the end data for a project</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Engagement_Type__c</fullName>
        <description>This field represents the engagement type with the Client for a particular project.</description>
        <externalId>false</externalId>
        <label>Engagement Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Managed Service</fullName>
                    <default>false</default>
                    <label>Managed Service</label>
                </value>
                <value>
                    <fullName>Product Implementation</fullName>
                    <default>false</default>
                    <label>Product Implementation</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Go_Live_Date__c</fullName>
        <description>This field represents the GoLive date for the project</description>
        <externalId>false</externalId>
        <label>Go Live Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Industry_Specialization__c</fullName>
        <description>This field represents the Industry specialization to which the projects belong to</description>
        <externalId>false</externalId>
        <label>Industry Specialization</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <controllingField>Industry__c</controllingField>
            <restricted>true</restricted>
            <valueSetName>Industry_Specialization</valueSetName>
            <valueSettings>
                <controllingFieldValue>Manufacturing</controllingFieldValue>
                <valueName>Discrete Manufacturing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Manufacturing</controllingFieldValue>
                <valueName>Process Manufacturing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Media &amp; Entertainment</controllingFieldValue>
                <valueName>Agency / Advertising</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Media &amp; Entertainment</controllingFieldValue>
                <valueName>Broadcasting</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Media &amp; Entertainment</controllingFieldValue>
                <valueName>Entertainment</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Media &amp; Entertainment</controllingFieldValue>
                <valueName>Publishing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Professional Services</controllingFieldValue>
                <valueName>Business Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Professional Services</controllingFieldValue>
                <valueName>Engineering, Accounting, Research, Mgmt Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Professional Services</controllingFieldValue>
                <valueName>Other Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Public Sector</controllingFieldValue>
                <valueName>Government</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Public Sector</controllingFieldValue>
                <valueName>Political Organizations</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Food, Drug, Convenience, Grocery, &amp; Restaurants</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Mass Merchants, Dept Stores, &amp; e-Retail</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Retail</controllingFieldValue>
                <valueName>Apparel, Footwear, Accessories, &amp; Speciality</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Salesforce.org Sectors</controllingFieldValue>
                <valueName>Nonprofit</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Salesforce.org Sectors</controllingFieldValue>
                <valueName>Education</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Travel, Transportation, and Hospitality</controllingFieldValue>
                <valueName>Travel</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Travel, Transportation, and Hospitality</controllingFieldValue>
                <valueName>Transportation</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Travel, Transportation, and Hospitality</controllingFieldValue>
                <valueName>Hospitatlity</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Agriculture &amp; Mining</controllingFieldValue>
                <valueName>Agriculture</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Agriculture &amp; Mining</controllingFieldValue>
                <valueName>Mining</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Automotive</controllingFieldValue>
                <valueName>Dealers</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Automotive</controllingFieldValue>
                <valueName>OEMs</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Communications</controllingFieldValue>
                <valueName>Communications</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Communications</controllingFieldValue>
                <valueName>Communications Equipment</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Consumer Goods</controllingFieldValue>
                <valueName>Fashion &amp; Beauty</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Consumer Goods</controllingFieldValue>
                <valueName>Food &amp; Beverage</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Consumer Goods</controllingFieldValue>
                <valueName>Household Products</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Consumer Goods</controllingFieldValue>
                <valueName>Other CG</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education</controllingFieldValue>
                <valueName>Education Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education</controllingFieldValue>
                <valueName>Educational Institutes</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Energy</controllingFieldValue>
                <valueName>Oil &amp; Gas</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Energy</controllingFieldValue>
                <valueName>Utilities</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Engineering, Construction, &amp; Real Estate</controllingFieldValue>
                <valueName>Construction</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Engineering, Construction, &amp; Real Estate</controllingFieldValue>
                <valueName>Engineering</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Engineering, Construction, &amp; Real Estate</controllingFieldValue>
                <valueName>Real Estate</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Financial Services</controllingFieldValue>
                <valueName>Banking</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Financial Services</controllingFieldValue>
                <valueName>Capital Markets</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Financial Services</controllingFieldValue>
                <valueName>Insurance</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Financial Services</controllingFieldValue>
                <valueName>Wealth &amp; Asset Management</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Healthcare &amp; Life Sciences</controllingFieldValue>
                <valueName>Health Plans</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Healthcare &amp; Life Sciences</controllingFieldValue>
                <valueName>Medical Devices and Diagnostics</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Healthcare &amp; Life Sciences</controllingFieldValue>
                <valueName>Pharma</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Healthcare &amp; Life Sciences</controllingFieldValue>
                <valueName>Providers</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>High Tech</controllingFieldValue>
                <valueName>Semiconductor &amp; Components</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>High Tech</controllingFieldValue>
                <valueName>Hardware Manufacturing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>High Tech</controllingFieldValue>
                <valueName>Software and Services</valueName>
            </valueSettings>
        </valueSet>
        <visibleLines>8</visibleLines>
    </fields>
    <fields>
        <fullName>Industry__c</fullName>
        <description>This picklist field represents the Industry to which the project belongs to.</description>
        <externalId>false</externalId>
        <label>Industry Category</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Industry_Category</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Number_of_Users__c</fullName>
        <description>This field represents the number of users for this project</description>
        <externalId>false</externalId>
        <label>Number of Users</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1</fullName>
                    <default>false</default>
                    <label>1</label>
                </value>
                <value>
                    <fullName>2</fullName>
                    <default>false</default>
                    <label>2</label>
                </value>
                <value>
                    <fullName>3</fullName>
                    <default>false</default>
                    <label>3</label>
                </value>
                <value>
                    <fullName>4</fullName>
                    <default>false</default>
                    <label>4</label>
                </value>
                <value>
                    <fullName>5</fullName>
                    <default>false</default>
                    <label>5</label>
                </value>
                <value>
                    <fullName>6</fullName>
                    <default>false</default>
                    <label>6</label>
                </value>
                <value>
                    <fullName>7</fullName>
                    <default>false</default>
                    <label>7</label>
                </value>
                <value>
                    <fullName>8</fullName>
                    <default>false</default>
                    <label>8</label>
                </value>
                <value>
                    <fullName>9</fullName>
                    <default>false</default>
                    <label>9</label>
                </value>
                <value>
                    <fullName>10</fullName>
                    <default>false</default>
                    <label>10</label>
                </value>
                <value>
                    <fullName>11</fullName>
                    <default>false</default>
                    <label>11</label>
                </value>
                <value>
                    <fullName>12</fullName>
                    <default>false</default>
                    <label>12</label>
                </value>
                <value>
                    <fullName>13</fullName>
                    <default>false</default>
                    <label>13</label>
                </value>
                <value>
                    <fullName>14</fullName>
                    <default>false</default>
                    <label>14</label>
                </value>
                <value>
                    <fullName>15</fullName>
                    <default>false</default>
                    <label>15</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Primary_Country__c</fullName>
        <externalId>false</externalId>
        <label>Primary Country</label>
        <length>30</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Relevant_Specialization__c</fullName>
        <externalId>false</externalId>
        <label>Product Relevant Specialization</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <controllingField>Product_Specialization_Category__c</controllingField>
            <restricted>true</restricted>
            <valueSetName>Relavant_Specialization</valueSetName>
            <valueSettings>
                <controllingFieldValue>Pardot</controllingFieldValue>
                <valueName>Pardot</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pardot</controllingFieldValue>
                <valueName>Account Based Marketing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Pardot</controllingFieldValue>
                <valueName>Business Units</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Analytics</controllingFieldValue>
                <valueName>Tableau</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Analytics</controllingFieldValue>
                <valueName>EA Industry Products</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Analytics</controllingFieldValue>
                <valueName>Discovery</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Analytics</controllingFieldValue>
                <valueName>Prediction Builder</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Sales</controllingFieldValue>
                <valueName>Billing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Sales</controllingFieldValue>
                <valueName>Core Salesforce Maps</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Sales</controllingFieldValue>
                <valueName>Sales Cloud</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Sales</controllingFieldValue>
                <valueName>Einstein for Sales</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Sales</controllingFieldValue>
                <valueName>CPQ</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2B Commerce</controllingFieldValue>
                <valueName>B2B Commerce</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Service</controllingFieldValue>
                <valueName>Service Cloud</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Service</controllingFieldValue>
                <valueName>Field Service Lightning</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Service</controllingFieldValue>
                <valueName>ClickSoftware FSE</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Service</controllingFieldValue>
                <valueName>Digital Engagement</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2C Commerce</controllingFieldValue>
                <valueName>B2C Commerce</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2C Commerce</controllingFieldValue>
                <valueName>Salesforce Order Management</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2C Commerce</controllingFieldValue>
                <valueName>Commerce Portals</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>B2C Commerce</controllingFieldValue>
                <valueName>Einstein for Commerce</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Community Cloud</controllingFieldValue>
                <valueName>Partner Portals (PRM)</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Community Cloud</controllingFieldValue>
                <valueName>CMS</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Community Cloud</controllingFieldValue>
                <valueName>Customer Portals (incl Self Service)</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Customer 360 Platform</controllingFieldValue>
                <valueName>Platform</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Customer 360 Platform</controllingFieldValue>
                <valueName>Mobile</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Customer 360 Platform</controllingFieldValue>
                <valueName>Heroku</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Customer 360 Platform</controllingFieldValue>
                <valueName>Shield</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Customer 360 Platform</controllingFieldValue>
                <valueName>Sustainability Cloud</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Customer 360 Platform</controllingFieldValue>
                <valueName>Integration Services</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Customer 360 Platform</controllingFieldValue>
                <valueName>Flow</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Customer 360 Platform</controllingFieldValue>
                <valueName>ISV/AppExchange</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Customer 360 Platform</controllingFieldValue>
                <valueName>Next Best Action</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Customer 360 Platform</controllingFieldValue>
                <valueName>Vision &amp; Language</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education</controllingFieldValue>
                <valueName>Recruiting</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education</controllingFieldValue>
                <valueName>Student Success</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education</controllingFieldValue>
                <valueName>Advancement</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education</controllingFieldValue>
                <controllingFieldValue>Nonprofit</controllingFieldValue>
                <valueName>Engagement</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Education</controllingFieldValue>
                <valueName>K-12</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Integration_MuleSoft</controllingFieldValue>
                <valueName>Mulesoft Anypoint Platform</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing</controllingFieldValue>
                <valueName>Core Messaging/Journeys</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing</controllingFieldValue>
                <valueName>MC Connect / Distributed Marketing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing</controllingFieldValue>
                <valueName>Datorama</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing</controllingFieldValue>
                <valueName>Social Studio</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing</controllingFieldValue>
                <valueName>Audience Studio</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing</controllingFieldValue>
                <valueName>Interaction Studio</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing</controllingFieldValue>
                <valueName>Advertising Studio</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Marketing</controllingFieldValue>
                <valueName>Einstein for Marketing</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Nonprofit</controllingFieldValue>
                <valueName>Fundraising</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Nonprofit</controllingFieldValue>
                <valueName>Program Management</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Nonprofit</controllingFieldValue>
                <valueName>Grantmaking</valueName>
            </valueSettings>
        </valueSet>
        <visibleLines>8</visibleLines>
    </fields>
    <fields>
        <fullName>Product_Specialization_Category__c</fullName>
        <externalId>false</externalId>
        <label>Product Specialization Category</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Specialization_Category</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Project_Status__c</fullName>
        <externalId>false</externalId>
        <label>Project Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>In Progress</fullName>
                    <default>false</default>
                    <label>In Progress</label>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                    <label>Completed</label>
                </value>
                <value>
                    <fullName>On Hold</fullName>
                    <default>false</default>
                    <label>On Hold</label>
                </value>
                <value>
                    <fullName>Terminated</fullName>
                    <default>false</default>
                    <label>Terminated</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Project_Submitted_For_Navigator_Approval__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Project Submitted For Navigator Approval</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Start_Date__c</fullName>
        <description>This field represents the Start date of the project</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Survey_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field points to the contact record and represents the Surveyor of the project.</description>
        <externalId>false</externalId>
        <label>Survey Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Projects (Survey Contact)</relationshipLabel>
        <relationshipName>Projects2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Project</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Client__c</columns>
        <columns>Industry__c</columns>
        <columns>Industry_Specialization__c</columns>
        <columns>Product_Relevant_Specialization__c</columns>
        <columns>Product_Specialization_Category__c</columns>
        <columns>Start_Date__c</columns>
        <columns>End_Date__c</columns>
        <columns>Go_Live_Date__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Project Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Projects</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
