<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>Candidate_Layout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>Candidate_Layout</fullName>
        <fields>Name__c</fields>
        <label>Candidate Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This is an object which stores the information of the candidates who are skilled in Salesforce.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Address__c</fullName>
        <description>Candidate&apos;s current address</description>
        <externalId>false</externalId>
        <label>Address</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Candidate_Url__c</fullName>
        <externalId>false</externalId>
        <formula>LEFT($Api.Partner_Server_URL_260, FIND( &apos;/services&apos;, $Api.Partner_Server_URL_260))+Id</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Candidate Url</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This is Employee record of candidate after hired</description>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Candidates (Contact)</relationshipLabel>
        <relationshipName>Candidate</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Current_CTC__c</fullName>
        <description>To describe the current CTC (Cost To Company) of the candidate.</description>
        <externalId>false</externalId>
        <label>Current CTC</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Current_Company__c</fullName>
        <externalId>false</externalId>
        <label>Current Company</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Date_Of_Joining__c</fullName>
        <description>Candidate&apos;s date of joining after getting hired.</description>
        <externalId>false</externalId>
        <label>Date Of Joining</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Days_Left__c</fullName>
        <externalId>false</externalId>
        <formula>TODAY() -  Form_Sent_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Days Left</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Email__c</fullName>
        <description>Candidate&apos;s Email Id</description>
        <externalId>false</externalId>
        <label>Email</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Experience_Months__c</fullName>
        <externalId>false</externalId>
        <label>Experience Months</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Experience_Years__c</fullName>
        <externalId>false</externalId>
        <label>Experience Years</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>First_Name__c</fullName>
        <externalId>false</externalId>
        <label>First Name</label>
        <length>32</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Form_Sent_Date__c</fullName>
        <externalId>false</externalId>
        <label>Form Sent Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Job_Role__c</fullName>
        <externalId>false</externalId>
        <label>Job Role</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Employee_Role</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Last_Name__c</fullName>
        <externalId>false</externalId>
        <label>Last Name</label>
        <length>32</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Linkedin_Candidate_ID__c</fullName>
        <description>This field will store the candidate ID received from linkdin</description>
        <externalId>false</externalId>
        <label>Linkedin Candidate ID</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Name__c</fullName>
        <externalId>false</externalId>
        <formula>First_Name__c &amp; &quot; &quot; &amp; Last_Name__c</formula>
        <label>Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Notice_Period__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <label>Notice Period</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Phone_No__c</fullName>
        <description>Candidate&apos;s Phone Number</description>
        <externalId>false</externalId>
        <label>Phone No</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Probable_Date_Of_Joining__c</fullName>
        <description>Date when a candidate can join the company, if hired.</description>
        <externalId>false</externalId>
        <label>Probable Date Of Joining</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Referred_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Referred By</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Candidates</relationshipLabel>
        <relationshipName>Candidates</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Score__c</fullName>
        <description>This score will be calculated based on skills and joining date</description>
        <externalId>false</externalId>
        <label>Score</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Screening_Questions__c</fullName>
        <externalId>false</externalId>
        <label>Screening Questions</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Skills__c</fullName>
        <description>Candidate&apos;s technical skills</description>
        <externalId>false</externalId>
        <label>Skills</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Visualforce Page</fullName>
                    <default>false</default>
                    <label>Visualforce Page</label>
                </value>
                <value>
                    <fullName>Javascript</fullName>
                    <default>false</default>
                    <label>Javascript</label>
                </value>
                <value>
                    <fullName>Integration</fullName>
                    <default>false</default>
                    <label>Integration</label>
                </value>
                <value>
                    <fullName>CSS</fullName>
                    <default>false</default>
                    <label>CSS</label>
                </value>
                <value>
                    <fullName>HTML</fullName>
                    <default>false</default>
                    <label>HTML</label>
                </value>
                <value>
                    <fullName>Aura Component</fullName>
                    <default>false</default>
                    <label>Aura Component</label>
                </value>
                <value>
                    <fullName>Lightning Web Component</fullName>
                    <default>false</default>
                    <label>Lightning Web Component</label>
                </value>
                <value>
                    <fullName>Apex</fullName>
                    <default>false</default>
                    <label>Apex</label>
                </value>
                <value>
                    <fullName>Domain Configuration</fullName>
                    <default>false</default>
                    <label>Domain Configuration</label>
                </value>
                <value>
                    <fullName>Community Builder</fullName>
                    <default>false</default>
                    <label>Community Builder</label>
                </value>
                <value>
                    <fullName>Process Builder</fullName>
                    <default>false</default>
                    <label>Process Builder</label>
                </value>
                <value>
                    <fullName>CPQ</fullName>
                    <default>false</default>
                    <label>CPQ</label>
                </value>
                <value>
                    <fullName>Community Cloud</fullName>
                    <default>false</default>
                    <label>Community Cloud</label>
                </value>
                <value>
                    <fullName>Service Cloud</fullName>
                    <default>false</default>
                    <label>Service Cloud</label>
                </value>
                <value>
                    <fullName>Sales Cloud</fullName>
                    <default>false</default>
                    <label>Sales Cloud</label>
                </value>
                <value>
                    <fullName>Bitbucket</fullName>
                    <default>false</default>
                    <label>Bitbucket</label>
                </value>
                <value>
                    <fullName>Gearset</fullName>
                    <default>false</default>
                    <label>Gearset</label>
                </value>
                <value>
                    <fullName>Data Loader</fullName>
                    <default>false</default>
                    <label>Data Loader</label>
                </value>
                <value>
                    <fullName>DocuSign</fullName>
                    <default>false</default>
                    <label>DocuSign</label>
                </value>
                <value>
                    <fullName>Git</fullName>
                    <default>false</default>
                    <label>Git</label>
                </value>
                <value>
                    <fullName>VSCode</fullName>
                    <default>false</default>
                    <label>VSCode</label>
                </value>
                <value>
                    <fullName>Jira</fullName>
                    <default>false</default>
                    <label>Jira</label>
                </value>
                <value>
                    <fullName>RPA</fullName>
                    <default>false</default>
                    <label>RPA</label>
                </value>
                <value>
                    <fullName>Flows</fullName>
                    <default>false</default>
                    <label>Flows</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>6</visibleLines>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>To describe the candidate&apos;s current stage in the hiring process.</description>
        <externalId>false</externalId>
        <inlineHelpText>It represents the current stage of the candidate in the hiring process..</inlineHelpText>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Not Viewed</fullName>
                    <default>true</default>
                    <label>Not Viewed</label>
                </value>
                <value>
                    <fullName>Screened</fullName>
                    <default>false</default>
                    <label>Screened</label>
                </value>
                <value>
                    <fullName>Form Sent</fullName>
                    <default>false</default>
                    <label>Form Sent</label>
                </value>
                <value>
                    <fullName>Form Submitted</fullName>
                    <default>false</default>
                    <label>Form Submitted</label>
                </value>
                <value>
                    <fullName>Interview In Progress</fullName>
                    <default>false</default>
                    <label>Interview In Progress</label>
                </value>
                <value>
                    <fullName>Interview Completed</fullName>
                    <default>false</default>
                    <label>Interview Completed</label>
                </value>
                <value>
                    <fullName>On Hold</fullName>
                    <default>false</default>
                    <label>On Hold</label>
                </value>
                <value>
                    <fullName>Rejected</fullName>
                    <default>false</default>
                    <label>Rejected</label>
                </value>
                <value>
                    <fullName>Hired</fullName>
                    <default>false</default>
                    <label>Hired</label>
                </value>
                <value>
                    <fullName>Blacklisted</fullName>
                    <default>false</default>
                    <label>Blacklisted</label>
                </value>
                <value>
                    <fullName>Joined</fullName>
                    <default>false</default>
                    <label>Joined</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Work_Location__c</fullName>
        <externalId>false</externalId>
        <label>Work Location</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Pune Office</fullName>
                    <default>false</default>
                    <label>Pune Office</label>
                </value>
                <value>
                    <fullName>Sydney Office</fullName>
                    <default>false</default>
                    <label>Sydney Office</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>email_sent__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>email sent</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Candidate</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Name__c</columns>
        <columns>Email__c</columns>
        <columns>Phone_No__c</columns>
        <columns>Status__c</columns>
        <columns>Job_Role__c</columns>
        <columns>Score__c</columns>
        <columns>Skills__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>NAME</field>
            <operation>contains</operation>
            <value>Candidate ID</value>
        </filters>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>Candidate ID - {00000}</displayFormat>
        <label>Candidate No</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Candidates</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Date_Of_Joining_should_be_mandatory</fullName>
        <active>true</active>
        <description>Date of joining should be mandatory if the status is &quot;Hired&quot;</description>
        <errorConditionFormula>IF(ISPICKVAL(Status__c, &quot;Joined&quot;), IF(ISNULL(Date_Of_Joining__c), true, false), false)</errorConditionFormula>
        <errorDisplayField>Date_Of_Joining__c</errorDisplayField>
        <errorMessage>Please enter the Date of Joining if the Status is marked as Hired.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>Show_Screening_Questions</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Show Screening Questions</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <url>{!Candidate__c.Screening_Questions__c}</url>
    </webLinks>
</CustomObject>
