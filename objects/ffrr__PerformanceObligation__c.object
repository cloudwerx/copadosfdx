<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Accept</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Large</formFactor>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <formFactor>Small</formFactor>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>ffrr__PerformanceObligationCompactLayout</compactLayoutAssignment>
    <compactLayouts>
        <fullName>ffrr__PerformanceObligationCompactLayout</fullName>
        <fields>Name</fields>
        <fields>ffrr__Description__c</fields>
        <fields>ffrr__Revenue__c</fields>
        <fields>ffrr__AllocatedRevenue__c</fields>
        <fields>ffrr__SSP__c</fields>
        <fields>ffrr__TotalSSP__c</fields>
        <fields>ffrr__AllocationStatus__c</fields>
        <label>Performance Obligation Compact Layout</label>
    </compactLayouts>
    <customHelpPage>ffrr__helprevenuecontract</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Delivery of independent goods and/or services to the customer.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>ffrr__AccountName__c</fullName>
        <deprecated>false</deprecated>
        <description>Information about the Performance Obligation. Often this will be the account name, but it can be any information.</description>
        <externalId>false</externalId>
        <inlineHelpText>Information about the Performance Obligation. Often this will be the account name, but it can be any information.</inlineHelpText>
        <label>Display Information</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__Active__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates whether the source record is active.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates whether the source record is active.</inlineHelpText>
        <label>Active</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__AllocatedRevenueAdjustment__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>Adjustment because of rounding issues.</description>
        <externalId>false</externalId>
        <inlineHelpText>Adjustment because of a difference between revenue and allocated revenue on the Revenue Contract caused by cumulative rounding errors on the Performance Obligations.</inlineHelpText>
        <label>Allocated Revenue Rounding Adjustment</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ffrr__AllocatedRevenueOverride__c</fullName>
        <deprecated>false</deprecated>
        <description>Override the calculated amount allocated to this Performance Obligation by entering a different value here.</description>
        <externalId>false</externalId>
        <inlineHelpText>Override the calculated amount allocated to this Performance Obligation by entering a different value here.</inlineHelpText>
        <label>Allocated Revenue Override</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ffrr__AllocatedRevenue__c</fullName>
        <deprecated>false</deprecated>
        <description>The amount allocated to this Performance Obligation by the Revenue Allocation calculation. This value is used for revenue recognition.</description>
        <externalId>false</externalId>
        <formula>ROUND(IF(ISNULL(ffrr__AllocatedRevenueOverride__c),
                    IF(ISNULL(ffrr__AllocationRatio__c),
                        null,
                        (ffrr__AllocationRatio__c * ffrr__SSP__c) +
                            IF(ISNULL(ffrr__AllocatedRevenueAdjustment__c),
                                0,
                                ffrr__AllocatedRevenueAdjustment__c)),
                    ffrr__AllocatedRevenueOverride__c), IF(ISBLANK(ffrr__CurrencyDP__c), 2, ffrr__CurrencyDP__c))</formula>
        <inlineHelpText>The amount allocated to this Performance Obligation by the Revenue Allocation calculation. This value is used for revenue recognition.</inlineHelpText>
        <label>Allocated Revenue</label>
        <precision>18</precision>
        <required>false</required>
        <scale>14</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ffrr__AllocationRatio__c</fullName>
        <deprecated>false</deprecated>
        <description>The ratio used to allocate revenue across the Revenue Contract.</description>
        <externalId>false</externalId>
        <inlineHelpText>The ratio used to allocate revenue across the Revenue Contract.</inlineHelpText>
        <label>Allocation Ratio</label>
        <precision>18</precision>
        <required>false</required>
        <scale>14</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__AllocationStatus__c</fullName>
        <deprecated>false</deprecated>
        <description>Shows if the Revenue for this performance obligation has been fully allocated, or needs reallocating.</description>
        <externalId>false</externalId>
        <formula>ffrr__RevenueContract__r.ffrr__AllocationStatus__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Shows if the Revenue for this performance obligation has been fully allocated, or needs reallocating.</inlineHelpText>
        <label>Allocation Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__AmortizedToDate__c</fullName>
        <deprecated>false</deprecated>
        <description>The amount amortized to date for this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Amortized to Date</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ffrr__BalanceSheetAccount__c</fullName>
        <deprecated>false</deprecated>
        <description>The balance sheet GLA to use for revenue values for this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Balance Sheet GLA</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__Completed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates whether the source record is complete.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates whether the source record is complete.</inlineHelpText>
        <label>Completed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__ControllingCostPOLI__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The Controlling Cost Performance Obligation Line Item. Used to populate the fields on the Performance Obligation.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Controlling Cost Performance Obligation Line Item. Used to populate the fields on the Performance Obligation.</inlineHelpText>
        <label>Controlling POLI (Cost)</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ffrr__PerformanceObligationLineItem__c.ffrr__PerformanceObligation__c</field>
                <operation>equals</operation>
                <valueField>$Source.Id</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ffrr__PerformanceObligationLineItem__c</referenceTo>
        <relationshipLabel>Controlling POLIs (Cost)</relationshipLabel>
        <relationshipName>ControllingCostPOLIs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ffrr__ControllingPOLI__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The Controlling Revenue Performance Obligation Line Item. Used to populate the fields on the Performance Obligation.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Controlling Revenue Performance Obligation Line Item. Used to populate the fields on the Performance Obligation.</inlineHelpText>
        <label>Controlling POLI (Revenue)</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ffrr__PerformanceObligationLineItem__c.ffrr__PerformanceObligation__c</field>
                <operation>equals</operation>
                <valueField>$Source.Id</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>ffrr__PerformanceObligationLineItem__c</referenceTo>
        <relationshipLabel>Controlling POLIs (Revenue)</relationshipLabel>
        <relationshipName>ControllingPOLIs</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ffrr__CostBalanceSheetAccount__c</fullName>
        <deprecated>false</deprecated>
        <description>The balance sheet GLA to use for cost values for this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Balance Sheet GLA (Cost)</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__CostCenter__c</fullName>
        <deprecated>false</deprecated>
        <description>The cost center for this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Cost Center</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__CostCostCenter__c</fullName>
        <deprecated>false</deprecated>
        <description>The cost center (cost) for this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Cost Center (Cost)</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__CostIncomeStatementAccount__c</fullName>
        <deprecated>false</deprecated>
        <description>The income statement GLA to use for cost values for this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Income Statement GLA (Cost)</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__Cost__c</fullName>
        <deprecated>false</deprecated>
        <description>Cost for this Performance Obligation.</description>
        <externalId>false</externalId>
        <inlineHelpText>Cost for this Performance Obligation</inlineHelpText>
        <label>Cost</label>
        <summarizedField>ffrr__PerformanceObligationLineItem__c.ffrr__Cost__c</summarizedField>
        <summaryFilterItems>
            <field>ffrr__PerformanceObligationLineItem__c.ffrr__ValueType__c</field>
            <operation>notEqual</operation>
            <value>Revenue</value>
        </summaryFilterItems>
        <summaryForeignKey>ffrr__PerformanceObligationLineItem__c.ffrr__PerformanceObligation__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>ffrr__CurrencyDP__c</fullName>
        <deprecated>false</deprecated>
        <description>Number of decimal places on the record&apos;s currency.</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of decimal places on the record&apos;s currency.</inlineHelpText>
        <label>Currency Decimal Places</label>
        <precision>1</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__Description__c</fullName>
        <deprecated>false</deprecated>
        <description>The description of this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Description</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__EndDate__c</fullName>
        <deprecated>false</deprecated>
        <description>The end date of this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ffrr__HasRevenue__c</fullName>
        <deprecated>false</deprecated>
        <description>Indicates that this Performance Obligation relates to revenue.</description>
        <externalId>false</externalId>
        <formula>NOT(ISBLANK(ffrr__SSPOverride__c)) ||
            (ffrr__RevenueCount__c != 0) ||
            NOT(ISBLANK(ffrr__AllocatedRevenueOverride__c))</formula>
        <inlineHelpText>Indicates that this Performance Obligation relates to revenue.</inlineHelpText>
        <label>Has Revenue</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__IncomeStatementAccount__c</fullName>
        <deprecated>false</deprecated>
        <description>The income statement GLA to use for revenue values for this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Income Statement GLA</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ffrr__NullSSPCount__c</fullName>
        <deprecated>false</deprecated>
        <description>The number of Performance Obligation Line Items relating to revenue that have a null SSP value.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Performance Obligation Line Items relating to revenue that have a null SSP value.</inlineHelpText>
        <label>Null SSP Count</label>
        <summaryFilterItems>
            <field>ffrr__PerformanceObligationLineItem__c.ffrr__SSP__c</field>
            <operation>equals</operation>
            <value></value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>ffrr__PerformanceObligationLineItem__c.ffrr__ValueType__c</field>
            <operation>notEqual</operation>
            <value>Cost</value>
        </summaryFilterItems>
        <summaryForeignKey>ffrr__PerformanceObligationLineItem__c.ffrr__PerformanceObligation__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>ffrr__PercentComplete__c</fullName>
        <deprecated>false</deprecated>
        <description>This Performance Obligation&apos;s percentage complete.</description>
        <externalId>false</externalId>
        <label>% Complete</label>
        <precision>18</precision>
        <required>false</required>
        <scale>10</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>ffrr__ReadyForRevenueRecognition__c</fullName>
        <deprecated>false</deprecated>
        <description>This is checked if the Revenue has been allocated and the Source Record is active. If unchecked this record cannot be included in Revenue Recognition.</description>
        <externalId>false</externalId>
        <formula>(ffrr__RevenueContract__r.ffrr__RevenueAllocated__c || !ffrr__HasRevenue__c) &amp;&amp; ffrr__Active__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>This is checked if the Revenue has been allocated and the Source Record is active. If unchecked this record cannot be included in Revenue Recognition.</inlineHelpText>
        <label>Ready for Revenue Recognition</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__RecognizedToDate__c</fullName>
        <deprecated>false</deprecated>
        <description>The amount recognized to date for this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Recognized to Date</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ffrr__RevenueContract__c</fullName>
        <deprecated>false</deprecated>
        <description>The Revenue Contract this Performance Obligation belongs to.</description>
        <externalId>false</externalId>
        <label>Revenue Contract</label>
        <referenceTo>ffrr__RevenueContract__c</referenceTo>
        <relationshipLabel>Performance Obligations</relationshipLabel>
        <relationshipName>PerformanceObligations</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ffrr__RevenueCount__c</fullName>
        <deprecated>false</deprecated>
        <description>The number of Performance Obligation Line Items that relate to revenue.</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Performance Obligation Line Items that relate to revenue.</inlineHelpText>
        <label>Revenue Count</label>
        <summaryFilterItems>
            <field>ffrr__PerformanceObligationLineItem__c.ffrr__ValueType__c</field>
            <operation>notEqual</operation>
            <value>Cost</value>
        </summaryFilterItems>
        <summaryForeignKey>ffrr__PerformanceObligationLineItem__c.ffrr__PerformanceObligation__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>ffrr__RevenueRecognitionComplete__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates whether this Performance Obligation is fully recognized and amortized.</description>
        <externalId>false</externalId>
        <inlineHelpText>When set, indicates that all the revenue for this Performance Obligation has been fully recognized and all cost fully amortized.</inlineHelpText>
        <label>Fully Recognized and Amortized</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ffrr__Revenue__c</fullName>
        <deprecated>false</deprecated>
        <description>Revenue for this Performance Obligation.</description>
        <externalId>false</externalId>
        <inlineHelpText>Revenue for this Performance Obligation</inlineHelpText>
        <label>Revenue</label>
        <summarizedField>ffrr__PerformanceObligationLineItem__c.ffrr__Revenue__c</summarizedField>
        <summaryFilterItems>
            <field>ffrr__PerformanceObligationLineItem__c.ffrr__ValueType__c</field>
            <operation>notEqual</operation>
            <value>Cost</value>
        </summaryFilterItems>
        <summaryForeignKey>ffrr__PerformanceObligationLineItem__c.ffrr__PerformanceObligation__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>ffrr__SSPOverride__c</fullName>
        <deprecated>false</deprecated>
        <description>Override the Standalone Selling Price from the source record by entering a different value here.</description>
        <externalId>false</externalId>
        <inlineHelpText>Override the Standalone Selling Price from the source record by entering a different value here.</inlineHelpText>
        <label>SSP Override</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ffrr__SSP__c</fullName>
        <deprecated>false</deprecated>
        <description>The Standalone Selling Price that will be used for this Performance Obligation. Uses Total SSP, or SSP Override if populated.</description>
        <externalId>false</externalId>
        <formula>ROUND(IF (ISBLANK(ffrr__SSPOverride__c), ffrr__TotalSSP__c, ffrr__SSPOverride__c ), IF(ISBLANK(ffrr__CurrencyDP__c), 2, ffrr__CurrencyDP__c))</formula>
        <inlineHelpText>The Standalone Selling Price that will be used for this Performance Obligation. Uses Total SSP, or SSP Override if populated.</inlineHelpText>
        <label>SSP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ffrr__StartDate__c</fullName>
        <deprecated>false</deprecated>
        <description>The start date for this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ffrr__TotalSSP__c</fullName>
        <deprecated>false</deprecated>
        <description>The amount that would be charged for this Performance Obligation if it was sold on its own.</description>
        <externalId>false</externalId>
        <inlineHelpText>Sum of Standalone Selling Prices from all linked Performance Obligation Line Items.</inlineHelpText>
        <label>Total SSP</label>
        <summarizedField>ffrr__PerformanceObligationLineItem__c.ffrr__SSP__c</summarizedField>
        <summaryFilterItems>
            <field>ffrr__PerformanceObligationLineItem__c.ffrr__ValueType__c</field>
            <operation>notEqual</operation>
            <value>Cost</value>
        </summaryFilterItems>
        <summaryForeignKey>ffrr__PerformanceObligationLineItem__c.ffrr__PerformanceObligation__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>ffrr__ffrrTemplate__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The template assigned to this Performance Obligation.</description>
        <externalId>false</externalId>
        <label>Template</label>
        <referenceTo>ffrr__Template__c</referenceTo>
        <relationshipLabel>Performance Obligations</relationshipLabel>
        <relationshipName>PerformanceObligations</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Performance Obligation</label>
    <listViews>
        <fullName>ffrr__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>POB-{00000000}</displayFormat>
        <label>Performance Obligation Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Performance Obligations</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>ffrr__AllocateRevenue</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Allocate Revenue</masterLabel>
        <openType>sidebar</openType>
        <page>ffrr__AllocateRevenueFromPO</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>ffrr__Update</fullName>
        <availability>online</availability>
        <description>Updates the related Performance Obligation Line Items based on their Field Mapping Definition</description>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Update Performance Obligation</masterLabel>
        <openType>sidebar</openType>
        <page>ffrr__PopulatePolisFromPO</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
