<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FFX_PSA_Alert_PMO_Professional_Services_Operations_New_RR</fullName>
        <description>PSA Alert PMO/Professional Services Operations - New RR</description>
        <protected>false</protected>
        <recipients>
            <recipient>FFX_PMO_Professional_Services_Operations</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FFX_PSA_Email_Templates/FFX_PSA_Alert_PMO_New_Resource_Request</template>
    </alerts>
    <alerts>
        <fullName>FFX_PSA_Alert_Resource_Named_RR_Held</fullName>
        <description>PSA Alert Resource - Named RR Held</description>
        <protected>false</protected>
        <recipients>
            <field>pse__Staffer_Resource__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>FFX_Project_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FFX_PSA_Email_Templates/FFX_PSA_Alert_Resource_Named_RR_Held</template>
    </alerts>
    <alerts>
        <fullName>FFX_PSA_EmailAlert_Resource_Project_Owner_that_RR_converted_to_Assignment</fullName>
        <description>PSA Alert Resource / Project Owner that RR converted to Assignment</description>
        <protected>false</protected>
        <recipients>
            <field>pse__Staffer_Resource__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>FFX_Project_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>FFX_PSA_Email_Templates/FFX_PSA_Alert_Resource_Project_Owner_RR_converted_to_Assignment</template>
    </alerts>
    <fieldUpdates>
        <fullName>FFX_PSA_RRUpdateCostRatefromSugCostRate</fullName>
        <field>pse__Average_Cost_Rate_Number__c</field>
        <formula>pse__Rate_Card__r.pse__Average_Cost_Rate__c</formula>
        <name>PSA RRUpdateCostRatefromSugCostRate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FFX_PSA_RR_Bill_Rate_From_Rate_Card</fullName>
        <field>pse__Requested_Bill_Rate__c</field>
        <formula>pse__Suggested_Bill_Rate_Number__c</formula>
        <name>PSA RR Bill Rate From Rate Card</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FFX_PSA_RR_Status_Hold</fullName>
        <field>pse__Status__c</field>
        <literalValue>Hold</literalValue>
        <name>PSA RR Status Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FFX_PSA_RR_Status_Ready_To_Staff</fullName>
        <field>pse__Status__c</field>
        <literalValue>Ready to Staff</literalValue>
        <name>PSA RR Status Ready To Staff</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FFX_PSA_Requested_Bill_Rate_0</fullName>
        <field>pse__Requested_Bill_Rate__c</field>
        <formula>0</formula>
        <name>PSA Requested Bill Rate = 0</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FFX_PSA_Set_Eligible_for_Schedule_Recalc</fullName>
        <field>pse__Eligible_for_Schedule_Recalculation__c</field>
        <literalValue>1</literalValue>
        <name>PSA Set Eligible for Schedule Recalc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FFX_PSA_Set_RR_Name</fullName>
        <field>pse__Resource_Request_Name__c</field>
        <formula>IF( pse__Resource_Held__c = TRUE,Name + &quot; - &quot; + pse__Staffer_Resource__r.FFX_Resource_Name__c + &quot; - &quot; + TEXT(pse__Resource_Role__c) + &quot; - &quot; + TEXT(pse__Status__c),Name + &quot; - &quot; + TEXT(pse__Resource_Role__c) + &quot; - &quot; + TEXT(pse__Status__c))</formula>
        <name>PSA Set RR Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FFX_PSA_Update_Project_Owner_Email</fullName>
        <field>FFX_Project_Owner_Email__c</field>
        <formula>pse__Project__r.Owner:User.Email</formula>
        <name>PSA Update Project Owner Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PSA_RR_Status_Ready_to_Staff</fullName>
        <field>pse__Status__c</field>
        <literalValue>Ready to Staff</literalValue>
        <name>PSA RR Status Ready to Staff</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>FFX PSA Resource Held on Resource Request</fullName>
        <actions>
            <name>FFX_PSA_RR_Status_Hold</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FFX_PSA_Set_RR_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>pse__Resource_Request__c.pse__Resource_Held__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Resource Held on Request</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FFX PSA Resource Request Created</fullName>
        <actions>
            <name>FFX_PSA_RR_Status_Ready_To_Staff</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>FFX_PSA_Set_Eligible_for_Schedule_Recalc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Resource Request Defaults</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FFX PSA Resource Request T%26M From Rate Card</fullName>
        <actions>
            <name>FFX_PSA_RR_Bill_Rate_From_Rate_Card</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Bill Rate From Rate Card</description>
        <formula>NOT(ISBLANK(pse__Project__c)) &amp;&amp; NOT(ISPICKVAL(pse__Project__r.pse__Billing_Type__c, &apos;Fixed Price&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FFX PSA Resource Unheld on Resource Request</fullName>
        <actions>
            <name>PSA_RR_Status_Ready_to_Staff</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Resource Unheld on Request</description>
        <formula>ISBLANK(pse__Assignment__c)  &amp;&amp;  pse__Resource_Held__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>FFX PSA Set RR Bill Rate to 0 for Fixed Price Projects</fullName>
        <actions>
            <name>FFX_PSA_Requested_Bill_Rate_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>0 Bill Rate for Fixed Price Projects</description>
        <formula>ISPICKVAL(pse__Project__r.pse__Billing_Type__c, &apos;Fixed Price&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>FFX PSA Update Project Owner Email</fullName>
        <actions>
            <name>FFX_PSA_Update_Project_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Resource Request T%26M Bill Rate From Rate Card</fullName>
        <actions>
            <name>FFX_PSA_RR_Bill_Rate_From_Rate_Card</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Bill Rate From Rate Card</description>
        <formula>NOT(ISBLANK(pse__Opportunity__c)) &amp;&amp; NOT(ISPICKVAL(pse__Opportunity__r.Billing_Type__c, &apos;Fixed Price&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set RR Bill Rate to 0 for Fixed Price Opportunities</fullName>
        <actions>
            <name>FFX_PSA_Requested_Bill_Rate_0</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>0 Bill Rate for Fixed Price Opportunities</description>
        <formula>ISPICKVAL(pse__Opportunity__r.Billing_Type__c, &apos;Fixed Price&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
