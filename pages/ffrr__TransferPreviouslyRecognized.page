<!--
    Copyright (c) 2017-2019 FinancialForce.com, inc. All rights reserved.
-->
<apex:page controller="ffrr.TransferPreviouslyRecognizedController" extensions="ffrr.HelpRedirectController" lightningStylesheets="true" tabStyle="RevenueManagementActions__tab" sidebar="false">
    <apex:form id="myform">
        <apex:sectionHeader title="{!$Label.ffrr__revenuemanagementactions}" subtitle="{!$Label.ffrr__transferpreviouslyrecognized}" help="{!TransferPreviouslyRecognizedHelp}"/>

        <!-- Input Values block for setting Inputs for the Transfer process -->
        <apex:pageBlock id="formblock" rendered="{!showForm}">

            <!-- Default text saying what this page will do -->
            <apex:pageMessage summary="{!$Label.ffrr__transferpreviouslyrecognizedhelp}" severity="info" strength="3"/>
            <!-- Displays a message if transfer previously recognized is already running -->
            <apex:pageMessage rendered="{!isTransferRunning}" summary="{!runningTransferJobsMessage}" severity="warning" strength="2" id="transferJobsRunningMessage">
                <apex:actionPoller reRender="formblock" action="{!checkRunningTransferJobs}" interval="30"/>
            </apex:pageMessage>

            <!-- Displays a message that there are in-progress transactions from a previous run -->
            <apex:pageMessage rendered="{!hasInProgressTransactions}" summary="{!hasInProgressTransactionsMessage}" severity="warning" strength="2" escape="false"/>

            <!-- Error message displays for when inputs are invalid -->
            <apex:pageMessages />

            <!-- This defines the area that will refresh when we change the filter field box-->
            <!-- It's important that it goes here, because the values in the date inputs affect if the value box changing works -->
            <!-- So by putting it here we can refresh the date fields to see 'invalid date' errors -->
            <apex:outputPanel id="dynamicFieldsBlock">

                <!-- Description input -->
                <apex:pageBlockSection >
                    <apex:pageBlockSectionItem helpText="{!$Label.ffrr__transferpreviouslyrecognizedhelptextdescription}">
                        <apex:outputLabel value="{!$Label.ffrr__revrecuidescription}" for="description"/>
                        <apex:inputField id="description" required="true" value="{!dummyContract.ffrr__Description__c}"/>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>

                <!-- Recognition Date input -->
                <apex:pageBlockSection >
                    <apex:pageBlockSectionItem helpText="{!$Label.ffrr__transferpreviouslyrecognizedhelptextrecognitiondate}">
                        <apex:outputLabel value="{!$Label.ffrr__revrecuirecognitiondate}" for="recognitionDate"/>
                        <apex:inputField id="recognitionDate" required="true" value="{!dummyContract.ffrr__StartDate__c}">
                            <apex:actionSupport event="onchange" action="{!onRecognitionDateChange}" reRender="periodLookup, cutoffDate"/>
                        </apex:inputField>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                <apex:pageBlockSection rendered="{!!showPeriodLookup}">
                    <apex:pageBlockSectionItem helpText="{!$Label.ffrr__transferpreviouslyrecognizedhelptextperiod}">
                        <apex:outputLabel value="{!periodTextLabel}" for="periodText"/>
                        <apex:inputField id="periodText" value="{!dummyTransaction.ffrr__Period__c}"/>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>
                <apex:pageBlockSection rendered="{!showPeriodLookup}">
                    <apex:pageBlockSectionItem helpText="{!$Label.ffrr__transferpreviouslyrecognizedhelptextperiod}">
                        <apex:outputLabel value="{!periodLookupsLabel}" for="periodLookup"/>
                        <apex:inputField id="periodLookup" required="true" value="{!dummyTransaction.ffrr__RecognitionPeriod__c}"/>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>

                <!-- Cutoff Date input -->
                <apex:pageBlockSection >
                    <apex:pageBlockSectionItem helpText="{!$Label.ffrr__transferpreviouslyrecognizedhelptextcutoffdate}">
                        <apex:outputLabel value="{!$Label.ffrr__transferpreviouslyrecognizedcutoffdate}" for="cutoffDate"/>
                        <apex:inputField id="cutoffDate" required="true" value="{!dummyContract.ffrr__EndDate__c}"/>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>

                <!-- Legislation Type input -->
                <apex:pageBlockSection >
                    <apex:pageBlockSectionItem helpText="{!$Label.ffrr__transferpreviouslyrecognizedhelptextlegislationtype}">
                        <apex:outputLabel value="{!legislationTypeLabel}" for="legislationType"/>
                        <apex:inputField id="legislationType" value="{!dummyTransaction.ffrr__LegislationType__c}" />
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>

            <!-- Filter Field input for filtering Contracts -->
            <apex:pageBlockSection >
                <apex:pageBlockSectionItem helpText="{!$Label.ffrr__transferpreviouslyrecognizedhelptextfilterfield}">
                    <apex:outputLabel value="{!filterFieldLabel}" for="filterField"/>
                    <apex:selectList value="{!filterField}" size="1">
                        <apex:selectOptions value="{!filterFieldList}" />
                        <apex:actionSupport event="onchange" rerender="dynamicFieldsBlock" />
                    </apex:selectList>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>

            <!--<apex:outputPanel id="dynamicFieldsBlock2">-->

                <!-- Filter Value input for filtering Contracts when the field used is not a lookup -->
                <apex:pageBlockSection rendered="{!filterField != null && activeText}">
                    <apex:pageBlockSectionItem helpText="{!$Label.ffrr__transferpreviouslyrecognizedhelptextfiltervalue}">
                        <apex:outputLabel value="{!filterLookupLabel}" for="filterValue"/>
                        <apex:inputText id="filterValue" value="{!filterValue}"/>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>

                <!-- Filter Value Lookup Fields for filtering Contracts when the field used is a lookup -->
                <apex:repeat value="{!filterLookups}" var="f">
                    <apex:pageBlockSection columns="1" rendered="{!f == filterField}">
                        <apex:pageBlockSectionItem helpText="{!$Label.ffrr__transferpreviouslyrecognizedhelptextfilterlookup}">
                            <apex:outputLabel value="{!filterLookupLabel}" for="filterLookup" />
                            <apex:inputField id="filterLookup" value="{!dummyContract[f]}" required="false"/>
                        </apex:pageBlockSectionItem>
                    </apex:pageBlockSection>
                </apex:repeat>

                <apex:pageBlockSection rendered="{!filterField != null && activeCheckbox}">
                    <apex:pageBlockSectionItem helpText="{!$Label.ffrr__transferpreviouslyrecognizedhelptextfiltervalue}">
                        <apex:outputLabel value="{!filterLookupLabel}" for="filterCheckbox"/>
                        <apex:inputCheckbox id="filterCheckbox" value="{!filterCheckbox}"/>
                    </apex:pageBlockSectionItem>
                </apex:pageBlockSection>

            </apex:outputPanel>

            <!-- Submit and Cancel buttons -->
            <apex:pageBlockButtons id="buttons" location="bottom">
                <apex:commandButton id="submit" value="{!$Label.ffrr__transfer}" action="{!confirmAction}" disabled="{!isTransferRunning}"/>
                <apex:commandButton immediate="true" id="cancel" value="{!$Label.ffrr__revrecuicancel}" action="{!cancel}" html-formnovalidate="formnovalidate"/>
            </apex:pageBlockButtons>
        </apex:pageBlock>

        <!-- Confirmation Page Block - either this or the one above will be shown, not both -->
        <apex:pageBlock rendered="{!showConfirm}">
            <apex:pageMessage summary="{!$Label.ffrr__transferpreviouslyrecognizedconfirmation}" severity="info" strength="3"/>
            <apex:pageBlockButtons location="bottom">
                <apex:commandButton id="submit" value="{!$Label.ffrr__doyouwanttocontinueyes}" action="{!doAction}" rerender="myform"/>
                <apex:commandButton immediate="true" value="{!$Label.ffrr__doyouwanttocontinueno}" action="{!cancel}" onComplete="populateFilterSelect()" rerender="myform" html-formnovalidate="formnovalidate"/>
            </apex:pageBlockButtons>
        </apex:pageBlock>

        <!-- Success Page Block -->
        <apex:pageBlock rendered="{!showSuccess}">
            <apex:pageMessages />
            <apex:pageBlockButtons location="bottom">
                <apex:commandButton immediate="true" value="{!$Label.ffrr__revrecuibuttonok}" action="{!cancel}" rerender="myform" html-formnovalidate="formnovalidate"/>
            </apex:pageBlockButtons>
        </apex:pageBlock>
    </apex:form>
</apex:page>