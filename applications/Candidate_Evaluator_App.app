<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>true</shouldOverrideOrgTheme>
    </brand>
    <description>App to test all Candidate Evaluator Components</description>
    <formFactors>Small</formFactors>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Candidate Evaluator App</label>
    <navType>Standard</navType>
    <tabs>standard-home</tabs>
    <tabs>Candidate__c</tabs>
    <tabs>Skillset__c</tabs>
    <tabs>Project_Resourcing__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>Data_Mining_App_UtilityBar</utilityBar>
</CustomApplication>
