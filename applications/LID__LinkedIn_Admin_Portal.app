<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <description>Allows CRM Admin to configure LinkedIn Sales Navigator package</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>true</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>true</isNavPersonalizationDisabled>
    <label>LinkedIn Admin Portal</label>
    <navType>Standard</navType>
    <tabs>LID__SalesNavigator_Post_Install_Wizard</tabs>
    <tabs>LID__Contact_Data_Validation1</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>LID__LinkedIn_Admin_Portal_UtilityBar</utilityBar>
</CustomApplication>
