({ doInit : function(component, event, helper) {
        
    	var contactId = new URL(window.location.href).searchParams.get('recordId');
        var isUserAuthenticated = new URL(window.location.href).searchParams.get('Authenticated');
        var isOnboardingComplete = new URL(window.location.href).searchParams.get('onboardingComplete');
        var type = new URL(window.location.href).searchParams.get('type');
    	//console.log(contactId);	
    	//console.log('contactId');	
    	component.set("v.contactId", contactId);
    	//console.log(component.get("v.contactId"));
	},
  	logOutClickHandle : function(component, event, helper) {
	 var navService = component.find("navService");
        var loginPageRef = {
            type: 'standard__namedPage',
            attributes: {
                pageName: 'home'
            }
        };
        //cmp.set("v.pageReference", pageReference); 
        navService.navigate(loginPageRef);
    },
    logoClickHandler : function(component, event, helper) {
	 	//console.log('------'+component.get("v.contactId"));
        var navService = component.find("navService");
        var landingPageRef = {
                type: 'comm__namedPage',
                attributes: {
                    name: 'LandingPage__c'	
                },
            	state : {
                    recordId : component.get("v.contactId")
                }
            };
        //cmp.set("v.pageReference", pageReference); homeClickHandle
        navService.navigate(landingPageRef);
    }
  
})