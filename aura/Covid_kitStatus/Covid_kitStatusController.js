({
    doInit : function(component, event, helper) {
        var statusDataList= [
            {
                "label":"Test1",
                "isActive" : "false",
                "isComplete":"true"
            },
            {
                "label":"Test 2 ---",
                "isActive" : "false",
                "isComplete":"true"
            },
            {
                "label":"Test1",
                "isActive" : "false",
                "isComplete":"true"
            },
            {
                "label":"Test1",
                "isActive" : "true",
                "isComplete":"false"
            }];

        component.set("v.statusData", statusDataList);
    }
})