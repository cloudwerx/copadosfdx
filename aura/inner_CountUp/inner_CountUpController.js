({
    scriptsLoaded: function(component, event, helper) {
        var easingFn = function(t, b, c, d) {
            var ts = (t /= d) * t;
            var tc = ts * t;
            return b + c * (tc * ts + -5 * ts * ts + 10 * tc + -10 * ts + 5 * t);
        };
        var options = {
            useEasing: true,
            easingFn: easingFn,
            useGrouping: true,
        };

        var maxValue = component.get("v.maxValue");
        var duration = component.get("v.duration");
        var demo = new CountUp("raspisDojo_countUp_element", 0, maxValue, decimals, duration, options);

        demo.start();
    }
});